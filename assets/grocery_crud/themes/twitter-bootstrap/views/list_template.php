<?php
	//$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/bootstrap.min.css');
	//$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/bootstrap-responsive.min.css');
	//$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/style.css');
	
	//$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/jquery-ui/flick/jquery-ui-1.9.2.custom.css');
	$version = "?version=".uniqid();
	
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);
	
	//	JAVASCRIPTS - JQUERY-UI
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery-ui/jquery-ui-1.9.2.custom.js');
	//	JAVASCRIPTS - JQUERY LAZY-LOAD
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');
	
	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}
	//	JAVASCRIPTS - TWITTER BOOTSTRAP
	//$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/bootstrap/bootstrap.min.js');
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/bootstrap/application.js');
	//	JAVASCRIPTS - MODERNIZR
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/modernizr/modernizr-2.6.1.custom.js');
	//	JAVASCRIPTS - TABLESORTER
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/tablesorter/jquery.tablesorter.min.js');
	//	JAVASCRIPTS - JQUERY-COOKIE
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/cookies.js');
	//	JAVASCRIPTS - JQUERY-FORM
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery.form.js');
	//	JAVASCRIPTS - JQUERY-NUMERIC
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
	//	JAVASCRIPTS - JQUERY-PRINT-ELEMENT
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/print-element/jquery.printElement.min.js');
	//	JAVASCRIPTS - JQUERY FANCYBOX
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	//	JAVASCRIPTS - JQUERY EASING
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');
	
	//	JAVASCRIPTS - twitter-bootstrap - CONFIGURAÇÕES
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/app/twitter-bootstrap.js');
	//	JAVASCRIPTS - JQUERY-FUNCTIONS
	$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery.functions.js');
?>

<?php
/** @var string $subject */
$table= str_replace("tb_","",$subject);
$ci = &get_instance();
?>

<script type="text/javascript">
	var base_url = "<?php echo base_url();?>",
		subject = "<?php echo $subject?>",
        ajax_list_info_url = "<?php /** @var string $ajax_list_info_url */
            echo $ajax_list_info_url?>",
        unique_hash = "<?php /** @var string $unique_hash */
            echo $unique_hash; ?>",
		message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
</script>


<div id="hidden-operations"></div>

<div class="twitter-bootstrap">
	<div class="main-header" style="margin-bottom: 10px">
		<h2><?php echo $subject?></h2>
		<em>...</em>
	</div>
	<div id="main-table-box">
		<br/>
		<div class="row">

                <?php /** @var boolean $bulkaction */
                if($bulkaction != true):?>
                    <div id="options-content" class="col-lg-2 col-md-2 col-sm-2" style="width: 10%;">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <?php echo $this->l('bulk_action'); ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <?php /** @var boolean $lisfilter */
                            if($lisfilter == true) { ?>
                                <li><a class="changedraf" id="Publish" href="#"><?php echo $this->l('bulk_pub'); ?></a></li>
                                <li><a class="changedraf" id="Moderation" href="#"><?php echo $this->l('bulk_mod'); ?></a></li>
                                <li><a class="changedraf" id="Draf" href="#"><?php echo $this->l('bulk_draf'); ?></a></li>
                                <li><a class="changedraf" id="Trash" href="#"><?php echo $this->l('bulk_trash'); ?></a></li>
                            <?php } ?>
                            <?php /** @var boolean $listfilterfeed */
                            if($listfilterfeed == true) { ?>
                                <li><a class="changedraf" id="Approved" href="#"><?php echo $this->l('feed_approve'); ?></a></li>
                                <li><a class="changedraf" id="Pending" href="#"><?php echo $this->l('feed_unapprove'); ?></a></li>
                                <li><a class="changedraf" id="Spam" href="#"><?php echo $this->l('feed_markspam'); ?></a></li>
                                <li><a class="changedraf" id="Trash" href="#"><?php echo $this->l('feed_movetrash'); ?></a></li>
                            <?php } ?>
                            <li><a id="deleteall" href="#"><?php echo $this->l('bulk_del'); ?></a></li>
                        </ul>
                    </div>
            </div>
                <?php endif;?>

			<div id="options-content" class="col-lg-10 col-md-10 col-sm-10">
				<?php
                /** @var boolean $unset_add */
                /** @var boolean $unset_export */
                /** @var boolean $unset_print */
				/** @var boolean $unset_find */
				if(!$unset_add || !$unset_export || !$unset_print || !$unset_find){?>
					<?php if(!$unset_add){?>
                        <a href="<?php /** @var string $add_url */
                        echo $add_url?>" title="<?php echo $this->l('list_add'); ?> <?php echo $subject?>" class="btn btn-success">
							<i class="fa fa-plus-square"></i>
							<?php echo $this->l('list_add'); ?> <?php echo $subject?>
						</a>
						<?php
					}
					if(!$unset_export) { ?>
                        <a class="export-anchor btn btn-warning" data-url="<?php /** @var string $export_url */
                        echo $export_url; ?>" rel="external">
							<i class="fa fa-download"></i>
							<?php echo $this->l('list_export');?>
						</a>
						<?php
					}
					if(!$unset_print) { ?>
                        <a class="print-anchor btn btn-default" data-url="<?php /** @var string $print_url */
                        echo $print_url; ?>">
							<i class="fa fa-print"></i>
							<?php echo $this->l('list_print');?>
						</a>
						<?php
					}
				} ?>
				<?php if(!$unset_find):?>
				<a class="btn btn-info" data-toggle="modal" href="#filtering-form-search" >
					<i class="fa fa-search"></i>
					<?php echo $this->l('list_search');?>
				</a>
				<?php endif;?>
			</div>

		</div>
		<?php /** @var array $list_filter_order */
		if( $lisfilter || $listfilterfeed ):?>
		<div class="row" style="margin-top: 15px;">
			<div class="col-lg-12">
				<?php if($lisfilter == true) { ?>
                    <a class="listtypea <?php echo $ci->base_config->activelinkCRUD(''); ?>" href="<?php /** @var string $state_list_url */
                    echo $state_list_url;?>"> <?php echo $this->l('bulk_all'); ?> </a>
					| <a class="listtypea <?php echo $ci->base_config->activelinkCRUD('publish'); ?>" href="<?php echo $state_list_url;?>/publish"><?php echo $this->l('bulk_inpub'); ?></a>
					| <a class="listtypea <?php echo $ci->base_config->activelinkCRUD('moderation'); ?>" href="<?php echo $state_list_url;?>/moderation"><?php echo $this->l('bulk_inmod'); ?></a>
					| <a class="listtypea <?php echo $ci->base_config->activelinkCRUD('draf'); ?>" href="<?php echo $state_list_url;?>/draf"><?php echo $this->l('bulk_indraf'); ?></a>
					| <a class="nobdright <?php echo $ci->base_config->activelinkCRUD('trash'); ?>" href="<?php echo  $state_list_url;?>/trash"><?php echo $this->l('bulk_intrash'); ?></a>
				<?php } ?>
				<?php if($listfilterfeed == true) { ?>
					<a class="listtypea <?php echo $ci->base_config->activelinkCRUD(''); ?>" href="<?php echo $state_list_url;?>"> <?php echo $this->l('bulk_all'); ?> </a>
					| <a class="listtypea <?php echo $ci->base_config->activelinkCRUD('approved'); ?>" href="<?php echo $state_list_url;?>/approved"><?php echo $this->l('feed_approve'); ?></a>
					| <a class="listtypea <?php echo $ci->base_config->activelinkCRUD('pending'); ?>" href="<?php echo $state_list_url;?>/pending"><?php echo $this->l('feed_pendding'); ?></a>
					| <a class="listtypea <?php echo $ci->base_config->activelinkCRUD('spam'); ?>" href="<?php echo  $state_list_url;?>/spam"><?php echo $this->l('feed_spam'); ?></a>
					| <a class="nobdright <?php echo $ci->base_config->activelinkCRUD('trash'); ?>" href="<?php echo  $state_list_url;?>/trash"><?php echo $this->l('feed_trash'); ?></a>
				<?php } ?>
			</div>
		</div>
		<?php endif;?>
		<?php if( $list_filter_order ):?>
			<form data-url="<?php echo base_url('cms/report_pemesanan/index');?>" id="form-report-order" action="" onsubmit="return false;" method="post">
		<div class="row" style="margin-top: 15px;">
			<?php if($list_filter_order == true):?>
				<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
					<select class="form-control" name="outlet">
						<option value="">Outlet</option>
						<?php foreach ($list_filter_order['outlet'] as $v):?>
							<option value="<?php echo $v['value'];?>"><?php echo $v['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
					<select class="form-control" name="status">
						<option value="">Status Lunas</option>
						<?php foreach ($list_filter_order['status'] as $v):?>
							<option value="<?php echo $v['value'];?>"><?php echo $v['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
					<select class="form-control" name="type">
						<option value="">Tipe Pembyrn</option>
						<?php foreach ($list_filter_order['type'] as $v):?>
							<option value="<?php echo $v['value'];?>"><?php echo $v['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
					<select class="form-control" name="user">
						<option value="">User</option>
						<?php foreach ($list_filter_order['user'] as $v):?>
							<option value="<?php echo $v['value'];?>"><?php echo $v['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
					<div class="input-group date form_date">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="from" value="" class="form-control" placeholder="Tgl Kirim dari">
					</div>
				</div>
				<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
					<div class="input-group date form_date">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="to" value="" class="form-control" placeholder="Tgl Kirim ke">
					</div>
				</div>
				<div class="col-lg-2" style="width: 7%;padding-left: 1px;padding-right: 1px;">
					<button type="submit" class="btn btn-info btn-block"><i class="fa fa-search"></i></button>
				</div>
				<div class="col-lg-2" style="width: 7%;padding-left: 1px;padding-right: 1px;">
					<button type="submit" class="btn btn-warning btn-block"><i class="fa fa-file-pdf-o"></i></button>
				</div>
			</form>
			<?php endif;?>
		</div>
		<?php endif;?>
		<br/>

		<!-- CONTENT FOR ALERT MESSAGES-->
        <div id="message-box" class="alert alert-success alert-dismissable <?php /** @var string $success_message */
        echo ($success_message !== null) ? '' : 'hide'; ?>">
			<a href="" class="close">×</a>
			<?php echo ($success_message !== null) ? $success_message : ''; ?>
		</div>

		<div id="ajax_list">
            <?php /** @var string $list_view */
            echo $list_view; ?>
		</div>

		<div class="pGroup span12">
			<select name="tb_per_page" id="tb_per_page">
                <?php /** @var array $paging_options */
                foreach($paging_options as $option){?>
                    <option value="<?php echo $option; ?>" <?php /** @var int $default_per_page */
                    echo ($option == $default_per_page) ? 'selected="selected"' : ''; ?> ><?php echo $option; ?></option>
				<?php }?>
			</select>

			<span class="pPageStat">
				<?php
				$paging_starts_from = '<span id="page-starts-from">1</span>';
                /** @var int $total_results */
                /** @var int $default_per_page */
                $paging_ends_to = '<span id="page-ends-to">'. ($total_results < $default_per_page ? $total_results : $default_per_page) .'</span>';
				$paging_total_results = '<span id="total_items" class="badge badge-info">'.$total_results.'</span>';
				echo str_replace( array('{start}','{end}','{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')); ?>
			</span>

			<span class="pcontrol">
				<?php echo $this->l('list_page'); ?>
				<input name="tb_crud_page" type="text" value="1" size="4" id="tb_crud_page">
				<?php echo $this->l('list_paging_of'); ?>
				<span id="last-page-number"><?php echo ceil($total_results / $default_per_page); ?></span>
			</span>

			<div class="hide loading" id="ajax-loading"><?php echo $this->l('form_update_loading'); ?></div>

			<ul class="pager">
				<li class="previous first-button"><a href="javascript:void(0);">&laquo; <?php echo $this->l('list_paging_first'); ?></a></li>
				<li class="prev-button"><a href="javascript:void(0);">&laquo; <?php echo $this->l('list_paging_previous'); ?></a></li>
				<li class="next-button"><a href="javascript:void(0);"><?php echo $this->l('list_paging_next'); ?> &raquo;</a></li>
				<li class="next last-button"><a href="javascript:void(0);"><?php echo $this->l('list_paging_last'); ?> &raquo;</a></li>
			</ul>
		</div>
	</div>
</div>

<!-- MODAL DIALOG -->
		<div class="modal fade" id="filtering-form-search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
                    <?php /** @var string $ajax_list_url */
                echo form_open( $ajax_list_url, 'method="post" id="filtering_form" autocomplete = "off"'); ?>
					<input type="hidden" name="page" value="1" size="4" id="crud_page">
					<input type="hidden" name="per_page" id="per_page" value="<?php echo $default_per_page; ?>" />
					<input type="hidden" name="order_by[0]" id="hidden-sorting" value="<?php if(!empty($order_by[0])){?><?php echo $order_by[0]?><?php }?>" />
					<input type="hidden" name="order_by[1]" id="hidden-ordering"  value="<?php if(!empty($order_by[1])){?><?php echo $order_by[1]?><?php }?>"/>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel"><?php echo $this->l('list_search') . ' ' . $subject; ?></h4>
					</div>
					<div class="modal-body">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-md-2 control-label"><?php echo $this->l('list_search');?></label>
								<div class="col-md-6">
								<input type="text" name="search_text" id="search_text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Kolom</label>
								<div class="col-md-6">
								<select class="form-control" name="search_field" id="search_field">
                                    <?php foreach($columns as $k => $column): ?>
									<?php if($bulkaction != true): ?>
											<?php if( $k > 0 ):?>
											<?php $display = strip_tags(preg_replace('/<[^>]*>/', '',html_entity_decode($column->display_as,ENT_QUOTES,'UTF-8'))); ?>
											<option value="<?php echo $column->field_name?>"><?php if(!empty($display)) echo $display; ?></option>
											<?php endif;?>
									<?php else:?>
											<?php $display = strip_tags(preg_replace('/<[^>]*>/', '',html_entity_decode($column->display_as,ENT_QUOTES,'UTF-8'))); ?>
											<option value="<?php echo $column->field_name?>"><?php if(!empty($display)) echo $display; ?></option>
									<?php endif;?>
									<?php endforeach;?>
								</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="<?php echo $this->l('list_search');?>" id="crud_search">
						<input type="button" class="btn btn-custom-primary" data-dismiss="modal" value="<?php echo $this->l('list_clear_filtering');?>" id="search_clear">
					</div>
				<?php echo form_close(); ?>
				</div>
			</div>
		</div>
<!-- END MODAL DIALOG -->

<script type="text/javascript">
    <?php /** @var string $list_upload */
    if($list_upload != null) {?>
    Dropzone.options.uploadmedia = {
        paramName: "file",
        addRemoveLinks:true,
        maxFilesize: 2,
        acceptedFiles: "audio/*,image/*,video/*",
        queuecomplete: function(file,response){
            $('#filtering_form').trigger('submit');
        }
    };
    <?php } ?>
    $("#deleteall").on("click",function(){
        bootbox.confirm("Apakah anda yakin ingin menghapus ini?", function(result) {
            if(result == true) {
                $(".removelist:checkbox").each(function () {
                    if ($(this).is(":checked")) {
                        var $this = $(this).closest('tr');
                        $this.hide("slow", "linear").delay(200).animate({"left": "-320px"}, 300, function () {
                            deteleGroceryCrudInformation($this.find('.delete-row').attr('data-target-url'));
                            $this.remove();
                        });
                    }
                });
            }
        });
    });
    <?php if($lisfilter == true) { ?>
    $(".changedraf").on("click",function(e){
        var status = $(this).attr('id');
        $(".removelist:checkbox").each(function() {
            if ($(this).is(":checked")) {
                var $this = $(this).closest('tr');
                var id    = $(this).attr('id');
                var url  = "<?php echo base_url(); ?>cms/posts/set_post_status/";
                if(id !== "") {
                    $.post(url,{ id:id, status:status}, function (html) {
                        if(status == 'Draf') {
                            $this.find('.label').text(status).removeClass('label-danger').removeClass('label-success').removeClass('label-info').toggleClass('label-warning');
                        }else if(status == 'Trash') {
                            $this.find('.label').text(status).removeClass('label-success').removeClass('label-warning').removeClass('label-info').toggleClass('label-danger');
                        }else if(status == 'Moderation') {
                            $this.find('.label').text(status).removeClass('label-success').removeClass('label-warning').removeClass('label-danger').toggleClass('label-info');
                        }else {
                            $this.find('.label').text(status).removeClass('label-danger').removeClass('label-warning').removeClass('label-info').toggleClass('label-success');
                        }
                    });
                }
            }
        });

    });
    <?php } ?>
    <?php if($listfilterfeed == true) { ?>
    $(".changedraf").on("click",function(){
        var status = $(this).attr('id');
        $(".removelist:checkbox").each(function() {
            if ($(this).is(":checked")) {
                var $this = $(this).closest('tr');
                var id    = $(this).attr('id');
                var url  = "<?php echo base_url(); ?>cms/comments/set_feed_status/";
                if(id !== "") {
                    $.post(url,{ id:id, status:status}, function (html) {
                        if(status == 'Pending') {
                            $this.find('.label').text(status).removeClass('label-danger').removeClass('label-success').toggleClass('label-warning');
                        }else if(status == 'Trash' || status == 'Spam') {
                            $this.find('.label').text(status).removeClass('label-success').removeClass('label-warning').toggleClass('label-danger');
                        }else {
                            $this.find('.label').text(status).removeClass('label-danger').removeClass('label-warning').toggleClass('label-success');
                        }
                    });
                }
            }
        });
    });
    <?php } ?>
</script>