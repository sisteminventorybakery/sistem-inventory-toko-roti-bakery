<?php
//$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/bootstrap.min.css');
//$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/bootstrap-responsive.min.css');
$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/style.css');
$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/jquery-ui/flick/jquery-ui-1.9.2.custom.css');
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery-ui/jquery-ui-1.9.2.custom.js');
$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

if (!$this->is_IE7()) {
	$this->set_js_lib($this->default_javascript_path.'/common/list.js');
}

//$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/bootstrap/bootstrap.min.js');
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/bootstrap/application.js');


$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/modernizr/modernizr-2.6.1.custom.js');

$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/tablesorter/jquery.tablesorter.min.js');

$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/cookies.js');

$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery.form.js');

$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');

$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/print-element/jquery.printElement.min.js');

$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');

$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

$this->set_js_config($this->default_theme_path.'/twitter-bootstrap/js/app/twitter-bootstrap-add.js');

$this->set_css($this->default_theme_path.'/twitter-bootstrap/js/libs/dropzone/dist/dropzone.css');
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/dropzone/dist/dropzone.js');


/** @var string $subject */
$table= str_replace("tb_","",$subject);
?>

<div class="content crud-form">
	<div class="main-header">
		<h2><?php echo $this->l('form_add'); ?> <?php echo $subject?></h2>
	</div>
	<!-- CONTENT FOR ALERT MESSAGES -->
	<div id="message-box" class="span12"></div>
	<div id="main-table-box span12" class="main-content">
	<?php
	echo form_open( $insert_url, 'method="post" id="crudForm" class="form-div span12"  enctype="multipart/form-data"');
	?>
	<div class="row">
	<div class="col-md-8">
			<?php $x = 0;?>
			<?php $tmp = false;?>
			<?php foreach($fields as $field): ?>
			<?php if( $input_fields[$field->field_name]->crud_type != "upload_file" && $input_fields[$field->field_name]->crud_type != "datetime" && $input_fields[$field->field_name]->crud_type != "date" && $input_fields[$field->field_name]->crud_type != "enum" ): ?>
			<?php $tmp = true;?>
			<?php if( $x < 1 ):?> 
		<!-- DEFAULT FORM -->
		<div class="widget">
			<div class="widget-header">
				<h3><i class="fa fa-chevron-right"></i> Form Detail</h3>
			</div>
			<div class="widget-content">
			<?php endif;?>
			    <p id="<?php echo $field->field_name; ?>_field_box"> </p>
			            <label id="<?php echo $field->field_name; ?>_display_as_box" for="<?php echo $field->field_name; ?>"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? '<span class="required">*</span>' : ""; ?></label>
						<?php echo $input_fields[$field->field_name]->input?>
			    <br>
			<?php $x++;?>
			<?php endif;?>
			<?php endforeach;?>	
			<?php if( $tmp == true ):?> 		

			<?php foreach($hidden_fields as $hidden_field){echo $hidden_field->input;} ?>

			</div>
		</div>
		<!-- END DEFAULT FORM -->
		<?php endif;?>
		</div>

		<div class="col-md-4">
			<?php $x = 0;?>
			<?php $tmp = false;?>
			<?php foreach($fields as $field): ?>
			<?php if( $input_fields[$field->field_name]->crud_type == "datetime" || $input_fields[$field->field_name]->crud_type == "date" ): ?>
			<?php $tmp = true;?>
			<?php if( $x < 1 ):?> 
			<!-- DATE PICKER -->
			<div class="widget">
				<div class="widget-header">
					<h3><i class="fa fa-chevron-right"></i> Form Date</h3>
				</div>
				<div class="widget-content clearfix">
			<?php endif;?>
					<p><?php echo $input_fields[$field->field_name]->display_as; ?>
					<?php echo ($input_fields[$field->field_name]->required)? '<span class="required">*</span>' : ""; ?> :</p>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<?php echo $input_fields[$field->field_name]->input?>
					</div><br>
			<?php $x++;?>
			<?php endif;?>
			<?php endforeach;?>	
			<?php if( $tmp == true ):?> 		
				</div>
			</div>
			<!-- END DATE PICKER -->
			<?php endif;?>

			<?php $x = 0;?>
			<?php $tmp = false;?>
			<?php foreach($fields as $field): ?>
			<?php if( $input_fields[$field->field_name]->crud_type == "enum" ): ?>
			<?php $tmp = true;?>
			<?php if( $x < 1 ):?> 
			<!-- SELECT -->
			<div class="widget">
				<div class="widget-header">
					<h3><i class="fa fa-chevron-right"></i> Form Select</h3>
				</div>
				<div class="widget-content clearfix">
			<?php endif;?>
					<div class="form-group">
						<p>
						<?php echo $input_fields[$field->field_name]->display_as; ?>
						<?php echo ($input_fields[$field->field_name]->required)? '<span class="required">*</span>' : ""; ?> :
						</p>
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
			<?php $x++;?>
			<?php endif;?>
			<?php endforeach;?>	
			<?php if( $tmp == true ):?>	
				</div>
			</div>
			<!-- END SELECT -->
			<?php endif;?>

			<!-- BUTTON ACTION -->
			<div class="widget">
				<div class="widget-header">
					<h3><i class="fa fa-chevron-right"></i> Action</h3>
				</div>
				<div class="widget-content">
					<input type="button" value="<?php echo $this->l('form_save'); ?>"  class="btn btn-info submit-form"/>
					<?php 	if(!$this->unset_back_to_list) { ?>
						<input type="button" value="<?php echo $this->l('form_save_and_go_back'); ?>" id="save-and-go-back-button"  class="btn btn-success"/>
						<input type="button" value="<?php echo $this->l('form_cancel'); ?>" class="btn btn-default return-to-list" />
					<?php 	} ?>
				</div>
			</div>
			<!-- BUTTON ACTION -->
		</div>
		</div>

			<div class="hide loading" id="ajax-loading"><?php echo $this->l('form_update_loading'); ?></div>
		<?php echo form_close(); ?>
	</div>
</div>
<script>
	var validation_url = "<?php echo $validation_url?>",
		list_url = "<?php echo $list_url?>",
		message_alert_add_form = "<?php echo $this->l('alert_add_form')?>",
		message_insert_error = "<?php echo $this->l('insert_error')?>";
</script>