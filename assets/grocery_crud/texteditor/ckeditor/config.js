/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	config.toolbar = 'Full';
 
/*	config.toolbar_Full =
	[
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript'] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
		{ name: 'styles', items : [ 'Format','Font'] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','PageBreak' ] },
		{ name: 'document', items : [ 'Source','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo'] },
		{ name: 'editing', items : [ 'Find'] },
		{ name: 'forms'},
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		
		{ name: 'tools', items : [ 'Maximize'] }
	];*/
	 config.toolbar_Full =
		[
			{ name: 'styles', items : [ 'Format','Font'] },
			{ name: 'colors', items : [ 'TextColor','BGColor' ] },
			{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript'] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
			'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
			{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','PageBreak' ] },
			{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
			{ name: 'links', items : [ 'Link','Anchor' ] },
			{ name: 'document', items : [ 'Source','Print','-','Templates' ] },
			{ name: 'editing', items : [ 'Find'] },
			{ name: 'forms'},
			{ name: 'tools', items : [ 'Maximize' ] }
		];
	config.smiley_path = '/duniamod2/assets/smiley/';
	config.smiley_images = [
    'regular_smile.png','sad_smile.png','wink_smile.png','teeth_smile.png','confused_smile.png','tongue_smile.png',
    'embarrassed_smile.png','omg_smile.png','whatchutalkingabout_smile.png','angry_smile.png','angel_smile.png','shades_smile.png',
    'devil_smile.png','cry_smile.png','lightbulb.png','thumbs_down.png','thumbs_up.png','heart.png',
    'broken_heart.png','kiss.png','envelope.png'
	];
	config.smiley_descriptions = [
    ':)', ':(', ';)', ':D', ':/', ':P', ':*)', ':-o',
    ':|', '>:(', 'o:)', '8-)', '>:-)', ';(', '', '', '',
    '', '', ':-*', ''
	];	
	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.skin = 'office2013';
};
