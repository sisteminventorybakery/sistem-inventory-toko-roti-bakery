$(function(){
	$("#field-status_promo_id").on("change", function(){
		var val = $("#field-status_promo_id").val();
		if(val == 1){
			$("#item_promo_free_display_as_box").html("Gratis");
			$("#field-item_promo_item_id").select2("readonly", false);
		}else{
			$("#item_promo_free_display_as_box").html("Gratis(Rp)");
			$("#field-item_promo_item_id").select2("readonly", true);
		}
	});
});