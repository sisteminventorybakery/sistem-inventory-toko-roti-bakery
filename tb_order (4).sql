-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2016 at 02:12 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2016_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `order_id` bigint(20) NOT NULL,
  `order_total` int(11) NOT NULL,
  `order_pay` int(11) NOT NULL,
  `order_address` varchar(255) NOT NULL,
  `order_discount_nota` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_lunas` enum('ya','tidak') NOT NULL DEFAULT 'tidak',
  `order_note` text NOT NULL,
  `order_type` enum('CASH','KREDIT','GIRO','KARTU_KREDIT') NOT NULL DEFAULT 'CASH',
  `order_kirim` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_ongkir` int(11) NOT NULL,
  `order_pelunasan` int(11) NOT NULL,
  `order_total_before_discount` int(11) NOT NULL,
  `order_discount_total` int(11) NOT NULL,
  `order_discount_product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`order_id`, `order_total`, `order_pay`, `order_address`, `order_discount_nota`, `order_date`, `order_lunas`, `order_note`, `order_type`, `order_kirim`, `customer_id`, `outlet_id`, `user_id`, `order_ongkir`, `order_pelunasan`, `order_total_before_discount`, `order_discount_total`, `order_discount_product`, `created`) VALUES
(2, 186500, 200000, 'jogja', 0, '2016-07-31 16:26:05', 'ya', '', 'CASH', '2016-08-04 16:00:00', 2, 1, 1, 0, 0, 0, 0, 0, '2016-07-31 09:26:05'),
(3, 179000, 200000, 'bandung', 0, '2016-08-31 16:27:45', 'ya', '', 'KREDIT', '2016-08-06 17:00:00', 3, 1, 1, 0, 0, 0, 0, 0, '2016-07-31 09:27:45'),
(4, 27000, 50000, 'surabaya', 0, '2016-08-22 16:29:17', 'ya', '', 'CASH', '2016-08-05 13:00:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-07-31 09:29:17'),
(5, 37500, 20000, 'jogja', 0, '2016-09-01 03:53:59', 'tidak', 'dikirim dan jangan telat', 'KREDIT', '2016-09-06 10:00:00', 2, 1, 1, 0, 0, 0, 0, 0, '2016-08-31 20:53:59'),
(6, 5400, 5400, 'semarang', 0, '2016-09-01 04:12:30', 'ya', '', 'CASH', '2016-09-01 06:10:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-08-31 21:12:30'),
(7, 36000, 40000, 'jogja', 0, '2016-09-02 00:34:57', 'tidak', 'tes pesan', 'KREDIT', '2016-09-14 00:00:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-09-01 17:34:57'),
(8, 29000, 29000, 'jogja', 0, '2016-09-02 06:28:06', 'ya', 'keterangan', 'KREDIT', '2016-09-14 10:00:00', 3, 1, 1, 0, 0, 0, 0, 0, '2016-09-01 23:28:06'),
(9, 160000, 160000, 'alaamt', 0, '2016-10-07 17:44:10', 'ya', 'ket', 'CASH', '2016-10-10 14:00:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-10-07 10:44:10'),
(10, 1500, 1500, 'jogja', 0, '2016-10-10 11:19:36', 'ya', 'keterangan', 'CASH', '2016-10-10 11:15:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-10-10 04:19:36'),
(11, 3000, 3000, 'eweweewewq', 0, '2016-10-10 11:21:09', 'ya', '', 'CASH', '2016-10-14 00:00:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-10-10 04:21:09'),
(12, 300, 300, 'dsaddsad', 0, '2016-10-10 11:23:44', 'ya', '', 'CASH', '2016-10-15 00:00:00', 1, 1, 1, 0, 0, 0, 0, 0, '2016-10-10 04:23:44'),
(13, 3500, 3500, 'karangmalang', 0, '2016-10-11 00:14:06', 'ya', '', 'CASH', '2016-10-13 14:00:00', 2, 1, 1, 0, 0, 0, 0, 0, '2016-10-10 17:14:06'),
(14, 1500, 3500, 'jakarta', 0, '2016-10-15 14:49:43', 'ya', '', 'CASH', '2016-10-20 05:25:00', 3, 1, 1, 2000, 0, 0, 0, 0, '2016-10-15 07:49:43'),
(20, 13800, 14800, 'karangmalang sleman yogyakarta', 0, '2016-10-16 15:26:50', 'ya', '', 'CASH', '2016-10-19 09:00:00', 2, 1, 1, 1000, 0, 14300, 500, 500, '2016-10-16 08:26:50'),
(21, 11255, 10000, 'karangmalang sleman yogyakarta', 1645, '2016-10-16 20:05:14', 'ya', '', 'CASH', '2016-10-19 06:00:00', 2, 1, 1, 3000, 0, 12900, 1645, 0, '2016-10-16 13:05:14'),
(22, 200000, 200000, 'karangmalang sleman yogyakarta', 200000, '2016-10-18 17:22:12', 'ya', '', 'CASH', '2016-10-19 10:00:00', 2, 1, 1, 1000, 0, 400000, 200000, 0, '2016-10-18 10:22:12'),
(23, 42000, 30000, 'karangmalang sleman yogyakarta', 4000, '2016-10-18 18:35:51', 'ya', '', 'CASH', '2016-10-19 13:00:00', 2, 1, 1, 6000, 0, 40000, 4000, 0, '2016-10-18 11:35:51'),
(24, 69000, 69000, 'indonesia', 500, '2016-10-19 06:34:12', 'tidak', '', 'GIRO', '2016-10-22 10:00:00', 1, 1, 1, 5000, 0, 64500, 500, 0, '2016-10-18 23:34:12'),
(25, 26100, 10000, 'indonesia', 9900, '2016-10-19 06:38:26', 'tidak', '', 'KREDIT', '2016-10-22 09:00:00', 1, 1, 1, 3000, 0, 33000, 9900, 0, '2016-10-18 23:38:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
