-- phpMyAdmin SQL Dump
-- version 4.5.5.1deb2.trusty~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 07, 2016 at 08:22 AM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kredit`
--

CREATE TABLE `tb_kredit` (
  `kredit_id` int(11) NOT NULL,
  `dp` bigint(20) NOT NULL DEFAULT '0',
  `jumlah_hutang` bigint(20) NOT NULL DEFAULT '0',
  `tanggal` date DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `order_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `jenis` enum('penjualan','pemesanan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_kredit`
--

INSERT INTO `tb_kredit` (`kredit_id`, `dp`, `jumlah_hutang`, `tanggal`, `jatuh_tempo`, `customer_id`, `created`, `order_id`, `sales_id`, `jenis`) VALUES
(2, 5000, 9000, '2016-10-26', '2016-10-26', 1, '0000-00-00 00:00:00', 0, 179, 'penjualan'),
(3, 20000, 46000, '2016-10-26', '2016-10-29', 1, '0000-00-00 00:00:00', 34, 0, 'penjualan'),
(4, 10000, 15000, '2016-10-26', '2016-10-26', 2, '0000-00-00 00:00:00', 0, 182, 'penjualan'),
(5, 100000, 243000, '2016-10-26', '2016-10-28', 5, '0000-00-00 00:00:00', 37, 0, 'penjualan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kredit`
--
ALTER TABLE `tb_kredit`
  ADD PRIMARY KEY (`kredit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kredit`
--
ALTER TABLE `tb_kredit`
  MODIFY `kredit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
