-- phpMyAdmin SQL Dump
-- version 4.5.5.1deb2.trusty~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2016 at 03:58 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `order_id` bigint(20) NOT NULL,
  `order_pay` int(11) NOT NULL,
  `order_total` int(11) NOT NULL,
  `order_address` varchar(255) NOT NULL,
  `order_discount_nota` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_lunas` enum('ya','tidak') NOT NULL DEFAULT 'tidak',
  `order_ongkir` int(11) DEFAULT NULL,
  `order_note` text NOT NULL,
  `order_type` enum('CASH','KREDIT','GIRO','KARTU_KREDIT') NOT NULL DEFAULT 'CASH',
  `order_kirim` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_pelunasan` int(11) NOT NULL,
  `order_total_before_discount` int(11) NOT NULL,
  `order_discount_total` int(11) NOT NULL,
  `order_discount_product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_nama` varchar(100) DEFAULT NULL,
  `order_nomor_kartu` varchar(100) DEFAULT NULL,
  `order_nama_bank` varchar(100) DEFAULT NULL,
  `order_nomor_giro` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`order_id`, `order_pay`, `order_total`, `order_address`, `order_discount_nota`, `order_date`, `order_lunas`, `order_ongkir`, `order_note`, `order_type`, `order_kirim`, `customer_id`, `outlet_id`, `user_id`, `order_pelunasan`, `order_total_before_discount`, `order_discount_total`, `order_discount_product`, `created`, `order_nama`, `order_nomor_kartu`, `order_nama_bank`, `order_nomor_giro`) VALUES
(2, 200000, 186500, 'jogja', 0, '2016-07-31 16:26:05', 'ya', NULL, '', 'CASH', '2016-08-04 16:00:00', 2, 1, 1, 0, 0, 0, 0, '2016-07-31 09:26:05', NULL, NULL, NULL, NULL),
(3, 200000, 179000, 'bandung', 0, '2016-08-31 16:27:45', 'ya', NULL, '', 'KREDIT', '2016-08-06 17:00:00', 3, 1, 1, 0, 0, 0, 0, '2016-07-31 09:27:45', NULL, NULL, NULL, NULL),
(4, 50000, 27000, 'surabaya', 0, '2016-08-22 16:29:17', 'ya', NULL, '', 'CASH', '2016-08-05 13:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-07-31 09:29:17', NULL, NULL, NULL, NULL),
(5, 20000, 37500, 'jogja', 0, '2016-09-01 03:53:59', 'tidak', NULL, 'dikirim dan jangan telat', 'KREDIT', '2016-09-06 10:00:00', 2, 1, 1, 0, 0, 0, 0, '2016-08-31 20:53:59', NULL, NULL, NULL, NULL),
(6, 5400, 5400, 'semarang', 0, '2016-09-01 04:12:30', 'ya', NULL, '', 'CASH', '2016-09-01 06:10:00', 1, 1, 1, 0, 0, 0, 0, '2016-08-31 21:12:30', NULL, NULL, NULL, NULL),
(7, 40000, 36000, 'jogja', 0, '2016-09-02 00:34:57', 'tidak', NULL, 'tes pesan', 'KREDIT', '2016-09-14 00:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-09-01 17:34:57', NULL, NULL, NULL, NULL),
(8, 29000, 29000, 'jogja', 0, '2016-09-02 06:28:06', 'ya', NULL, 'keterangan', 'KREDIT', '2016-09-14 10:00:00', 3, 1, 1, 0, 0, 0, 0, '2016-09-01 23:28:06', NULL, NULL, NULL, NULL),
(9, 160000, 160000, 'alaamt', 0, '2016-10-07 17:44:10', 'ya', NULL, 'ket', 'CASH', '2016-10-10 14:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-07 10:44:10', NULL, NULL, NULL, NULL),
(10, 1500, 1500, 'jogja', 0, '2016-10-10 11:19:36', 'ya', NULL, 'keterangan', 'CASH', '2016-10-10 11:15:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-10 04:19:36', NULL, NULL, NULL, NULL),
(11, 3000, 3000, 'eweweewewq', 0, '2016-10-10 11:21:09', 'ya', NULL, '', 'CASH', '2016-10-14 00:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-10 04:21:09', NULL, NULL, NULL, NULL),
(12, 300, 300, 'dsaddsad', 0, '2016-10-10 11:23:44', 'ya', NULL, '', 'CASH', '2016-10-15 00:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-10 04:23:44', NULL, NULL, NULL, NULL),
(13, 3500, 3500, 'karangmalang', 0, '2016-10-11 00:14:06', 'ya', NULL, '', 'CASH', '2016-10-13 14:00:00', 2, 1, 1, 0, 0, 0, 0, '2016-10-10 17:14:06', NULL, NULL, NULL, NULL),
(14, 8500, 8500, 'indonesia', 0, '2016-10-11 06:25:53', 'ya', NULL, '', 'CASH', '2016-10-14 14:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-10 23:25:53', NULL, NULL, NULL, NULL),
(15, 8500, 8500, 'indonesia', 0, '2016-10-11 06:30:40', 'ya', NULL, '', 'CASH', '2016-10-15 05:00:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-10 23:30:40', NULL, NULL, NULL, NULL),
(16, 100000, 349990, 'Ngagel Surabaya', 10, '2016-10-12 13:48:11', 'ya', NULL, 'Pesanan akan dilunasi ketika barang sudah sampai, roti  SANDWITCH TUNA', 'KREDIT', '2016-10-14 13:40:00', 5, 1, 1, 0, 0, 0, 0, '2016-10-12 06:49:35', NULL, NULL, NULL, NULL),
(17, 123123, 2300, 'karangmalang sleman yogyakarta', 0, '2016-10-13 14:40:40', 'tidak', NULL, 'qwe', 'KREDIT', '2016-10-06 18:30:00', 2, 1, 1, 0, 0, 0, 0, '2016-10-13 07:40:38', NULL, NULL, NULL, NULL),
(18, 209300, 184300, 'Ngagel Surabaya', 0, '2016-10-13 15:13:44', 'ya', 25000, 'diantar ke kantor\r\n@pergudangan margomulyo permai AD8', 'CASH', '2016-10-18 23:10:00', 5, 1, 1, 0, 0, 0, 0, '2016-10-13 08:13:41', NULL, NULL, NULL, NULL),
(19, 2147483647, 2002000, 'ffff', 0, '2016-10-14 12:51:00', 'tidak', NULL, 'ffff', 'CASH', '2016-10-15 12:55:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-14 05:51:00', NULL, NULL, NULL, NULL),
(20, 99, 30000, 'jjj', 0, '2016-10-14 13:08:20', 'tidak', NULL, 'jjjj', 'CASH', '2016-10-14 13:15:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-14 06:08:20', NULL, NULL, NULL, NULL),
(21, 2147483647, 3000000, 'hhhhh', 0, '2016-10-14 13:09:25', 'tidak', NULL, 'hhhhh', 'CASH', '2016-10-14 13:15:00', 1, 1, 1, 0, 0, 0, 0, '2016-10-14 06:09:25', NULL, NULL, NULL, NULL),
(22, 1, 290000, 'karangmalang sleman yogyakarta', 10000, '2016-10-14 13:33:40', 'tidak', NULL, '', 'GIRO', '2016-10-14 13:20:00', 2, 1, 1, 0, 0, 0, 0, '2016-10-14 06:33:40', NULL, NULL, NULL, NULL),
(23, 9, 1500, 'karangmalang sleman yogyakarta', 0, '2016-10-14 13:39:02', 'tidak', NULL, 'kkkk', 'CASH', '2016-10-14 13:40:00', 2, 3, 1, 0, 0, 0, 0, '2016-10-14 06:39:02', NULL, NULL, NULL, NULL),
(24, 46000, 41000, 'karangmalang sleman yogyakarta', 0, '2016-10-17 08:49:34', 'ya', 5000, '', 'CASH', '2016-10-19 09:00:00', 2, 1, 1, 0, 41000, 0, 0, '2016-10-17 01:49:34', NULL, NULL, NULL, NULL),
(25, 119960, 119878, 'ponorogo', 0, '2016-10-18 13:58:54', 'tidak', 0, 'pembelian jumlah 20', 'CASH', '2016-10-18 13:50:00', 1, 1, 1, 0, 120000, 122, 122, '2016-10-18 06:58:54', NULL, NULL, NULL, NULL),
(26, 100000, 592078, 'Ponorogo', 7800, '2016-10-18 14:26:55', 'tidak', 500, 'Jumlah 100', 'CASH', '2016-10-18 14:55:00', 1, 1, 1, 0, 600000, 7922, 122, '2016-10-18 07:26:55', NULL, NULL, NULL, NULL),
(27, 100000, 541098, 'karangmalang sleman yogyakarta', 58780, '2016-10-18 14:34:20', 'tidak', 0, '', 'CASH', '2016-10-18 14:30:00', 2, 1, 1, 0, 600000, 58902, 122, '2016-10-18 07:34:20', NULL, NULL, NULL, NULL),
(28, 150000, 150000, 'karangmalang sleman yogyakarta', 0, '2016-10-18 15:07:08', 'tidak', 0, '', 'CASH', '2016-10-18 15:05:00', 2, 3, 1, 0, 150000, 0, 0, '2016-10-18 08:07:08', NULL, NULL, NULL, NULL),
(29, 300000, 600000, 'Ngagel Surabaya', 50000, '2016-10-25 10:31:57', 'ya', 50000, 'Roti Abon Sapi Exp 21/10/2016', 'CASH', '2016-10-26 10:25:00', 5, 1, 1, 0, 600000, 50000, 0, '2016-10-25 03:31:57', NULL, NULL, NULL, NULL),
(30, 65000, 65000, 'jakarta', 0, '2016-10-25 10:38:33', 'ya', 5000, 'tgghhgh', 'GIRO', '2016-10-26 10:30:00', 3, 1, 1, 0, 60000, 0, 0, '2016-10-25 03:43:33', NULL, NULL, NULL, NULL),
(31, 80000, 180000, 'semarang', 0, '2016-10-25 11:12:26', 'tidak', 0, '', 'KREDIT', '2016-10-25 11:10:00', 4, 1, 1, 0, 180000, 0, 0, '2016-10-25 04:12:26', NULL, NULL, NULL, NULL),
(32, 10, 120000, 'karangmalang sleman yogyakarta', 0, '2016-10-25 11:15:11', 'ya', 0, '', 'CASH', '2016-10-25 21:10:00', 2, 1, 1, 0, 120000, 0, 0, '2016-10-25 04:15:11', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
