-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2016 at 07:13 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2016_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `category_desc` text CHARACTER SET utf8 NOT NULL,
  `category_slug` varchar(100) CHARACTER SET utf8 NOT NULL,
  `category_parent` int(11) NOT NULL,
  `category_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `category_lineage` longtext CHARACTER SET utf8 NOT NULL,
  `category_deep` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_name`, `category_desc`, `category_slug`, `category_parent`, `category_type`, `category_lineage`, `category_deep`) VALUES
(152, 'News', 'This category to display news', 'news', 0, 'category', '00152', 0),
(156, 'Slider', 'This category to Admin post a Slider Post', 'slider', 0, 'category', '00156', 0),
(161, 'Event', '', 'event-1', 0, 'tags', '00161', 0),
(166, '154', '154', '21', 0, '1', '00166', 0),
(167, '153', '153', '21', 0, '1', '00167', 0),
(168, '152', '152', '21', 0, '1', '00168', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_address` text,
  `customer_phone` varchar(20) DEFAULT NULL,
  `created` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_customer`
--

INSERT INTO `tb_customer` (`customer_id`, `customer_name`, `customer_address`, `customer_phone`, `created`) VALUES
(1, 'umum', 'indonesia', '085', '2016-05-29 07:44:02'),
(2, 'zazin', 'karangmalang sleman yogyakarta', '089654564500', '2016-06-01 18:26:05'),
(3, 'andi', 'jakarta', '0877', '2016-06-01 19:42:31'),
(4, 'indra', 'semarang', '0897676767', '2016-10-08 04:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_giro`
--

CREATE TABLE `tb_giro` (
  `giro_id` int(11) NOT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `jumlah_hutang` bigint(20) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL,
  `created` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_groups`
--

CREATE TABLE `tb_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_groups`
--

INSERT INTO `tb_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'ini adalah user biasa'),
(3, 'kasir', 'ini adalah user yang bertugas di kasir');

-- --------------------------------------------------------

--
-- Table structure for table `tb_inventory`
--

CREATE TABLE `tb_inventory` (
  `inventory_id` int(11) NOT NULL,
  `inventory_stok` int(11) NOT NULL,
  `inventory_discount` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `inventory_price_sales` int(11) NOT NULL,
  `inventory_price_outlet` int(11) NOT NULL,
  `inventory_price_real` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_inventory`
--

INSERT INTO `tb_inventory` (`inventory_id`, `inventory_stok`, `inventory_discount`, `outlet_id`, `inventory_price_sales`, `inventory_price_outlet`, `inventory_price_real`, `item_id`, `created`) VALUES
(1, 90, 10, 1, 3000, 2000, 1000, 1, '2016-10-07 12:20:26'),
(2, 100, 0, 1, 1500, 1000, 500, 2, '2016-09-29 01:01:38'),
(3, 50, 15, 2, 5000, 4000, 3000, 1, '2016-10-07 12:20:26'),
(6, 34, 0, 2, 3000, 2000, 1000, 2, '2016-08-31 14:27:26'),
(7, 0, 0, 3, 4000, 3000, 2000, 11, '2016-08-31 13:57:38'),
(8, 15, 0, 3, 6000, 5000, 4000, 9, '2016-10-07 11:22:16'),
(9, 0, 0, 4, 7500, 5500, 3500, 5, '2016-08-31 13:58:40'),
(10, 100, 0, 1, 9000, 7000, 4000, 3, '2016-10-07 12:20:26'),
(11, 90, 0, 1, 8000, 6000, 3000, 4, '2016-10-07 12:15:30'),
(12, 100, 0, 1, 0, 0, 0, 5, '2016-09-29 01:01:57'),
(13, 100, 0, 1, 0, 0, 0, 6, '2016-09-29 01:02:04'),
(14, 100, 0, 1, 0, 0, 0, 11, '2016-09-29 01:02:27'),
(15, 85, 0, 1, 5000, 4000, 3000, 9, '2016-10-07 11:22:16'),
(16, 100, 0, 1, 0, 0, 0, 8, '2016-09-29 01:02:23'),
(17, 100, 25, 1, 0, 0, 0, 7, '2016-09-29 01:02:10'),
(18, 44, 0, 2, 0, 0, 0, 3, '2016-10-07 12:20:26'),
(19, 0, 0, 2, 600, 300, 200, 36, '2016-08-31 16:09:35'),
(20, 100, 0, 1, 4000, 2000, 1000, 37, '2016-10-07 12:20:26'),
(21, 30, 0, 2, 5000, 4000, 3000, 37, '2016-10-07 12:20:26'),
(22, 0, 20, 3, 6000, 5000, 4000, 37, '2016-09-01 23:37:16'),
(23, 100, 0, 1, 0, 0, 0, 36, '2016-09-29 01:02:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_item_promo`
--

CREATE TABLE `tb_item_promo` (
  `item_promo_id` bigint(20) NOT NULL,
  `item_promo_qty` int(11) NOT NULL,
  `item_promo_free` int(11) NOT NULL,
  `item_promo_date_start` date NOT NULL,
  `item_promo_date_end` date NOT NULL,
  `item_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_item_promo`
--

INSERT INTO `tb_item_promo` (`item_promo_id`, `item_promo_qty`, `item_promo_free`, `item_promo_date_start`, `item_promo_date_end`, `item_id`, `outlet_id`, `created`) VALUES
(1, 20, 1, '2016-08-20', '2016-08-31', 4, 1, '2016-08-31 15:42:05'),
(2, 10, 2, '2016-08-20', '2016-08-31', 8, 2, '2016-08-31 15:42:08'),
(3, 20, 2, '2016-08-31', '2016-09-10', 4, 2, '2016-08-31 15:56:01');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kredit`
--

CREATE TABLE `tb_kredit` (
  `kd_id` int(11) NOT NULL,
  `dp` bigint(20) NOT NULL DEFAULT '0',
  `jumlah_hutang` bigint(20) NOT NULL DEFAULT '0',
  `tanggal` date DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `created` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_login_attempts`
--

CREATE TABLE `tb_login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_bahan`
--

CREATE TABLE `tb_master_bahan` (
  `master_bahan_id` int(11) NOT NULL,
  `master_bahan_name` varchar(200) NOT NULL,
  `master_bahan_desc` text NOT NULL,
  `master_bahan_kategori` int(11) NOT NULL,
  `konversi_ke_kg` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `harga` int(11) NOT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_bahan`
--

INSERT INTO `tb_master_bahan` (`master_bahan_id`, `master_bahan_name`, `master_bahan_desc`, `master_bahan_kategori`, `konversi_ke_kg`, `created`, `harga`, `satuan`, `stok`) VALUES
(1, 'Tepung', '', 1, 50, '2016-10-07 12:35:46', 100000, 'sak', 10),
(2, 'Gula Pasir', '', 1, 30, '2016-10-07 12:36:33', 200000, 'sak', 5),
(3, 'Gula Merah', '', 1, 30, '2016-10-07 12:36:33', 300000, 'sak', 11),
(4, 'Telur', '', 1, 10, '2016-10-07 12:37:12', 200000, 'pak', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_category`
--

CREATE TABLE `tb_master_category` (
  `master_category_id` int(11) NOT NULL,
  `master_category_name` varchar(100) NOT NULL,
  `master_category_desc` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_category`
--

INSERT INTO `tb_master_category` (`master_category_id`, `master_category_name`, `master_category_desc`, `created`) VALUES
(1, 'Bahan', 'Kategori Bahan', '2016-05-26 01:02:34'),
(2, 'Jasa', 'Kategori Jasa', '2016-05-26 01:02:43'),
(3, 'Konsinyasi', 'Kaategori Konsinyasi', '2016-06-28 23:30:06'),
(17, 'Bahan Pelengkap', '', '2016-06-29 22:34:54'),
(18, 'Minuman', '', '2016-06-29 22:35:05'),
(19, 'Perlengkapan', '', '2016-06-29 22:35:20'),
(20, 'Produk Jadi', '', '2016-06-29 22:35:33'),
(21, 'Produk Roti', '', '2016-06-29 22:35:52'),
(22, 'Retail', 'Retail ( Ada orang menitipkan barang )', '2016-06-29 22:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_item`
--

CREATE TABLE `tb_master_item` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_disc` int(11) NOT NULL,
  `item_stok` int(11) NOT NULL,
  `item_satuan` int(11) NOT NULL,
  `item_category` int(11) NOT NULL,
  `item_jenis` int(11) NOT NULL,
  `created` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `item_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_item`
--

INSERT INTO `tb_master_item` (`item_id`, `item_name`, `item_disc`, `item_stok`, `item_satuan`, `item_category`, `item_jenis`, `created`, `item_harga`) VALUES
(1, 'KESET MOLEN', 0, 0, 4, 21, 6, '2016-10-06 02:33:45', 1000),
(2, 'TUNA ROLL', 0, 0, 4, 21, 9, '2016-10-06 02:33:48', 2000),
(3, 'ROTI ABON SAPI', 0, 0, 4, 21, 5, '2016-10-06 02:33:51', 3000),
(4, 'KELAPA PANDAN SRIKAYA', 0, 0, 4, 21, 6, '2016-10-06 02:33:54', 4000),
(5, 'ONTBIJKOEK', 0, 0, 4, 21, 5, '2016-10-06 02:33:56', 1500),
(6, 'SANDWICH TUNA', 0, 0, 4, 21, 10, '2016-10-06 02:33:59', 2500),
(7, 'ROTI SSM DUS', 0, 0, 4, 21, 5, '2016-10-06 02:34:01', 5000),
(8, 'ONTBIJKOEK PTG', 0, 0, 4, 21, 10, '2016-10-06 02:34:03', 4000),
(9, 'CAKE BREAD KERING', 0, 20, 4, 21, 10, '2016-10-07 12:48:33', 3000),
(11, 'RAISIN ROLL', 0, 0, 2, 21, 6, '2016-10-06 02:34:08', 50000),
(36, 'RETAIL ROTI', 0, 0, 2, 22, 10, '2016-10-06 02:34:10', 3000),
(37, 'ROTI BARU', 0, 0, 1, 21, 8, '2016-10-06 02:34:12', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_jenis`
--

CREATE TABLE `tb_master_jenis` (
  `master_jenis_id` int(11) NOT NULL,
  `master_jenis_name` varchar(100) NOT NULL,
  `master_jenis_desc` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_jenis`
--

INSERT INTO `tb_master_jenis` (`master_jenis_id`, `master_jenis_name`, `master_jenis_desc`, `created`) VALUES
(1, 'Bahan Baku', '', '2016-06-29 22:36:44'),
(2, 'Bahan Pelengkap', '', '2016-06-29 22:37:02'),
(3, 'Minuman', '', '2016-06-29 22:37:16'),
(4, 'Ice Cream', '', '2016-06-29 22:37:28'),
(5, 'Kue Basah', '', '2016-06-29 22:37:35'),
(6, 'Kue Kering', '', '2016-06-29 22:37:42'),
(7, 'Puding', '', '2016-06-29 22:38:11'),
(8, 'Roti Manis', '', '2016-06-29 22:38:21'),
(9, 'Cake', '', '2016-06-29 22:38:29'),
(10, 'Donat', '', '2016-06-29 22:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_outlet`
--

CREATE TABLE `tb_master_outlet` (
  `master_outlet_id` int(11) NOT NULL,
  `master_outlet_name` varchar(100) NOT NULL,
  `master_outlet_desc` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_outlet`
--

INSERT INTO `tb_master_outlet` (`master_outlet_id`, `master_outlet_name`, `master_outlet_desc`, `created`) VALUES
(1, 'Surabaya 001', '', '2016-06-30 17:05:14'),
(2, 'Surabaya 002', '', '2016-06-30 17:05:25'),
(3, 'Jakarta 001', '', '2016-06-30 17:11:22'),
(4, 'Jakarta 002', '', '2016-06-30 17:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_resep`
--

CREATE TABLE `tb_master_resep` (
  `master_resep_id` int(11) NOT NULL,
  `master_resep_name` varchar(100) NOT NULL,
  `master_resep_desc` text NOT NULL,
  `master_resep_estimasi` int(11) NOT NULL,
  `master_resep_quantity` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_resep`
--

INSERT INTO `tb_master_resep` (`master_resep_id`, `master_resep_name`, `master_resep_desc`, `master_resep_estimasi`, `master_resep_quantity`, `updated`) VALUES
(5, 'resep 1', 'resep 1 desc', 3, 1, '2016-05-26 01:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_satuan`
--

CREATE TABLE `tb_master_satuan` (
  `master_satuan_id` int(11) NOT NULL,
  `master_satuan_name` varchar(100) NOT NULL,
  `master_satuan_desc` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_satuan`
--

INSERT INTO `tb_master_satuan` (`master_satuan_id`, `master_satuan_name`, `master_satuan_desc`, `created`) VALUES
(1, 'Kilogram', 'satuan kilogram', '2016-05-22 07:02:23'),
(2, 'Ons', 'satuan ons', '2016-05-22 07:03:48'),
(3, 'Gram', 'satuan gram', '2016-05-22 07:03:59'),
(4, 'Pcs', 'Pcs desc', '2016-05-29 09:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mutasi`
--

CREATE TABLE `tb_mutasi` (
  `mutasi_id` bigint(20) NOT NULL,
  `mutasi_date` datetime NOT NULL,
  `mutasi_status` enum('YA','TIDAK') NOT NULL DEFAULT 'TIDAK',
  `mutasi_to` int(11) NOT NULL,
  `mutasi_from` int(11) NOT NULL,
  `mutasi_total` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mutasi_desc` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_mutasi`
--

INSERT INTO `tb_mutasi` (`mutasi_id`, `mutasi_date`, `mutasi_status`, `mutasi_to`, `mutasi_from`, `mutasi_total`, `user_id`, `mutasi_desc`, `created`) VALUES
(18, '2016-09-01 23:57:58', 'TIDAK', 3, 1, 19000, 1, 'tes mutasi ke jakarta 001', '2016-09-01 16:57:58'),
(19, '2016-09-02 06:46:21', 'YA', 2, 1, 37000, 1, 'pindah produk ke surabaya', '2016-10-07 12:20:26'),
(20, '2016-09-02 06:48:04', 'YA', 3, 2, 2400, 1, 'pindah barang ke jakarta 1', '2016-09-01 23:48:56'),
(21, '2016-10-07 18:21:22', 'YA', 3, 1, 75000, 1, 'di transfer ke jakarta 001', '2016-10-07 11:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mutasi_dapur`
--

CREATE TABLE `tb_mutasi_dapur` (
  `mutasi_dapur_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `disetujui` enum('YA','TIDAK') NOT NULL DEFAULT 'TIDAK',
  `expired` date NOT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `diskon_nota` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_mutasi_dapur`
--

INSERT INTO `tb_mutasi_dapur` (`mutasi_dapur_id`, `outlet_id`, `user_id`, `total`, `disetujui`, `expired`, `keterangan`, `diskon_nota`, `created`) VALUES
(1, 3, 1, 40000, '', '2016-10-13', 'ketrangan mutasi dapur', 0, '2016-10-06 02:59:52'),
(2, 1, 1, 40000, 'TIDAK', '2016-10-12', 'tgl 12 expired lho', 0, '2016-10-07 12:15:29'),
(3, 2, 1, 60000, 'YA', '2016-10-14', '', 0, '2016-10-07 12:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mutasi_dapur_detail`
--

CREATE TABLE `tb_mutasi_dapur_detail` (
  `mutasi_dapur_detail_id` int(11) NOT NULL,
  `mutasi_dapur_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `diskon` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_mutasi_dapur_detail`
--

INSERT INTO `tb_mutasi_dapur_detail` (`mutasi_dapur_detail_id`, `mutasi_dapur_id`, `item_id`, `harga`, `jumlah`, `subtotal`, `diskon`, `created`) VALUES
(1, 1, 4, 4000, 10, 40000, 0, '2016-10-06 02:59:53'),
(2, 2, 4, 4000, 10, 40000, 0, '2016-10-07 11:34:57'),
(3, 3, 37, 2000, 30, 60000, 0, '2016-10-07 12:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mutasi_detail`
--

CREATE TABLE `tb_mutasi_detail` (
  `mutasi_detail_id` bigint(20) NOT NULL,
  `mutasi_id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `mutasi_detail_qty` int(11) NOT NULL DEFAULT '1',
  `mutasi_detail_total` int(11) NOT NULL,
  `mutasi_detail_price` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_mutasi_detail`
--

INSERT INTO `tb_mutasi_detail` (`mutasi_detail_id`, `mutasi_id`, `item_id`, `mutasi_detail_qty`, `mutasi_detail_total`, `mutasi_detail_price`, `created`) VALUES
(1, 18, 4, 2, 16000, 8000, '2016-09-01 16:57:58'),
(2, 18, 2, 2, 3000, 1500, '2016-09-01 16:57:58'),
(3, 19, 3, 1, 9000, 9000, '2016-09-01 23:46:21'),
(4, 19, 37, 4, 16000, 4000, '2016-09-01 23:46:21'),
(5, 19, 1, 4, 12000, 3000, '2016-09-01 23:46:21'),
(6, 20, 36, 4, 2400, 600, '2016-09-01 23:48:04'),
(7, 21, 9, 15, 75000, 5000, '2016-10-07 11:21:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notification`
--

CREATE TABLE `tb_notification` (
  `notification_id` bigint(20) NOT NULL,
  `notification_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `notification_user` bigint(20) NOT NULL,
  `notification_parent` bigint(20) NOT NULL,
  `notification_link` varchar(100) NOT NULL,
  `notification_desc` tinytext CHARACTER SET utf8 NOT NULL,
  `notification_status` varchar(10) CHARACTER SET utf8 NOT NULL,
  `notification_icon` varchar(50) CHARACTER SET utf8 NOT NULL,
  `notification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notification`
--

INSERT INTO `tb_notification` (`notification_id`, `notification_type`, `notification_user`, `notification_parent`, `notification_link`, `notification_desc`, `notification_status`, `notification_icon`, `notification_date`) VALUES
(1, 'timeline', 1, 10, 'javascript:void(0)', 'Delete data ', 'inactive', 'delete', '2016-06-29 21:30:50'),
(2, 'timeline', 1, 14, 'cms/users/index/edit/14', 'Update data ', 'inactive', 'update', '2016-06-29 21:39:50'),
(3, 'timeline', 1, 13, 'cms/users/index/edit/13', 'Update data ', 'inactive', 'update', '2016-06-29 21:40:07'),
(4, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 00:01:18'),
(5, 'timeline', 1, 6, 'cms/users/index/edit/6', 'Update data ', 'inactive', 'update', '2016-06-30 00:01:47'),
(6, 'timeline', 1, 11, 'cms/users/index/edit/11', 'Update data ', 'inactive', 'update', '2016-06-30 00:02:00'),
(7, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Update data ', 'inactive', 'update', '2016-06-30 00:02:13'),
(8, 'timeline', 1, 13, 'cms/users/index/edit/13', 'Update data ', 'inactive', 'update', '2016-06-30 00:02:23'),
(9, 'timeline', 1, 14, 'cms/users/index/edit/14', 'Update data ', 'inactive', 'update', '2016-06-30 00:02:29'),
(10, 'timeline', 1, 14, 'javascript:void(0)', 'Delete data ', 'inactive', 'delete', '2016-06-30 00:02:33'),
(11, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 00:10:34'),
(12, 'timeline', 1, 6, 'cms/users/index/edit/6', 'Update data ', 'inactive', 'update', '2016-06-30 00:10:48'),
(13, 'timeline', 1, 11, 'cms/users/index/edit/11', 'Update data ', 'inactive', 'update', '2016-06-30 00:10:56'),
(14, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Update data ', 'inactive', 'update', '2016-06-30 00:11:04'),
(15, 'timeline', 1, 13, 'cms/users/index/edit/13', 'Update data ', 'inactive', 'update', '2016-06-30 00:11:11'),
(16, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 00:54:13'),
(17, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 01:48:40'),
(18, 'timeline', 1, 6, 'cms/users/index/edit/6', 'Update data ', 'inactive', 'update', '2016-06-30 01:48:46'),
(19, 'timeline', 1, 11, 'cms/users/index/edit/11', 'Update data ', 'inactive', 'update', '2016-06-30 01:48:53'),
(20, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Update data ', 'inactive', 'update', '2016-06-30 01:49:01'),
(21, 'timeline', 1, 13, 'cms/users/index/edit/13', 'Update data ', 'inactive', 'update', '2016-06-30 01:49:09'),
(22, 'timeline', 1, 13, 'javascript:void(0)', 'Delete data ', 'inactive', 'delete', '2016-06-30 10:31:13'),
(23, 'timeline', 1, 12, 'javascript:void(0)', 'Delete data ', 'inactive', 'delete', '2016-06-30 10:31:16'),
(24, 'timeline', 1, 11, 'cms/users/index/edit/11', 'Update data ', 'inactive', 'update', '2016-06-30 10:33:23'),
(25, 'timeline', 1, 11, 'cms/users/index/edit/11', 'Update data ', 'inactive', 'update', '2016-06-30 10:33:43'),
(26, 'timeline', 1, 6, 'cms/users/index/edit/6', 'Update data ', 'inactive', 'update', '2016-06-30 10:33:50'),
(27, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 10:33:58'),
(28, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 17:10:56'),
(29, 'timeline', 1, 6, 'cms/users/index/edit/6', 'Update data ', 'inactive', 'update', '2016-06-30 17:11:03'),
(30, 'timeline', 1, 11, 'cms/users/index/edit/11', 'Update data ', 'inactive', 'update', '2016-06-30 17:11:53'),
(31, 'timeline', 1, 6, 'cms/users/index/edit/6', 'Update data ', 'inactive', 'update', '2016-06-30 18:59:53'),
(32, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-06-30 19:17:35'),
(33, 'timeline', 1, 1, 'cms/users/index/edit/1', 'Update data ', 'inactive', 'update', '2016-07-01 01:21:01'),
(34, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Add new data ', 'inactive', 'add', '2016-07-27 17:02:46'),
(35, 'timeline', 1, 12, 'javascript:void(0)', 'Delete data ', 'inactive', 'delete', '2016-07-27 17:03:02'),
(36, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Add new data ', 'inactive', 'add', '2016-08-05 13:24:04'),
(37, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Update data ', 'inactive', 'update', '2016-08-05 13:25:04'),
(38, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Update data ', 'inactive', 'update', '2016-08-07 03:21:26'),
(39, 'timeline', 1, 12, 'cms/users/index/edit/12', 'Update data ', 'inactive', 'update', '2016-08-07 03:23:06');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `order_id` bigint(20) NOT NULL,
  `order_pay` int(11) NOT NULL,
  `order_total` int(11) NOT NULL,
  `order_address` varchar(255) NOT NULL,
  `order_discount` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_lunas` enum('ya','tidak') NOT NULL DEFAULT 'tidak',
  `order_note` text NOT NULL,
  `order_type` enum('CASH','KREDIT','GIRO') NOT NULL DEFAULT 'CASH',
  `order_kirim` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`order_id`, `order_pay`, `order_total`, `order_address`, `order_discount`, `order_date`, `order_lunas`, `order_note`, `order_type`, `order_kirim`, `customer_id`, `outlet_id`, `user_id`, `created`) VALUES
(2, 200000, 186500, 'jogja', 0, '2016-07-31 16:26:05', 'ya', '', 'CASH', '2016-08-04 16:00:00', 2, 1, 1, '2016-07-31 09:26:05'),
(3, 200000, 179000, 'bandung', 0, '2016-08-31 16:27:45', 'ya', '', 'KREDIT', '2016-08-06 17:00:00', 3, 1, 1, '2016-07-31 09:27:45'),
(4, 50000, 27000, 'surabaya', 0, '2016-08-22 16:29:17', 'ya', '', 'CASH', '2016-08-05 13:00:00', 1, 1, 1, '2016-07-31 09:29:17'),
(5, 20000, 37500, 'jogja', 0, '2016-09-01 03:53:59', 'tidak', 'dikirim dan jangan telat', 'KREDIT', '2016-09-06 10:00:00', 2, 1, 1, '2016-08-31 20:53:59'),
(6, 5400, 5400, 'semarang', 0, '2016-09-01 04:12:30', 'ya', '', 'CASH', '2016-09-01 06:10:00', 1, 1, 1, '2016-08-31 21:12:30'),
(7, 40000, 36000, 'jogja', 0, '2016-09-02 00:34:57', 'tidak', 'tes pesan', 'KREDIT', '2016-09-14 00:00:00', 1, 1, 1, '2016-09-01 17:34:57'),
(8, 29000, 29000, 'jogja', 0, '2016-09-02 06:28:06', 'ya', 'keterangan', 'KREDIT', '2016-09-14 10:00:00', 3, 1, 1, '2016-09-01 23:28:06'),
(9, 160000, 160000, 'alaamt', 0, '2016-10-07 17:44:10', 'ya', 'ket', 'CASH', '2016-10-10 14:00:00', 1, 1, 1, '2016-10-07 10:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_dapur`
--

CREATE TABLE `tb_order_dapur` (
  `order_dapur_id` bigint(20) NOT NULL,
  `order_dapur_date` datetime NOT NULL,
  `order_dapur_tgl_butuh` datetime NOT NULL,
  `order_dapur_status` enum('belum','ok') NOT NULL,
  `order_dapur_tgl_terima` datetime NOT NULL,
  `order_dapur_jumlah` int(11) NOT NULL,
  `order_dapur_jumlah_kirim` int(11) NOT NULL,
  `order_dapur_jumlah_terima` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `order_detail_id` bigint(20) NOT NULL,
  `order_detail_price` bigint(20) NOT NULL,
  `order_detail_qty` int(11) NOT NULL,
  `order_detail_disc` bigint(20) NOT NULL,
  `order_detail_total` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order_detail`
--

INSERT INTO `tb_order_detail` (`order_detail_id`, `order_detail_price`, `order_detail_qty`, `order_detail_disc`, `order_detail_total`, `item_id`, `order_id`, `created`) VALUES
(1, 7500, 28, 0, 210000, 4, 1, '2016-07-31 09:13:07'),
(2, 12500, 12, 0, 150000, 9, 1, '2016-07-31 09:13:07'),
(3, 9500, 3, 0, 28500, 3, 1, '2016-07-31 09:13:07'),
(4, 20000, 1, 0, 20000, 11, 1, '2016-07-31 09:13:07'),
(5, 33000, 4, 0, 132000, 7, 2, '2016-07-31 09:26:05'),
(6, 23500, 2, 0, 47000, 5, 2, '2016-07-31 09:26:05'),
(7, 7500, 1, 0, 7500, 4, 2, '2016-07-31 09:26:05'),
(8, 33000, 4, 0, 132000, 7, 3, '2016-07-31 09:27:45'),
(9, 23500, 2, 0, 47000, 5, 3, '2016-07-31 09:27:45'),
(10, 20000, 1, 0, 20000, 11, 4, '2016-07-31 09:29:17'),
(11, 3500, 2, 0, 7000, 8, 4, '2016-07-31 09:29:17'),
(12, 3000, 10, 0, 30000, 1, 5, '2016-08-31 20:53:59'),
(13, 1500, 5, 0, 7500, 2, 5, '2016-08-31 20:53:59'),
(14, 3000, 2, 10, 5400, 1, 6, '2016-08-31 21:12:30'),
(15, 9000, 4, 0, 36000, 3, 7, '2016-09-01 17:34:57'),
(16, 4000, 1, 0, 4000, 37, 8, '2016-09-01 23:28:06'),
(17, 9000, 1, 0, 9000, 3, 8, '2016-09-01 23:28:06'),
(18, 8000, 2, 0, 16000, 4, 8, '2016-09-01 23:28:06'),
(19, 8000, 20, 0, 160000, 4, 9, '2016-10-07 10:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembelian_bahan`
--

CREATE TABLE `tb_pembelian_bahan` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pembelian_bahan`
--

INSERT INTO `tb_pembelian_bahan` (`id`, `total`, `tanggal`, `user_id`, `created`, `keterangan`) VALUES
(1, 900000, '2016-10-02', 1, '2016-10-02 05:21:59', 'belanja bahan mentah'),
(6, 1900000, '2016-10-07', 1, '2016-10-07 12:36:33', ''),
(7, 2000000, '2016-10-07', 1, '2016-10-07 12:37:12', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembelian_bahan_detail`
--

CREATE TABLE `tb_pembelian_bahan_detail` (
  `id` int(11) NOT NULL,
  `pembelian_bahan_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pembelian_bahan_detail`
--

INSERT INTO `tb_pembelian_bahan_detail` (`id`, `pembelian_bahan_id`, `bahan_id`, `harga`, `jumlah`, `total`, `created`) VALUES
(1, 1, 3, 300000, 1, 300000, '2016-10-02 05:22:00'),
(2, 1, 4, 200000, 1, 200000, '2016-10-02 05:22:00'),
(3, 1, 2, 200000, 2, 400000, '2016-10-02 05:22:00'),
(4, 4, 3, 300000, 5, 1500000, '2016-10-07 12:35:26'),
(5, 4, 1, 100000, 5, 500000, '2016-10-07 12:35:26'),
(6, 5, 3, 300000, 3, 900000, '2016-10-07 12:35:46'),
(7, 5, 1, 100000, 5, 500000, '2016-10-07 12:35:46'),
(8, 6, 3, 300000, 3, 900000, '2016-10-07 12:36:33'),
(9, 6, 2, 200000, 5, 1000000, '2016-10-07 12:36:33'),
(10, 7, 4, 200000, 10, 2000000, '2016-10-07 12:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pesanan`
--

CREATE TABLE `tb_pesanan` (
  `pesanan_id` bigint(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `pesanan_nota` char(10) NOT NULL,
  `pesanan_alamat` text NOT NULL,
  `pesanan_tgl_kirim` datetime NOT NULL,
  `pesanan_od` enum('0','1') NOT NULL,
  `pesanan_kirim` enum('0','1') NOT NULL,
  `pesanan_lunas` enum('0','1') NOT NULL,
  `pesanan_total` int(11) NOT NULL,
  `pesanan_bayar` int(11) NOT NULL,
  `pesanan_uangmuka` int(11) NOT NULL,
  `pesanan_pelunasan` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_resep`
--

CREATE TABLE `tb_resep` (
  `resep_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `jumlah_produksi` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `biaya_total` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_resep`
--

INSERT INTO `tb_resep` (`resep_id`, `tanggal`, `keterangan`, `jumlah_produksi`, `item_id`, `biaya_total`, `user_id`, `created`) VALUES
(1, '2016-10-02', 'keterangan tambahan', 20, 4, 34100, 1, '2016-10-02 17:05:11'),
(2, '2016-10-02', 'ket. lagi', 30, 2, 26100, 1, '2016-10-02 17:05:13'),
(3, '2016-10-03', '', 30, 5, 34100, 1, '2016-10-02 17:04:28'),
(4, '2016-10-07', '', 100, 37, 56000, 1, '2016-10-07 12:18:49'),
(5, '2016-10-07', '', 20, 9, 90200, 1, '2016-10-07 12:43:50'),
(6, '2016-10-07', '', 20, 9, 130000, 1, '2016-10-07 12:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_resep_detail`
--

CREATE TABLE `tb_resep_detail` (
  `resep_detail_id` int(11) NOT NULL,
  `resep_id` int(11) NOT NULL,
  `bahan_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_resep_detail`
--

INSERT INTO `tb_resep_detail` (`resep_detail_id`, `resep_id`, `bahan_id`, `jumlah`, `harga`, `subtotal`, `created`) VALUES
(1, 1, 2, 3, 6700, 20100, '2016-10-02 14:09:49'),
(2, 1, 3, 1, 10000, 10000, '2016-10-02 14:09:49'),
(3, 1, 1, 2, 2000, 4000, '2016-10-02 14:09:49'),
(4, 2, 2, 3, 6700, 20100, '2016-10-02 14:10:29'),
(5, 2, 1, 3, 2000, 6000, '2016-10-02 14:10:29'),
(6, 3, 2, 3, 6700, 20100, '2016-10-02 17:04:28'),
(7, 3, 1, 2, 2000, 4000, '2016-10-02 17:04:28'),
(8, 3, 3, 1, 10000, 10000, '2016-10-02 17:04:28'),
(9, 4, 3, 3, 10000, 30000, '2016-10-07 12:18:49'),
(10, 4, 4, 1, 20000, 20000, '2016-10-07 12:18:49'),
(11, 4, 1, 3, 2000, 6000, '2016-10-07 12:18:49'),
(12, 5, 3, 3, 10000, 30000, '2016-10-07 12:43:50'),
(13, 5, 4, 1, 20000, 20000, '2016-10-07 12:43:50'),
(14, 5, 2, 6, 6700, 40200, '2016-10-07 12:43:50'),
(15, 6, 3, 3, 10000, 30000, '2016-10-07 12:48:33'),
(16, 6, 4, 5, 20000, 100000, '2016-10-07 12:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_retur`
--

CREATE TABLE `tb_retur` (
  `retur_id` bigint(20) NOT NULL,
  `retur_date` datetime NOT NULL,
  `retur_desc` varchar(255) DEFAULT NULL,
  `outlet_id` int(11) NOT NULL,
  `retur_type` enum('PEMBELI','OUTLET','DAPUR') NOT NULL DEFAULT 'PEMBELI',
  `retur_status` enum('YA','TIDAK') NOT NULL DEFAULT 'TIDAK',
  `user_id` int(11) NOT NULL,
  `retur_total` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_retur`
--

INSERT INTO `tb_retur` (`retur_id`, `retur_date`, `retur_desc`, `outlet_id`, `retur_type`, `retur_status`, `user_id`, `retur_total`, `created`) VALUES
(2, '2016-08-30 22:16:33', 'label rusak', 4, 'PEMBELI', 'TIDAK', 0, 0, '2016-08-30 15:16:33'),
(4, '2016-08-30 22:29:06', 'plastik rusak', 4, 'OUTLET', 'TIDAK', 0, 0, '2016-08-31 18:57:18'),
(5, '2016-08-30 22:31:48', 'label rusak', 4, 'DAPUR', 'TIDAK', 0, 0, '2016-08-31 18:57:22'),
(6, '2016-09-01 02:00:56', 'produk expired', 4, 'PEMBELI', 'TIDAK', 0, 0, '2016-08-31 19:00:56'),
(7, '2016-09-01 22:25:48', 'testing retur pembeli', 1, 'PEMBELI', 'TIDAK', 1, 16000, '2016-09-01 15:25:48'),
(8, '2016-09-01 23:12:51', 'testing retur pembeli lagi', 1, 'PEMBELI', 'TIDAK', 1, 40000, '2016-09-01 16:12:51'),
(9, '2016-09-01 23:15:23', 'testing lagi', 1, 'PEMBELI', 'TIDAK', 1, 40000, '2016-09-01 16:15:23'),
(10, '2016-09-01 23:16:01', 'tes lagi', 1, 'PEMBELI', 'TIDAK', 1, 44000, '2016-09-01 16:16:01'),
(11, '2016-09-01 23:23:12', 'retur outlet', 1, 'OUTLET', 'TIDAK', 1, 21000, '2016-09-01 16:23:12'),
(12, '2016-09-01 23:28:44', 'retur dapur', 1, 'DAPUR', 'TIDAK', 1, 8000, '2016-09-01 16:28:44'),
(13, '2016-09-02 06:41:33', 'barang expired', 1, 'PEMBELI', 'YA', 1, 9000, '2016-09-01 23:42:01'),
(14, '2016-09-02 06:42:54', 'barang kadarluarsa', 1, 'OUTLET', 'YA', 1, 14000, '2016-09-01 23:43:07'),
(15, '2016-09-02 06:43:37', 'stok di toko saya kebanyakan', 1, 'DAPUR', 'YA', 1, 10000, '2016-09-01 23:43:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_retur_detail`
--

CREATE TABLE `tb_retur_detail` (
  `retur_detail_id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `retur_detail_qty` int(11) NOT NULL,
  `retur_detail_price` int(11) NOT NULL,
  `retur_id` bigint(20) NOT NULL,
  `retur_detail_total` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_retur_detail`
--

INSERT INTO `tb_retur_detail` (`retur_detail_id`, `item_id`, `retur_detail_qty`, `retur_detail_price`, `retur_id`, `retur_detail_total`, `created`) VALUES
(1, 4, 1, 6000, 7, 6000, '2016-09-01 15:25:48'),
(2, 1, 1, 2000, 7, 2000, '2016-09-01 15:25:48'),
(3, 3, 1, 7000, 7, 7000, '2016-09-01 15:25:48'),
(4, 2, 1, 1000, 7, 1000, '2016-09-01 15:25:48'),
(5, 4, 2, 6000, 8, 12000, '2016-09-01 16:12:52'),
(6, 3, 4, 7000, 8, 28000, '2016-09-01 16:12:52'),
(7, 4, 2, 6000, 9, 12000, '2016-09-01 16:15:23'),
(8, 3, 4, 7000, 9, 28000, '2016-09-01 16:15:23'),
(9, 4, 2, 6000, 10, 12000, '2016-09-01 16:16:01'),
(10, 3, 4, 7000, 10, 28000, '2016-09-01 16:16:01'),
(11, 2, 4, 1000, 10, 4000, '2016-09-01 16:16:01'),
(12, 3, 3, 7000, 11, 21000, '2016-09-01 16:23:12'),
(13, 1, 4, 2000, 12, 8000, '2016-09-01 16:28:44'),
(14, 3, 1, 7000, 13, 7000, '2016-09-01 23:41:33'),
(15, 37, 1, 2000, 13, 2000, '2016-09-01 23:41:33'),
(16, 3, 2, 7000, 14, 14000, '2016-09-01 23:42:54'),
(17, 1, 1, 2000, 15, 2000, '2016-09-01 23:43:37'),
(18, 3, 1, 7000, 15, 7000, '2016-09-01 23:43:37'),
(19, 2, 1, 1000, 15, 1000, '2016-09-01 23:43:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sales`
--

CREATE TABLE `tb_sales` (
  `sales_id` bigint(20) NOT NULL,
  `sales_pay` bigint(20) NOT NULL,
  `sales_cashback` bigint(20) NOT NULL,
  `sales_total` bigint(20) NOT NULL,
  `sales_discount` bigint(20) NOT NULL,
  `sales_disc_product` bigint(20) NOT NULL,
  `sales_date` datetime NOT NULL,
  `sales_shift` enum('1','2') NOT NULL DEFAULT '1',
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `sales_lunas` tinyint(1) NOT NULL DEFAULT '0',
  `sales_note` varchar(200) DEFAULT NULL,
  `sales_type` enum('cash','kredit','transfer') NOT NULL DEFAULT 'cash',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_sales`
--

INSERT INTO `tb_sales` (`sales_id`, `sales_pay`, `sales_cashback`, `sales_total`, `sales_discount`, `sales_disc_product`, `sales_date`, `sales_shift`, `customer_id`, `user_id`, `outlet_id`, `sales_lunas`, `sales_note`, `sales_type`, `created`) VALUES
(1, 60000, 5000, 55000, 400, 0, '2016-08-05 10:02:18', '1', 2, 1, 2, 0, NULL, 'cash', '2016-08-05 03:02:18'),
(2, 100000, 7000, 93000, 5, 0, '2016-08-06 12:47:26', '1', 3, 1, 2, 0, NULL, 'cash', '2016-08-05 05:47:26'),
(3, 100000, 2500, 97500, 5, 0, '2016-08-31 12:48:27', '1', 1, 1, 2, 0, NULL, 'cash', '2016-08-05 05:48:27'),
(4, 20000, 0, 20000, 1, 0, '2016-08-22 12:50:01', '1', 1, 1, 2, 0, NULL, 'cash', '2016-08-05 05:50:01'),
(5, 15000, 2500, 12500, 1, 0, '2016-08-22 12:50:44', '1', 1, 1, 2, 0, NULL, 'cash', '2016-08-05 05:50:44'),
(6, 15000, 2500, 12500, 1, 0, '2016-08-22 12:51:25', '1', 1, 1, 2, 0, NULL, 'cash', '2016-08-05 05:51:25'),
(7, 50000, 20000, 30000, 0, 0, '2016-08-22 12:53:13', '1', 1, 1, 2, 0, NULL, 'cash', '2016-08-05 05:53:13'),
(8, 50000, 7500, 42500, 0, 0, '2016-08-29 12:55:05', '2', 1, 1, 0, 0, NULL, 'cash', '2016-08-05 05:55:05'),
(9, 50000, 0, 50000, 0, 0, '2016-08-22 13:09:31', '2', 1, 1, 3, 0, NULL, 'cash', '2016-08-05 06:09:31'),
(10, 50000, 22500, 27500, 0, 0, '2016-08-05 13:10:09', '1', 1, 1, 4, 0, NULL, 'cash', '2016-08-05 06:10:09'),
(11, 50000, 50000, 0, 0, 0, '2016-08-22 13:10:10', '1', 1, 1, 4, 0, NULL, 'cash', '2016-08-05 06:10:10'),
(12, 50000, 25000, 25000, 0, 0, '2016-08-22 13:11:06', '2', 1, 1, 2, 0, NULL, 'cash', '2016-08-05 06:11:06'),
(13, 50000, 7500, 42500, 0, 0, '2016-08-22 13:14:03', '1', 1, 1, 4, 0, NULL, 'cash', '2016-08-05 06:14:03'),
(14, 100000, 65000, 35000, 0, 0, '2016-08-05 13:15:42', '1', 1, 1, 4, 0, NULL, 'cash', '2016-08-05 06:15:42'),
(15, 30000, 12500, 17500, 0, 0, '2016-08-05 13:17:03', '2', 1, 1, 4, 0, NULL, 'cash', '2016-08-05 06:17:03'),
(16, 60000, 19500, 40500, 0, 0, '2016-08-05 13:17:13', '1', 1, 1, 4, 0, NULL, 'cash', '2016-08-05 06:17:13'),
(17, 50000, 15000, 35000, 0, 0, '2016-08-10 18:19:43', '1', 3, 1, 2, 0, NULL, 'cash', '2016-08-05 11:19:43'),
(18, 5000, -3400, 8400, 5, 0, '2016-09-01 05:53:07', '1', 2, 1, 1, 0, NULL, 'cash', '2016-08-31 22:53:07'),
(19, 10000, -6200, 16200, 7, 0, '2016-09-01 05:57:25', '1', 2, 1, 1, 0, NULL, 'cash', '2016-08-31 22:57:25'),
(20, 10000, -3500, 13500, 7, 0, '2016-09-01 06:00:48', '1', 2, 1, 1, 0, NULL, 'cash', '2016-08-31 23:00:48'),
(21, 5000, 500, 4500, 1, 0, '2016-09-01 06:01:13', '1', 1, 1, 1, 0, NULL, 'cash', '2016-08-31 23:01:13'),
(22, 5000, 2000, 3000, 500, 0, '2016-09-01 06:02:24', '1', 1, 1, 1, 0, NULL, 'cash', '2016-08-31 23:02:24'),
(23, 100000, 25000, 75000, 0, 0, '2016-09-02 06:32:34', '1', 2, 1, 4, 0, NULL, 'cash', '2016-09-01 23:32:34'),
(24, 20000, -2500, 22500, 5, 0, '2016-09-02 06:33:07', '1', 2, 1, 4, 0, NULL, 'cash', '2016-09-01 23:33:07'),
(25, 20000, 9200, 10800, 0, 0, '2016-09-02 06:38:03', '1', 1, 1, 3, 0, NULL, 'cash', '2016-09-01 23:38:03'),
(26, 100000, 39200, 60800, 0, 0, '2016-09-02 06:38:17', '1', 1, 1, 3, 0, NULL, 'cash', '2016-09-01 23:38:17'),
(27, 20000, 2500, 17500, 0, 0, '2016-09-08 00:25:06', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:25:06'),
(28, 15000, 100, 14900, 0, 0, '2016-09-08 00:39:07', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:39:07'),
(29, 20000, 3800, 16200, 0, 0, '2016-09-08 00:40:19', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:40:19'),
(30, 50000, 33800, 16200, 0, 0, '2016-09-08 00:42:29', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:42:29'),
(31, 50000, 29800, 20200, 0, 0, '2016-09-08 00:42:58', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:42:58'),
(32, 20000, 3800, 16200, 0, 0, '2016-09-08 00:47:27', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:47:27'),
(33, 30000, 13800, 16200, 0, 0, '2016-09-08 00:49:35', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:49:35'),
(34, 50000, 21600, 28400, 0, 0, '2016-09-08 00:55:13', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:55:13'),
(35, 50000, 33800, 16200, 0, 0, '2016-09-08 00:56:42', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:56:42'),
(36, 20000, 3800, 16200, 0, 0, '2016-09-08 00:58:53', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 17:58:53'),
(37, 40000, 23800, 16200, 0, 0, '2016-09-08 01:02:35', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:02:35'),
(38, 20000, 3800, 16200, 0, 0, '2016-09-08 01:03:08', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:03:08'),
(39, 30000, 13800, 16200, 0, 0, '2016-09-08 01:03:44', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:03:44'),
(40, 20000, 3800, 16200, 0, 0, '2016-09-08 01:35:50', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:35:50'),
(41, 20000, 3800, 16200, 0, 0, '2016-09-08 01:37:36', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:37:36'),
(42, 40000, 23800, 16200, 0, 0, '2016-09-08 01:38:35', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:38:35'),
(43, 30000, 1600, 28400, 0, 0, '2016-09-08 01:42:06', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:42:06'),
(44, 30000, 1800, 28200, 0, 0, '2016-09-08 01:42:52', '1', 1, 1, 1, 0, NULL, 'cash', '2016-09-07 18:42:52'),
(45, 80000, 1000, 79000, 0, 0, '2016-10-05 19:34:43', '1', 1, 1, 1, 0, NULL, 'cash', '2016-10-05 12:34:43'),
(46, 10000, -17900, 27900, 0, 0, '2016-10-06 23:38:13', '1', 1, 1, 1, 0, NULL, 'kredit', '2016-10-06 16:38:13'),
(47, 30000, 3000, 27000, 0, 0, '2016-10-07 17:41:22', '1', 1, 1, 1, 1, NULL, 'cash', '2016-10-07 10:41:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sales_detail`
--

CREATE TABLE `tb_sales_detail` (
  `sales_detail_id` bigint(20) NOT NULL,
  `sales_detail_price` bigint(20) NOT NULL,
  `sales_detail_qty` int(11) NOT NULL,
  `sales_detail_total` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `sales_id` bigint(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sales_detail_disc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_sales_detail`
--

INSERT INTO `tb_sales_detail` (`sales_detail_id`, `sales_detail_price`, `sales_detail_qty`, `sales_detail_total`, `item_id`, `sales_id`, `created`, `sales_detail_disc`) VALUES
(1, 17500, 1, 17500, 1, 1, '2016-08-05 03:02:18', 0),
(2, 10000, 1, 10000, 2, 1, '2016-08-05 03:02:18', 0),
(3, 12500, 1, 12500, 9, 1, '2016-08-05 03:02:18', 0),
(4, 7500, 2, 15000, 4, 1, '2016-08-05 03:02:18', 0),
(5, 3500, 1, 3500, 8, 2, '2016-08-05 05:47:26', 0),
(6, 23500, 2, 47000, 5, 2, '2016-08-05 05:47:26', 0),
(7, 17500, 2, 35000, 1, 2, '2016-08-05 05:47:26', 0),
(8, 7500, 1, 7500, 4, 2, '2016-08-05 05:47:26', 0),
(9, 3500, 1, 3500, 8, 3, '2016-08-05 05:48:27', 0),
(10, 23500, 4, 94000, 5, 3, '2016-08-05 05:48:27', 0),
(11, 12500, 1, 12500, 9, 4, '2016-08-05 05:50:01', 0),
(12, 7500, 1, 7500, 4, 4, '2016-08-05 05:50:01', 0),
(13, 12500, 1, 12500, 9, 5, '2016-08-05 05:50:44', 0),
(14, 12500, 1, 12500, 9, 6, '2016-08-05 05:51:25', 0),
(15, 7500, 4, 30000, 4, 7, '2016-08-05 05:53:13', 0),
(16, 9500, 1, 9500, 6, 8, '2016-08-05 05:55:05', 0),
(17, 33000, 1, 33000, 7, 8, '2016-08-05 05:55:05', 0),
(18, 12500, 4, 50000, 9, 9, '2016-08-05 06:09:31', 0),
(19, 12500, 1, 12500, 9, 10, '2016-08-05 06:10:09', 0),
(20, 7500, 2, 15000, 4, 10, '2016-08-05 06:10:09', 0),
(21, 12500, 2, 25000, 9, 12, '2016-08-05 06:11:06', 0),
(22, 17500, 2, 35000, 1, 13, '2016-08-05 06:14:03', 0),
(23, 7500, 1, 7500, 4, 13, '2016-08-05 06:14:03', 0),
(24, 17500, 2, 35000, 1, 14, '2016-08-05 06:15:42', 0),
(25, 17500, 1, 17500, 1, 15, '2016-08-05 06:17:03', 0),
(26, 7500, 1, 7500, 4, 16, '2016-08-05 06:17:13', 0),
(27, 33000, 1, 33000, 7, 16, '2016-08-05 06:17:13', 0),
(28, 17500, 2, 35000, 1, 17, '2016-08-05 11:19:43', 0),
(29, 3000, 2, 5400, 1, 18, '2016-08-31 22:53:08', 10),
(30, 1500, 2, 3000, 2, 18, '2016-08-31 22:53:08', 0),
(31, 3000, 6, 16200, 1, 19, '2016-08-31 22:57:25', 10),
(32, 3000, 5, 13500, 1, 20, '2016-08-31 23:00:48', 10),
(33, 1500, 3, 4500, 2, 21, '2016-08-31 23:01:13', 0),
(34, 1500, 2, 3000, 2, 22, '2016-08-31 23:02:24', 0),
(35, 7500, 10, 75000, 5, 23, '2016-09-01 23:32:34', 0),
(36, 7500, 3, 22500, 5, 24, '2016-09-01 23:33:07', 0),
(37, 6000, 1, 4800, 37, 25, '2016-09-01 23:38:03', 20),
(38, 6000, 1, 6000, 9, 25, '2016-09-01 23:38:03', 0),
(39, 6000, 6, 28800, 37, 26, '2016-09-01 23:38:17', 20),
(40, 4000, 2, 8000, 11, 26, '2016-09-01 23:38:17', 0),
(41, 6000, 4, 24000, 9, 26, '2016-09-01 23:38:17', 0),
(42, 8000, 2, 16000, 4, 27, '2016-09-07 17:25:06', 0),
(43, 1500, 1, 1500, 2, 27, '2016-09-07 17:25:06', 0),
(44, 1500, 1, 1500, 2, 28, '2016-09-07 17:39:07', 0),
(45, 3000, 2, 5400, 1, 28, '2016-09-07 17:39:07', 10),
(46, 8000, 1, 8000, 4, 28, '2016-09-07 17:39:07', 0),
(47, 3000, 1, 2700, 1, 29, '2016-09-07 17:40:19', 10),
(48, 1500, 1, 1500, 2, 29, '2016-09-07 17:40:19', 0),
(49, 8000, 1, 8000, 4, 29, '2016-09-07 17:40:19', 0),
(50, 4000, 1, 4000, 37, 29, '2016-09-07 17:40:19', 0),
(51, 3000, 1, 2700, 1, 30, '2016-09-07 17:42:30', 10),
(52, 1500, 1, 1500, 2, 30, '2016-09-07 17:42:30', 0),
(53, 8000, 1, 8000, 4, 30, '2016-09-07 17:42:30', 0),
(54, 4000, 1, 4000, 37, 30, '2016-09-07 17:42:30', 0),
(55, 3000, 1, 2700, 1, 31, '2016-09-07 17:42:58', 10),
(56, 1500, 1, 1500, 2, 31, '2016-09-07 17:42:58', 0),
(57, 8000, 1, 8000, 4, 31, '2016-09-07 17:42:58', 0),
(58, 4000, 2, 8000, 37, 31, '2016-09-07 17:42:58', 0),
(59, 3000, 1, 2700, 1, 32, '2016-09-07 17:47:27', 10),
(60, 1500, 1, 1500, 2, 32, '2016-09-07 17:47:27', 0),
(61, 8000, 1, 8000, 4, 32, '2016-09-07 17:47:27', 0),
(62, 4000, 1, 4000, 37, 32, '2016-09-07 17:47:27', 0),
(63, 3000, 1, 2700, 1, 33, '2016-09-07 17:49:35', 10),
(64, 1500, 1, 1500, 2, 33, '2016-09-07 17:49:35', 0),
(65, 8000, 1, 8000, 4, 33, '2016-09-07 17:49:35', 0),
(66, 4000, 1, 4000, 37, 33, '2016-09-07 17:49:35', 0),
(67, 3000, 2, 5400, 1, 34, '2016-09-07 17:55:13', 10),
(68, 1500, 2, 3000, 2, 34, '2016-09-07 17:55:13', 0),
(69, 8000, 2, 16000, 4, 34, '2016-09-07 17:55:13', 0),
(70, 4000, 1, 4000, 37, 34, '2016-09-07 17:55:13', 0),
(71, 3000, 1, 2700, 1, 35, '2016-09-07 17:56:42', 10),
(72, 1500, 1, 1500, 2, 35, '2016-09-07 17:56:42', 0),
(73, 8000, 1, 8000, 4, 35, '2016-09-07 17:56:42', 0),
(74, 4000, 1, 4000, 37, 35, '2016-09-07 17:56:42', 0),
(75, 3000, 1, 2700, 1, 36, '2016-09-07 17:58:54', 10),
(76, 1500, 1, 1500, 2, 36, '2016-09-07 17:58:54', 0),
(77, 8000, 1, 8000, 4, 36, '2016-09-07 17:58:54', 0),
(78, 4000, 1, 4000, 37, 36, '2016-09-07 17:58:54', 0),
(79, 3000, 1, 2700, 1, 37, '2016-09-07 18:02:35', 10),
(80, 1500, 1, 1500, 2, 37, '2016-09-07 18:02:35', 0),
(81, 8000, 1, 8000, 4, 37, '2016-09-07 18:02:35', 0),
(82, 4000, 1, 4000, 37, 37, '2016-09-07 18:02:35', 0),
(83, 3000, 1, 2700, 1, 38, '2016-09-07 18:03:08', 10),
(84, 1500, 1, 1500, 2, 38, '2016-09-07 18:03:08', 0),
(85, 8000, 1, 8000, 4, 38, '2016-09-07 18:03:08', 0),
(86, 4000, 1, 4000, 37, 38, '2016-09-07 18:03:08', 0),
(87, 3000, 1, 2700, 1, 39, '2016-09-07 18:03:44', 10),
(88, 1500, 1, 1500, 2, 39, '2016-09-07 18:03:44', 0),
(89, 8000, 1, 8000, 4, 39, '2016-09-07 18:03:44', 0),
(90, 4000, 1, 4000, 37, 39, '2016-09-07 18:03:44', 0),
(91, 3000, 1, 2700, 1, 40, '2016-09-07 18:35:50', 10),
(92, 1500, 1, 1500, 2, 40, '2016-09-07 18:35:50', 0),
(93, 8000, 1, 8000, 4, 40, '2016-09-07 18:35:50', 0),
(94, 4000, 1, 4000, 37, 40, '2016-09-07 18:35:50', 0),
(95, 3000, 1, 2700, 1, 41, '2016-09-07 18:37:37', 10),
(96, 1500, 1, 1500, 2, 41, '2016-09-07 18:37:37', 0),
(97, 8000, 1, 8000, 4, 41, '2016-09-07 18:37:37', 0),
(98, 4000, 1, 4000, 37, 41, '2016-09-07 18:37:37', 0),
(99, 3000, 1, 2700, 1, 42, '2016-09-07 18:38:36', 10),
(100, 1500, 1, 1500, 2, 42, '2016-09-07 18:38:36', 0),
(101, 8000, 1, 8000, 4, 42, '2016-09-07 18:38:36', 0),
(102, 4000, 1, 4000, 37, 42, '2016-09-07 18:38:36', 0),
(103, 4000, 1, 4000, 37, 43, '2016-09-07 18:42:06', 0),
(104, 8000, 2, 16000, 4, 43, '2016-09-07 18:42:06', 0),
(105, 1500, 2, 3000, 2, 43, '2016-09-07 18:42:06', 0),
(106, 3000, 2, 5400, 1, 43, '2016-09-07 18:42:06', 10),
(107, 3000, 1, 2700, 1, 44, '2016-09-07 18:42:52', 10),
(108, 1500, 1, 1500, 2, 44, '2016-09-07 18:42:52', 0),
(109, 4000, 2, 8000, 37, 44, '2016-09-07 18:42:52', 0),
(110, 8000, 2, 16000, 4, 44, '2016-09-07 18:42:52', 0),
(111, 8000, 2, 16000, 4, 45, '2016-10-05 12:34:43', 0),
(112, 9000, 7, 63000, 3, 45, '2016-10-05 12:34:43', 0),
(113, 3000, 2, 5400, 1, 46, '2016-10-06 16:38:13', 10),
(114, 1500, 1, 1500, 2, 46, '2016-10-06 16:38:13', 0),
(115, 9000, 1, 9000, 3, 46, '2016-10-06 16:38:13', 0),
(116, 8000, 1, 8000, 4, 46, '2016-10-06 16:38:13', 0),
(117, 4000, 1, 4000, 37, 46, '2016-10-06 16:38:13', 0),
(118, 3000, 10, 27000, 1, 47, '2016-10-07 10:41:22', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting`
--

CREATE TABLE `tb_setting` (
  `setting_id` bigint(20) NOT NULL,
  `setting_type` varchar(60) CHARACTER SET utf8 NOT NULL,
  `setting_name` varchar(60) CHARACTER SET utf8 NOT NULL,
  `setting_value` text CHARACTER SET utf8 NOT NULL,
  `setting_desc` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setting`
--

INSERT INTO `tb_setting` (`setting_id`, `setting_type`, `setting_name`, `setting_value`, `setting_desc`) VALUES
(3, 'cpanel', 'theme_back', 'king', ''),
(4, 'cpanel', 'Title', 'Bootstrab', ''),
(5, 'cpanel', 'Logo', 'panellogo.png', ''),
(6, 'front-customize', 'Site Logo', 'ed92c-logo.png', 'image'),
(7, 'front-customize', 'Link Color', '#7e24c2', 'color'),
(8, 'front-customize', 'Background Color', '#00ff88', 'color'),
(9, 'front-customize', 'Backgound Image', '65b00-201147.png', 'image'),
(10, 'front-customize', 'Header Color', '#cf0000', 'color'),
(11, 'front-customize', 'Primary Menu', '21', 'menu'),
(12, 'front-customize', 'Footer Menu', '23', 'menu'),
(13, 'front-customize', 'Static Frontend', '210', 'page'),
(14, 'front-widgets', 'Facebook Fans Page', '', 'deactive'),
(15, 'front-widgets', 'Twitter', '', 'deactive'),
(16, 'front-widgets', 'Histats', '', 'deactive'),
(18, 'front-widgets', 'Add This', '', 'active'),
(19, 'front-widgets', 'Zopim', '', 'deactive'),
(20, 'front-widgets', 'Instagram', '', 'deactive'),
(21, 'front-menus', 'menus_positions', 'Primary Menu', '21'),
(23, 'front-menus', 'menus_positions', 'Footer Menu', '23'),
(80, 'setting_general', 'Site Title', 'Roti Bakery', 'site_title'),
(81, 'setting_general', 'Site Description', 'Description', 'site_desc'),
(82, 'setting_general', 'Keyword', 'Keyword', 'site_keyword'),
(83, 'setting_general', 'Tag Line', 'Tag Line', 'site_tagline'),
(84, 'setting_general', 'Admin E-mail Address', 'admin@admin.com', 'site_email'),
(85, 'setting_company', 'Company Name', 'Bakery', 'company_name'),
(86, 'setting_company', 'Company Address', 'Yogyakarta', 'company_address'),
(87, 'setting_company', 'Company Phone', 'Telp. +62 274', 'company_telephone'),
(91, 'setting_company', 'Company Email', '', 'company_email'),
(102, 'setting_seo', 'Google Webmaster Tools', '', 'google_webmaster_tools'),
(103, 'setting_seo', 'Alexa Verification ID', '', 'alexa_verification'),
(104, 'setting_seo', 'Bing Webmaster Tools', '', 'bing_webmaster'),
(105, 'setting_seo', 'Pinterest', '', 'pinterest'),
(106, 'setting_seo', 'Yandex Webmaster Tools', '', 'yandex_webmaster'),
(107, 'setting_seo', 'Google Analyty', '', 'google_analyty'),
(108, 'setting_seo', 'Google Publisher Page', '', 'google_publisher'),
(109, 'setting_seo', 'Meta Author', '', 'meta_author'),
(110, 'setting_seo', 'Robot Index', '', 'robot_index'),
(111, 'setting_seo', 'Google Boot', '', 'google_boot'),
(112, 'setting_seo', 'Site Map', '', 'site_map'),
(123, 'front-customize', 'Header Image', '24c65-2048.jpg', 'image'),
(124, 'setting_seo', 'Google Author Page', '', 'google_author_page'),
(125, 'setting_seo', 'Account Twitter SEO', '', 'twitter_seo'),
(133, 'setting_company', 'company footer', '© 2016 Roti Bakery All Right Reserved', 'company_footer'),
(162, 'front-theme', 'tezku-default-theme', 'active', '34389-hasilmoupsat.jpg'),
(233, 'front-customize', 'Secondary Menu', '235', 'menu'),
(234, 'front-menus-lists', '', 'Secondary Menu', ''),
(235, 'front-menus', 'menus_positions', 'Secondary Menu', ''),
(236, 'front-menus-lists', '', 'Secondary Menu', '235'),
(261, 'setting_mitra', 'BPS', 'c24f6-bps.png', 'https://www.bps.go.id'),
(262, 'setting_mitra', 'Perdagangan', '1567a-perdagangan.png', 'http://dinperindag.jatengprov.go.id'),
(263, 'setting_mitra', 'Pertanian', '21951-pertanian.png', 'http://dinpertantph.jatengprov.go.id'),
(264, 'setting_mitra', 'Agromaret', '3bdf5-agromaret.png', 'http://agromaret.com/15981/pasar_lelang_semarang'),
(265, 'setting_mitra', 'BMKG', '2d59d-bmkg.png', 'http://www.bmkg.go.id/BMKG_Pusat/Klimatologi/Prakiraan_Hujan_Bulanan.bmkg'),
(266, 'setting_mitra', 'PIHPS', '283e5-pihps.png', 'http://hargapangan.id'),
(313, 'role_type', 'Add users', 'add', 'users'),
(314, 'role_type', 'Delete users', 'delete', 'users'),
(315, 'role_type', 'Edit users', 'edit', 'users'),
(316, 'role_type', 'Export users', 'export', 'users'),
(318, 'role_type', 'Add groups', 'add', 'groups'),
(319, 'role_type', 'Delete groups', 'delete', 'groups'),
(320, 'role_type', 'Edit groups', 'edit', 'groups'),
(321, 'role_type', 'Export groups', 'export', 'groups'),
(336, 'role_type', 'Add Setting General', 'add', 'setting_general'),
(337, 'role_type', 'Delete Setting General', 'delete', 'setting_general'),
(338, 'role_type', 'Edit Setting General', 'edit', 'setting_general'),
(339, 'role_type', 'Print Setting General', 'print', 'setting_general'),
(340, 'role_type', 'Export Setting General', 'export', 'setting_general'),
(341, 'role_type', 'Menu Setting General', 'setting_general', 'menu'),
(343, 'role_type', 'Add Setting Company', 'add', 'setting_company'),
(344, 'role_type', 'Delete Setting Company', 'delete', 'setting_company'),
(345, 'role_type', 'Edit Setting Company', 'edit', 'setting_company'),
(346, 'role_type', 'Print Setting Company', 'print', 'setting_company'),
(347, 'role_type', 'Export Setting Company', 'export', 'setting_company'),
(348, 'role_type', 'Menu Setting Company', 'setting_company', 'menu'),
(350, 'role_type', 'Add Master Outlet', 'add', 'master_outlet'),
(351, 'role_type', 'Delete Master Outlet', 'delete', 'master_outlet'),
(352, 'role_type', 'Edit Master Outlet', 'edit', 'master_outlet'),
(353, 'role_type', 'Print Master Outlet', 'print', 'master_outlet'),
(354, 'role_type', 'Export Master Outlet', 'export', 'master_outlet'),
(355, 'role_type', 'Menu Master Outlet', 'master_outlet', 'menu'),
(357, 'role_type', 'Add Master resep', 'add', 'master_resep'),
(358, 'role_type', 'Delete Master resep', 'delete', 'master_resep'),
(359, 'role_type', 'Edit Master resep', 'edit', 'master_resep'),
(360, 'role_type', 'Print Master resep', 'print', 'master_resep'),
(361, 'role_type', 'Export Master resep', 'export', 'master_resep'),
(362, 'role_type', 'Menu Master resep', 'master_resep', 'menu'),
(364, 'role_type', 'Add Master Category', 'add', 'master_category'),
(365, 'role_type', 'Delete Master Category', 'delete', 'master_category'),
(366, 'role_type', 'Edit Master Category', 'edit', 'master_category'),
(367, 'role_type', 'Print Master Category', 'print', 'master_category'),
(368, 'role_type', 'Export Master Category', 'export', 'master_category'),
(369, 'role_type', 'Menu Master Category', 'master_category', 'menu'),
(371, 'role_type', 'Add Master Jenis', 'add', 'master_jenis'),
(372, 'role_type', 'Delete Master Jenis', 'delete', 'master_jenis'),
(373, 'role_type', 'Edit Master Jenis', 'edit', 'master_jenis'),
(374, 'role_type', 'Print Master Jenis', 'print', 'master_jenis'),
(375, 'role_type', 'Export Master Jenis', 'export', 'master_jenis'),
(376, 'role_type', 'Menu Master Jenis', 'master_jenis', 'menu'),
(378, 'role_type', 'Add Master Satuan', 'add', 'master_satuan'),
(379, 'role_type', 'Delete Master Satuan', 'delete', 'master_satuan'),
(380, 'role_type', 'Edit Master Satuan', 'edit', 'master_satuan'),
(381, 'role_type', 'Print Master Satuan', 'print', 'master_satuan'),
(382, 'role_type', 'Export Master Satuan', 'export', 'master_satuan'),
(383, 'role_type', 'Menu Master Satuan', 'master_satuan', 'menu'),
(385, 'role_type', 'Add Master Bahan', 'add', 'master_bahan'),
(386, 'role_type', 'Delete Master Bahan', 'delete', 'master_bahan'),
(387, 'role_type', 'Edit Master Bahan', 'edit', 'master_bahan'),
(388, 'role_type', 'Print Master Bahan', 'print', 'master_bahan'),
(389, 'role_type', 'Export Master Bahan', 'export', 'master_bahan'),
(390, 'role_type', 'Menu Master Bahan', 'master_bahan', 'menu'),
(392, 'role_type', 'Add Customers', 'add', 'customers'),
(393, 'role_type', 'Delete Customers', 'delete', 'customers'),
(394, 'role_type', 'Edit Customers', 'edit', 'customers'),
(395, 'role_type', 'Print Customers', 'print', 'customers'),
(396, 'role_type', 'Export Customers', 'export', 'customers'),
(397, 'role_type', 'Menu Customers', 'customers', 'menu'),
(399, 'role_type', 'Add Master Item', 'add', 'master_item'),
(400, 'role_type', 'Delete Master Item', 'delete', 'master_item'),
(401, 'role_type', 'Edit Master Item', 'edit', 'master_item'),
(402, 'role_type', 'Print Master Item', 'print', 'master_item'),
(403, 'role_type', 'Export Master Item', 'export', 'master_item'),
(404, 'role_type', 'Menu Master Item', 'master_item', 'menu'),
(406, 'role_type', 'Add Pemesanan', 'add', 'pemesanan'),
(407, 'role_type', 'Delete Pemesanan', 'delete', 'pemesanan'),
(408, 'role_type', 'Edit Pemesanan', 'edit', 'pemesanan'),
(409, 'role_type', 'Print Pemesanan', 'print', 'pemesanan'),
(410, 'role_type', 'Export Pemesanan', 'export', 'pemesanan'),
(411, 'role_type', 'Menu Pemesanan', 'pemesanan', 'menu'),
(413, 'role_type', 'Add Daftar Pemesanan', 'add', 'daftar_pemesanan'),
(414, 'role_type', 'Delete Daftar Pemesanan', 'delete', 'daftar_pemesanan'),
(415, 'role_type', 'Edit Daftar Pemesanan', 'edit', 'daftar_pemesanan'),
(416, 'role_type', 'Print Daftar Pemesanan', 'print', 'daftar_pemesanan'),
(417, 'role_type', 'Export Daftar Pemesanan', 'export', 'daftar_pemesanan'),
(418, 'role_type', 'Menu Daftar Pemesanan', 'daftar_pemesanan', 'menu'),
(420, 'role_type', 'Add Penjualan', 'add', 'penjualan'),
(421, 'role_type', 'Delete Penjualan', 'delete', 'penjualan'),
(422, 'role_type', 'Edit Penjualan', 'edit', 'penjualan'),
(423, 'role_type', 'Print Penjualan', 'print', 'penjualan'),
(424, 'role_type', 'Export Penjualan', 'export', 'penjualan'),
(425, 'role_type', 'Menu Penjualan', 'penjualan', 'menu'),
(427, 'role_type', 'Add Daftar Penjualan', 'add', 'daftar_penjualan'),
(428, 'role_type', 'Delete Daftar Penjualan', 'delete', 'daftar_penjualan'),
(429, 'role_type', 'Edit Daftar Penjualan', 'edit', 'daftar_penjualan'),
(430, 'role_type', 'Print Daftar Penjualan', 'print', 'daftar_penjualan'),
(431, 'role_type', 'Export Daftar Penjualan', 'export', 'daftar_penjualan'),
(432, 'role_type', 'Menu Daftar Penjualan', 'daftar_penjualan', 'menu'),
(434, 'role_type', 'Add Report Pemesanan', 'add', 'report_pemesanan'),
(435, 'role_type', 'Delete Report Pemesanan', 'delete', 'report_pemesanan'),
(436, 'role_type', 'Edit Report Pemesanan', 'edit', 'report_pemesanan'),
(437, 'role_type', 'Print Report Pemesanan', 'print', 'report_pemesanan'),
(438, 'role_type', 'Export Report Pemesanan', 'export', 'report_pemesanan'),
(439, 'role_type', 'Menu Report Pemesanan', 'report_pemesanan', 'menu'),
(441, 'role_type', 'Add Report Penjualan', 'add', 'report_penjualan'),
(442, 'role_type', 'Delete Report Penjualan', 'delete', 'report_penjualan'),
(443, 'role_type', 'Edit Report Penjualan', 'edit', 'report_penjualan'),
(444, 'role_type', 'Print Report Penjualan', 'print', 'report_penjualan'),
(445, 'role_type', 'Export Report Penjualan', 'export', 'report_penjualan'),
(446, 'role_type', 'Menu Report Penjualan', 'report_penjualan', 'menu'),
(462, 'role_type', 'Add Roti Diskon', 'add', 'roti_diskon'),
(463, 'role_type', 'Delete Roti Diskon', 'delete', 'roti_diskon'),
(464, 'role_type', 'Edit Roti Diskon', 'edit', 'roti_diskon'),
(465, 'role_type', 'Print Roti Diskon', 'print', 'roti_diskon'),
(466, 'role_type', 'Export Roti Diskon', 'export', 'roti_diskon'),
(467, 'role_type', 'Menu Roti Diskon', 'roti_diskon', 'menu'),
(468, 'setting_company', 'company header struk', 'header struk', 'company_header_struk'),
(472, 'setting_company', 'company footer struk', 'footer struk', 'company_footer_struk'),
(473, 'setting_company', 'company notif limit stok', '10', 'company_notif_limit_stok');

-- --------------------------------------------------------

--
-- Table structure for table `tb_terms`
--

CREATE TABLE `tb_terms` (
  `terms_id` bigint(20) NOT NULL,
  `terms_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_terms`
--

INSERT INTO `tb_terms` (`terms_id`, `terms_type`, `category_id`, `post_id`) VALUES
(5, 'nama_bahan', 2, 5),
(6, 'nama_bahan', 3, 5),
(7, 'cabang', 21, 11),
(8, 'cabang', 2, 6),
(9, 'cabang', 3, 1),
(10, 'user_outlet', 1, 1),
(11, 'user_outlet', 2, 6),
(12, 'user_outlet', 3, 11),
(13, 'user_outlet', 4, 6),
(14, 'user_outlet', 4, 1),
(15, 'user_outlet', 3, 1),
(16, 'user_outlet', 2, 1),
(19, 'role', 348, 3),
(20, 'role', 341, 2),
(23, 'user_outlet', 1, 12),
(24, 'item_outlet', 3, 1),
(25, 'item_outlet', 4, 1),
(26, 'item_outlet', 1, 1),
(27, 'item_outlet', 2, 1),
(28, 'item_outlet', 3, 2),
(29, 'item_outlet', 4, 2),
(30, 'item_outlet', 1, 2),
(31, 'item_outlet', 2, 2),
(32, 'item_outlet', 3, 3),
(33, 'item_outlet', 4, 3),
(34, 'item_outlet', 1, 3),
(35, 'item_outlet', 2, 3),
(36, 'item_outlet', 3, 4),
(37, 'item_outlet', 4, 4),
(38, 'item_outlet', 1, 4),
(39, 'item_outlet', 2, 4),
(40, 'item_outlet', 3, 5),
(41, 'item_outlet', 4, 5),
(42, 'item_outlet', 1, 5),
(43, 'item_outlet', 2, 5),
(44, 'item_outlet', 3, 6),
(45, 'item_outlet', 4, 6),
(46, 'item_outlet', 1, 6),
(47, 'item_outlet', 2, 6),
(48, 'item_outlet', 3, 7),
(49, 'item_outlet', 4, 7),
(50, 'item_outlet', 1, 7),
(51, 'item_outlet', 2, 7),
(52, 'item_outlet', 3, 8),
(53, 'item_outlet', 4, 8),
(54, 'item_outlet', 1, 8),
(55, 'item_outlet', 2, 8),
(56, 'item_outlet', 3, 9),
(57, 'item_outlet', 4, 9),
(58, 'item_outlet', 1, 9),
(59, 'item_outlet', 2, 9),
(60, 'item_outlet', 3, 11),
(61, 'item_outlet', 4, 11),
(62, 'item_outlet', 1, 11),
(63, 'item_outlet', 2, 11),
(64, 'item_outlet', 3, 12),
(65, 'item_outlet', 4, 12),
(66, 'item_outlet', 1, 12),
(67, 'item_outlet', 2, 12),
(68, 'item_outlet', 3, 13),
(69, 'item_outlet', 4, 13),
(70, 'item_outlet', 1, 13),
(71, 'item_outlet', 2, 13),
(76, 'item_outlet', 3, 15),
(77, 'item_outlet', 4, 15),
(78, 'item_outlet', 1, 15),
(79, 'item_outlet', 2, 15);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED DEFAULT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `user_created` timestamp NOT NULL,
  `user_display_name` varchar(100) DEFAULT NULL,
  `user_avatar` varchar(100) DEFAULT NULL,
  `user_gender` enum('pria','wanita','lainnya') DEFAULT 'pria',
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_date_birth` date DEFAULT NULL,
  `user_address` text,
  `user_identitas` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `phone`, `active`, `ip_address`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `company`, `user_created`, `user_display_name`, `user_avatar`, `user_gender`, `user_mobile`, `user_date_birth`, `user_address`, `user_identitas`) VALUES
(1, 'admin', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', 'admin@admin.com', 'Admin', 'istrator', '0', 1, '127.0.0.1', '', '', NULL, NULL, NULL, 1268889823, 1475895020, 'ADMIN', '2016-05-25 13:43:17', 'admin', '', 'pria', '', '1991-03-29', NULL, NULL),
(6, 'user 1', '$2y$08$OFq0Mu0CrDO9qFSKRfyp7.HKxu34IxsrMmstx8ZePDlZL/WArtF3O', 'xx.cool@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-05-25 17:51:02', NULL, NULL, 'pria', '89654564500', '0000-00-00', NULL, NULL),
(11, 'user 2', '$2y$08$twN/F44jGtfrtPC3GufEuOIEjfma/445w2fJR6WPgFY.H.hC3.v1i', 'nurza.cool@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-05-25 23:30:24', NULL, NULL, 'pria', '89654564500', '2016-10-05', NULL, NULL),
(12, 'general', '$2y$08$wjR0w6fDZU1.nPT/UEqimOa8gRy7UikP9AuYDWShcsLNsiFM/MPFm', 'didik@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1470403476, NULL, '2016-08-05 13:24:03', NULL, NULL, 'pria', '', '1989-06-13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_users_feeds`
--

CREATE TABLE `tb_users_feeds` (
  `feed_id` bigint(20) NOT NULL,
  `feed_parent` bigint(20) NOT NULL,
  `feed_author` varchar(50) CHARACTER SET utf8 NOT NULL,
  `feed_author_email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `feed_author_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `feed_content` text CHARACTER SET utf8 NOT NULL,
  `feed_status` enum('approved','pending','spam','trash') CHARACTER SET utf8 NOT NULL,
  `feed_type` enum('comments','testimony','contact') CHARACTER SET utf8 NOT NULL,
  `feed_date` datetime NOT NULL,
  `feed_ip` varchar(18) CHARACTER SET utf8 NOT NULL,
  `feed_agent` varchar(255) CHARACTER SET utf8 NOT NULL,
  `feed_user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_users_groups`
--

CREATE TABLE `tb_users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_users_groups`
--

INSERT INTO `tb_users_groups` (`id`, `user_id`, `group_id`) VALUES
(3, 1, 1),
(4, 6, 2),
(11, 11, 3),
(13, 12, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tb_giro`
--
ALTER TABLE `tb_giro`
  ADD PRIMARY KEY (`giro_id`);

--
-- Indexes for table `tb_groups`
--
ALTER TABLE `tb_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_inventory`
--
ALTER TABLE `tb_inventory`
  ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `tb_item_promo`
--
ALTER TABLE `tb_item_promo`
  ADD PRIMARY KEY (`item_promo_id`);

--
-- Indexes for table `tb_kredit`
--
ALTER TABLE `tb_kredit`
  ADD PRIMARY KEY (`kd_id`);

--
-- Indexes for table `tb_login_attempts`
--
ALTER TABLE `tb_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_master_bahan`
--
ALTER TABLE `tb_master_bahan`
  ADD PRIMARY KEY (`master_bahan_id`);

--
-- Indexes for table `tb_master_category`
--
ALTER TABLE `tb_master_category`
  ADD PRIMARY KEY (`master_category_id`);

--
-- Indexes for table `tb_master_item`
--
ALTER TABLE `tb_master_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `tb_master_jenis`
--
ALTER TABLE `tb_master_jenis`
  ADD PRIMARY KEY (`master_jenis_id`);

--
-- Indexes for table `tb_master_outlet`
--
ALTER TABLE `tb_master_outlet`
  ADD PRIMARY KEY (`master_outlet_id`);

--
-- Indexes for table `tb_master_resep`
--
ALTER TABLE `tb_master_resep`
  ADD PRIMARY KEY (`master_resep_id`);

--
-- Indexes for table `tb_master_satuan`
--
ALTER TABLE `tb_master_satuan`
  ADD PRIMARY KEY (`master_satuan_id`);

--
-- Indexes for table `tb_mutasi`
--
ALTER TABLE `tb_mutasi`
  ADD PRIMARY KEY (`mutasi_id`);

--
-- Indexes for table `tb_mutasi_dapur`
--
ALTER TABLE `tb_mutasi_dapur`
  ADD PRIMARY KEY (`mutasi_dapur_id`),
  ADD UNIQUE KEY `tb_mutasi_dapur_mutasi_dapur_id_uindex` (`mutasi_dapur_id`);

--
-- Indexes for table `tb_mutasi_dapur_detail`
--
ALTER TABLE `tb_mutasi_dapur_detail`
  ADD PRIMARY KEY (`mutasi_dapur_detail_id`),
  ADD UNIQUE KEY `tb_mutasi_dapur_detail_mutasi_dapur_detail_id_uindex` (`mutasi_dapur_detail_id`);

--
-- Indexes for table `tb_mutasi_detail`
--
ALTER TABLE `tb_mutasi_detail`
  ADD PRIMARY KEY (`mutasi_detail_id`);

--
-- Indexes for table `tb_notification`
--
ALTER TABLE `tb_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tb_order_dapur`
--
ALTER TABLE `tb_order_dapur`
  ADD PRIMARY KEY (`order_dapur_id`);

--
-- Indexes for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD PRIMARY KEY (`order_detail_id`);

--
-- Indexes for table `tb_pembelian_bahan`
--
ALTER TABLE `tb_pembelian_bahan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_pembelian_bahan_id_uindex` (`id`);

--
-- Indexes for table `tb_pembelian_bahan_detail`
--
ALTER TABLE `tb_pembelian_bahan_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_pembelian_bahan_detail_id_uindex` (`id`);

--
-- Indexes for table `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  ADD PRIMARY KEY (`pesanan_id`);

--
-- Indexes for table `tb_resep`
--
ALTER TABLE `tb_resep`
  ADD PRIMARY KEY (`resep_id`),
  ADD UNIQUE KEY `tb_resep_resep_id_uindex` (`resep_id`);

--
-- Indexes for table `tb_resep_detail`
--
ALTER TABLE `tb_resep_detail`
  ADD PRIMARY KEY (`resep_detail_id`),
  ADD UNIQUE KEY `tb_resep_detail_resep_detail_id_uindex` (`resep_detail_id`);

--
-- Indexes for table `tb_retur`
--
ALTER TABLE `tb_retur`
  ADD PRIMARY KEY (`retur_id`);

--
-- Indexes for table `tb_retur_detail`
--
ALTER TABLE `tb_retur_detail`
  ADD PRIMARY KEY (`retur_detail_id`);

--
-- Indexes for table `tb_sales`
--
ALTER TABLE `tb_sales`
  ADD PRIMARY KEY (`sales_id`);

--
-- Indexes for table `tb_sales_detail`
--
ALTER TABLE `tb_sales_detail`
  ADD PRIMARY KEY (`sales_detail_id`);

--
-- Indexes for table `tb_setting`
--
ALTER TABLE `tb_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `tb_terms`
--
ALTER TABLE `tb_terms`
  ADD PRIMARY KEY (`terms_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_user_username_uindex` (`username`);

--
-- Indexes for table `tb_users_feeds`
--
ALTER TABLE `tb_users_feeds`
  ADD PRIMARY KEY (`feed_id`);

--
-- Indexes for table `tb_users_groups`
--
ALTER TABLE `tb_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `tb_customer`
--
ALTER TABLE `tb_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_giro`
--
ALTER TABLE `tb_giro`
  MODIFY `giro_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_groups`
--
ALTER TABLE `tb_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_inventory`
--
ALTER TABLE `tb_inventory`
  MODIFY `inventory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_item_promo`
--
ALTER TABLE `tb_item_promo`
  MODIFY `item_promo_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_kredit`
--
ALTER TABLE `tb_kredit`
  MODIFY `kd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_login_attempts`
--
ALTER TABLE `tb_login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_master_bahan`
--
ALTER TABLE `tb_master_bahan`
  MODIFY `master_bahan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_master_category`
--
ALTER TABLE `tb_master_category`
  MODIFY `master_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tb_master_item`
--
ALTER TABLE `tb_master_item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_master_jenis`
--
ALTER TABLE `tb_master_jenis`
  MODIFY `master_jenis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_master_outlet`
--
ALTER TABLE `tb_master_outlet`
  MODIFY `master_outlet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_master_resep`
--
ALTER TABLE `tb_master_resep`
  MODIFY `master_resep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_master_satuan`
--
ALTER TABLE `tb_master_satuan`
  MODIFY `master_satuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_mutasi`
--
ALTER TABLE `tb_mutasi`
  MODIFY `mutasi_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_mutasi_dapur`
--
ALTER TABLE `tb_mutasi_dapur`
  MODIFY `mutasi_dapur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_mutasi_dapur_detail`
--
ALTER TABLE `tb_mutasi_dapur_detail`
  MODIFY `mutasi_dapur_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_mutasi_detail`
--
ALTER TABLE `tb_mutasi_detail`
  MODIFY `mutasi_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_notification`
--
ALTER TABLE `tb_notification`
  MODIFY `notification_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_order_dapur`
--
ALTER TABLE `tb_order_dapur`
  MODIFY `order_dapur_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  MODIFY `order_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tb_pembelian_bahan`
--
ALTER TABLE `tb_pembelian_bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_pembelian_bahan_detail`
--
ALTER TABLE `tb_pembelian_bahan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  MODIFY `pesanan_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_resep`
--
ALTER TABLE `tb_resep`
  MODIFY `resep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_resep_detail`
--
ALTER TABLE `tb_resep_detail`
  MODIFY `resep_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_retur`
--
ALTER TABLE `tb_retur`
  MODIFY `retur_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_retur_detail`
--
ALTER TABLE `tb_retur_detail`
  MODIFY `retur_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tb_sales`
--
ALTER TABLE `tb_sales`
  MODIFY `sales_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `tb_sales_detail`
--
ALTER TABLE `tb_sales_detail`
  MODIFY `sales_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `tb_setting`
--
ALTER TABLE `tb_setting`
  MODIFY `setting_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=474;
--
-- AUTO_INCREMENT for table `tb_terms`
--
ALTER TABLE `tb_terms`
  MODIFY `terms_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_users_feeds`
--
ALTER TABLE `tb_users_feeds`
  MODIFY `feed_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_users_groups`
--
ALTER TABLE `tb_users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_users_groups`
--
ALTER TABLE `tb_users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `tb_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
