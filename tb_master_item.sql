-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2016 at 06:25 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2016_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_item`
--

CREATE TABLE `tb_master_item` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_disc` int(11) NOT NULL,
  `item_satuan` int(11) NOT NULL,
  `item_category` int(11) NOT NULL,
  `item_jenis` int(11) NOT NULL,
  `item_harga` int(11) NOT NULL,
  `item_stok` int(11) NOT NULL,
  `item_stok_tidak_laku` int(11) NOT NULL,
  `item_stok_roti_diskon` int(11) NOT NULL,
  `item_harga_outlet` int(11) NOT NULL,
  `item_stok_retur` int(11) NOT NULL,
  `item_disc_percent` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_master_item`
--

INSERT INTO `tb_master_item` (`item_id`, `item_name`, `item_disc`, `item_satuan`, `item_category`, `item_jenis`, `item_harga`, `item_stok`, `item_stok_tidak_laku`, `item_stok_roti_diskon`, `item_harga_outlet`, `item_stok_retur`, `item_disc_percent`, `created`) VALUES
(1, 'KESET MOLEN', 1000, 4, 21, 6, 1000, 0, 0, 0, 0, 0, 0, '2016-10-16 03:48:54'),
(2, 'TUNA ROLL', 0, 4, 21, 9, 2000, 0, 0, 0, 0, 0, 0, '2016-10-09 20:03:16'),
(3, 'ROTI ABON SAPI', 0, 4, 21, 5, 3000, 30, 0, 0, 0, 0, 0, '2016-10-09 20:02:12'),
(4, 'KELAPA PANDAN SRIKAYA', 0, 4, 21, 6, 4000, 100, 0, 0, 0, 0, 0, '2016-10-16 02:49:14'),
(5, 'ONTBIJKOEK', 0, 4, 21, 5, 1500, 0, 0, 0, 0, 0, 0, '2016-10-09 20:01:25'),
(6, 'SANDWICH TUNA', 0, 4, 21, 10, 2500, 0, 0, 0, 0, 0, 0, '2016-10-09 20:03:01'),
(7, 'ROTI SSM DUS', 0, 4, 21, 5, 5000, 0, 0, 0, 0, 0, 0, '2016-10-09 20:02:47'),
(8, 'ONTBIJKOEK PTG', 0, 4, 21, 10, 4000, 0, 0, 0, 0, 0, 0, '2016-10-09 20:01:36'),
(9, 'CAKE BREAD KERING', 0, 4, 21, 10, 3000, 50, 0, 0, 0, 0, 0, '2016-10-16 02:48:57'),
(11, 'RAISIN ROLL', 0, 2, 21, 6, 50000, 0, 0, 0, 0, 0, 0, '2016-10-09 20:01:45'),
(36, 'RETAIL ROTI', 0, 2, 22, 10, 3000, 0, 0, 0, 0, 0, 0, '2016-10-09 20:01:58'),
(37, 'ROTI BARU', 0, 1, 21, 8, 2000, 0, 0, 0, 0, 0, 0, '2016-10-09 20:02:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_master_item`
--
ALTER TABLE `tb_master_item`
  ADD PRIMARY KEY (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_master_item`
--
ALTER TABLE `tb_master_item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
