-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2016 at 03:54 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2016_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_mutasi_dapur`
--

CREATE TABLE `tb_mutasi_dapur` (
  `mutasi_dapur_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `disetujui` enum('YA','TIDAK','DITOLAK') NOT NULL DEFAULT 'TIDAK',
  `keterangan` varchar(200) DEFAULT NULL,
  `diskon_nota` int(11) DEFAULT NULL,
  `biaya_lain` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_mutasi_dapur`
--

INSERT INTO `tb_mutasi_dapur` (`mutasi_dapur_id`, `outlet_id`, `user_id`, `total`, `disetujui`, `keterangan`, `diskon_nota`, `biaya_lain`, `tanggal`, `created`) VALUES
(1, 3, 1, 40000, '', 'ketrangan mutasi dapur', 0, 0, '2016-10-10', '2016-10-16 01:15:16'),
(2, 1, 1, 40000, 'TIDAK', 'tgl 12 expired lho', 0, 0, '2016-10-09', '2016-10-16 01:15:20'),
(3, 2, 1, 60000, 'YA', '', 0, 0, '2016-10-12', '2016-10-16 01:15:22'),
(4, 3, 1, 1600, 'TIDAK', '', 0, 0, '2016-10-14', '2016-10-16 01:15:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_mutasi_dapur`
--
ALTER TABLE `tb_mutasi_dapur`
  ADD PRIMARY KEY (`mutasi_dapur_id`),
  ADD UNIQUE KEY `tb_mutasi_dapur_mutasi_dapur_id_uindex` (`mutasi_dapur_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_mutasi_dapur`
--
ALTER TABLE `tb_mutasi_dapur`
  MODIFY `mutasi_dapur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
