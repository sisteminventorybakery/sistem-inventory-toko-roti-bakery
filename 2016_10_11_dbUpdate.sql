ALTER TABLE `tb_item_promo`
ADD COLUMN `status_promo_id`  int NOT NULL AFTER `item_id`,
ADD COLUMN `item_promo_item_id`  int NOT NULL AFTER `status_promo_id`;

CREATE TABLE `tb_status_promo` (
`status_promo_id`  int NOT NULL AUTO_INCREMENT ,
`status_promo_name`  varchar(255) NOT NULL ,
PRIMARY KEY (`status_promo_id`)
);

INSERT INTO tb_status_promo VALUES('', 'Gratis Roti');
INSERT INTO tb_status_promo VALUES('', 'Gratis Uang');