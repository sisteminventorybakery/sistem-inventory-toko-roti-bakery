<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Form_validation $form_validation
 * @property CI_Session session
 */
class Cms_new_retur_dapur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('item_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_new_retur_dapur', $output);
        $this->load->view('v_footer', $output);
        $this->load->view('v_footer_anan', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','new_retur_dapur') ) show_404();
        $data = $this->base_config->panel_setting();
        $data['outlet'] = $this->_get_outlet();
        $output = new stdClass();
        $output->data = $data;
        $output->js_files = array();
        $output->css_files = array();
        $this->_example_output($output);
    }

    public function get_inventory_dapur()
    {              
        $results = $this->item_model->get_inventory_dapur();
        if( $results ){
            header('Content-Type: application/json');
            echo json_encode(['status'=>1, 'message' => 'Data ditemukan', 'data' => $results]);
            exit();
        }else{
            header('Content-Type: application/json');
            echo json_encode(['status'=>0, 'message' => 'Tidak ada data', 'data' => $results]);
            exit();
        }
    }

    public function get_order()
    {
     $id = $this->uri->segment(4);
     $this->db->join('tb_customer','tb_customer.customer_id=tb_order.customer_id');
     $this->db->join('tb_order_detail', 'tb_order_detail.order_id = tb_order.order_id');
     $this->db->join('tb_master_item', 'tb_master_item.item_id = tb_order_detail.item_id');
     $this->db->where('tb_order.order_id', $id);
     $results = $this->db->get('tb_order')->result_array();
     if( $results ){
         header('Content-Type: application/json');
         echo json_encode(['status'=>1, 'message' => 'Data ditemukan', 'data' => $results]);
         exit();
     }else{
         header('Content-Type: application/json');
         echo json_encode(['status'=>0, 'message' => 'Tidak ada data', 'data' => $results]);
         exit();
     }
 } 

 public function get_retur()
 {
    $id = $this->uri->segment(4);

    $this->db->where('tb_retur.retur_id', $id);
    $this->db->where('tb_retur.retur_type', 'OUTLET');
    $this->db->join('tb_retur_detail', 'tb_retur_detail.retur_id = tb_retur.retur_id');
    $this->db->join('tb_master_item', 'tb_master_item.item_id = tb_retur_detail.item_id');
    $results = $this->db->get('tb_retur')->result();

    if( $results ){
        header('Content-Type: application/json');
        echo json_encode(['status'=>1, 'message' => 'Data ditemukan', 'data' => $results]);
        exit();
    }else{
        header('Content-Type: application/json');
        echo json_encode(['status'=>0, 'message' => 'Tidak ada data', 'data' => $results]);
        exit();
    }
}

public function ajax()
{
    $this->M_base_config->cekaAuth();
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    $this->form_validation->set_rules('sales-id', 'Nota', 'required');

    if ($this->form_validation->run() == FALSE) {
        $data['status'] = 0;
        $data['message'] = validation_errors();
        echo json_encode($data);
        exit();
    } else {
        $keterangan = $this->input->post('retur_note');
        $sales_id   = $this->input->post('sales-id');
        $outlet_id  = $this->input->post('outlet', TRUE);
        $item_id    = $this->input->post('item_id'); //array
        $harga      = $this->input->post('harga'); //array
        $qty_roti   = $this->input->post('qty_roti'); //array
        $qty_uang   = $this->input->post('qty_uang'); //array

        $retur_total = 0;
        if ($qty_roti) {
            foreach ($qty_roti as $key => $item) {
                $retur_total += $harga[$key]*$qty_roti[$key];
            }
        }

        if ($qty_uang) {
            foreach ($qty_uang as $key => $item) {
                $retur_total += $qty_uang[$key]*1;
            }
        }
        
        $data_retur = array(
            'retur_date'        => date('Y-m-d H:i:s'),
            'retur_status'      => 'ya',
            'retur_desc'        => $keterangan,
            'user_id'           => $this->ion_auth->user()->row()->id,
            'outlet_id'         => $outlet_id,
            'retur_total'       => $retur_total,
            'retur_type'        => 'DAPUR',
            );
        
        $this->db->insert('tb_retur', $data_retur);
        $retur_id = $this->db->insert_id();

        $data_retur_detail = array();
        $this->db->join('tb_sales_detail','tb_sales_detail.sales_id=tb_sales.sales_id');
        $results = $this->db->where('tb_sales.sales_id', $sales_id)->get('tb_sales')->result_array();
        if ($qty_roti) {
            foreach ($qty_roti as $key => $item) {
                $data_retur_detail1[$key] = array(
                    'retur_detail_price'    => $harga[$key],
                    'retur_detail_qty'      => $qty_roti[$key],
                    'retur_detail_total'    => $harga[$key]*$qty_roti[$key],
                    'retur_id'              => $retur_id,
                    'item_id'               => $item
                );
            }
        }else{
            $data_retur_detail1 = array();
        }

        if ($qty_uang) {
            foreach ($qty_uang as $key => $item) {
                $data_retur_detail2[$key] = array(
                    'retur_detail_price'    => $qty_uang[$key],
                    'retur_detail_qty'      => 1,
                    'retur_detail_total'    => $qty_uang[$key]*1,
                    'retur_id'              => $retur_id,
                    'item_id'               => NULL
                );
            }
        }else{
            $data_retur_detail2 = array();
        }

        $data_retur_detail = array_merge($data_retur_detail1, $data_retur_detail2);

        $query = $this->db->insert_batch('tb_retur_detail', $data_retur_detail);
        if ($query) {
            echo json_encode(array('status' => 1, 'message' => 'Sukses Disimpan' ));
            exit();
        } else {
            echo json_encode(array('status' => 0, 'message' => 'Proses gagal, silahkan ulangi lagi'));
            exit();
        }
    }

}

protected  function _get_outlet()
{
    $res = $this->db->get('tb_master_outlet')->result();
    $tmp = array();
    foreach ($res as $v){
        $tmp[] = array(
            'name' => $v->master_outlet_name,
            'id' => $v->master_outlet_id
        );
    }
    return $tmp;
}

}