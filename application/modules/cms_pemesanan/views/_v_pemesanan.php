<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-header">
			<h2>Pemesanan</h2>
			<em>...</em>
		</div>
		<div class="main-content">
			<form id="form-order" method="post" onsubmit="return false;">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-10">
						<p>Nama Item</p>
						<select id="item_id" class="select2">
							<?php foreach ( $items as $item):?>
								<option value="<?php echo $item['inventory_id'];?>"
										data-discount="<?php echo $item['item_disc'];?>"
										data-discount-percent="<?php echo $item['item_disc_percent'];?>"
										data-stok="<?php echo $item['inventory_stok'];?>"
										data-price="<?php echo $item['inventory_price_sales'];?>"><?php echo $item['item_name'].' EXP.'.$item['inventory_expired'].' ('.$item['inventory_stok'].')';?>
								</option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2">
						<p>Harga Produk</p>
						<input type="text" class="form-control" id="price_product" value="0" readonly>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-2">
						<p>Jumlah</p>
						<input type="text" class="form-control numeric2" value="1" name="qty" id="qty">
					</div>
					<div class="col-lg-3">
						<p>Disc.Produk Rp</p>
						<input type="text" class="form-control numeric2" value="0" name="discount_product" id="discount_product">
					</div>
					<div class="col-lg-3">
						<p>Disc.Produk %</p>
						<input type="text" class="form-control numeric2" value="0" id="discount_product_percent">
					</div>
					<div class="col-lg-2">
						<p>Sub Total</p>
						<input type="text" class="form-control" value="0" id="order_product_subtotal" readonly>
					</div>
					<div class="col-lg-2">
						<p>-</p>
						<button title="tambahkan ke cart" type="button" id="btn-add-order" class="btn btn-info btn-block"><i class="fa fa-plus-circle fa-2x"></i></button>
					</div>
				</div>
				<br>
				<div class="row">
					<table class="table table-condensed table-dark-header" id="table-order">
						<thead>
						<tr>
							<th>Nama Item</th>
							<th class="text-right">Harga</th>
							<th class="text-center">Qty</th>
							<th class="text-center">Disc</th>
							<th class="text-right">Sub Total</th>
                            <th class="text-center">Pilihan</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-right">
							<table border="0" width="100%" style="font-size: large">
								<tr>
									<td>Total Sebelum Diskon </td>
									<td> : </td>
									<td><span id="total-order-before-discount">0</span></td>
								</tr>
								<tr style="color: blue">
									<td>Total Setelah Diskon </td>
									<td> : </td>
									<td><span id="total-order">0</span></td>
								</tr>
								<tr style="color: red">
									<td>Total Bayar </td>
									<td> : </td>
									<td><span id="total-order-bayar">0</span></td>
								</tr>
							</table>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-lg-4">
				<!-- SELECT2 -->
				<div class="widget">
					<div class="widget-header">
						<h3><i class="fa fa-edit"></i>Customer</h3> <em> - data customer</em>
					</div>
					<div class="widget-content">
                        <div style="margin-top: 10px;" class="input-group date form_datetime">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" id="order_date" name="order_date" class="form-control" placeholder="Tanggal Kirim" required>
                        </div>
						<p style="margin-top: 10px;">Customer | <a href="#" data-toggle="modal" data-target="#modal-customer">New Customer</a></p>
						<select name="customer_id" id="customer_id" class="select2" required>
							<?php foreach ( $customers as $c => $customer ):?>
								<option data-address="<?php echo $customer['customer_address'];?>" value="<?php echo $customer['customer_id'];?>" <?php if( $c == 0 ){echo "selected";}?>><?php echo $customer['customer_name_phone'];?></option>
							<?php endforeach;?>
						</select>
						<textarea style="margin-top: 10px;" class="form-control" id="order_address" name="order_address" rows="2" cols="30" placeholder="Alamat" required>indonesia</textarea>
						<textarea style="margin-top: 10px;" class="form-control" id="order_note" name="order_note" rows="2" cols="30" placeholder="Keterangan"></textarea>
					</div>
				</div>
				<div class="widget">
					<div class="widget-header">
						<h3><i class="fa fa-money"></i>Pembayaran</h3> <em> - data pembayaran</em>
					</div>
					<div class="widget-content">
						<p>Diskon Nota (%)</p>
						<input style="margin-top: 10px;" type="text" class="form-control numeric2" id="order_discount_percent" placeholder="Diskon (%)" value="0">
						<p style="margin-top: 10px;">Diskon Nota (Rp)</p>
						<input style="margin-top: 10px;" type="text" class="form-control numeric2" name="order_discount" id="order_discount" placeholder="Diskon (Rp)" value="0">
						<p style="margin-top: 10px;">Ongkos Kirim (Rp)</p>
						<input style="margin-top: 10px;" type="text" class="form-control numeric2" name="order_ongkir" id="order_ongkir" placeholder="Ongkos Kirim (Rp)" value="0">
						
						<p style="margin-top: 10px;">Jenis dan Status Pembayaran</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <select name="order_type" id="order_type" class="form-control" required>
									<option value="cash" selected>Cash</option>
									<option value="kredit">Kredit</option>
                                    <option value="giro">Giro</option>
                                    <option value="kartu_kredit">Kartu Kredit</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <select name="order_lunas" id="order_lunas" class="form-control" disabled="disabled">
                                    <option value="ya">Lunas</option>
									<option value="tidak">Belum Lunas</option>
                                </select>
                            </div>
                        </div>
                        <input style="margin-top: 10px;" type="text" class="form-control numeric2" name="order_pay" id="order_pay" placeholder="Bayar (Rp)">
						<div class="box-hide" id="box_tanggal_cair" style="display: none;">
							<div style="margin-top: 10px;" class="input-group date form_date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" id="order_tanggal_cair" name="order_tanggal_cair" class="form-control" placeholder="Tanggal Cair">
							</div>
						</div>
						<div class="box-hide" id="box_nomor_giro" style="display: none;">
							<input style="margin-top: 10px;" type="text" class="form-control" name="order_nomor_giro" id="order_nomor_giro" placeholder="Nomor Giro">
						</div>
						<div class="box-hide" id="box_nama_bank" style="display: none;">
							<input style="margin-top: 10px;" type="text" class="form-control" name="order_nama_bank" id="order_nama_bank" placeholder="Nama Bank">
						</div>
						<div class="box-hide" id="box_nama" style="display: none;">
							<input style="margin-top: 10px;" type="text" class="form-control" name="order_nama" id="order_nama" placeholder="Atas Nama">
						</div>
						<div class="box-hide" id="box_nomor_kartu" style="display: none;">
							<input style="margin-top: 10px;" type="text" class="form-control" name="order_nomor_kartu" id="order_nomor_kartu" placeholder="Nomor Kartu">
						</div>
					</div>
				</div>	
				<!-- END SELECT2 -->
				<button type="submit" class="btn btn-success btn-block btn-order-submit" disabled="disabled"><i class="fa fa-check-circle"></i> Simpan</button>
                <br>
                <div class="message"></div>
			</div>
			</form>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->

<!-- Modal -->
<div id="modal-customer" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Customer</h4>
			</div>
			<form id="form-customer" method="post">
			<div class="modal-body">
				<label for="name">Nama</label>
				<input class="form-control" type="text" name="name" id="name" required>
				<label for="phone">Telpon</label>
				<input class="form-control" type="text" name="phone" id="phone">
				<label for="address">Alamat Lengkap</label>
				<textarea class="form-control" rows="3" id="address" name="address"></textarea>
				<br><br>
				<div class="msg"></div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Simpan</button>
			</div>
			</form>
		</div>

	</div>
</div>

<script type="text/javascript">
	var page = "pemesanan";
</script>