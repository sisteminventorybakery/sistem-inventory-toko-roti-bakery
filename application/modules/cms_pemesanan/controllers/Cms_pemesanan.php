<?php

/**
 * @property M_base_config M_base_config
 * @property base_config base_config
 * @property Ion_auth|Ion_auth_model ion_auth
 * @property CI_Lang lang
 * @property CI_URI uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver db
 * @property CI_Config config
 * @property CI_Input input
 * @property CI_User_agent $agent
 * @property CI_Output output
 * @property CI_Session session
 * @property CI_Form_validation form_validation
 * @property Item_model Item_model
 * @property Stok_model Stok_model
 */
class Cms_pemesanan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Item_model');
        $this->load->model('Stok_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function _example_output2($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_pemesanan', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        if( $this->uri->segment(4) == 'add' ) {
            $this->add();
        }else{
            try {
                $crud = $this->base_config->groups_access('pemesanan');
                $crud->set_theme('twitter-bootstrap');
                $crud->set_table('tb_order');
                $crud->set_subject('Pemesanan');
                $crud->order_by('order_date','desc');
                $crud->where('outlet_id', $this->session->userdata('outlet_id'));
                $crud->columns('user_id','customer_id','order_date','order_kirim','order_address','order_discount_nota','order_total','order_ongkir','order_lunas','created');
                $crud->edit_fields('order_lunas','order_pay');
                $crud->set_relation('customer_id', 'tb_customer', 'customer_name');
                $crud->unset_texteditor('order_note');
                $crud->display_as('customer_id','Pemesan');
                $crud->display_as('order_date','Tgl Pesan');
                $crud->display_as('order_kirim','Tgl Kirim');
                $crud->display_as('order_address','Alamat');
                $crud->display_as('order_discount_nota', 'Diskon Nota');
                $crud->display_as('order_total','Total');
                $crud->display_as('order_ongkir','Ongkir');
                $crud->display_as('order_pay','Bayar');
                $crud->display_as('order_lunas','Lunas');
                $crud->display_as('created','informasi pembayaran cicilan');
                $crud->display_as('user_id','Nota');
                $crud->display_as('preview','informasi pembayaran cicilan');
                $crud->callback_column('created', array($this,'_callback_detail'));
                $crud->callback_column('user_id', array($this,'_callback_nota'));
                $crud->callback_column('order_pay', array($this,'_callback_sparator'));
                $crud->callback_column('order_total', array($this,'_callback_sparator'));
                $crud->callback_column('order_ongkir', array($this,'_callback_sparator'));
                $crud->callback_column('order_discount_nota', array($this,'_callback_sparator'));
                $crud->unset_print();
                $crud->unset_edit();
                $state = $crud->getState();
                if( $state == 'export' ){
                    $crud->columns('user_id','customer_id','order_date','order_kirim','order_address','order_discount_nota','order_total','order_ongkir','order_type','order_lunas');
                }else{
                    $crud->callback_column('order_lunas', array($this,'_callback_lunas'));
                    $crud->columns('user_id','customer_id','order_date','order_kirim','order_address','order_discount_nota','order_total','order_ongkir','order_lunas','created');
                }
                $crud->set_bulkactionfalse(true);
                $output = $crud->render();
                $output->data = $data;
                $this->_example_output($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        }
    }

    public function _callback_order_date($value, $row)
    {
        $tgl_pesan = $value;
        $tgl_kirim = $row->order_kirim;
        return "$tgl_pesan<hr style='margin: 10px 0 10px 0'>$tgl_kirim";
    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }

    public function _callback_sparator($value)
    {
        return number_format($value);
    }

    public function _callback_lunas($value, $row)
    {
        $type = $row->order_type;
        if (strtolower($row->order_lunas) == 'ya')
            $active = '<span class="label label-success">' . 'LUNAS' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'TIDAK' . '</span>';

        return "$type<br>$active";
    }

    public function _callback_detail($value, $row)
    {
        $id = $row->order_id;
        $btn = "<a href='#' data-id='$id' data-modul='order' class='btn-preview btn btn-info'><i class='fa fa-eye'></i></a>";
        $pdf = '<a class="btn btn-sm btn-warning" target="_blank" title="Detail" href="'.base_url('cms/pemesanan/export/pdf/'. $row->order_id .'').'"><i class="fa fa-file-pdf-o"></i></a>';
        return '<div class="btn-group btn-group-sm">'.$btn.$pdf.'</div>';
    }

    public function _callback_nota($value, $row)
    {
        return 'OD-'.sprintf("%04s", $row->order_id);
    }

    public function cicilan()
    {
        $this->M_base_config->cekaAuth();
        $id = $this->uri->segment(4);
        if( !$id ) show_404();
        $this->db->join('tb_order','tb_order.order_id=tb_cicilan.order_id');
        $this->db->where('tb_order.order_id', $id);
        $data = $this->db->get('tb_cicilan')->result_array();
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    public function get()
    {
        $this->M_base_config->cekaAuth();
        $id = $this->uri->segment(4);
        if( !$id ) show_404();
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_order_detail.item_id');
        $this->db->join('tb_order','tb_order.order_id=tb_order_detail.order_id');
        $this->db->where('tb_order_detail.order_id', $id);
        $data = $this->db->get('tb_order_detail')->result_array();
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    public function export()
    {
        $this->M_base_config->cekaAuth();
        $export = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $orders = array();
        $result = array();
        $total = 0;
        $total_qty = 0;
        $total_price = 0;
        $total_price_after_disc = 0;
        $total_disc = 0;
        $this->db->join('tb_order','tb_order.order_id=tb_order_detail.order_id');
        $this->db->join('tb_customer','tb_customer.customer_id=tb_order.customer_id');
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_order.outlet_id');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_order_detail.item_id');
        $this->db->join('tb_user','tb_user.id=tb_order.user_id');
        $this->db->where('tb_order_detail.order_id', $id);
        $result_row = $this->db->get('tb_order_detail');
        if( !$result_row->num_rows() ) show_404();
        foreach ($result_row->result() as $k => $v){
            $price      = (int)$v->order_detail_price;
            $qty        = (int)$v->order_detail_qty;
            $disc       = (int)$v->order_detail_disc;
            $subtotal   = (int)$v->order_detail_total;
            $price_after_disc = $price - $disc;
            $result[]   = array(
                'no'                => $k+1,
                'item'              => $v->item_name,
                'disc'              => number_format($disc),
                'price'             => number_format($price),
                'price_after_disc'  => number_format($price_after_disc),
                'qty'               => number_format($qty),
                'subtotal'          => number_format($subtotal),
            );

            $total_qty += $qty;
            $total_price += $price;
            $total_price_after_disc += $price_after_disc;
            $total_disc += $disc;

            $total = (int)$v->order_total;
            $orders = array(
                'id'        => $v->order_id,
                'customer'  => $v->customer_name,
                'nota'      => 'OD-'.sprintf("%04s", $v->order_id),
                'disc'      => number_format($v->order_discount_nota),
                'total'     => $total,
                'lunas'     => $v->order_lunas,
                'outlet'    => $v->master_outlet_name,
                'date'      => $v->order_date,
                'date_kirim'=> $v->order_kirim,
                'address'   => $v->order_address,
                'payment'   => $v->order_type,
                'telepon'   => $v->customer_phone,
                'pay'       => number_format( (int)$v->order_pay),
                'note'      => $v->order_note,
                'user'      => $v->username,
				'ongkir'	=> number_format( (int)$v->order_ongkir)
            );
        }
        $data['result']                 = $result;
        $data['total']                  = number_format($total);
        $data['total_qty']              = number_format($total_qty);
        $data['total_price']            = number_format($total_price);
        $data['total_disc']             = number_format($total_disc);
        $data['total_price_after_disc'] = number_format($total_price_after_disc);
        $data['orders']                 = $orders;
        $data['title']                  = 'LAPORAN PEMESANAN';
        $file_name = $data['title'].' '.date('d/m/Y');
        switch ( $export ){
            case 'pdf':
                $this->load->view( '_v_report_pemesanan', $data );
                $html = $this->output->get_output();
                $dompdf = new \Dompdf\Dompdf();
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4', 'potrait');
                $dompdf->render();
                $dompdf->stream("$file_name.pdf",array('Attachment'=>0));
                exit();
                break;
            case 'excel':
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename = $file_name.xls");
                $this->load->view( '_v_report_pemesanan', $data );
                exit();
                break;
        }
    }

    public function add()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','pemesanan') ) show_404();
        $data = $this->base_config->panel_setting();
        $output = new stdClass();
        $output->my_outlets = $this->M_base_config->get_my_outlet();
        $output->categories = $this->db->get('tb_master_category')->result();
        $output->items = $this->Item_model->get_inventory_item($this->session->userdata('outlet_id'));
        $output->jenis = $this->db->get('tb_master_jenis')->result();
        $output->customers = $this->M_base_config->get_customer();
        $output->data = $data;
        $output->js_files = array();
        $output->css_files = array();
        $this->_example_output2($output);
    }

    public function ajax()
    {
        if (!$this->ion_auth->logged_in()) {
            echo json_encode(array('status' => 0, 'message' => 'Harus login untuk mengakses halaman ini'));
            exit();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('order_date', 'Tanggal Pemesanan', 'required');
        $this->form_validation->set_rules('customer_id', 'ID Pelanggan', 'required');
        $this->form_validation->set_rules('order_address', 'Alamat', 'required');
        $this->form_validation->set_rules('order_type', 'Jenis Pembayaran', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = 0;
            $data['message'] = validation_errors();
            echo json_encode($data);
            exit();
        } else {
            $customer_id = $this->input->post('customer_id');
            $order_type = $this->input->post('order_type');
            $order_lunas = 'tidak';
            $qty_s = $this->input->post('qty');
            $item_s = $this->input->post('item_id');
            $discount_s = $this->input->post('discount');
            $discount_nota = $this->number($this->input->post('order_discount'));
            $ongkir = $this->number($this->input->post('order_ongkir'));
            $bayar = $this->number($this->input->post('order_pay'));
            $order_nama                = $this->input->post('order_nama');
            $order_nama_bank           = $this->input->post('order_nama_bank');
            $order_nomor_giro          = $this->input->post('order_nomor_giro');
            $order_nomor_kartu         = $this->input->post('order_nomor_kartu');
            $order_tanggal_cair        = $this->input->post('order_tanggal_cair');
            $all_total_after_discount = 0;
            $all_total_before_discount = 0;
            $discount_product_total = 0;
            $discount_nota_total = 0;

            foreach ($item_s as $key => $inv_id) {
                $discount_product = $this->number($discount_s[$key]);
                $this->db->join('tb_master_item','tb_master_item.item_id=tb_inventory.item_id');
                $this->db->where('outlet_id', $this->outlet_id());
                $this->db->where('inventory_id', $inv_id);
                $result = $this->db->get('tb_inventory',1);
                $item_id = $result->row('item_id');
                $price = $this->Item_model->get_price( $this->outlet_id(), $item_id )['sales'];
                $total_after_discount = ( $price - $discount_product ) * (int)$qty_s[$key];
                $total_before_discount = $price * (int)$qty_s[$key];
                $all_total_after_discount += $total_after_discount;
                $all_total_before_discount += $total_before_discount;
                $discount_product_total += $discount_product;
            }

            if( $discount_nota > 0 ) $discount_nota_total += $discount_nota;
            $discount_total = $discount_nota_total + $discount_product_total;
            $all_total_after_discount = $all_total_before_discount - $discount_total;

            switch ( $order_type ){
                case 'kartu_kredit':
                    $order_lunas = 'ya';
                    $order_nomor_giro='';
                    break;
                case 'cash':
                    $order_lunas = 'ya';
                    $order_nama='';
                    $order_nama_bank='';
                    $order_nomor_giro='';
                    $order_nomor_kartu='';
                    break;
                case 'giro':
                    $order_lunas = 'tidak';
                    $order_nama='';
                    $order_nomor_kartu='';
                    break;
                case 'kredit':
                    $order_lunas = 'tidak';
                    $order_nama='';
                    $order_nama_bank='';
                    $order_nomor_giro='';
                    $order_nomor_kartu='';
                    break;
            }

            $data_order = array(
                'order_nama'                    => $order_nama,
                'order_nama_bank'               => $order_nama_bank,
                'order_nomor_giro'              => $order_nomor_giro,
                'order_nomor_kartu'             => $order_nomor_kartu,
                'order_discount_nota'           => $discount_nota_total,
                'order_discount_product'        => $discount_product_total,
                'order_discount_total'          => $discount_total,
                'order_total_before_discount'   => $all_total_before_discount,
                'order_date'                    => date('Y-m-d H:i:s'),
                'order_kirim'                   => $this->input->post('order_date'),
                'order_lunas'                   => $order_lunas,
                'order_note'                    => $this->input->post('order_note'),
                'customer_id'                   => $customer_id,
                'user_id'                       => $this->ion_auth->user()->row()->id,
                'outlet_id'                     => $this->session->userdata('outlet_id'),
                'order_address'                 => $this->input->post('order_address'),
                'order_total'                   => $all_total_after_discount + $ongkir,
                'order_type'                    => $order_type,
                'order_pay'                     => $bayar,
                'order_ongkir'                  => $ongkir
            );
            $this->db->insert('tb_order', $data_order);
            $order_id = $this->db->insert_id();

            if ( $order_type == 'giro' ){
                $data_giro = [
                    'order_id' => $order_id,
                    'nomor' => $order_nomor_giro,
                    'nama_bank' => $order_nama_bank,
                    'tanggal' => date('Y-m-d'),
                    'jatuh_tempo' => $order_tanggal_cair,
                    'jumlah_hutang' => $all_total_after_discount+$ongkir,
                    'customer_id' => $customer_id,
                ];
                $this->db->insert('tb_giro', $data_giro);
            }

            if( $order_type == 'kredit' ){
                $data_kredit = [
                    'jenis' => 'pemesanan',
                    'order_id' => $order_id,
                    'dp' => $bayar,
                    'jumlah_hutang' => $all_total_after_discount+$ongkir,
                    'tanggal' => date('Y-m-d'),
                    'jatuh_tempo' => $this->input->post('order_date'),
                    'customer_id' => $customer_id,
                ];
                $this->db->insert('tb_kredit', $data_kredit);
            }

            $data_order_detail = array();
            foreach ($item_s as $key => $inv_id) {
                $discount_product = $this->number($discount_s[$key]);
                $this->db->join('tb_master_item','tb_master_item.item_id=tb_inventory.item_id');
                $this->db->where('outlet_id', $this->outlet_id());
                $this->db->where('inventory_id', $inv_id);
                $result = $this->db->get('tb_inventory',1);
                $item_id = $result->row('item_id');
                $expired = $result->row('inventory_expired');
                $price = $this->Item_model->get_price( $this->outlet_id(), $item_id )['sales'];
                $total = ($price - $discount_product) * (int)$qty_s[$key];
                $total_before_discount = $price * (int)$qty_s[$key];
                $data_order_detail[] = array(
                    'order_detail_price'                    => $price,
                    'order_detail_qty'                      => $qty_s[$key],
                    'order_detail_total'                    => $total,
                    'order_detail_total_before_discount'    => $total_before_discount,
                    'order_detail_disc'                     => $discount_product,
                    'order_id'                              => $order_id,
                    'item_id'                               => $item_id,
                    'expired'                               => $expired
                );
                $this->Stok_model->remove_inventory_outlet($this->outlet_id(), $item_id, $expired, $qty_s[$key]);
            }
            $query = $this->db->insert_batch('tb_order_detail', $data_order_detail);
            if ($query) {
                echo json_encode(array('status' => 1, 'message' => 'Sukses Disimpan' ));
                exit();
            } else {
                echo json_encode(array('status' => 0, 'message' => 'Proses gagal, silahkan ulangi lagi'));
                exit();
            }
        }

    }

    protected function number($string)
    {
        return intval(preg_replace('/[^\d.]/', '', $string ));
    }

}