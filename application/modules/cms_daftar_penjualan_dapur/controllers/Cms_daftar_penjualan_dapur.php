<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Output $output
 * @property CI_Session session
 */
class Cms_daftar_penjualan_dapur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        $user = $this->ion_auth->user()->row();
        try {
            $crud = $this->base_config->groups_access('daftar_penjualan_dapur');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_mutasi_dapur');
            $crud->set_subject('Daftar Penjualan Dapur');
            $crud->order_by('created','desc');
            $crud->columns('user_id','mutasi_dapur_id','tanggal','outlet_id','total','disetujui','biaya_lain','keterangan','diskon_nota','created');
            $crud->set_relation('outlet_id', 'tb_master_outlet', 'master_outlet_name');
            $crud->display_as('user_id', 'ID');
            $crud->callback_column('created', array($this,'_callback_detail'));
            $crud->callback_column('mutasi_dapur_id', array($this,'_callback_nota'));
            $crud->callback_column('user_id', array($this,'_callback_id'));
            $crud->callback_column('total', array($this,'_callback_sparator'));
            $crud->callback_column('biaya_lain', array($this,'_callback_sparator'));
            $crud->callback_column('diskon_nota', array($this,'_callback_sparator'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_export();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_sparator($v)
    {
        return number_format($v);
    }

    public function _callback_detail($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail Pemesanan" href="'.base_url('cms/daftar_penjualan_dapur/export/pdf/'. $row->user_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    public function _callback_id($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return $row->mutasi_dapur_id;
    }

    public function _callback_nota($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return 'PJD-'.sprintf("%04s", $row->mutasi_dapur_id);
    }

    public function export()
    {
        $this->M_base_config->cekaAuth();
        $export = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $sales = array();
        $result = array();
        $total = 0;
        $total_qty = 0;
        $total_price = 0;
        $total_price_after_disc = 0;
        $total_disc = 0;
        $this->db->join('tb_mutasi_dapur','tb_mutasi_dapur.mutasi_dapur_id=tb_mutasi_dapur_detail.mutasi_dapur_id');
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_mutasi_dapur.outlet_id');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_mutasi_dapur_detail.item_id');
        $this->db->join('tb_user','tb_user.id=tb_mutasi_dapur.user_id');
        $this->db->where('tb_mutasi_dapur_detail.mutasi_dapur_id', $id);
        $result_row = $this->db->get('tb_mutasi_dapur_detail')->result();
        foreach ($result_row as $k => $v){
            $price      = (int)$v->harga;
            $qty        = (int)$v->jumlah;
            $disc       = (int)$v->diskon;
            $price_after_disc = $price - ($price * ($disc/100));
            $subtotal   = (int)$v->subtotal;
            $result[]   = array(
                'no'                => $k+1,
                'item'              => $v->item_name,
                'disc'              => number_format($disc),
                'price'             => number_format($price),
                'price_after_disc'  => number_format($price_after_disc),
                'qty'               => number_format($qty),
                'subtotal'          => number_format($subtotal),
            );

            $total_qty += $qty;
            $total_price += $price;
            $total_price_after_disc += $price_after_disc;
            $total_disc += $disc;

            $total = (int)$v->total;
            $sales = array(
                'id'        => $v->mutasi_dapur_id,
                'nota'      => 'PJD-'.sprintf("%04s", $v->mutasi_dapur_id),
                'disc'      => $v->diskon_nota,
                'total'     => $total,
                'outlet'    => $v->master_outlet_name,
                'date'      => $v->expired,
                'user'      => $v->username
            );
        }
        $data['result']                 = $result;
        $data['total']                  = number_format($total);
        $data['total_qty']              = number_format($total_qty);
        $data['total_price']            = number_format($total_price);
        $data['total_disc']             = number_format($total_disc);
        $data['total_price_after_disc'] = number_format($total_price_after_disc);
        $data['sales']                  = $sales;

        //$this->M_base_config->tes( $result_row );
        $data['title'] = 'LAPORAN PENJUALAN DAPUR';
        $date = date('d-m-Y');
        switch ( $export ){
            case 'pdf':
                $this->load->view( 'v_report_template', $data );
                $html = $this->output->get_output();
                $dompdf = new \Dompdf\Dompdf();
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4', 'potrait');
                $dompdf->render();
                $dompdf->stream("report-$date.pdf",array('Attachment'=>0));
                exit();
                break;
            case 'excel':
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename = report-$date.xls");
                $this->load->view( 'v_report_template', $data );
                //exit();
                break;
        }
    }

}