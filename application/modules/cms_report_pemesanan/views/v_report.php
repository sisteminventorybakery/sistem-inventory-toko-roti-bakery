<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-content">

				<div class="row" style="margin-top: 15px;">
					<form data-url="<?php echo base_url('cms/report_pemesanan');?>" id="form-report-order" method="get">
					<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="outlet">
							<option value="">Outlet</option>
							<?php foreach ($outlet as $v):?>
							<option value="<?php echo $v['id'];?>" <?php $input = $this->input->get('outlet'); if($input && $v['id']==$input) echo 'selected';?>><?php echo $v['name'];?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="lunas">
							<option value="">Status Lunas</option>
							<option value="ya" <?php $input = $this->input->get('lunas'); if($input && 'ya'==$input) echo 'selected';?>>Ya</option>
							<option value="tidak" <?php $input = $this->input->get('lunas'); if($input && 'tidak'==$input) echo 'selected';?>>Tidak</option>
						</select>
					</div>
					<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="type">
							<option value="">Tipe Pembyrn</option>
							<option value="cash" <?php $input = $this->input->get('type'); if($input && 'cash'==$input) echo 'selected';?>>Cash</option>
							<option value="kredit" <?php $input = $this->input->get('type'); if($input && 'kredit'==$input) echo 'selected';?>>Kredit</option>
						</select>
					</div>
					<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="user">
							<option value="">User</option>
							<?php foreach ($user as $v):?>
								<option value="<?php echo $v['id'];?>" <?php $input = $this->input->get('user'); if($input && $v['id']==$input) echo 'selected';?>><?php echo $v['name'];?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
						<div class="input-group date form_date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" name="from" value="<?php echo $this->input->get('from');?>" class="form-control" placeholder="Tgl Kirim dari">
						</div>
					</div>
					<div class="col-lg-2" style="width: 14%;padding-left: 1px;padding-right: 1px;">
						<div class="input-group date form_date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" name="to" value="<?php echo $this->input->get('to');?>" class="form-control" placeholder="Tgl Kirim ke">
						</div>
					</div>
					<div class="col-lg-2" style="width: 7%;padding-left: 1px;padding-right: 1px;">
						<button type="submit" class="btn btn-info btn-block"><i class="fa fa-search"></i></button>
					</div>
					<div class="col-lg-2" style="width: 7%;padding-left: 1px;padding-right: 1px;">
						<button type="button" id="export-pdf" class="btn btn-warning btn-block"><i class="fa fa-file-pdf-o"></i></button>
					</div>
					</form>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<table class="table table-condensed table-dark-header">
						<thead>
						<tr>
							<th>Nota</th>
							<th>Pemesan</th>
							<th>Tgl Pesanan</th>
							<th>Tgl Kirim</th>
							<th>Alamat</th>
							<th>Telepon</th>
							<th>Catatan</th>
							<th>Diskon</th>
							<th>Total</th>
							<th>Ongkir</th>
							<th>Bayar</th>
							<th>Lunas</th>
							<th>Tipe Pemb.</th>
							<th>Detail</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($result as $v):?>
						<tr>
							<td><?php echo $v['nota'];?></td>
							<td><?php echo $v['customer'];?></td>
							<td><?php echo $v['date'];?></td>
							<td><?php echo $v['date_kirim'];?></td>
							<td><?php echo $v['address'];?></td>
							<td><?php echo $v['telepon'];?></td>
							<td><?php echo $v['note'];?></td>
							<td><?php echo $v['disc'];?></td>
							<td><?php echo $v['total'];?></td>
							<td><?php echo $v['ongkir'];?></td>
							<td><?php echo $v['pay'];?></td>
							<td><?php echo $v['lunas'];?></td>
							<td><?php echo $v['payment'];?></td>
							<td><?php echo $v['detail'];?></td>
						</tr>
						<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
