<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title;?></title>
    <style type="text/css">

        table{
            font-size: 10pt;
            width: 100%;
        }

        table.border {
            border-collapse: collapse;
            width: 100%;
        }

        table.border, table.border th, table.border td {
            border: 1px solid black;
        }

        table.border th, table.border td{
            padding: 5px;
        }

        table.border td{
            vertical-align: middle;
        }

    </style>
</head>
<body>
<div id="">
    <div id="">
        <div class="">
            <div class="">
<p style="text-align: center;"><strong><?php echo $title;?></strong></p>
                <p>&nbsp;</p>
                <table border="0" width="50%">
                    <tbody>
                    <tr>
                        <td>Outlet</td>
                        <td>:</td>
                        <td><?php echo $orders['outlet'];?></td>
                    </tr>
                    <tr>
                        <td>Lunas</td>
                        <td>:</td>
                        <td><?php echo $orders['lunas'];?></td>
                    </tr>
                    <tr>
                        <td>Tipe Pembayaran</td>
                        <td>:</td>
                        <td><?php echo $orders['tipe'];?></td>
                    </tr>
                    <tr>
                        <td>Kasir</td>
                        <td>:</td>
                        <td><?php echo $orders['user'];?></td>
                    </tr>
                    <tr>
                        <td>Tgl Kirim Dari</td>
                        <td>:</td>
                        <td><?php echo $orders['from'];?></td>
                    </tr>
                    <tr>
                        <td>Tgl Kirim Sampai</td>
                        <td>:</td>
                        <td><?php echo $orders['to'];?></td>
                    </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                <?php /** @var array $result */
                if( count( $result ) > 0 ):?>
                    <table class="border" width="100%">
                        <thead>
                        <tr>
                            <th>Nota</th>
                            <th>Pemesan</th>
                            <th>Tgl Pesanan</th>
                            <th>Tgl Kirim</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Catatan</th>
                            <th align="centre">Lunas</th>
                            <th align="centre">Tipe Pemb.</th>
                            <th align="centre">Bayar</th>
                            <th align="centre">Diskon</th>
                            <th align="centre">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result as $v):?>
                            <tr>
                                <td><?php echo $v['nota'];?></td>
                                <td><?php echo $v['customer'];?></td>
                                <td><?php echo $v['date'];?></td>
                                <td><?php echo $v['date_kirim'];?></td>
                                <td><?php echo $v['address'];?></td>
                                <td><?php echo $v['telepon'];?></td>
                                <td><?php echo $v['note'];?></td>
                                <td align="centre"><?php echo $v['lunas'];?></td>
                                <td align="centre"><?php echo $v['payment'];?></td>
                                <td align="right"><?php echo $v['pay'];?></td>
                                <td align="right"><?php echo $v['disc'];?></td>
                                <td align="right"><?php echo $v['total'];?></td>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                           <th align="centre" colspan="9">Total</th>
                            <th align="right"><?php echo $total_pay;?></th>
                            <th align="right"><?php echo $total_disc;?></th>
                            <th align="right"><?php echo $total;?></th>
                        </tr>
                        </tbody>
                    </table>
                <?php else:?>
                    <h3 align="center">No Data</h3>
                <?php endif;?>
            </div>
        </div>

    </div>
</div>
</body>
</html>
