<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_report_pemesanan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output['theme_path'] = $path;
        $output['js_files'] = array();
        $output['css_files'] = array();
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_report', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','report_pemesanan') ) show_404();
        $data = $this->base_config->panel_setting();
        $user = $this->ion_auth->user()->row();
        $f_outlet = $this->input->get('outlet');
        $f_lunas = $this->input->get('lunas');
        $f_type = $this->input->get('type');
        $f_user = $this->input->get('user');
        $f_from = $this->input->get('from');
        $f_to = $this->input->get('to');

        $result = array();
        $orders = array();
        $all_total = 0;
        $all_total_pay = 0;
        $all_total_disc = 0;
        $filter_outlet = 'Kosong';
        $filter_user = 'Kosong';
        $this->db->join('tb_customer','tb_customer.customer_id=tb_order.customer_id');
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_order.outlet_id');
        $this->db->join('tb_user','tb_user.id=tb_order.user_id');
        if( $f_lunas ) $this->db->where('order_lunas', $f_lunas);
        if( $f_type ) $this->db->where('order_type', $f_type);
        if( $f_user ) $this->db->where('user_id', $f_user);
        if( $f_outlet ) $this->db->where('outlet_id', $f_outlet);
        if( $f_from ) $this->db->where('order_kirim >=', $f_from);
        if( $f_to ) $this->db->where('order_kirim <=', $f_to);
        $this->db->order_by('order_date','desc');
        $result_row = $this->db->get('tb_order')->result();
        //$this->M_base_config->tes($result_row);
        foreach ($result_row as $v){
            $total = (int)$v->order_total;
            $result[] = array(
                'id'        => $v->order_id,
                'customer'  => $v->customer_name,
                'nota'      => $this->_callback_nota($v),
                'disc'      => $v->order_discount_nota,
                'total'     => number_format($total),
                'lunas'     => $this->_callback_lunas($v),
                'outlet'    => $v->master_outlet_name,
                'date'      => $v->order_date,
                'date_kirim'=> $v->order_kirim,
                'address'   => $v->order_address,
                'payment'   => $v->order_type,
                'telepon'   => $v->customer_phone,
                'pay'       => number_format( (int)$v->order_pay),
                'note'      => $v->order_note,
                'detail'    => $this->_callback_detail($v),
				'ongkir'	=> $v->order_ongkir
            );
            $all_total += $total;
            $all_total_pay += (int)$v->order_pay;
            $all_total_disc += (int)$v->order_discount_nota;
            $filter_user = $v->username;
            $filter_outlet = $v->master_outlet_name;
        }
        $orders = array(
            'outlet' => ( $f_outlet ? $filter_outlet : 'Semua'),
            'lunas' => ( $f_lunas ? $f_lunas : 'Semua'),
            'tipe' => ( $f_type ? $f_type : 'Semua'),
            'user' => ( $f_user ? $filter_user : 'Semua'),
            'from' => ( $f_from ? $f_from : 'Semua'),
            'to' => ( $f_to ? $f_to : 'Semua'),
        );

        $data['orders'] = $orders;
        $data['total'] = number_format($all_total);
        $data['total_pay'] = number_format($all_total_pay);
        $data['total_disc'] = number_format($all_total_disc);
        $data['title'] = 'LAPORAN PEMESANAN';
        $data['result'] = $result;
        $data['user'] = $this->_get_user();
        $data['outlet'] = $this->_get_outlet();
        $export = $this->input->get('export');
        if($export){
            switch ( $export ){
                case 'pdf':
                    $this->load->view( 'v_report_template', $data );
                    $html = $this->output->get_output();
                    $dompdf = new \Dompdf\Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4','landscape');
                    $dompdf->render();
                    $dompdf->stream("report.pdf",array('Attachment'=>0));
                    exit();
                    break;
                case 'excel':
                    header("Content-Type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename = report.xls");
                    $this->load->view( 'v_report_template', $data );
                    //exit();
                    break;
            }
        }else{
            $this->_example_output($data);
        }

    }

    protected  function _get_outlet()
    {
        $res = $this->db->get('tb_master_outlet')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->master_outlet_name,
                'id' => $v->master_outlet_id
            );
        }
        return $tmp;
    }

    protected  function _get_user()
    {
        $res = $this->db->get('tb_user')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->username,
                'id' => $v->id
            );
        }
        return $tmp;
    }

    protected function _callback_lunas($row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->order_lunas == 'ya')
            $active = '<span class="label label-success">' . 'Ya' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'Tidak' . '</span>';

        return $active;
    }

    protected function _callback_detail($row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail Pemesanan" href="'.base_url('cms/pemesanan/export/pdf/'. $row->order_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    protected function _callback_nota($row)
    {
        $this->M_base_config->cekaAuth();
        return 'OD-'.sprintf("%04s", $row->order_id);
    }
}