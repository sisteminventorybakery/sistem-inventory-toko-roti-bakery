<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title;?></title>
    <style type="text/css">

        table{
            font-size: 10pt;
            width: 100%;
        }

        table.border {
            border-collapse: collapse;
            width: 100%;
        }

        table.border, table.border th, table.border td {
            border: 1px solid black;
        }

        table.border th, table.border td{
            padding: 5px;
        }

        table.border td{
            vertical-align: middle;
        }

    </style>
</head>
<body>
<div id="">
    <div id="">
        <div class="">
            <div class="">
<p style="text-align: center;"><strong><?php echo $title;?></strong></p>
                <p>&nbsp;</p>
                <table border="0" width="50%">
                    <tbody>
                    <tr>
                        <td>Nota</td>
                        <td>:</td>
                        <td><?php echo $sales['nota'];?></td>
                    </tr>
                    <tr>
                        <td>Tgl Dari</td>
                        <td>:</td>
                        <td><?php echo $sales['from'];?></td>
                    </tr>
                    <tr>
                        <td>Tgl Sampai</td>
                        <td>:</td>
                        <td><?php echo $sales['to'];?></td>
                    </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                <?php /** @var array $result */
                if( count( $result ) > 0 ):?>
                    <table class="border" width="100%">
                        <thead>
                        <tr>
                            <th>Nota</th>
                            <th>Tgl Pembelian</th>
                            <th align="centre">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result as $v):?>
                            <tr>
                                <td><?php echo $v['nota'];?></td>
                                <td><?php echo $v['date'];?></td>
                                <td align="right"><?php echo $v['total'];?></td>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                           <th align="centre" colspan="2">Total</th>
                            <th align="right"><?php echo $total;?></th>
                        </tr>
                        </tbody>
                    </table>
                <?php else:?>
                    <h3 align="center">No Data</h3>
                <?php endif;?>
            </div>
        </div>

    </div>
</div>
</body>
</html>
