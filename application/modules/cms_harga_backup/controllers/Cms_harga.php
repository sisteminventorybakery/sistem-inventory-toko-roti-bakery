<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_harga extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        //$this->session->keep_flashdata('tes');
        //if ($this->session->flashdata('tes')) $this->base_config->tes($this->session->flashdata('tes'));
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('master_item');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_item');
            $crud->set_subject('Harga Roti');
            $crud->columns('item_name', 'item_category', 'item_jenis','item_harga','item_harga_outlet','item_satuan');
            $crud->edit_fields('harga_outlet','harga_sales');
            $crud->change_field_type('item_harga', 'integer');
            $crud->change_field_type('item_harga_outlet', 'integer');
            $crud->display_as('item_name', 'Nama Item');
            $crud->display_as('item_price', 'Harga_jual');
            $crud->display_as('item_satuan', 'Satuan');
            $crud->display_as('item_category', 'Kategori');
            $crud->display_as('item_jenis', 'Jenis');
            $crud->set_relation('item_category', 'tb_master_category', 'master_category_name');
            $crud->set_relation('item_jenis', 'tb_master_jenis', 'master_jenis_name');
            $crud->set_relation('item_satuan', 'tb_master_satuan', 'master_satuan_name');
            $crud->callback_column('item_harga', array($this, '_callback_column_sales'));
            $crud->callback_column('item_harga_outlet', array($this, '_callback_column_outlet'));
            //$crud->callback_edit_field('item_harga',array($this,'_set_input_sales'));
            //$crud->callback_edit_field('item_harga_outlet',array($this,'_set_input_outlet'));
            $crud->callback_before_update(array($this, '_callback_before_update'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_delete();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_before_update($post_array, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->outlet_id());
        $result = $this->db->get('tb_harga',1);
        if( $result->num_rows() ){
            $data = [
                'harga_outlet' => $post_array['harga_outlet'],
                'harga_sales' => $post_array['harga_sales'],
            ];
            $this->db->where('item_id', $primary_key);
            $this->db->where('outlet_id', $this->outlet_id());
            $this->db->update('tb_harga', $data);
        }else{
            $data = [
                'item_id' => $primary_key,
                'outlet_id' => $this->outlet_id(),
                'harga_outlet' => $post_array['harga_outlet'],
                'harga_sales' => $post_array['harga_sales'],
            ];
            $this->db->insert('tb_harga', $data);
        }
        unset($post_array['harga_sales']);
        unset($post_array['harga_outlet']);
        //echo json_encode($post_array); exit();
        return $post_array;
    }

    public function _callback_column_sales($value, $row)
    {
        $this->db->where('outlet_id', $this->outlet_id());
        $this->db->where('item_id', $row->item_id);
        return number_format( (int)$this->db->get('tb_harga',1)->row('harga_sales') );
    }

    public function _callback_column_outlet($value, $row)
    {
        $this->db->where('outlet_id', $this->outlet_id());
        $this->db->where('item_id', $row->item_id);
        return number_format( (int)$this->db->get('tb_harga',1)->row('harga_outlet') );
    }

    public function _set_input_sales($value, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->outlet_id());
        $price = (int)$this->db->get('tb_harga')->row('harga_sales');
        return '<input class="numeric form-control" name="item_harga" type="number" value="'.$price.'">';
    }

    public function _set_input_outlet($value, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->outlet_id());
        $price = (int)$this->db->get('tb_harga')->row('harga_outlet');
        return '<input class="numeric form-control" name="item_harga_outlet" type="number" value="'.$price.'">';
    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }

}