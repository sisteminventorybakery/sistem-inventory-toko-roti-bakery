<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_stok_roti_diskon extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Item_model');
    }

    public function ubah_diskon()
    {
        $retur_detail_id    = $this->uri->segment(4);
        $diskon             = $this->uri->segment(5);
        $master_category_id = $this->uri->segment(6);
        $item_id            = $this->uri->segment(7);

        $this->db->set('retur_detail_price', "$diskon", FALSE);
        $this->db->where('retur_detail_id', $retur_detail_id);
        $this->db->update('tb_retur_detail');

        $this->db->set('item_category', $master_category_id);
        $this->db->where('item_id', $item_id);
        $this->db->update('tb_master_item');

        redirect('cms/stok_roti_diskon','refresh');
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $this->base_config->groups_access('stok_roti_diskon');

            $output = new stdClass();
            $this->db->like('master_category_name', 'Roti Diskon ', 'after');
            $roti_diskon = $this->db->get('tb_master_category')->result();
            $data['roti_diskon'] = $roti_diskon;

            if ($this->session->userdata('outlet_id') != 0) $this->db->where('tb_retur.outlet_id', $this->session->userdata('outlet_id'));
            
            $this->db->where('tb_retur_detail.retur_detail_status', 'YA');
            $this->db->join('tb_retur_detail', 'tb_retur_detail.retur_id = tb_retur.retur_id');
            $this->db->join('tb_master_item', 'tb_master_item.item_id = tb_retur_detail.item_id');
            $data['tabel'] = $this->db->get('tb_retur')->result();
            // echo "<pre>";
            // print_r($data['tabel']);
            // echo "</pre>";
            // exit();

            $output->data = $data;
            $output->js_files = array();
            $output->css_files = array();
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_stok_roti_diskon', $output);
        $this->load->view('v_footer', $output);
    }

    public function _callback_column_retur_detail_status($value)
    {   
        switch ($value) {
            case 'YA':
                $status = 'Diterima';
                $label = 'label-success';
                break;

            case 'TIDAK':
                $status = 'Belum Dicek';
                $label = 'label-warning';
                break;

            case 'DITOLAK':
                $status = 'Ditolak';
                $label = 'label-danger';
                break;
        }
        $retur = "<span class='label ".$label."'>".$status."</span>";
        return $retur;
    }

    public function _callback_column_expired($value)
    {
        if (empty($value)) {
            $return = '0000-00-00';
        }else{
            $return = $value;
        }

        return $return;
    }

    public function ubah_status()
    {
        $retur_detail_id = $this->input->post('retur_detail_id', TRUE);
        $status = $this->input->post('status', TRUE);

        if ($status == 'DITOLAK') {
            if ($this->session->userdata('outlet_id') == 0) { //OUTLET PUSAT
                $object = array('retur_detail_status' => 'DITOLAK', );
            }else{
                $object = array('retur_detail_status' => 'TIDAK', );
            }
        }else{
            $object = array('retur_detail_status' => $status, );
        }

        

        $this->db->where('retur_detail_id', $retur_detail_id);
        $this->db->update('tb_retur_detail', $object);

        redirect('cms/stok_roti_diskon','refresh');

    }

}