<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_stok_roti_diskon extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('stok_roti_diskon');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_retur_detail');
            $crud->set_subject('Stok Roti Diskon');
            // $crud->order_by('item_name', 'asc');
            $crud->columns('retur_id','item_id','retur_detail_qty','retur_detail_price','expired','retur_detail_status', 'aksi');
            $crud->display_as('retur_id', 'Id Retur');
            $crud->display_as('item_id', 'Nama Item');
            $crud->display_as('retur_detail_qty', 'Jumlah Retur');
            $crud->display_as('expired', 'EXP');
            $crud->display_as('created', 'Tgl Retur');
            $crud->display_as('retur_detail_status', 'Status');
            $crud->set_relation('item_id', 'tb_master_item', 'item_name');
            $crud->callback_column('retur_detail_status', array($this, '_callback_column_retur_detail_status'));
            $crud->callback_column('aksi', array($this, '_callback_column_aksi'));
            $crud->callback_column('expired', array($this, '_callback_column_expired'));
            $crud->callback_column('retur_detail_price', array($this, '_callback_column_retur_detail_price'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->where('retur_detail_status', 'YA');
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_column_retur_detail_status($value)
    {   
        switch ($value) {
            case 'YA':
                $status = 'Diterima';
                $label = 'label-success';
                break;

            case 'TIDAK':
                $status = 'Belum Dicek';
                $label = 'label-warning';
                break;

            case 'DITOLAK':
                $status = 'Ditolak';
                $label = 'label-danger';
                break;
        }
        $retur = "<span class='label ".$label."'>".$status."</span>";
        return $retur;
    }

    public function _callback_column_aksi($value, $row)
    {   
        $this->db->like('master_category_name', 'Roti Diskon ', 'after');
        $roti_diskon = $this->db->get('tb_master_category')->result();

        $roti_diskon2 = '';
        foreach ($roti_diskon as $key => $value) {
            $roti_diskon2 .= "<li><a href='".base_url()."cms/stok_roti_diskon/ubah_diskon/".$row->retur_detail_id."/".$value->master_category_desc."'>".$value->master_category_name."</a></li>";
        }

        $return = "
        <form action='stok_roti_diskon/ubah_status' method='POST'>
        <input type='hidden' name='retur_detail_id' value=".$row->retur_detail_id.">
        <div class='btn-group'>
            <button class='btn btn-success' type='submit' name='status' value='YA'>Terima</button>
            <button class='btn btn-warning' type='submit' name='status' value='DITOLAK'>Tolak</button>
            <div class='dropdown'>
              <button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>Kategori
              <span class='caret'></span></button>
              <ul class='dropdown-menu'>
                ".$roti_diskon2."
              </ul>
            </div>
        </div>
        </form>
        ";
        return $return;
    }

    public function _callback_column_retur_detail_price($value)
    {
        return number_format($value);
    }

    public function _callback_column_expired($value)
    {
        if (empty($value)) {
            $return = '0000-00-00';
        }else{
            $return = $value;
        }

        return $return;
    }

    public function ubah_status()
    {
        $retur_detail_id = $this->input->post('retur_detail_id', TRUE);
        $status = $this->input->post('status', TRUE);
        $object = array('retur_detail_status' => $status, );
        $this->db->where('retur_detail_id', $retur_detail_id);
        $this->db->update('tb_retur_detail', $object);
        redirect('cms/stok_roti_diskon','refresh');
    }

    public function ubah_diskon()
    {
        $retur_detail_id = $this->uri->segment(4);
        $diskon = $this->uri->segment(5);
        
        $this->db->set('retur_detail_price', "retur_detail_price-$diskon", FALSE);
        $this->db->where('retur_detail_id', $retur_detail_id);
        $this->db->update('tb_retur_detail');

        redirect('cms/stok_roti_diskon','refresh');
    }

}