<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Output $output
 * @property CI_Session session
 */
class Cms_penjualan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        if( $this->uri->segment(4) == 'add' ) {
            $this->add();
        }else{
            try {
                $crud = $this->base_config->groups_access('penjualan');
                $crud->set_theme('twitter-bootstrap');
                $crud->set_table('tb_sales');
                $crud->set_subject('Penjualan');
                $crud->order_by('sales_date','desc');
                $crud->where('outlet_id', $this->session->userdata('outlet_id'));
                $crud->edit_fields('sales_lunas','sales_pay');
                $crud->set_relation('customer_id', 'tb_customer', 'customer_name');
                $crud->display_as('customer_id','Pembeli');
                $crud->display_as('sales_date','Tgl Pesan');
                $crud->display_as('sales_discount', 'Diskon');
                $crud->display_as('sales_total','Total');
                $crud->display_as('sales_lunas','Lunas');
                $crud->display_as('sales_pay','Bayar');
                $crud->display_as('created','informasi pembayaran cicilan');
                $crud->display_as('sales_id','Nota');
                $crud->display_as('sales_type','Jenis Pemb.');
                $crud->display_as('user_id','Nota');
                $crud->callback_column('created', array($this,'_callback_detail'));
                $crud->callback_column('user_id', array($this,'_callback_nota'));
                $crud->callback_column('sales_total', array($this,'_callback_sparator'));
                $crud->callback_column('sales_pay', array($this,'_callback_sparator'));
                $crud->unset_print();
                $crud->unset_edit();
                $state = $crud->getState();
                if( $state == 'export' ){
                    $crud->columns('user_id','customer_id','sales_date','sales_discount','sales_total','sales_pay','sales_type','sales_lunas');
                }else{
                    $crud->callback_column('sales_lunas', array($this,'_callback_lunas'));
                    $crud->columns('user_id','customer_id','sales_date','sales_discount','sales_total','sales_pay','sales_lunas','created');
                }
                $crud->set_bulkactionfalse(true);
                $output = $crud->render();
                $output->data = $data;
                $this->_example_output($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        }
    }

    public function add()
    {
        redirect('pos');
    }

    public function _callback_sparator($value)
    {
        return number_format($value);
    }

    public function _callback_lunas($value, $row)
    {
        $type = $row->sales_type;
        if (strtolower($row->sales_lunas) == 'ya')
            $active = '<span class="label label-success">' . 'LUNAS' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'TIDAK' . '</span>';

        return "$type<br>$active";
    }

    public function _callback_detail($value, $row)
    {
        $id = $row->sales_id;
        $btn = "<a href='#' data-id='$id' data-modul='sales' class='btn-preview btn btn-info'><i class='fa fa-eye'></i></a>";
        $pdf = '<a class="btn btn-sm btn-warning" target="_blank" title="Detail" href="'.base_url('cms/penjualan/export/pdf/'. $row->sales_id .'').'"><i class="fa fa-file-pdf-o"></i></a>';
        return '<div class="btn-group btn-group-sm">'.$btn.$pdf.'</div>';
    }

    public function _callback_id($value, $row)
    {
        return $row->sales_id;
    }

    public function _callback_nota($value, $row)
    {
        return 'PJ-'.sprintf("%04s", $row->sales_id);
    }

    public function get()
    {
        $this->M_base_config->cekaAuth();
        $id = $this->uri->segment(4);
        if( !$id ) show_404();
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_sales_detail.item_id');
        $this->db->join('tb_sales','tb_sales.sales_id=tb_sales_detail.sales_id');
        $this->db->where('tb_sales_detail.sales_id', $id);
        $data = $this->db->get('tb_sales_detail')->result_array();
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    public function export()
    {
        $this->M_base_config->cekaAuth();
        $export = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $sales = array();
        $result = array();
        $total = 0;
        $total_qty = 0;
        $total_price = 0;
        $total_price_after_disc = 0;
        $total_disc = 0;
        $this->db->join('tb_sales','tb_sales.sales_id=tb_sales_detail.sales_id');
        $this->db->join('tb_customer','tb_customer.customer_id=tb_sales.customer_id');
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_sales.outlet_id');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_sales_detail.item_id');
        $this->db->join('tb_user','tb_user.id=tb_sales.user_id');
        $this->db->where('tb_sales_detail.sales_id', $id);
        $result_row = $this->db->get('tb_sales_detail')->result();
        foreach ($result_row as $k => $v){
            $price      = (int)$v->sales_detail_price;
            $qty        = (int)$v->sales_detail_qty;
            $disc       = (int)$v->sales_detail_disc;
            $price_after_disc = $price - $disc;
            $subtotal   = (int)$v->sales_detail_total;
            $result[]   = array(
                'no'                => $k+1,
                'item'              => $v->item_name,
                'disc'              => number_format($disc),
                'price'             => number_format($price),
                'price_after_disc'  => number_format($price_after_disc),
                'qty'               => number_format($qty),
                'subtotal'          => number_format($subtotal),
            );

            $total_qty += $qty;
            $total_price += $price;
            $total_price_after_disc += $price_after_disc;
            $total_disc += $disc;

            $total = (int)$v->sales_total;
            $sales = array(
                'id'        => $v->sales_id,
                'customer'  => $v->customer_name,
                'nota'      => 'PJ-'.sprintf("%04s", $v->sales_id),
                'disc'      => number_format($v->sales_discount),
                'total'     => $total,
                'outlet'    => $v->master_outlet_name,
                'date'      => $v->sales_date,
                'telepon'   => $v->customer_phone,
                'pay'       => number_format( (int)$v->sales_pay),
                'user'      => $v->username
            );
        }
        $data['result']                 = $result;
        $data['total']                  = number_format($total);
        $data['total_qty']              = number_format($total_qty);
        $data['total_price']            = number_format($total_price);
        $data['total_disc']             = number_format($total_disc);
        $data['total_price_after_disc'] = number_format($total_price_after_disc);
        $data['sales']                  = $sales;

        //$this->M_base_config->tes( $result_row );
        $data['title'] = 'LAPORAN PENJUALAN';
        $date = date('d-m-Y');
        switch ( $export ){
            case 'pdf':
                $this->load->view( 'v_report_template', $data );
                $html = $this->output->get_output();
                $dompdf = new \Dompdf\Dompdf();
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4', 'potrait');
                $dompdf->render();
                $dompdf->stream("report-$date.pdf",array('Attachment'=>0));
                exit();
                break;
            case 'excel':
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename = report-$date.xls");
                $this->load->view( 'v_report_template', $data );
                //exit();
                break;
        }
    }

}