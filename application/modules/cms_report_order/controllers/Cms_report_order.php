<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_report_order extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_report_order', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','report_order') ) show_404();
        $data = $this->base_config->panel_setting();
        $output = new stdClass();
        $output->data = $data;
        $output->js_files = array();
        $output->css_files = array();
        $this->_example_output($output);
    }

    protected function get_filter( $outlet=null, $startDate=null, $endDate=null, $user=null, $customer=null ){
        $this->db->join('tb_sales_detail', 'tb_sales_detail.sales_id = tb_sales.sales_id');
        if( $outlet ) $this->db->where('outlet_id', $outlet);
        if( $startDate && $endDate ) {
            $this->db->where('sales_date >=', $startDate);
            $this->db->where('sales_date <=', $endDate);
        }
        if( $user ) $this->db->where('user_id', $user);
        if( $customer ) $this->db->where('customer_id', $customer);
        $this->db->get('tb_sales')->result();
    }
}