<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Output $output
 * @property CI_Session session
 */
class Cms_daftar_pembelian_bahan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        $user = $this->ion_auth->user()->row();
        try {
            $crud = $this->base_config->groups_access('daftar_pembelian_bahan');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_pembelian_bahan');
            $crud->set_subject('Daftar Pembelian Bahan');
            $crud->order_by('tanggal','desc');
            $crud->columns('user_id','tanggal','keterangan','total','created');
            $crud->edit_fields('order_lunas','order_pay');
            $crud->display_as('created','Detail');
            $crud->display_as('user_id','Nota');
            $crud->callback_column('created', array($this,'_callback_detail'));
            $crud->callback_column('user_id', array($this,'_callback_nota'));
            $crud->callback_column('total', array($this,'_callback_total'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_export();
            $crud->unset_edit();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_detail($value, $row)
    {
        return '<a target="_blank" title="Detail Pembelian" href="'.base_url('cms/daftar_pembelian_bahan/export/pdf/'. $row->id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    public function _callback_nota($value, $row)
    {
        return 'PB-'.sprintf("%04s", $row->id);
    }

    public function _callback_total($value)
    {
        $this->M_base_config->cekaAuth();
        return number_format((int)$value);
    }

    public function export()
    {
        $this->M_base_config->cekaAuth();
        $export = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $orders = array();
        $result = array();
        $total = 0;
        $total_qty = 0;
        $total_price = 0;
        $total_price_after_disc = 0;
        $total_disc = 0;
        $this->db->select('*,tb_pembelian_bahan.total as pembelian_total,tb_pembelian_bahan_detail.total as pembelian_detail_total');
        $this->db->join('tb_pembelian_bahan','tb_pembelian_bahan.id=tb_pembelian_bahan_detail.pembelian_bahan_id');
        $this->db->join('tb_master_bahan','tb_master_bahan.master_bahan_id=tb_pembelian_bahan_detail.bahan_id');
        $this->db->join('tb_user','tb_user.id=tb_pembelian_bahan.user_id');
        $this->db->where('tb_pembelian_bahan_detail.pembelian_bahan_id', $id);
        $result_row = $this->db->get('tb_pembelian_bahan_detail')->result();
        foreach ($result_row as $k => $v){
            $price      = (int)$v->harga;
            $qty        = (int)$v->jumlah;
            $disc       = 0;
            $subtotal   = (int)$v->pembelian_detail_total;
            $price_after_disc = $price - ($price * ($disc/100));
            $result[]   = array(
                'no'                => $k+1,
                'item'              => $v->master_bahan_name,
                'disc'              => number_format($disc),
                'price'             => number_format($price),
                'price_after_disc'  => number_format($price_after_disc),
                'qty'               => number_format($qty),
                'subtotal'          => number_format($subtotal),
            );

            $total_qty += $qty;
            $total_price += $price;
            $total_price_after_disc += $price_after_disc;
            $total_disc += $disc;

            $total = (int)$v->pembelian_total;
            $orders = array(
                'id'        => $v->pembelian_bahan_id,
                'nota'      => 'PB-'.sprintf("%04s", $v->pembelian_bahan_id),
                'disc'      => 0,
                'total'     => $total,
                'date'      => date('d-m-Y', strtotime($v->tanggal)),
                'note'      => $v->keterangan,
                'user'      => $v->username
            );
        }
        $data['result']                 = $result;
        $data['total']                  = number_format($total);
        $data['total_qty']              = number_format($total_qty);
        $data['total_price']            = number_format($total_price);
        $data['total_disc']             = number_format($total_disc);
        $data['total_price_after_disc'] = number_format($total_price_after_disc);
        $data['orders']                 = $orders;
        $data['title']                  = 'LAPORAN PEMBELIAN BAHAN';

        switch ( $export ){
            case 'pdf':
                $this->load->view( 'v_report_template', $data );
                $html = $this->output->get_output();
                $dompdf = new \Dompdf\Dompdf();
                $dompdf->load_html($html);
                $dompdf->set_paper('A4', 'potrait');
                $dompdf->render();
                $dompdf->stream("report.pdf",array('Attachment'=>0));
                exit();
                break;
            case 'excel':
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename = report.xls");
                $this->load->view( 'v_report_template', $data );
                //exit();
                break;
        }
    }

}