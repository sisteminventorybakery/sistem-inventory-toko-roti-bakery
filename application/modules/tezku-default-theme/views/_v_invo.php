<html>
	<head>
		<style>
			table{
				font-size: 10px;
			}
			.head{
				border-bottom: 1px solid;
			}
		</style>
	</head>
	<body onload="window.print()">
		<table style="width: 100%">
			<tr>
				<td colspan="5" align="center" style="border-bottom: 1px solid"><?php echo $header_struk?></td>
			</tr>
			<tr>
				<td colspan="3"><?php echo $header->sales_date?></td>
				<td colspan="2" align="right"><?php echo $header->first_name?></td>
			</tr>
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			<?php if( $header->sales_type == 'cash' ):?>
				<tr>
					<td align="right" colspan="3">PEMBAYARAN</td>
					<td align="right" colspan="2"><?php echo strtoupper($header->sales_type)?></td>
				</tr>
			<?php elseif( $header->sales_type == 'kredit' ):?>
                <tr>
                    <td align="right" colspan="3">PEMBAYARAN</td>
                    <td align="right" colspan="2"><?php echo strtoupper($header->sales_type)?></td>
                </tr>
			<?php elseif( $header->sales_type == 'transfer' ):?>
                <tr>
                    <td align="right" colspan="3">PEMBAYARAN</td>
                    <td align="right" colspan="2"><?php echo strtoupper($header->sales_type)?></td>
                </tr>
				<tr>
					<td align="right" colspan="3">ATAS NAMA</td>
					<td align="right" colspan="2"><?php echo $header->sales_nama?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">NAMA BANK</td>
					<td align="right" colspan="2"><?php echo $header->sales_nama_bank?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">NOMOR REKENING</td>
					<td align="right" colspan="2"><?php echo $header->sales_nomor_rekening?></td>
				</tr>
			<?php elseif( $header->sales_type == 'kartu_kredit' ):?>
                <tr>
                    <td align="right" colspan="3">PEMBAYARAN</td>
                    <td align="right" colspan="2"><?php echo strtoupper($header->sales_type)?></td>
                </tr>
				<tr>
					<td align="right" colspan="3">ATAS NAMA</td>
					<td align="right" colspan="2"><?php echo $header->sales_nama?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">NAMA BANK</td>
					<td align="right" colspan="2"><?php echo $header->sales_nama_bank?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">NOMOR KARTU</td>
					<td align="right" colspan="2"><?php echo $header->sales_nomor_kartu?></td>
				</tr>
			<?php endif; ?>
            <tr>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
            </tr>
			<tr>
				<td class="head" align="center">NAMA</td>
				<td class="head" align="center">HARGA</td>
				<td class="head" align="center">QTY</td>
				<td class="head" align="center">DISC</td>
				<td class="head" align="center">SUBTOTAL</td>
			</tr>
			<?php foreach($data as $dt){?>
				<tr>
					<td align="center"><?php echo $dt->item_name?></td>
					<td align="right"><?php echo number_format($dt->sales_detail_price, 0, ',', '.');?></td>
					<td align="right"><?php echo $dt->sales_detail_qty?></td>
					<td align="right"><?php echo number_format($dt->sales_detail_disc, 0, ',', '.')?></td>
					<td align="right"><?php echo number_format($dt->sales_detail_total, 0, ',', '.')?></td>
				</tr>
			<?php }?>
				<tr>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
				</tr>
			<?php if( $header->sales_discount > 0 ):?>
				<tr>
					<td align="right" colspan="3">DISKON</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_discount, 0, ',', '.')?></td>
				</tr>
			<?php endif;?>
			<?php if( $header->sales_fee > 0 ):?>
				<tr>
					<td align="right" colspan="3">BIAYA TAMBAHAN</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_fee, 0, ',', '.')?></td>
				</tr>
			<?php endif;?>
				<tr>
					<td align="right" colspan="3">TOTAL HARGA</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_total, 0, ',', '.')?></td>
				</tr>

			<?php if( $header->sales_type == 'cash' ):?>
				<tr>
					<td align="right" colspan="3">BAYAR</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_pay, 0, ',', '.')?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">KEMBALI</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_cashback, 0, ',', '.')?></td>
				</tr>
			<?php elseif( $header->sales_type == 'kredit' ):?>
				<tr>
					<td align="right" colspan="3">UANG MUKA</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_dp, 0, ',', '.')?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">KURANG BAYAR</td>
					<td align="right" colspan="2"><?php echo number_format($header->sales_total - $header->sales_dp, 0, ',', '.')?></td>
				</tr>
			<?php endif; ?>
		</table>
	</body>
</html>