<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_report_resep extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output['theme_path'] = $path;
        $output['js_files'] = array();
        $output['css_files'] = array();
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_report', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','report_resep') ) show_404();
        $data = $this->base_config->panel_setting();
        $f_nota = $this->input->get('nota');
        $f_from = $this->input->get('from');
        $f_to = $this->input->get('to');
        $f_exp_from = $this->input->get('exp-from');
        $f_exp_to = $this->input->get('exp-to');

        $result = array();
        $all_total = 0;
        $filter_user = 'Kosong';
        if( $f_nota ) $this->db->where('id', $f_nota);
        if( $f_from ) $this->db->where('tanggal >=', $f_from);
        if( $f_to ) $this->db->where('tanggal <=', $f_to);
        if( $f_exp_from ) $this->db->where('expired >=', $f_exp_from);
        if( $f_exp_to ) $this->db->where('expired <=', $f_exp_to);
        $this->db->order_by('expired','desc');
        $result_row = $this->db->get('tb_resep')->result();
        foreach ($result_row as $v){
            $total = (int)$v->biaya_total;
            $result[] = array(
                'id'        => $v->resep_id,
                'nota'      => $this->_callback_nota($v),
                'total'     => number_format($total),
                'date'      => $v->tanggal,
                'detail'    => $this->_callback_detail($v),
                'expired'    => $v->expired,
            );
            $all_total += $total;
        }
        $sales = array(
            'nota' => ( $f_nota ? $filter_user : 'Semua'),
            'from' => ( $f_from ? $f_from : 'Semua'),
            'to' => ( $f_to ? $f_to : 'Semua'),
            'exp-from' => ( $f_exp_from ? $f_exp_from : 'Semua'),
            'exp-to' => ( $f_exp_to ? $f_exp_to : 'Semua'),
        );
        $data['sales'] = $sales;
        $data['total'] = number_format($all_total);
        $data['title'] = 'LAPORAN RESEP';
        $data['result'] = $result;
        $data['user'] = $this->_get_user();
        $data['outlet'] = $this->_get_outlet();
        $export = $this->input->get('export');
        if($export){
            switch ( $export ){
                case 'pdf':
                    $this->load->view( 'v_report_template', $data );
                    $html = $this->output->get_output();
                    $dompdf = new \Dompdf\Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4','potrait');
                    $dompdf->render();
                    $dompdf->stream("report.pdf",array('Attachment'=>0));
                    exit();
                    break;
                case 'excel':
                    header("Content-Type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename = report.xls");
                    $this->load->view( 'v_report_template', $data );
                    //exit();
                    break;
            }
        }else{
            $this->_example_output($data);
        }

    }

    protected  function _get_outlet()
    {
        $res = $this->db->get('tb_master_outlet')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->master_outlet_name,
                'id' => $v->master_outlet_id
            );
        }
        return $tmp;
    }

    protected  function _get_user()
    {
        $res = $this->db->get('tb_user')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->username,
                'id' => $v->id
            );
        }
        return $tmp;
    }

    protected function _callback_detail($row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail" href="'.base_url('cms/daftar_resep/export/pdf/'. $row->resep_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    protected function _callback_nota($row)
    {
        $this->M_base_config->cekaAuth();
        return 'RS-'.sprintf("%04s", $row->resep_id);
    }
}