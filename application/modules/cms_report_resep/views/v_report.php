<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-content">

				<div class="row" style="margin-top: 15px;">
					<form data-url="<?php echo base_url('cms/report_pemesanan');?>" id="form-report-order" method="get">
						<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
								<input type="number" class="form-control" name="nota" value="<?php echo $this->input->get('nota');?>" placeholder="No. Nota">
						</div>
						<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
							<div class="input-group date form_date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" name="from" value="<?php echo $this->input->get('from');?>" class="form-control" placeholder="Tgl Dari">
							</div>
						</div>
						<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
							<div class="input-group date form_date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" name="to" value="<?php echo $this->input->get('to');?>" class="form-control" placeholder="Tgl Ke">
							</div>
						</div>
						<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
							<div class="input-group date form_date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" name="exp-from" value="<?php echo $this->input->get('exp-from');?>" class="form-control" placeholder="Tgl Expired Dari">
							</div>
						</div>
						<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
							<div class="input-group date form_date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type="text" name="exp-to" value="<?php echo $this->input->get('exp-to');?>" class="form-control" placeholder="Tgl Expired Ke">
							</div>
						</div>
						<div class="col-lg-2" style="width: 8%;padding-left: 1px;padding-right: 1px;">
							<button type="submit" class="btn btn-info btn-block"><i class="fa fa-search"></i></button>
						</div>
						<div class="col-lg-2" style="width: 8%;padding-left: 1px;padding-right: 1px;">
							<button type="button" id="export-pdf" class="btn btn-warning btn-block"><i class="fa fa-file-pdf-o"></i></button>
						</div>
					</form>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<table class="table table-condensed table-dark-header">
						<thead>
						<tr>
							<th>Nota</th>
							<th>Tgl Pembuatan</th>
							<th>Tgl Expired</th>
							<th>Total</th>
							<th>Detail</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($result as $v):?>
						<tr>
							<td><?php echo $v['nota'];?></td>
							<td><?php echo $v['date'];?></td>
							<td><?php echo $v['expired'];?></td>
							<td><?php echo $v['total'];?></td>
							<td><?php echo $v['detail'];?></td>
						</tr>
						<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
