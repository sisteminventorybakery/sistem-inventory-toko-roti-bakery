<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-header">
			<h2>Stok Retur</h2>
			<em>...</em>
		</div>
		<div class="main-content">
			<table id="data-export" class="table table-sorting table-striped table-hover table-bordered table-dark-header table-responsive">
				<thead>
					<tr>
						<th width="10">
							Id Retur</th>
							<th>
								<div class="text-left field-sorting "
								rel="s82bfda79">
								Nama Item				</div>
							</th>
							<th>
								<div class="text-left field-sorting "
								rel="retur_detail_qty">
								Jumlah Retur				</div>
							</th>
							<th>
								<div class="text-left field-sorting "
								rel="expired">
								EXP				</div>
							</th>
							<th>
								<div class="text-left field-sorting "
								rel="retur_detail_status">
								Status				</div>
							</th>
							<th width="150">
								<div class="text-left field-sorting "
								rel="aksi">
								Aksi				</div>
							</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data['tabel'] as $key => $value): ?>
						<tr class="">
							<td class="">
								<div class="text-left"><?=++$key?></div>
							</td>
							<td class="">
								<div class="text-left"><?=$value->item_name?></div>
							</td>
							<td class="">
								<div class="text-left"><?=$value->retur_detail_qty?></div>
							</td>
							<td class="">
								<div class="text-left"><?=$value->expired?></div>
							</td>
							<td class="">
							<?php 
								switch ($value->retur_detail_status) {
								    case 'YA':
								        $status = 'Diterima';
								        $label = 'label-success';
								        break;

								    case 'TIDAK':
								        $status = 'Belum Dicek';
								        $label = 'label-warning';
								        break;

								    case 'DITOLAK':
								        $status = 'Ditolak';
								        $label = 'label-danger';
								        break;
								}
							 ?>
								<div class="text-left"><span class='label <?=$label?>'><?=$status?></span></div>
							</td>
							<td class="">
								<div class="text-left">
									<form action='stok_retur/ubah_status' method='POST'>
										<input type='hidden' name='retur_detail_id' value=<?=$value->retur_detail_id?>>
										<div class='btn-group'>
											<button class='btn btn-success' type='submit' name='status' value='YA'>Terima</button>
											<button class='btn btn-warning' type='submit' name='status' value='DITOLAK'>Tolak</button>
										</div>
									</form>
								</div>
							</td>
						</tr>
					<?php endforeach ?>
						
					</tbody>
				</table>
				<!-- /main-content -->
			<!-- /main -->
		</div>
		<!-- /content-wrapper -->

		<script type="text/javascript">
			var page = "pemesanan";
		</script>