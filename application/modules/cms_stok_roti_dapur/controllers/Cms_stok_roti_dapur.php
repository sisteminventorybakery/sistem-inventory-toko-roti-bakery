<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 * @property Stok_model Stok_model
 */
class Cms_stok_roti_dapur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Stok_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        //$this->M_base_config->tes( $this->Stok_model->get_inventory_dapur(9) );
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('stok_roti_dapur');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_item');
            $crud->set_subject('Stok Roti Dapur');
            $crud->order_by('item_name', 'asc');
            $crud->columns('item_name','item_category','item_jenis','harga_real','item_stok','item_satuan');
            $crud->display_as('item_name', 'Nama Item');
            $crud->display_as('item_price', 'Harga_jual');
            $crud->display_as('item_satuan', 'Satuan');
            $crud->display_as('item_category', 'Kategori');
            $crud->display_as('item_jenis', 'Jenis');
            $crud->set_relation('item_category', 'tb_master_category', 'master_category_name');
            $crud->set_relation('item_jenis', 'tb_master_jenis', 'master_jenis_name');
            $crud->set_relation('item_satuan', 'tb_master_satuan', 'master_satuan_name');
            $crud->field_type('created', 'hidden');
            $crud->callback_before_delete(array($this, '_callback_before_delete'));
            $crud->callback_before_update(array($this, '_callback_before_update'));
            $crud->callback_column('harga_real', array($this, '_callback_column_harga_real'));
            $crud->callback_column('item_stok', array($this, '_callback_column_stok'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_column_harga_real($value, $row)
    {
        $tmp = [];
        $results = $this->Stok_model->get_inventory_dapur($row->item_id);
        foreach ($results as $result) {
            $tmp[] = number_format($result['inv_harga']);
        }
        return implode('<br>', $tmp);
    }

    public function _callback_column_stok($value, $row)
    {
        $tmp = [];
        $results = $this->Stok_model->get_inventory_dapur($row->item_id);
        foreach ($results as $result) {
            $tmp[] = 'EXP. '.$this->M_base_config->my_date($result['inv_expired']).' ('.number_format($result['inv_stok']).')';
        }
        return implode('<br>', $tmp);
    }

}