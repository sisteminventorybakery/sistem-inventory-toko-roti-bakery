<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_harga_item extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('master_item');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_item');
            $crud->set_subject('Master Item');
            $crud->order_by('item_name', 'asc');
            $crud->columns('item_name', 'item_category', 'item_jenis', /*'harga_real',*/ 'harga_outlet', 'harga_jual','item_satuan');
            $crud->add_fields('item_name', 'item_satuan', 'item_category', 'item_jenis');
            $crud->edit_fields(/*'harga_real',*/ 'harga_outlet', 'harga_jual','created');
            $crud->change_field_type('harga_outlet', 'integer');
            //$crud->change_field_type('harga_real', 'integer');
            $crud->change_field_type('harga_jual', 'integer');
            $crud->required_fields('harga_jual',/*'harga_real',*/'harga_outlet');
            $crud->display_as('item_name', 'Nama Item');
            $crud->display_as('item_price', 'Harga_jual');
            $crud->display_as('item_satuan', 'Satuan');
            $crud->display_as('item_category', 'Kategori');
            $crud->display_as('item_jenis', 'Jenis');
            $crud->set_relation('item_category', 'tb_master_category', 'master_category_name');
            $crud->set_relation('item_jenis', 'tb_master_jenis', 'master_jenis_name');
            $crud->set_relation('item_satuan', 'tb_master_satuan', 'master_satuan_name');
            $crud->field_type('created', 'hidden', date('Y-m-d H:i:s'));
            $crud->callback_before_delete(array($this, '_callback_before_delete'));
            $crud->callback_before_update(array($this, '_callback_before_update'));
            $crud->callback_column('harga_jual', array($this, '_callback_column_harga_jual'));
            //$crud->callback_column('harga_real', array($this, '_callback_column_harga_real'));
            $crud->callback_column('harga_outlet', array($this, '_callback_column_harga_outlet'));
            $crud->callback_edit_field('harga_jual',array($this,'_set_input_harga_jual'));
            $crud->callback_edit_field('harga_outlet',array($this,'_set_input_harga_outlet'));
            //$crud->callback_edit_field('harga_real',array($this,'_set_input_harga_real'));
            $crud->unset_print();
            $crud->unset_delete();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_before_update($post_array, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $result = $this->db->get('tb_harga',1)->result_array();
        if( $result ){
            $data = [
                'harga_sales' => $post_array['harga_jual'],
                'harga_outlet' => $post_array['harga_outlet'],
                //'harga_real' => $post_array['harga_real'],
            ];
            $this->db->where('item_id', $primary_key);
            $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
            $this->db->update('tb_harga', $data);
        }else{
            $data = [
                'item_id' => $primary_key,
                'outlet_id' => $this->session->userdata('outlet_id'),
                'harga_sales' => $post_array['harga_jual'],
                'harga_outlet' => $post_array['harga_outlet'],
                //'harga_real' => $post_array['harga_real'],
            ];
            $this->db->insert('tb_harga', $data);
        }
        unset($post_array['harga_jual']);
        unset($post_array['harga_real']);
        unset($post_array['harga_outlet']);
        return $post_array;
    }

    public function _callback_before_delete($primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        return $this->db->delete('tb_harga');
    }

    public function _callback_column_harga_jual($value, $row)
    {
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $row->item_id);
        return number_format( (int)$this->db->get('tb_harga',1)->row('harga_sales') );
    }

    public function _callback_column_harga_real($value, $row)
    {
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $row->item_id);
        return number_format( (int)$this->db->get('tb_harga',1)->row('harga_real') );
    }

    public function _callback_column_harga_outlet($value, $row)
    {
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $row->item_id);
        return number_format( (int)$this->db->get('tb_harga',1)->row('harga_outlet') );
    }

    public function _set_input_harga_jual($value, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $price = (int)$this->db->get('tb_harga')->row('harga_sales');
        return '<input class="numeric form-control" name="harga_jual" type="number" value="'.$price.'">';
    }

    public function _set_input_harga_real($value, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $price = (int)$this->db->get('tb_harga',1)->row('harga_real');
        return '<input class="numeric form-control" name="harga_real" type="number" value="'.$price.'">';
    }

    public function _set_input_harga_outlet($value, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $price = (int)$this->db->get('tb_harga')->row('harga_outlet');
        return '<input class="numeric form-control" name="harga_outlet" type="number" value="'.$price.'">';
    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }

}