<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Front $Front
 * @property  CI_Form_validation $form_validation
 * @property  CI_Session $session
 */
class Cms_auth extends CI_Controller {
    public function __construct()
        {
        parent::__construct();
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
     }
    // log the user in
    public function index()
    {
        //cek auth
        $this->M_base_config->ifLogin();
        //Get Panel setting
        $data=$this->base_config->panel_setting();
        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true)
        {
            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
            {
                //if the login is successful
                //redirect them back to the home page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('cms', 'refresh');
            }
            else
            {
                // if the login was un-successful
                // redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('cms/auth', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }
        else
        {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                if(!empty($data['message'])) {
                    $info = "";           
                }else {
                    $info = "";    
                }
            $data['identity']           = array(
                        'name'          => 'identity',
                        'id'            => 'login_username',
                        'type'          => 'text',
                        'value'         => $this->form_validation->set_value('identity'),
                        'class'         => 'form-control '.$info,
                        'placeholder'   => 'Email atau Username'
            );
            $data['password']           = array(
                        'name'          => 'password',
                        'id'            => 'login_username',
                        'type'          => 'password',
                        'class'         => 'form-control '.$info,
                        'placeholder'   => 'Password'
            );

            $version                    = '?vesion='.uniqid();
            $path                       = $this->base_config->asset_back();
            $data['asset']              = $path;
            $data['theme_path']         = $path;
            $data['js_files']           = array( 
                                            $path.'js/jquery/jquery-2.1.0.min.js',
                                            $path.'js/bootstrap/bootstrap.js',
                                            $path.'js/plugins/modernizr/modernizr.js' );
            $data['css_files']          = array( 
                                            $path.'css/bootstrap.min.css',
                                            $path.'css/font-awesome.min.css',
                                            $path.'css/main.css' );
            $data['nav']                = 'no';
            $data['viewspage']          = 'auth';
            $this->load->view( 'auth' ,$data);
        }
    }
    // log the user out
    public function logout()
    {
        $data['title'] = "Logout";
        // log the user out
        $logout = $this->ion_auth->logout();
        // redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('cms/auth', 'refresh'); 
    }
}