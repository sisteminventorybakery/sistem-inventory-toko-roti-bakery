<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <title>Login | <?php echo $this->base_config->sigleSetting('setting_general','Site Title');?> - Admin Dashboard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Admin Dashboard">
    <meta name="author" content="nurza.cool@gmail.com">
    <!-- CSS -->
    <!--[if lte IE 9]>
    <link href="<?php /** @var string $theme_path */
echo $theme_path;?>css/main-ie.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $theme_path;?>css/main-ie-part2.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!-- Fav and touch icons -->
<?php
/** @var array $css_files */
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $theme_path;?>ico/my-favicon144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $theme_path;?>ico/my-favicon114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $theme_path;?>ico/my-favicon72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $theme_path;?>ico/my-favicon57x57.png">
    <link rel="shortcut icon" href="<?php echo $theme_path;?>ico/favicon.png">
</head>

<body>
    <div class="wrapper full-page-wrapper page-auth page-login text-center">
        <div class="inner-page">
            <div class="logo">
                <a href="<?php echo base_url();?>"><img src="<?php echo $theme_path;?>img/my-logo.jpg" alt="" /></a>
            </div>

            <div class="login-box center-block">
<?php 
$attributes = array('class' => 'form-horizontal', 'role' => 'form');
echo form_open('cms/auth', $attributes);
?>
                    <p class="title" style="text-align:center;">Gunakan email atau username</p>
                    <div class="form-group">
                        <label for="username" class="control-label sr-only"><?php echo lang('login_identity_label', 'identity');?></label>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <?php /** @var string $identity */
                                echo form_input($identity);?>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                        </div>
                    </div>
                    <label for="password" class="control-label sr-only"><?php echo lang('login_password_label', 'password');?></label>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <?php /** @var string $password */
                                 echo form_input($password);?>
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-custom-primary btn-lg btn-block btn-auth"><i class="fa fa-arrow-circle-o-right"></i> <?php echo lang('login_submit_btn');?></button>
<?php echo form_close();?>
                <div class="links">
            <?php if( !empty($message) ):?>
                <div class="alert alert-danger alert-dismissable">
                    <a href="" class="close">×</a>
                    <?php echo $message;?>
                </div>
            <?php endif;?>
                    <!--<p><a href="#">Lupa Password?</a></p>-->
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">&copy; <?php echo date('Y');?> Roti Bakery All Right Reserved</footer>
    <!-- Javascript -->
    <?php /** @var array $js_files */
foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; 
?>
</body>

</html>
