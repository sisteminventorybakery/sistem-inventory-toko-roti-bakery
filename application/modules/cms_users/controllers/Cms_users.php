<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Ion_auth_model $Ion_auth_model
 */
class Cms_users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->lang->load('auth');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        try {
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $tables = $this->config->item('tables', 'ion_auth');
            $crud = $this->base_config->groups_access('users');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_user');
            $crud->set_subject('User Data');
            $crud->set_composer('Rombak');
            $crud->fields('username', 'email', 'groups', 'user_identitas','user_address','user_outlet','active', 'user_gender', 'user_mobile', 'user_date_birth', 'password');
            $crud->edit_fields('username', 'email', 'groups','user_outlet','user_identitas','user_address', 'active', 'user_gender', 'user_mobile','password', 'user_date_birth');
            $crud->unset_texteditor('user_address');
            $crud->display_as('user_avatar', lang('user_avatar'));
            $crud->display_as('groups', 'Jabatan');
            $crud->display_as('user_address', 'Alamat');
            $crud->display_as('user_date_birth', 'Tanggal Lahir');
            $crud->display_as('user_identitas', 'No KTP/SIM');
            $crud->display_as('user_mobile', 'no. HP');
            $crud->display_as('user_gender', 'Jenis Kelamin');
            $crud->display_as('user_display_name', lang('user_display_name'));
            $crud->display_as('user_email', lang('user_email'));
            $crud->display_as('user_last_login', lang('user_last_login'));
            $crud->display_as('user_active', lang('user_active'));
            $crud->display_as('user_created','<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('last_login', array($this, '_last_login'));
            $crud->set_relation_n_n('groups', 'tb_users_groups', 'tb_groups', 'user_id', 'group_id', 'name');
            $crud->set_relation_n_n('user_outlet', 'tb_terms', 'tb_master_outlet', 'post_id', 'category_id', 'master_outlet_name');
            $crud->order_by('user_created','desc');
            $crud->field_type('password', 'password');
            $crud->field_type('user_created', 'hidden', date("Y-m-d H:i:s"));
            //$crud->callback_add_field('password', array($this, '_user_pass_add'));
            //$crud->callback_edit_field('password', array($this, '_user_pass_edit'));
            $crud->callback_before_insert(array($this, 'encrypt_password_callback'));
            $crud->callback_before_update(array($this, 'encrypt_password_callback'));
            $crud->callback_column('user_created',array($this,'_callback_user_created'));
            $crud->callback_after_insert(array($this, 'notification_user_after_insert'));
            $crud->callback_after_update(array($this, 'notification_user_after_update'));
            $crud->callback_after_delete(array($this, 'notification_user_after_delete'));
            $crud->set_composer(false);
            $crud->unset_print();
            $crud->unset_jquery_ui();
            $state = $crud->getState();
            if( $state == 'export' ){
                $crud->columns('username','groups','user_outlet', 'user_identitas','user_mobile','user_address','last_login');
            }else{
                $crud->columns('user_created','username','groups','user_outlet', 'user_identitas','user_mobile','user_address','last_login');
            }
            $crud->set_rules('email', 'Email', 'trim|required|xss_clean');
            $crud->set_rules('username', 'Username', 'trim|required|xss_clean');
            $crud->set_rules('password', 'Password', 'trim|required|xss_clean|matches[password]');
            //$crud->set_rules('konfirmpass', 'Confirmation password', 'trim|required|xss_clean');
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    public function _last_login($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->active == 1)
            $active = '<span class="label label-success">Aktif</span> <br><small>' . timespan($value) . '</small>';
        else
            $active = '<span class="label label-danger">Tidak Aktif</span><br> <small>' . timespan($value) . '</small>';

        return $active;
    }

    public function _callback_user_created($value, $row)
    {
        $id = $row->id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

    public function encrypt_password_callback($post_array, $primary_key = null)
    {
        $this->load->model('Ion_auth_model');
        $salt = $this->Ion_auth_model->store_salt ? $this->Ion_auth_model->salt() : FALSE;
        $password = $this->Ion_auth_model->hash_password($post_array['password'], $salt);
        $post_array['password'] = $password;
        return $post_array;
    }

    public function _user_pass_add()
    {
        return '<input id="pas1" type="password" class="form-control" name="konfirmpass"/><label>Confirmation password* :</label><input id="pas2" type="password" class="form-control" name="password"/>';
        //return "<span data-uk-modal=\"{target:'#multipleupload'}\" class='md-btn md-btn-primary'> Multiple Upload</span>";
    }

    public function _user_pass_edit()
    {
        return '<input id="pas1" type="password" class="form-control" name="konfirmpass"/><label>Confirmation password* :</label><input id="pas2" type="password" class="form-control" name="password"/>';
    }


    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function notification_user_after_insert($post_array, $primary_key)
    {
        return true;
    }

    public function notification_user_after_update($post_array, $primary_key)
    {
        return true;
    }

    public function notification_user_after_delete($primary_key)
    {
        return true;
    }
}