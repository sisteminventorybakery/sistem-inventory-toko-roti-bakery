<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-header">
			<h2>Mutasi Baru</h2>
			<em>...</em>
		</div>
		<div class="main-content">
			<form id="form" method="post" onsubmit="return false;">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-10">
						<p style="margin-top: 10px;">Nama Item</p>
						<select id="item_id" class="select2">
							<?php foreach ( $items  as $item):?>
								<option value="<?php echo $item['inventory_id'];?>" data-discount="0" data-price="<?php echo $item['inventory_price_sales'];?>"><?php echo $item['item_name'].' EXP.'.$item['inventory_expired'];?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2">
						<p>Harga Produk</p>
						<input type="text" class="form-control" id="price_product" value="0" readonly>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-2">
						<p>Jumlah</p>
						<input type="text" class="form-control numeric2" value="1" name="qty" id="qty">
					</div>
					<div class="col-lg-3">
						<p>Disc.Produk Rp</p>
						<input type="text" class="form-control numeric2" value="0" name="discount_product" id="discount_product" readonly>
					</div>
					<div class="col-lg-3">
						<p>Disc.Produk %</p>
						<input type="text" class="form-control numeric2" value="0" id="discount_product_percent" readonly>
					</div>
					<div class="col-lg-2">
						<p>Sub Total</p>
						<input type="text" class="form-control" value="0" id="order_product_subtotal" readonly>
					</div>
					<div class="col-lg-2">
						<p>-</p>
						<button title="tambahkan ke cart" type="button" id="btn-add-order" class="btn btn-info btn-block"><i class="fa fa-plus-circle fa-2x"></i></button>
					</div>
				</div>
				<br>
				<div class="row">
					<table class="table table-condensed table-dark-header" id="table-order">
						<thead>
						<tr>
							<th>Nama Item</th>
							<th class="text-right">Harga</th>
							<th class="text-center">Qty</th>
							<th class="text-center">Disc</th>
							<th class="text-right">Sub Total</th>
                            <th class="text-center">Pilihan</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-right">
                            <h4>Total : <span id="total-order">0</span></h4>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-lg-4">
				<!-- SELECT2 -->
				<div class="widget">
					<div class="widget-header">
						<h3><i class="fa fa-edit"></i>Retur</h3> <em> - data retur</em>
					</div>
					<div class="widget-content">
						<p style="margin-top: 10px;">Ditransfer ke :</p>
						<select id="mutasi_to" name="mutasi_to" class="select2">
							<?php foreach ( $all_outlets  as $item):?>
								<option value="<?php echo $item['master_outlet_id'];?>"><?php echo $item['master_outlet_name'];?></option>
							<?php endforeach;?>
						</select>
						<textarea style="margin-top: 10px;" class="form-control" id="mutasi_note" name="mutasi_note" rows="5" cols="30" placeholder="Keterangan"></textarea>
					</div>
				</div>
				<!-- END SELECT2 -->
				<button type="submit" class="btn btn-success btn-block btn-order-submit"><i class="fa fa-check-circle"></i> Simpan</button>
                <br>
                <div class="message"></div>
			</div>
			</form>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
<script type="text/javascript">
	var page = 'mutasi_baru';
</script>