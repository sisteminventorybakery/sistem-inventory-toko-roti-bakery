<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Form_validation $form_validation
 * @property CI_Session session
 * @property Item_model Item_model
 * @property Stok_model Stok_model
 */
class Cms_mutasi_baru extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Item_model');
        $this->load->model('Stok_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_new_mutasi', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','mutasi_baru') ) show_404();
        $data = $this->base_config->panel_setting();
        $output = new stdClass();
        $output->all_outlets = $this->db->where_not_in('master_outlet_id', $this->session->userdata('outlet_id'))->order_by('master_outlet_name','asc')->get('tb_master_outlet')->result_array();
        $output->my_outlets = $this->M_base_config->get_my_outlet();
        $output->categories = $this->db->get('tb_master_category')->result();
        $output->items = $this->Item_model->get_inventory_item($this->session->userdata('outlet_id'));
        $output->jenis = $this->db->get('tb_master_jenis')->result();
        $output->c = $this->db->get('tb_customer')->result();
        $output->customers = $this->db->get('tb_customer')->result();
        $output->data = $data;
        $output->js_files = array();
        $output->css_files = array();
        $this->_example_output($output);
    }

    public function ajax()
    {
        $this->M_base_config->cekaAuth();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $this->form_validation->set_rules('item_id[]', 'item_id', 'required');
        $this->form_validation->set_rules('qty[]', 'qty', 'required');
        $this->form_validation->set_rules('mutasi_to', 'mutasi_to', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = 0;
            $data['message'] = validation_errors();
            echo json_encode($data);
            exit();
        } else {
            $qty_s = $this->input->post('qty');
            $item_s = $this->input->post('item_id');
            $all_total = 0;
            foreach ($item_s as $key => $inv_id) {
                $this->db->where('outlet_id', $this->outlet_id());
                $this->db->where('inventory_id', $inv_id);
                $result = $this->db->get('tb_inventory');
                $item_id = $result->row('item_id');
                $price = $this->Item_model->get_price( $this->outlet_id(), $item_id )['sales'];
                $total = $price * (int)$qty_s[$key];
                $all_total = $all_total + $total;
            }
            $data_mutasi = array(
                'mutasi_date'       => date('Y-m-d H:i:s'),
                'mutasi_status'     => 'tidak',
                'mutasi_desc'       => $this->input->post('mutasi_note'),
                'user_id'           => $this->ion_auth->user()->row()->id,
                'mutasi_total'      => $all_total,
                'mutasi_to'         => $this->input->post('mutasi_to'),
                'mutasi_from'       => $this->outlet_id(),
            );
            $this->db->insert('tb_mutasi', $data_mutasi);
            $mutasi_id = $this->db->insert_id();
            $data_mutasi_detail = array();
            foreach ($item_s as $key => $inv_id) {
                $this->db->where('outlet_id', $this->outlet_id());
                $this->db->where('inventory_id', $inv_id);
                $result = $this->db->get('tb_inventory');
                $item_id = $result->row('item_id');
                $expired = $result->row('inventory_expired');
                $price = $this->Item_model->get_price( $this->outlet_id(), $item_id )['sales'];
                $total = $price * (int)$qty_s[$key];
                $data_mutasi_detail[] = array(
                    'mutasi_detail_price'    => $price,
                    'mutasi_detail_qty'      => $qty_s[$key],
                    'mutasi_detail_total'    => $total,
                    'mutasi_id'              => $mutasi_id,
                    'item_id'                => $item_id,
                    'expired'                => $expired
                );
            }
            $query = $this->db->insert_batch('tb_mutasi_detail', $data_mutasi_detail);
            if ($query) {
                echo json_encode(array('status' => 1, 'message' => 'Sukses Disimpan' ));
                exit();
            } else {
                echo json_encode(array('status' => 0, 'message' => 'Proses gagal, silahkan ulangi lagi'));
                exit();
            }
        }

    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }
}