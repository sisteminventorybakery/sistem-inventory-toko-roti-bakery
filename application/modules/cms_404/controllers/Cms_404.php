<?php 
class Cms_404 extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
	 	$this->load->library('Base_config');
	}

	public function _example_output($output = null)
	{
		$this->load->view('v_404',$output);
	}

    public function index()
    {
        //$this->load->view('_v_login');
        $path = $this->base_config->Asset_back();
        //$path = base_url('themes/tezku-back/king');
        //$this->base_config->tes( $data );
        $this->_example_output((object)array(
        								'output' 	    => '' ,
                                        'theme_path'    =>  $path,
        								'js_files' 	    => array( 
                                                                    $path.'js/jquery/jquery-2.1.0.min.js', 
                                                                    $path.'js/bootstrap/bootstrap.js', 
                                                                    $path.'js/plugins/modernizr/modernizr.js' ) , 
        								'css_files'     => array( 
                                                                    $path.'css/bootstrap.min.css', 
                                                                    $path.'css/font-awesome.min.css', 
                                                                    $path.'css/main.css' )
        								)
        						);
    }
    
}