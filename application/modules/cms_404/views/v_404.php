<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>404 halaman tidak ditemukan | Roti Bakery</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="nurza.cool@gmail.com">
	<!-- CSS -->
<!-- 
	<link href="<?php echo $theme_path;?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $theme_path;?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $theme_path;?>/css/main.css" rel="stylesheet" type="text/css"> 
-->
	<!--[if lte IE 9]>
		<link href="<?php echo $theme_path;?>/css/main-ie.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $theme_path;?>/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!-- Fav and touch icons -->
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; 
?>
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $theme_path;?>/ico/my-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $theme_path;?>/ico/my-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $theme_path;?>/ico/my-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $theme_path;?>/ico/my-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo $theme_path;?>/ico/favicon.png">
</head>

<body>
	<div class="wrapper full-page-wrapper page-error text-center">
		<div class="inner-page">
			<h1>
				<span class="clearfix title">
					<span class="number">404</span> <span class="text">Oops! <br/>Halaman tidak ditemukan</span>
				</span>
			</h1>
			<p>Halaman yang anda cari tidak ditemukan, silahkan kembali ke <a href="<?php echo base_url();?>">halaman utama</a></p>
		<br/><br/>
<br/>			<div>
				<a href="javascript:history.go(-1)" class="btn btn-custom-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
				<a href="<?php echo base_url();?>" class="btn btn-primary"><i class="fa fa-home"></i> Home</a>
			</div>
		</div>
	</div>
	<footer class="footer">&copy; <?php echo date('Y');?> Roti Bakery All Right Reserved</footer>
	<!-- Javascript -->
<!-- 	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.js"></script>
	<script src="assets/js/plugins/modernizr/modernizr.js"></script> -->
</body>

</html>
