<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-header">
			<h2>New Retur Pembeli</h2>
			<em>Cek Nota</em>
		</div>
		<div class="main-content">
			<form id="form" method="post" onsubmit="return false;">
				<div class="col-lg-8">
					<br>
					<div class="row">
						<div class="col-lg-12">
							<div class="message"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							Nota Penjualan 
						</div>
						<div class="col-lg-9">
							<label class="control-inline fancy-radio custom-color-green">
								<input value="tb_sales" checked="" type="radio" name="retur_pembeli">
								<span><i></i>Penjualan</span>
							</label>
							<label class="control-inline fancy-radio custom-color-green">
								<input value="tb_order" type="radio" name="retur_pembeli">
								<span><i></i>Pemesanan</span>
							</label>
						</div>
						<div class="col-lg-8">
							<input type="number" class="form-control" name="sales-id" id="sales-id" placeholder="masukkan angka saja">
						</div>
						<div class="col-lg-4">
							<button type="button" class="btn btn-info btn-block" id="btn-cek-nota">Cek Nomor Penjualan</button>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<div class="msg"></div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<table class="table table-condensed table-dark-header" id="table-sales">
								<thead>
									<tr>
										<td>Nota</td>
										<td>Pembeli</td>
										<td>Diskon Nota</td>
										<td>Diskon Produk</td>
										<td>Total</td>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							<table class="table table-condensed table-dark-header" id="table-sales-detail">
								<thead>
									<tr>
										<th>Item</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Diskon</th>
										<th>Total</th>
										<th>EXP</th>
										<th>Retur Roti</th>
										<th style="width: 30">Jml Retur Roti</th>
										<th style="width: 30">Retur Uang</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<!-- SELECT2 -->
					<div class="widget">
						<div class="widget-header">
							<h3><i class="fa fa-edit"></i>Retur</h3> <em> - data retur</em>
						</div>
						<div class="widget-content">
							<textarea style="margin-top: 10px;" class="form-control" id="retur_note" name="retur_note" rows="5" cols="30" placeholder="Keterangan"></textarea>
						</div>
					</div>
					<!-- END SELECT2 -->
					<button type="submit" class="btn btn-success btn-block btn-retur-submit disabled"><i class="fa fa-check-circle"></i> Submit</button>
					<br>
					<div id="message"></div>
				</div>
			</form>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->

<!-- Modal -->
<div id="modal-retur" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Stok Roti</h4>
			</div>
			<form id="form-customer" method="post">
			<div class="modal-body">
				<table class="table table-condensed table-dark-header" id="table-sales-modal">
					<thead>
						<tr>
							<th>Item</th>
							<th>Stok</th>
							<th>Harga</th>
							<th style="width: 30">Pilih</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var page = 'new_retur_pembeli';


</script>