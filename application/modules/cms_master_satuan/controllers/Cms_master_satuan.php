<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_master_satuan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {

        try {
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $user = $this->ion_auth->user()->row();
            $crud = $this->base_config->groups_access('master_satuan');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_satuan');
            $crud->set_subject('Master Satuan');
            $crud->fields('master_satuan_name', 'master_satuan_desc');
            $crud->display_as('master_satuan_name', 'Nama Master Satuan');
            $crud->display_as('master_satuan_desc', 'Deskripsi');
            $crud->set_rules('master_satuan_name', 'Nama Master Satuan', 'required');
            $crud->unset_texteditor('master_satuan_desc');
            $crud->field_type('created','hidden');
            $crud->display_as('created', '<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('created', array($this, '_callback_id'));
            $crud->unset_print();
            $state = $crud->getState();
            if( $state == 'export' ){
                $crud->columns('master_satuan_name', 'master_satuan_desc');
            }else{
                $crud->columns('created','master_satuan_name', 'master_satuan_desc');
            }
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    public function _callback_id($value, $row)
    {
        $id = $row->master_satuan_id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

}