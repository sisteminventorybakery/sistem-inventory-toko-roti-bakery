<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_stok extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        //$this->session->keep_flashdata('tes');
        //if ($this->session->flashdata('tes')) $this->base_config->tes($this->session->flashdata('tes'));
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('master_item');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_item');
            $crud->set_subject('Stok Item');
            $crud->order_by('item_name', 'asc');
            $crud->columns('item_name','stok', 'item_satuan', 'item_category', 'item_jenis');
            $crud->edit_fields('created','stok');
            $crud->field_type('created','hidden', date('Y-m-d H:i:s'));
            $crud->change_field_type('stok', 'integer');
            $crud->required_fields('stok');
            $crud->display_as('item_name', 'Nama Item');
            $crud->display_as('item_price', 'Harga_jual');
            $crud->display_as('item_satuan', 'Satuan');
            $crud->display_as('item_category', 'Kategori');
            $crud->display_as('item_jenis', 'Jenis');
            $crud->set_relation('item_category', 'tb_master_category', 'master_category_name');
            $crud->set_relation('item_jenis', 'tb_master_jenis', 'master_jenis_name');
            $crud->set_relation('item_satuan', 'tb_master_satuan', 'master_satuan_name');
            $crud->callback_before_update(array($this, '_callback_before_update'));
            $crud->callback_column('stok', array($this, '_callback_column_stok'));
            $crud->callback_edit_field('stok',array($this,'_set_input_stok'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_before_update($post_array, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $result = $this->db->get('tb_inventory',1)->result_array();
        if( $result ){
            $data = [
                'inventory_stok' => $post_array['stok'],
            ];
            $this->db->where('item_id', $primary_key);
            $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
            $this->db->update('tb_inventory', $data);
        }else{
            $data = [
                'item_id' => $primary_key,
                'outlet_id' => $this->session->userdata('outlet_id'),
                'inventory_stok' => $post_array['stok'],
            ];
            $this->db->insert('tb_inventory', $data);
        }
        unset($post_array['stok']);
        return $post_array;
    }

    /*public function _callback_column_stok($value, $row)
    {
        $total_penjualan = $this->get_total_penjualan($row->item_id);
        $total_pemesanan = $this->get_total_pemesanan($row->item_id);
        $mutasi_keluar = $this->get_total_mutasi_keluar($row->item_id);
        $mutasi_masuk = $this->get_total_mutasi_masuk($row->item_id);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $row->item_id);
        $stok_awal = (int)$this->db->get('tb_inventory',1)->row('inventory_stok');
        $sisa_stok = $stok_awal - ($total_pemesanan+$total_penjualan+$mutasi_keluar) + $mutasi_masuk;
        return "stok awal : $stok_awal<br>penjualan(-) : $total_penjualan<br>pemesanan(-) : $total_pemesanan<br>mutasi masuk(+) : $mutasi_masuk<br>mutasi keluar(-) : $mutasi_keluar<br> sisa stok : $sisa_stok";
    }*/

    public function _callback_column_stok($value, $row)
    {
        $this->db->where('item_id', $row->item_id);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $stok = (int)$this->db->get('tb_inventory',1)->row('inventory_stok');
        return number_format($stok);
    }

    public function _set_input_stok($value, $primary_key)
    {
        $this->db->where('item_id', $primary_key);
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $price = (int)$this->db->get('tb_inventory')->row('inventory_stok');
        return '<input class="numeric form-control" name="stok" type="number" value="'.$price.'">';
    }

    protected function get_total_penjualan($item_id)
    {
        $this->db->select_sum('tb_sales_detail.sales_detail_qty','total');
        $this->db->join('tb_sales','tb_sales.sales_id=tb_sales_detail.sales_id');
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $item_id);
        return (int)$this->db->get('tb_sales_detail',1)->row('total');
    }

    protected function get_total_pemesanan($item_id)
    {
        $this->db->select_sum('tb_order_detail.order_detail_qty','total');
        $this->db->join('tb_order','tb_order.order_id=tb_order_detail.order_id');
        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $item_id);
        return (int)$this->db->get('tb_order_detail',1)->row('total');
    }

    protected function get_total_mutasi_masuk($item_id)
    {
        $this->db->select_sum('tb_mutasi_detail.mutasi_detail_qty','total');
        $this->db->join('tb_mutasi','tb_mutasi.mutasi_id=tb_mutasi_detail.mutasi_id');
        $this->db->where('mutasi_to', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $item_id);
        return (int)$this->db->get('tb_mutasi_detail',1)->row('total');
    }

    protected function get_total_mutasi_keluar($item_id)
    {
        $this->db->select_sum('tb_mutasi_detail.mutasi_detail_qty','total');
        $this->db->join('tb_mutasi','tb_mutasi.mutasi_id=tb_mutasi_detail.mutasi_id');
        $this->db->where('mutasi_from', $this->session->userdata('outlet_id'));
        $this->db->where('item_id', $item_id);
        return (int)$this->db->get('tb_mutasi_detail',1)->row('total');
    }

}