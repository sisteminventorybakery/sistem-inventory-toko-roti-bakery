<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_master_retail extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $category_id = '22'; //RETAIL
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('master_item');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_item');
            $crud->set_subject('Master Retail');
            $crud->where('item_category', $category_id);
            $crud->add_fields('item_name', 'item_satuan', 'item_category', 'item_jenis');
            $crud->edit_fields('item_name', 'item_satuan', 'item_category', 'item_jenis');
            $crud->change_field_type('item_category','hidden', $category_id);
            $crud->required_fields('item_name');
            $crud->display_as('item_name', 'Nama Item');
            $crud->display_as('item_price', 'Harga_jual');
            $crud->display_as('item_satuan', 'Satuan');
            $crud->display_as('item_category', 'Kategori');
            $crud->display_as('item_jenis', 'Jenis');
            $crud->set_relation('item_jenis', 'tb_master_jenis', 'master_jenis_name');
            $crud->set_relation('item_satuan', 'tb_master_satuan', 'master_satuan_name');
            $crud->field_type('created', 'hidden');
            $crud->display_as('created', '<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('created', array($this, '_callback_id'));
            $crud->unset_print();
            $state = $crud->getState();
            if( $state == 'export' ){
                $crud->columns('item_name', 'item_satuan', 'item_jenis');
            }else{
                $crud->columns('created','item_name', 'item_satuan', 'item_jenis');
            }
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_id($value, $row)
    {
        $id = $row->item_id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

}