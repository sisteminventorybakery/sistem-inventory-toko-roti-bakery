<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title;?></title>
    <style type="text/css">

        table{
            font-size: 10pt;
            width: 100%;
        }

        table.border {
            border-collapse: collapse;
            width: 100%;
        }

        table.border, table.border th, table.border td {
            border: 1px solid black;
        }

        table.border th, table.border td{
            padding: 5px;
        }

        table.border td{
            vertical-align: middle;
        }

    </style>
</head>
<body>
<div id="">
    <div id="">
        <div class="">
            <div class="">
<p style="text-align: center;"><strong><?php echo $title;?></strong></p>
                <p>&nbsp;</p>
                <table border="0" width="50%">
                    <tbody>
                    <tr>
                        <td>Outlet</td>
                        <td>:</td>
                        <td><?php echo $orders['outlet'];?></td>
                    </tr>
                    <tr>
                        <td>Jenis Retur</td>
                        <td>:</td>
                        <td><?php echo $orders['type'];?></td>
                    </tr>
                    <tr>
                        <td>Status (disetujui)</td>
                        <td>:</td>
                        <td><?php echo $orders['status'];?></td>
                    </tr>
                    <tr>
                        <td>Kasir</td>
                        <td>:</td>
                        <td><?php echo $orders['user'];?></td>
                    </tr>
                    <tr>
                        <td>Tgl Dari</td>
                        <td>:</td>
                        <td><?php echo $orders['from'];?></td>
                    </tr>
                    <tr>
                        <td>Tgl Sampai</td>
                        <td>:</td>
                        <td><?php echo $orders['to'];?></td>
                    </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                <?php /** @var array $result */
                if( count( $result ) > 0 ):?>
                    <table class="border" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nota</th>
                            <th>Tanggal</th>
                            <th>Outlet</th>
                            <th>Jenis</th>
                            <th>Catatan</th>
                            <th align="centre">Disetujui</th>
                            <th align="centre">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result as $k=>$v):?>
                            <tr>
                                <td align="centre"><?php echo $k+1;?></td>
                                <td><?php echo $v['nota'];?></td>
                                <td><?php echo $v['date'];?></td>
                                <td><?php echo $v['outlet'];?></td>
                                <td><?php echo $v['type'];?></td>
                                <td><?php echo $v['note'];?></td>
                                <td align="centre"><?php echo $v['status'];?></td>
                                <td align="right"><?php echo $v['total'];?></td>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                           <th align="centre" colspan="7">Total</th>
                            <th align="right"><?php echo $total;?></th>
                        </tr>
                        </tbody>
                    </table>
                <?php else:?>
                    <h3 align="center">No Data</h3>
                <?php endif;?>
            </div>
        </div>

    </div>
</div>
</body>
</html>
