<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_report_retur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output['theme_path'] = $path;
        $output['js_files'] = array();
        $output['css_files'] = array();
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_report', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','report_retur') ) show_404();
        $data = $this->base_config->panel_setting();
        $user = $this->ion_auth->user()->row();
        $f_outlet = $this->input->get('outlet');
        $f_status = $this->input->get('status');
        $f_type = $this->input->get('type');
        $f_user = $this->input->get('user');
        $f_from = $this->input->get('from');
        $f_to = $this->input->get('to');

        $result = array();
        $all_total = 0;
        $filter_outlet = 'Kosong';
        $filter_user = 'Kosong';
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_retur.outlet_id');
        $this->db->join('tb_user','tb_user.id=tb_retur.user_id');
        if( $f_status ) $this->db->where('retur_status', $f_status);
        if( $f_type ) $this->db->where('retur_type', $f_type);
        if( $f_user ) $this->db->where('user_id', $f_user);
        if( $f_outlet ) $this->db->where('outlet_id', $f_outlet);
        if( $f_from ) $this->db->where('retur_date >=', $f_from);
        if( $f_to ) $this->db->where('retur_date <=', $f_to);
        $this->db->order_by('retur_date','desc');
        $result_row = $this->db->get('tb_retur')->result();

        //$this->M_base_config->tes($result_row);

        foreach ($result_row as $v){
            $total = (int)$v->retur_total;
            $result[] = array(
                'id'        => $v->retur_id,
                'nota'      => $this->_callback_nota($v),
                'total'     => number_format($total),
                'status'     => $this->_callback_status($v),
                'date'      => $v->retur_date,
                'note'      => $v->retur_desc,
                'outlet'    => $v->master_outlet_name,
                'type'      => $v->retur_type,
                'detail'    => $this->_callback_detail($v)
            );
            $all_total += $total;
            $filter_user = $v->username;
        }
        $orders = array(
            'outlet' => ( $f_outlet ? $filter_outlet : 'Semua'),
            'type' => ( $f_type ? $f_type : 'Semua'),
            'status' => ( $f_status ? $f_status : 'Semua'),
            'user' => ( $f_user ? $filter_user : 'Semua'),
            'from' => ( $f_from ? $f_from : 'Semua'),
            'to' => ( $f_to ? $f_to : 'Semua'),
        );

        $data['orders'] = $orders;
        $data['total'] = number_format($all_total);
        $data['title'] = 'LAPORAN RETUR';
        $data['result'] = $result;
        $data['user'] = $this->_get_user();
        $data['outlet'] = $this->_get_outlet();

        //$this->M_base_config->tes($data);

        $export = $this->input->get('export');
        if($export){
            switch ( $export ){
                case 'pdf':
                    $this->load->view( 'v_report_template', $data );
                    $html = $this->output->get_output();
                    $dompdf = new \Dompdf\Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4','landscape');
                    $dompdf->render();
                    $dompdf->stream("report.pdf",array('Attachment'=>0));
                    exit();
                    break;
                case 'excel':
                    header("Content-Type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename = report.xls");
                    $this->load->view( 'v_report_template', $data );
                    //exit();
                    break;
            }
        }else{
            $this->_example_output($data);
        }

    }

    protected  function _get_outlet()
    {
        $res = $this->db->get('tb_master_outlet')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->master_outlet_name,
                'id' => $v->master_outlet_id
            );
        }
        return $tmp;
    }

    protected  function _get_user()
    {
        $res = $this->db->get('tb_user')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->username,
                'id' => $v->id
            );
        }
        return $tmp;
    }

    protected function _callback_status($row)
    {
        $this->M_base_config->cekaAuth();
        if (strtolower($row->retur_status) == 'ya')
            $active = '<span class="label label-success">' . 'Ya' . '</span>';
        elseif(strtolower($row->retur_status) == 'ditolak')
            $active = '<span class="label label-warning">' . 'Ditolak' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'Tidak' . '</span>';
        return $active;
    }

    protected function _callback_detail($row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail retur" href="'.base_url('cms/retur_pembeli/export/pdf/'. $row->retur_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    protected function _callback_nota($row)
    {
        $this->M_base_config->cekaAuth();
        return 'RT-'.sprintf("%04s", $row->retur_id);
    }
}