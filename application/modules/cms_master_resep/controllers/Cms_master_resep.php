<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_master_resep extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        try {
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $user = $this->ion_auth->user()->row();
            $crud = $this->base_config->groups_access('master_resep');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_resep');
            $crud->set_subject('Master Resep');
            $crud->columns('master_resep_name', 'master_resep_desc', 'nama_bahan', 'master_resep_estimasi', 'master_resep_quantity','created');
            $crud->fields('master_resep_name', 'master_resep_desc', 'nama_bahan', 'master_resep_estimasi', 'master_resep_quantity');
            $crud->display_as('master_resep_name', 'Nama Resep');
            $crud->display_as('master_resep_desc', 'Deskripsi');
            $crud->display_as('nama_bahan', 'Nama Bahan');
            $crud->display_as('master_resep_estimasi', 'Estimasi jam Pembuatan');
            $crud->display_as('master_resep_quantity', 'Quantity Per Estimasi');
            $crud->set_relation_n_n('nama_bahan', 'tb_terms', 'tb_master_bahan', 'post_id', 'category_id', 'master_bahan_name');
            $crud->set_rules('master_resep_name', 'Nama Master Resep', 'required');
            $crud->unset_texteditor('master_resep_desc');
            $crud->field_type('created','hidden');
            $crud->display_as('created', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $crud->callback_column('created', array($this, '_callback_id'));
            $crud->unset_print();
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_id($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="'.$row->master_resep_id.'" name="'.$row->master_resep_id.'" id="'.$row->master_resep_id.'" data-md-icheck />';
    }


}