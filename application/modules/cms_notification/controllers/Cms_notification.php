<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_notification extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->lang->load('auth');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        try {
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $user = $this->ion_auth->user()->row();
            $this->db->where('notification_user', $user->id)->update('tb_notification' , array('notification_status'=>'inactive'));
            $crud = new grocery_CRUD();
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_notification');
            $crud->set_subject('Notifications');
            $crud->columns('notification_link','notification_type', 'notification_desc', 'notification_date');
            $crud->field_type('notification_link','hidden');
            $crud->where('notification_user', $user->id);
            $crud->order_by('notification_date', 'desc');
            $crud->display_as('notification_link', '<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('notification_link', array($this, '_callback_id'));
            $crud->unset_add();
            $crud->unset_edit();
            $crud->callback_column('notification_date', array($this, '_notif_date'))->callback_column('notification_status', array($this, '_callback_notification_type'));
            $crud->callback_before_insert(array($this, '_set_callback_before_insert'));
            $crud->callback_before_update(array($this, '_set_callback_before_update'));
            $crud->unset_print();
            $crud->set_composer(false);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    public function _callback_id($value, $row)
    {
        $id = $row->notification_id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

    public function _notif_date($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->notification_status == 'active') {
            $status = '<span class="label label-success">' . $row->notification_status . '</span>';
        } else {
            $status = '<span class="label label-warning">' . $row->notification_status . '</span>';
        }
        return lang('in_notif') . " : " . $this->base_config->timeAgo($value) . "<br>" . $status;
    }

    public function _callback_notification_type($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="' . $row->notification_id . '" name="' . $row->notification_id . '" id="' . $row->notification_id . '" data-md-icheck />';
    }


}