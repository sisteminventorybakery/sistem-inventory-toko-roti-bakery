<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_kredit extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('kredit');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_kredit');
            $crud->set_subject('Kredit');
            $crud->display_as('customer_id', 'Nama Customer');
            $crud->set_relation('customer_id', 'tb_customer', 'customer_name');
            $crud->field_type('created','hidden');
            $crud->display_as('created', '<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('created', array($this, '_callback_id'));
            $crud->callback_column('dp', array($this, '_callback_sparator'));
            $crud->callback_column('jumlah_hutang', array($this, '_callback_sparator'));
            $crud->unset_print();
            $crud->unset_add();
            $state = $crud->getState();
            if( $state == 'export' ){
                $crud->columns('dp','jumlah_hutang','tanggal','jatuh_tempo','customer_id','jenis');
            }else{
                $crud->columns('created','dp','jumlah_hutang','tanggal','jatuh_tempo','customer_id','jenis');
            }
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    public function _callback_sparator($v)
    {
        return number_format($v);
    }

    public function _callback_id($value, $row)
    {
        $id = $row->kredit_id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

}