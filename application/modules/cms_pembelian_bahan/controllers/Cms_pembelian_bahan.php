<?php

/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_Input $input
 * @property CI_User_agent $agent
 * @property CI_Form_validation $form_validation
 * @property CI_Session session
 * @property Stok_model Stok_model
 */
class Cms_pembelian_bahan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Stok_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_new_pembelian_bahan', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','pembelian_bahan') ) show_404();
        $data = $this->base_config->panel_setting();
        $output = new stdClass();
        $output->my_outlets = $this->M_base_config->get_my_outlet();
        $output->categories = $this->db->get('tb_master_category')->result();
        $this->db->order_by('master_bahan_name', 'asc');
        $output->items = $this->db->get('tb_master_bahan')->result();
        $output->jenis = $this->db->get('tb_master_jenis')->result();
        $output->customers = $this->db->get('tb_customer')->result();
        $output->data = $data;
        $output->js_files = array();
        $output->css_files = array();
        $this->_example_output($output);
    }

    public function ajax()
    {
        if (!$this->ion_auth->logged_in()) {
            echo json_encode(array('status' => 0, 'message' => 'Harus login untuk mengakses halaman ini'));
            exit();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $this->form_validation->set_rules('order_date', 'order_date', 'required');
        $this->form_validation->set_rules('item_id[]', 'item', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = 0;
            $data['message'] = validation_errors();
            echo json_encode($data);
            exit();
        } else {
            $qty_s = $this->input->post('qty');
            $item_s = $this->input->post('item_id');
            $discount = (int)$this->input->post('order_discount');
            $all_total = 0;

            foreach ($item_s as $key => $item_id) {
                $this->db->where('master_bahan_id', $item_id);
                $price_result = $this->db->get('tb_master_bahan',1)->result_array();
                $price = 0;
                $disc = 0;
                $disc_value = 0;
                foreach ($price_result as $v) {
                    $price = (int)$v['harga'];
                    $disc = 0;
                }
                if( $disc > 0 ) $disc_value = $price * ($disc/100);
                $total = ( $price - $disc_value ) * (int)$qty_s[$key];
                $all_total = $all_total + $total;
            }
            $data_order = array(
                'tanggal'           => $this->input->post('order_date'),
                'keterangan'        => $this->input->post('order_note'),
                'user_id'           => $this->ion_auth->user()->row()->id,
                'total'             => $all_total - $discount,
            );
            $this->db->insert('tb_pembelian_bahan', $data_order);
            $order_id = $this->db->insert_id();
            $data_order_detail = array();
            foreach ($item_s as $key => $item_id) {
                $this->Stok_model->add_bahan( $item_id, $qty_s[$key] );
                $this->db->where('master_bahan_id', $item_id);
                $price_result = $this->db->get('tb_master_bahan',1)->result_array();
                $price = 0;
                $disc = 0;
                $disc_value = 0;
                foreach ($price_result as $v) {
                    $price = (int)$v['harga'];
                    $disc = 0;
                }
                if( $disc > 0 ) $disc_value = $price * ($disc/100);
                $total = ( $price - $disc_value ) * (int)$qty_s[$key];
                $data_order_detail[] = array(
                    'harga'                 => $price,
                    'jumlah'                => $qty_s[$key],
                    'total'                 => $total,
                    'pembelian_bahan_id'    => $order_id,
                    'bahan_id'              => $item_id
                );
            }
            $query = $this->db->insert_batch('tb_pembelian_bahan_detail', $data_order_detail);
            if ($query) {
                echo json_encode(array('status' => 1, 'message' => 'Sukses Disimpan' ));
                exit();
            } else {
                echo json_encode(array('status' => 0, 'message' => 'Proses gagal, silahkan ulangi lagi'));
                exit();
            }
        }

    }
}