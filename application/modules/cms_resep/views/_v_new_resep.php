<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-header">
			<h2>Resep</h2>
			<em>...</em>
		</div>
		<div class="main-content">
			<form id="form" method="post" onsubmit="return false;">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-10">
						<p>Nama Bahan</p>
						<select id="item_id" class="select2">
							<?php foreach ( $bahan  as $item):?>
								<option value="<?php echo $item['master_bahan_id'];?>"
										data-discount="0"
										data-price="<?php echo $item['harga'];?>"><?php echo $item['master_bahan_name'].' ('. $item['satuan_kecil'].')';?>
								</option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2">
						<p>Harga Produk</p>
						<input type="text" class="form-control" id="price_product" value="0" readonly>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-2">
						<p>Jumlah</p>
						<input type="text" class="form-control numeric2" value="1" name="qty" id="qty">
					</div>
					<div class="col-lg-3">
						<p>Disc.Produk Rp</p>
						<input type="text" class="form-control numeric2" value="0" name="discount_product" id="discount_product" readonly>
					</div>
					<div class="col-lg-3">
						<p>Disc.Produk %</p>
						<input type="text" class="form-control numeric2" value="0" id="discount_product_percent" readonly>
					</div>
					<div class="col-lg-2">
						<p>Sub Total</p>
						<input type="text" class="form-control" value="0" id="order_product_subtotal" readonly>
					</div>
					<div class="col-lg-2">
						<p>-</p>
						<button title="tambahkan ke cart" type="button" id="btn-add-order" class="btn btn-info btn-block"><i class="fa fa-plus-circle fa-2x"></i></button>
					</div>
				</div>
				<br>
				<div class="row">
					<table class="table table-condensed table-dark-header" id="table-order">
						<thead>
						<tr>
							<th>Nama Item</th>
							<th class="text-right">Harga</th>
							<th class="text-center">Qty</th>
							<th class="text-center">Disc</th>
							<th class="text-right">Sub Total</th>
                            <th class="text-center">Pilihan</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-right">
                            <h4>Total : <span id="total-order">0</span></h4>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-lg-4">
				<!-- SELECT2 -->
				<div class="widget">
					<div class="widget-header">
						<h3><i class="fa fa-edit"></i>Data Resep</h3> <em> - </em>
					</div>
					<div class="widget-content">
						<p>Nama item yang akan dibuat</p>
						<select id="item_id2" name="roti_id" class="select2">
							<?php foreach ( $items  as $item):?>
								<option value="<?php echo $item->item_id;?>"
										data-discount="0"
										data-price="0"><?php echo $item->item_name;?>
								</option>
							<?php endforeach;?>
						</select>
						<hr>
						<p style="margin-top: 10px;">Tanggal Expired</p>
                        <div style="margin-top: 10px;" class="input-group date form_date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" id="order_date" name="order_date" class="form-control" placeholder="Tanggal Expired" required>
                        </div>
						<textarea style="margin-top: 10px;" class="form-control" id="order_note" name="order_note" rows="5" cols="30" placeholder="Keterangan"></textarea>
                        <p style="margin-top: 10px;">Jumlah Produksi Roti</p>
                        <input style="margin-top: 10px;" type="number" class="form-control" name="order_jumlah" placeholder="Jumlah" required>
					</div>
				</div>
				<!-- END SELECT2 -->
				<button type="submit" class="btn btn-success btn-block btn-order-submit"><i class="fa fa-check-circle"></i> Simpan</button>
                <br>
                <div class="message"></div>
			</div>
			</form>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
