<html>
	<head>
		<style>
			table{
				font-size: 10px;
			}
			.head{
				border-bottom: 1px solid;
			}
		</style>
	</head>
	<body onload="window.print()">
		<table style="width: 100%">
			<tr>
				<td colspan="5" align="center" style="border-bottom: 1px solid"><?php echo $header_struk?></td>
			</tr>
			<tr>
				<td colspan="3"><?php echo date('Y-m-d h:i:s')?></td>
				<td colspan="2" align="right"><?=$data[0]->master_outlet_name?></td>
			</tr>
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			<tr>
				<td align="right" colspan="3">RETUR</td>
			</tr>
            <tr>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
                <td class="head" align="center"></td>
            </tr>
			<tr>
				<td class="head" align="center">NAMA</td>
				<td class="head" align="center">HARGA</td>
				<td class="head" align="center">QTY</td>
				<td class="head" align="center">SUBTOTAL</td>
			</tr>
			<?php 
			$jumlah_retur = 0;
			foreach($data as $dt){
			$jumlah_retur += $dt->retur_detail_qty;
			?>
				<tr>
					<td align="center"><?php echo $dt->item_name?></td>
					<td align="right"><?php echo number_format($dt->retur_detail_price, 0, ',', '.');?></td>
					<td align="right"><?php echo $dt->retur_detail_qty?></td>
					<td align="right"><?php echo number_format($dt->retur_detail_total, 0, ',', '.')?></td>
				</tr>
			<?php }
			?>
				<tr>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
					<td class="head" align="center"></td>
				</tr>
				<tr>
					<td align="right" colspan="3">JUMLAH RETUR</td>
					<td align="right" colspan="2"><?php echo number_format($jumlah_retur, 0, ',', '.')?></td>
				</tr>
				<tr>
					<td align="right" colspan="3">TOTAL RETUR</td>
					<td align="right" colspan="2"><?php echo number_format($data[0]->retur_total, 0, ',', '.')?></td>
				</tr>
		</table>
	</body>
</html>