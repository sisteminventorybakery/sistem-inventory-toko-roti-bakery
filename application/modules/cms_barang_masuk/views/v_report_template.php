<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title;?></title>
    <style type="text/css">

        table{
            font-size: 10pt;
            width: 100%;
        }

        table.border {
            border-collapse: collapse;
            width: 100%;
        }

        table.border, table.border th, table.border td {
            border: 1px solid black;
        }

        table.border th, table.border td{
            padding: 5px;
        }

        table.border td{
            vertical-align: middle;
        }

    </style>
</head>
<body>
<div id="">
    <div id="">
        <div class="">
            <div class="">
<p style="text-align: center;"><strong><?php echo $title;?></strong></p>
                <p>&nbsp;</p>
                <table border="0" width="50%">
                    <tbody>
                    <tr>
                        <td>Dari Outlet</td>
                        <td>:</td>
                        <td><?php echo $orders['outlet_from'];?></td>
                    </tr>
                    <tr>
                        <td>Ke Outlet</td>
                        <td>:</td>
                        <td><?php echo $orders['outlet_to'];?></td>
                    </tr>
                    <tr>
                        <td>Nama Kasir</td>
                        <td>:</td>
                        <td><?php echo $orders['user'];?></td>
                    </tr>
                    <tr>
                        <td>No Pemesanan</td>
                        <td>:</td>
                        <td><?php echo $orders['nota'];?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Pemesanan</td>
                        <td>:</td>
                        <td><?php echo $orders['date'];?></td>
                    </tr>
                    <tr>
                        <td>Status (Disetujui)</td>
                        <td>:</td>
                        <td><?php echo $orders['status'];?></td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td><?php echo $orders['note'];?></td>
                    </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                <?php /** @var array $result */
                if( count( $result ) > 0 ):?>
                    <table class="border" width="100%">
                        <thead>
                        <tr>
                            <th align="center">No</th>
                            <th align="center">Nama Item</th>
                            <th align="center">Harga</th>
                            <th align="center">Qty</th>
                            <th align="center">Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result as $v):?>
                        <tr>
                            <td align="center"><?php echo $v['no']?></td>
                            <td><?php echo $v['item'];?></td>
                            <td align="right"><?php echo $v['price'];?></td>
                            <td align="center"><?php echo $v['qty'];?></td>
                            <td align="right"><?php echo $v['subtotal'];?></td>
                        </tr>
                        <?php endforeach;?>
                        <tr>
                            <th align="center" colspan="2">Total</th>
                            <th align="right"><?php echo $total_price;?></th>
                            <th align="center"><?php echo $total_qty;?></th>
                            <th align="right"><?php echo $total;?></th>
                        </tr>
                        </tbody>
                    </table>
                <?php else:?>
                    <h3 align="center">No Data</h3>
                <?php endif;?>
            </div>
        </div>

    </div>
</div>
</body>
</html>
