<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Output $output
 * @property CI_Session session
 * @property Stok_model Stok_model
 * @property Item_model Item_model
 */
class Cms_barang_masuk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Stok_model');
        $this->load->model('Item_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('barang_masuk');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_mutasi');
            $crud->set_subject('Barang Masuk');
            $crud->where('mutasi_to', $this->session->userdata('outlet_id'));
            $crud->change_field_type('mutasi_to','hidden', $this->session->userdata('outlet_id'));
            $crud->order_by('mutasi_date','desc');
            $crud->columns('mutasi_id','nota','mutasi_date','mutasi_from','mutasi_total','mutasi_desc','mutasi_status','created');
            $crud->edit_fields('mutasi_status');
            $crud->add_fields('mutasi_date','mutasi_from','mutasi_to','mutasi_status');
            $crud->set_relation('mutasi_from', 'tb_master_outlet', 'master_outlet_name');
            $crud->display_as('mutasi_from','Barang dari');
            $crud->display_as('mutasi_to','Ditransfer Ke');
            $crud->display_as('mutasi_status','Disetujui');
            $crud->display_as('created','Detail');
            $crud->display_as('mutasi_id','ID');
            $crud->callback_column('created', array($this,'_callback_detail'));
            $crud->callback_column('mutasi_status', array($this,'_callback_status'));
            $crud->callback_column('nota', array($this,'_callback_nota'));
            $crud->callback_column('mutasi_total', array($this,'_callback_total'));
            $crud->callback_before_insert(array($this,'_callback_before_insert'));
            $crud->callback_before_update(array($this,'_callback_before_update'));
            $crud->callback_after_update(array($this,'_callback_after_update'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_export();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_total($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return number_format($value);
    }

    public function _callback_after_update($post_array,$primary_key) //TAMBAH DAN KURANGI STOK
    {
        $this->db->join('tb_mutasi','tb_mutasi.mutasi_id=tb_mutasi_detail.mutasi_id');
        $this->db->where('tb_mutasi_detail.mutasi_id', $primary_key);
        $results = $this->db->get('tb_mutasi_detail')->result_array();
        if( strtolower($post_array['mutasi_status']) == 'ya' ){
            foreach ($results as $item) {
                $this->Stok_model->add_inventory_outlet( $item['mutasi_to'], $item['item_id'], $item['expired'],$item['mutasi_detail_qty'] );
                $this->Stok_model->remove_inventory_outlet( $item['mutasi_from'], $item['item_id'], $item['expired'],$item['mutasi_detail_qty'] );
            }
        }
    }

    public function _callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();
        if( empty($post_array['mutasi_date']) ) $post_array['mutasi_date'] = date('Y-m-d h:i:s');
        if( empty($post_array['mutasi_status']) ) $post_array['mutasi_status'] = 'tidak';
        return $post_array;
    }

    public function _callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        if( empty($post_array['mutasi_date']) ) $post_array['mutasi_date'] = date('Y-m-d h:i:s');
        if( empty($post_array['mutasi_status']) ) $post_array['mutasi_status'] = 'tidak';
        return $post_array;
    }

    public function _callback_status($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->mutasi_status == 'YA')
            $active = '<span class="label label-success">' . 'YA' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'TIDAK' . '</span>';

        return $active;
    }

    public function _callback_detail($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail Mutasi" href="'.base_url('cms/barang_masuk/export/pdf/'. $row->mutasi_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    public function _callback_nota($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return 'MT-'.sprintf("%04s", $row->mutasi_id);
    }

    public function export()
    {
        $this->M_base_config->cekaAuth();
        $export = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $orders = array();
        $result = array();
        $total = 0;
        $total_qty = 0;
        $total_price = 0;
        $this->db->join('tb_mutasi','tb_mutasi.mutasi_id=tb_mutasi_detail.mutasi_id');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_mutasi_detail.item_id');
        $this->db->join('tb_user','tb_user.id=tb_mutasi.user_id');
        $this->db->where('tb_mutasi_detail.mutasi_id', $id);
        $result_row = $this->db->get('tb_mutasi_detail')->result();
        foreach ($result_row as $k => $v){
            $price      = (int)$v->mutasi_detail_price;
            $qty        = (int)$v->mutasi_detail_qty;
            $subtotal   = (int)$v->mutasi_detail_total;
            $result[]   = array(
                'no'                => $k+1,
                'item'              => $v->item_name,
                'price'             => number_format($price),
                'qty'               => number_format($qty),
                'subtotal'          => number_format($subtotal),
            );
            $total_qty += $qty;
            $total_price += $price;
            $total = (int)$v->mutasi_total;
            $orders = array(
                'id'        => $v->mutasi_id,
                'nota'      => 'MT-'.sprintf("%04s", $v->mutasi_id),
                'total'     => $total,
                'status'    => $v->mutasi_status,
                'date'      => $v->mutasi_date,
                'note'      => $v->mutasi_desc,
                'user'      => $v->username,
                'mutasi_from' => $v->mutasi_from,
                'mutasi_to' => $v->mutasi_to,
            );
        }

        $orders['outlet_from'] = $this->db->where('master_outlet_id', $orders['mutasi_from'])->get('tb_master_outlet',1)->row('master_outlet_name');
        $orders['outlet_to'] = $this->db->where('master_outlet_id', $orders['mutasi_to'])->get('tb_master_outlet',1)->row('master_outlet_name');

        $data['result']     = $result;
        $data['total']      = number_format($total);
        $data['total_qty']              = number_format($total_qty);
        $data['total_price']            = number_format($total_price);
        $data['orders']     = $orders;
        $data['title'] = 'LAPORAN MUTASI';

        //$this->M_base_config->tes($data);

        switch ( $export ){
            case 'pdf':
                $this->load->view( 'v_report_template', $data );
                $html = $this->output->get_output();
                $dompdf = new \Dompdf\Dompdf();
                $dompdf->load_html($html);
                $dompdf->set_paper('A4', 'potrait');
                $dompdf->render();
                $dompdf->stream("report.pdf",array('Attachment'=>0));
                exit();
                break;
            case 'excel':
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename = report.xls");
                $this->load->view( 'v_report_template', $data );
                //exit();
                break;
        }
    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }

}