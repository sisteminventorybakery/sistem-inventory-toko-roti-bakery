<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 */
class Cms_role_type extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->lang->load('auth');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        try {
            if( !$this->ion_auth->is_admin() ) show_404();
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $tables = $this->config->item('tables', 'ion_auth');
            $crud = new grocery_CRUD();
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_setting');
            $crud->set_subject('Settting Role Type');
            $where = 'setting_type="role_type" GROUP BY setting_desc';
            $crud->where ($where);
            $crud->columns('setting_desc');
            $crud->add_fields('setting_name', 'setting_value','setting_type','setting_desc');
            $crud->display_as('setting_value', 'Setting (contoh : setting_general , Tidak boleh ada spasi)');
            $crud->display_as('setting_name', 'Deskripsi (contoh : Setting General )');
            $crud->field_type('setting_type', 'hidden','role_type_testing');
            $crud->field_type('setting_desc', 'hidden','role_type_testing');
            $crud->callback_after_insert(array($this, '_callback_after_insert'));
            $crud->callback_before_insert(array($this, '_callback_before_insert'));
            $crud->callback_before_delete(array($this, '_callback_before_delete'));
            //$crud->where('setting_type', 'role_type');
            $crud->order_by('setting_id', 'desc');
            $crud->order_by('setting_desc', 'asc');
            $crud->unset_texteditor('setting_value');
            $crud->unset_export();
            $crud->unset_print();
            $crud->unset_edit();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }

    }

    public function _callback_before_insert()
    {
        $setting = $this->input->post('setting_value');
        $setting_description = $this->input->post('setting_name');
        $data_role = array(
            array(
                'setting_type' => 'role_type',
                'setting_name' => 'Add ' . $setting_description,
                'setting_value' => 'add',
                'setting_desc' => $setting
            ),
            array(
                'setting_type' => 'role_type',
                'setting_name' => 'Delete ' . $setting_description,
                'setting_value' => 'delete',
                'setting_desc' => $setting
            ),
            array(
                'setting_type' => 'role_type',
                'setting_name' => 'Edit ' . $setting_description,
                'setting_value' => 'edit',
                'setting_desc' => $setting
            ),
//            array(
//                'setting_type' => 'role_type',
//                'setting_name' => 'Print ' . $setting_description,
//                'setting_value' => 'print',
//                'setting_desc' => $setting
//            ),
//            array(
//                'setting_type' => 'role_type',
//                'setting_name' => 'Export ' . $setting_description,
//                'setting_value' => 'export',
//                'setting_desc' => $setting
//            ),
            array(
                'setting_type' => 'role_type',
                'setting_name' => 'Menu ' . $setting_description,
                'setting_value' => $setting,
                'setting_desc' => 'menu'
            )
        );
        $this->db->insert_batch('tb_setting', $data_role);
    }

    public function _callback_after_insert()
    {
        $this->db->where('setting_type','role_type_testing');
        $this->db->where('setting_desc','role_type_testing');
        $this->db->delete('tb_setting');
    }

    public function _callback_before_delete($primary_key)
    {
        $this->db->where('setting_id', $primary_key);
        $result = $this->db->get('tb_setting')->result();
        $setting = null;
        foreach ( $result as $s ){
            $setting = $s->setting_desc;
        }
        if( $setting ){
            $this->db->where('setting_type', 'role_type');
            $this->db->where('setting_desc', $setting);
            $this->db->or_where('setting_value', $setting);
            $this->db->delete('tb_setting');
        }
    }


}