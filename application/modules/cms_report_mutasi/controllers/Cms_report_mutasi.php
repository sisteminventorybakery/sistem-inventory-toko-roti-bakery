<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_report_mutasi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output['theme_path'] = $path;
        $output['js_files'] = array();
        $output['css_files'] = array();
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_report', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','report_mutasi') ) show_404();
        $data = $this->base_config->panel_setting();
        $user = $this->ion_auth->user()->row();
        $f_outlet_from = $this->input->get('outlet_from');
        $f_outlet_to = $this->input->get('outlet_to');
        $f_status = $this->input->get('status');
        $f_user = $this->input->get('user');
        $f_from = $this->input->get('from');
        $f_to = $this->input->get('to');

        $result = array();
        $all_total = 0;
        $filter_outlet_from = 'Kosong';
        $filter_outlet_to = 'Kosong';
        $filter_user = 'Kosong';
        $this->db->join('tb_user','tb_user.id=tb_mutasi.user_id');
        if( $f_status ) $this->db->where('mutasi_status', $f_status);
        if( $f_user ) $this->db->where('user_id', $f_user);
        if( $f_outlet_from ) $this->db->where('mutasi_from', $f_outlet_from);
        if( $f_outlet_to ) $this->db->where('mutasi_to', $f_outlet_to);
        if( $f_from ) $this->db->where('mutasi_date >=', $f_from);
        if( $f_to ) $this->db->where('mutasi_date <=', $f_to);
        $this->db->order_by('mutasi_date','desc');
        $result_row = $this->db->get('tb_mutasi')->result();

        //$this->M_base_config->tes($result_row);

        foreach ($result_row as $v){
            $total = (int)$v->mutasi_total;
            $outlet_name_from = $this->db->where('master_outlet_id', $v->mutasi_from)->get('tb_master_outlet',1)->row('master_outlet_name');
            $outlet_name_to = $this->db->where('master_outlet_id', $v->mutasi_to)->get('tb_master_outlet',1)->row('master_outlet_name');
            $result[] = array(
                'id'        => $v->mutasi_id,
                'nota'      => $this->_callback_nota($v),
                'total'     => number_format($total),
                'status'     => $this->_callback_status($v),
                'outlet_from'  => $outlet_name_from,
                'outlet_to'    => $outlet_name_to,
                'date'      => $v->mutasi_date,
                'note'      => $v->mutasi_desc,
                'detail'    => $this->_callback_detail($v)
            );
            $all_total += $total;
            $filter_user = $v->username;
            $filter_outlet_from = $outlet_name_from;
            $filter_outlet_to = $outlet_name_to;
        }
        $orders = array(
            'outlet_from' => ( $f_outlet_from ? $filter_outlet_from : 'Semua'),
            'outlet_to' => ( $f_outlet_to ? $filter_outlet_to : 'Semua'),
            'status' => ( $f_status ? $f_status : 'Semua'),
            'user' => ( $f_user ? $filter_user : 'Semua'),
            'from' => ( $f_from ? $f_from : 'Semua'),
            'to' => ( $f_to ? $f_to : 'Semua'),
        );

        $data['orders'] = $orders;
        $data['total'] = number_format($all_total);
        $data['title'] = 'LAPORAN MUTASI';
        $data['result'] = $result;
        $data['user'] = $this->_get_user();
        $data['outlet'] = $this->_get_outlet();

        //$this->M_base_config->tes($data);

        $export = $this->input->get('export');
        if($export){
            switch ( $export ){
                case 'pdf':
                    $this->load->view( 'v_report_template', $data );
                    $html = $this->output->get_output();
                    $dompdf = new \Dompdf\Dompdf();
                    $dompdf->load_html($html);
                    $dompdf->set_paper('A4','landscape');
                    $dompdf->render();
                    $dompdf->stream("report.pdf",array('Attachment'=>0));
                    exit();
                    break;
                case 'excel':
                    header("Content-Type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename = report.xls");
                    $this->load->view( 'v_report_template', $data );
                    //exit();
                    break;
            }
        }else{
            $this->_example_output($data);
        }

    }

    protected  function _get_outlet()
    {
        $res = $this->db->get('tb_master_outlet')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->master_outlet_name,
                'id' => $v->master_outlet_id
            );
        }
        return $tmp;
    }

    protected  function _get_user()
    {
        $res = $this->db->get('tb_user')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->username,
                'id' => $v->id
            );
        }
        return $tmp;
    }

    protected function _callback_status($row)
    {
        $this->M_base_config->cekaAuth();
        if (strtolower($row->mutasi_status) == 'ya')
            $active = '<span class="label label-success">' . 'Ya' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'Tidak' . '</span>';

        return $active;
    }

    protected function _callback_detail($row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail mutasi" href="'.base_url('cms/barang_keluar/export/pdf/'. $row->mutasi_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    protected function _callback_nota($row)
    {
        $this->M_base_config->cekaAuth();
        return 'MT-'.sprintf("%04s", $row->mutasi_id);
    }
}