<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_stok_bahan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        try {
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $user = $this->ion_auth->user()->row();
            $crud = $this->base_config->groups_access('stok_bahan');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_bahan');
            $crud->set_subject('Stok Bahan');
            $crud->columns('master_bahan_name', 'master_bahan_kategori','stok','stok_sisa','konversi','harga');
            $crud->display_as('master_bahan_name', 'Nama Master Bahan');
            $crud->display_as('master_bahan_desc', 'Stok');
            $crud->display_as('harga', 'Harga (satuan besar)');
            $crud->set_relation('master_bahan_kategori', 'tb_master_category', 'master_category_name');
            $crud->callback_column('harga', array($this, '_callback_harga'));
            $crud->callback_column('stok', array($this, '_callback_stok'));
            $crud->callback_column('stok_sisa', array($this, '_callback_stok_sisa'));
            $crud->unset_print();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_export();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    public function _callback_harga($value)
    {
        return number_format((int)$value);
    }

    public function _callback_stok($value, $row)
    {
        return number_format($value).' '.$row->satuan_besar;
    }

    public function _callback_stok_sisa($value, $row)
    {
        return number_format($value).' '.$row->satuan_kecil;
    }

}