<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Form_validation form_validation
 */
class Cms_customers extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('customers');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_customer');
            $crud->set_subject('Customer');
            $crud->fields('customer_name', 'customer_phone', 'customer_address');
            $crud->unset_texteditor('customer_address');
            $crud->field_type('created','hidden');
            $crud->display_as('created', '<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('created', array($this, '_callback_id'));
            $crud->unset_print();
            $state = $crud->getState();
            if( $state == 'export' ){
                $crud->columns('customer_name', 'customer_phone', 'customer_address');
            }else{
                $crud->columns('created','customer_name', 'customer_phone', 'customer_address');
            }
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function add()
    {
        if( !$this->ion_auth->logged_in() ){
            header('Content-type: application/json');
            echo json_encode(['status'=>0, 'message' => 'Harus login untuk mengakses halaman ini']);
            exit();
        }
        $this->form_validation->set_rules('name','nama','required');
        if( $this->form_validation->run() == FALSE ){
            header('Content-type: application/json');
            echo json_encode(['status'=>0, 'message' => validation_errors()]);
            exit();
        }else{
            $data = [
                'customer_name' => $this->input->post('name'),
                'customer_address' => $this->input->post('address'),
                'customer_phone' => $this->input->post('phone'),
            ];
            $query = $this->db->insert('tb_customer', $data);
            if( $query ){
                header('Content-type: application/json');
                echo json_encode(['status'=>1, 'message' => 'Sukses']);
                exit();
            }else{
                header('Content-type: application/json');
                echo json_encode(['status'=>0, 'message' => $this->db->display_error()]);
                exit();
            }
        }
    }

    public function _callback_id($value, $row)
    {
        $id = $row->customer_id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

}