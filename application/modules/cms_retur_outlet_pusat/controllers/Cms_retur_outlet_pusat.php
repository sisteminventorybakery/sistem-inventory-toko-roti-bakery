<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  CI_Output $output
 * @property CI_Session session
 * @property Stok_model Stok_model
 */
class Cms_retur_outlet_pusat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Stok_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('retur_outlet_pusat');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_retur');
            $crud->set_subject('Retur Outlet Pusat');
            $crud->order_by('retur_date','desc');
            $crud->columns('retur_id','nota','retur_date','retur_desc','retur_type','retur_total','retur_status','created');
            $crud->edit_fields('retur_status', 'retur_desc');
            $crud->where('outlet_id', $this->session->userdata('outlet_id'));
            $crud->change_field_type('outlet_id','hidden', $this->session->userdata('outlet_id'));
            $crud->where('retur_type','outlet');
            $crud->field_type('retur_type','hidden','outlet');
            $crud->field_type('outlet_id','hidden', $this->session->userdata('outlet_id'));
            $crud->display_as('retur_status','Disetujui');
            $crud->display_as('retur_type','Jenis Retur');
            $crud->display_as('retur_desc','Keterangan');
            $crud->display_as('retur_date','Tanggal');
            $crud->display_as('created','Detail');
            $crud->display_as('nota','Nota');
            $crud->display_as('retur_id','ID');
            $crud->callback_column('created', array($this,'_callback_detail'));
            $crud->callback_column('retur_total', array($this,'_callback_total'));
            $crud->callback_column('retur_status', array($this,'_callback_status'));
            $crud->callback_column('nota', array($this,'_callback_nota'));
            $crud->callback_column('retur_id', array($this,'_callback_id'));
            $crud->callback_before_insert(array($this,'_callback_before_insert'));
            $crud->callback_before_update(array($this,'_callback_before_update'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_export();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();
        if( empty($post_array['retur_date']) ) $post_array['retur_date'] = date('Y-m-d H:i:s');
        if( empty($post_array['retur_status']) ) $post_array['retur_status'] = 'tidak';
        return $post_array;
    }

    public function _callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        if( empty($post_array['retur_date']) ) $post_array['retur_date'] = date('Y-m-d H:i:s');
        if( empty($post_array['retur_status']) ) $post_array['retur_status'] = 'tidak';
        return $post_array;
    }

    public function _callback_status($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ( strtolower($row->retur_status) == 'ya' )
            $active = '<span class="label label-success">' . 'YA' . '</span>';
        elseif( strtolower($row->retur_status) == 'ditolak' )
            $active = '<span class="label label-warning">' . 'DITOLAK' . '</span>';
        else
            $active = '<span class="label label-danger">' . 'TIDAK' . '</span>';
        return $active;
    }

    public function _callback_detail($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail Mutasi" href="'.base_url('cms/retur_outlet_pusat/export/pdf/'. $row->retur_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    public function _callback_nota($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return 'RT-'.sprintf("%04s", $row->retur_id);
    }

    public function _callback_id($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return $row->retur_id;
    }

    public function _callback_total($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return number_format($value);
    }

    public function export()
    {
        $this->M_base_config->cekaAuth();
        $export = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $orders = array();
        $result = array();
        $total = 0;
        $total_qty = 0;
        $total_price = 0;
        $this->db->join('tb_retur','tb_retur.retur_id=tb_retur_detail.retur_id');
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_retur.outlet_id');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_retur_detail.item_id');
        $this->db->join('tb_user','tb_user.id=tb_retur.user_id');
        $this->db->where('tb_retur.retur_id', $id);
        $result_row = $this->db->get('tb_retur_detail')->result();
        foreach ($result_row as $k => $v){
            $price      = (int)$v->retur_detail_price;
            $qty        = (int)$v->retur_detail_qty;
            $subtotal   = (int)$v->retur_detail_total;
            $result[]   = array(
                'no'                => $k+1,
                'item'              => $v->item_name,
                'price'             => number_format($price),
                'qty'               => number_format($qty),
                'subtotal'          => number_format($subtotal),
            );
            $total_qty += $qty;
            $total_price += $price;
            $total = (int)$v->retur_total;
            $orders = array(
                'id'        => $v->retur_id,
                'nota'      => 'RT-'.sprintf("%04s", $v->retur_id),
                'total'     => $total,
                'status'     => $v->retur_status,
                'outlet'    => $v->master_outlet_name,
                'date'      => $v->retur_date,
                'type'      => $v->retur_type,
                'desc'      => $v->retur_desc,
                'user'      => $v->username
            );
        }
        $data['result']     = $result;
        $data['total']      = number_format($total);
        $data['total_qty']              = number_format($total_qty);
        $data['total_price']            = number_format($total_price);
        $data['orders']     = $orders;
        $data['title'] = 'LAPORAN RETUR';
        switch ( $export ){
            case 'pdf':
                $this->load->view( 'v_report_template', $data );
                $html = $this->output->get_output();
                $dompdf = new \Dompdf\Dompdf();
                $dompdf->load_html($html);
                $dompdf->set_paper('A4', 'potrait');
                $dompdf->render();
                $dompdf->stream("report.pdf",array('Attachment'=>0));
                exit();
                break;
            case 'excel':
                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename = report.xls");
                $this->load->view( 'v_report_template', $data );
                //exit();
                break;
        }
    }

}