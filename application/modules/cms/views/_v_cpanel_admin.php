<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-lg-4 ">
			<div class="main-header">
				<h2>DASHBOARD</h2>
				<em><?php echo strtoupper($outlets['value']);?></em>
			</div>
		</div>
		<div class="col-lg-8 ">
			<div class="top-content">
				<ul class="list-inline quick-access">
					<li>
						<a target="_blank" href="<?php echo base_url('pos');?>">
							<div class="quick-access-item bg-color-green">
								<i class="fa fa-bar-chart-o"></i>
								<h5>PENJUALAN</h5><em>Transaksi Penjualan</em>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('cms/pemesanan');?>">
							<div class="quick-access-item bg-color-blue">
								<i class="fa fa-envelope"></i>
								<h5>PEMESANAN</h5><em>Transaksi pemesanan</em>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- main -->
	<div class="content">
		<div class="main-content">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<!-- WIDGET NUMBER-CHART STAT -->
					<div class="widget">
						<div class="widget-header">
							<h3><i class="fa fa-bar-chart-o"></i> Total pendapatan sales dan order mingguan</h3>
							<div class="btn-group widget-header-toolbar">
								<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
								<a href="#" title="Expand/Collapse" class="btn-borderless btn-toggle-expand"><i class="fa fa-chevron-up"></i></a>
								<a href="#" title="Remove" class="btn-borderless btn-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="widget-content">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 col-xs-12">
									<div class="number-chart">
										<div class="number pull-left"><span>Rp <?php echo number_format($total);?></span> <span>PENDAPATAN</span></div>
										<div class="mini-stat">
											<div id="number-chart-earning" class="inlinesparkline">Loading...</div>
											<?php
											$v1 = $total;
											$v2 = $total2;
											$persentase = 0;
											$status = 'tetap';
											$class = 'fa fa-caret-right yellow-font';

											if( $v1 == $v2 ){
												$persentase = 0;
												$status = 'tetap';
												$class = 'fa fa-caret-right yellow-font';
											} else if( $v1 > $v2 ){
												$persentase = (($v1 - $v2)/$v1)*100;
												$status = 'naik';
												$class = 'fa fa-caret-up green-font';
											}else if( $v1 < $v2 ){
												$persentase = (($v2 - $v1)/$v2)*100;
												$status = 'turun';
												$class = 'fa fa-caret-down red-font';
											}
											?>
											<p class="text-muted"><i class="<?php echo $class;?>"></i> <?php echo $status.' '.number_format($persentase);?>% dari minggu lalu</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4  col-md-4  col-xs-12 col-sm-12 col-xs-12">
									<div class="number-chart">
										<div class="number pull-left"><span><?php echo number_format($sales_qty);?></span> <span>SALES</span></div>
										<div class="mini-stat">
											<div id="number-chart-sales" class="inlinesparkline">Loading...</div>
											<?php
											$v1 = $sales_qty;
											$v2 = $sales_qty2;
											$persentase = 0;
											$status = 'tetap';
											$class = 'fa fa-caret-right yellow-font';

											if( $v1 == $v2 ){
												$persentase = 0;
												$status = 'tetap';
												$class = 'fa fa-caret-right yellow-font';
											} else if( $v1 > $v2 ){
												$persentase = (($v1 - $v2)/$v1)*100;
												$status = 'naik';
												$class = 'fa fa-caret-up green-font';
											}else if( $v1 < $v2 ){
												$persentase = (($v2 - $v1)/$v2)*100;
												$status = 'turun';
												$class = 'fa fa-caret-down red-font';
											}
											?>
											<p class="text-muted"><i class="<?php echo $class;?>"></i> <?php echo $status.' '.number_format($persentase);?>% dari minggu lalu</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4  col-md-4  col-xs-12 col-sm-12 col-xs-12">
									<div class="number-chart">
										<div class="number pull-left"><span><?php echo number_format($order_qty);?></span> <span>ORDER</span></div>
										<div class="mini-stat">
											<div id="number-chart-order" class="inlinesparkline">Loading...</div>
											<?php
											$v1 = $order_qty;
											$v2 = $order_qty2;
											$persentase = 0;
											$status = 'tetap';
											$class = 'fa fa-caret-right yellow-font';

											if( $v1 == $v2 ){
												$persentase = 0;
												$status = 'tetap';
												$class = 'fa fa-caret-right yellow-font';
											} else if( $v1 > $v2 ){
												$persentase = (($v1 - $v2)/$v1)*100;
												$status = 'naik';
												$class = 'fa fa-caret-up green-font';
											}else if( $v1 < $v2 ){
												$persentase = (($v2 - $v1)/$v2)*100;
												$status = 'turun';
												$class = 'fa fa-caret-down red-font';
											}
											?>
											<p class="text-muted"><i class="<?php echo $class;?>"></i> <?php echo $status.' '.number_format($persentase);?>% dari minggu lalu</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END NUMBER-CHART STAT -->
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 col-md-12">
					<!-- WIDGET VISIT AND SALES CHART -->
					<div class="widget">
						<div class="widget-header">
							<h3><i class="fa fa-bar-chart-o"></i> Grafik order dan sales mingguan</h3>
							<div class="btn-group widget-header-toolbar">
								<a href="#" title="Focus" class="btn-borderless btn-focus"><i class="fa fa-eye"></i></a>
								<a href="#" title="Expand/Collapse" class="btn-borderless btn-toggle-expand"><i class="fa fa-chevron-up"></i></a>
								<a href="#" title="Remove" class="btn-borderless btn-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="widget-content">
							<div class="demo-flot-chart sales-chart"></div>
						</div>
					</div>
					<!-- END WIDGET VISIT AND SALES CHART -->
				</div>
			</div>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
<script type="text/javascript">
	var page = 'home';
	$(document).ready(function(){

		if( $('.sales-chart').length > 0 ) {
			$placeholder = $('.sales-chart');
			$placeholder.attr('data-ctype', '#week');
			chartWeek($placeholder);

			// tabbed chart
			$('#sales-stat-tab a').click(function(e) {
				e.preventDefault();

				$chartType = $(this).attr('href');

				// remove active state
				$('#sales-stat-tab li').removeClass('active');
				$(this).parents('li').addClass('active');

				if($chartType == '#week') {
					chartWeek($placeholder);
				}else if($chartType == '#month') {
					chartMonth($placeholder);
				}else if($chartType == '#year') {
					chartYear($placeholder);
				}

				$placeholder.attr('data-ctype', $chartType);
			});

			var previousPoint = null;
		}

		// init flot chart: current week
		function chartWeek(placeholder) {

			var sales = [
				<?php echo $sales_chart;?>
			];

			var order = [
				<?php echo $order_chart;?>
			];

			var plot = $.plot(placeholder,
				[
					{
						data: sales,
						label: "Sales",
						lines: {
							show: true,
							fill: true
						},
						points: {
							show: true,
							fill: true,
							fillColor: "#fafafa"
						}
					},
					{
						data: order,
						label: "Order",
						lines: {
							show: true,
							lineWidth: 2,
							fill: true
						},
						points: {
							show: true,
							lineWidth: 3,
							fill: true,
							fillColor: "#fafafa"
						}
					}
				],

				{
					series: {
						lines: {
							lineWidth: 2,
							fillColor: { colors: [ { opacity: 0.1 }, { opacity: 0.1 } ] }
						},
						points: {
							lineWidth: 3,
						},

						shadowSize: 0
					},
					grid: {
						hoverable: true,
						clickable: true,
						borderWidth: 0,
						tickColor: "#E4E4E4"
					},
					colors: ["#E7A13D", "#FF3300"],
					yaxis: {
						font: { color: "#555" },
						ticks: 5
					},
					xaxis: {
						mode: "time",
						timezone: "browser",
						minTickSize: [1, "day"],
						timeformat: "%a",
						font: { color: "#555" },
						tickColor: "#fafafa",
						autoscaleMargin: 0.02
					},
					legend: {
						labelBoxBorderColor: "transparent",
						backgroundColor: "transparent"
					},
					tooltip: true,
					tooltipOpts: {
						content: '%s: %y'
					}

				});
		}

		// get day function
		function gt(y, m, d) {
			return new Date(y, m-1, d).getTime();
		}

		//*******************************************
		/*	NUMBER-CHART SPARKLINE (Dashboard v2)
		 /********************************************/

		if( $('.number-chart .inlinesparkline').length > 0 ) {

			var randomV = getRandomValues();
			var sparklineNumberChart = function() {

				var params = {
					width: '140px',
					height: '30px',
					lineWidth: '2',
					lineColor: '#1D92AF',
					fillColor: false,
					spotRadius: '2',
					highlightLineColor: '#aedaff',
					highlightSpotColor: '#71aadb',
					spotColor: false,
					minSpotColor: false,
					maxSpotColor: false,
					disableInteraction: false
				}

				$('#number-chart-earning').sparkline(randomV[0], params);
				$('#number-chart-sales').sparkline(randomV[1], params);
				$('#number-chart-order').sparkline(randomV[2], params);
			}

			sparklineNumberChart();
		}

		/* sparkline on window resize */
		var sparkResize;

		$(window).resize(function(e) {
			clearTimeout(sparkResize);

			if( $('.sparkline-stat-item .inlinesparkline').length > 0 ) {
				sparkResize = setTimeout(sparklineStat, 200);
			}

			if( $('.secondary-stat-item .inlinesparkline').length > 0 ) {
				sparkResize = setTimeout(sparklineWidget, 200);
			}
		});

		function getRandomValues() {
			// data setup
			var values = new Array(3);
			values[0] = <?php echo $sales_total_this_week;?>;
			values[1] = <?php echo $sales_data_this_week;?>;
			values[2] = <?php echo $order_data_this_week;?>;
			return values;
		}

		function randomVal(){
			return Math.floor( Math.random() * 80 );
		}


	}); //READY FUNCTION
</script>