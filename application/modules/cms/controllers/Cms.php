<?php

/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Input $input
 * @property CI_Session session
 * @property Ion_auth_model|Ion_auth ion_auth
 * @property Notif_model Notif_model
 */
class Cms extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Base_config');
        $this->load->model('Notif_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_cpanel_admin', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $outlets = $this->M_base_config->get_my_outlet();
        if( count($outlets)>0 ){
            if( !$this->session->userdata('outlet_id') ) $this->session->set_userdata('outlet_id',$outlets[0]['id']);
        }else{
            $this->session->sess_destroy();
            show_404();
        }

        $data = $this->base_config->panel_setting();
        $data['asset'] = $this->base_config->asset_back();
        $path = $this->base_config->asset_back();
        if($_POST){
            $outlets = [];
            $id_outlet = $this->input->post('outlet');
            $my_outlets = $this->M_base_config->get_my_outlet();
            foreach ($my_outlets as $my_outlet) {
                $outlets[] = $my_outlet['id'];
            }
            $status = false;
            if( in_array($id_outlet,$outlets) ) $status = true;
            if( !$status ){
                show_404();
            }else{
                $this->session->set_userdata('outlet_id',$id_outlet);
            }
        }

        $last_week = date('Y-m-d', strtotime('-14 days'));
        $this_week = date('Y-m-d', strtotime('-7 days'));

        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('DATE(sales_date) >=', $last_week);
        $this->db->where('DATE(sales_date) <=', $this_week);
        $result_sales2 = $this->db->get('tb_sales')->result_array();

        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('DATE(order_date) >=', $last_week);
        $this->db->where('DATE(order_date) <=', $this_week);
        $result_order2 = $this->db->get('tb_order')->result_array();

        $total_sales_last_week = 0; 
        $sales_qty_last_week = 0; 
        foreach ($result_sales2 as $sales) {
            $total_sales_last_week += $sales['sales_total'];
            $sales_qty_last_week++;
        }

        $total_order_last_week = 0;
        $order_qty_last_week = 0;
        foreach ($result_order2 as $order) {
            $total_order_last_week += $order['order_pay'];
            $order_qty_last_week++;
        }

        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('DATE(sales_date) >=', $this_week);
        $this->db->where('DATE(sales_date) <=', date('Y-m-d'));
        $result_sales = $this->db->get('tb_sales')->result_array();

        $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
        $this->db->where('DATE(order_date) >=', $this_week);
        $this->db->where('DATE(order_date) <=', date('Y-m-d'));
        $result_order = $this->db->get('tb_order')->result_array();

        $total_sales = 0;
        $sales_qty = 0;
        foreach ($result_sales as $sales) {
            $total_sales += $sales['sales_total'];
            $sales_qty++;
        }

        $total_order = 0;
        $order_qty = 0;
        foreach ($result_order as $order) {
            $total_order += $order['order_pay'];
            $order_qty++;
        }

        $sales_data_this_week = [];
        $earning_total_this_week = [];
        $order_data_this_week = [];

        $sales_cart = [];
        $order_cart = [];

        $begin = new DateTime( $this_week );
        $end   = new DateTime( date('Y-m-d') );
        for($i = $begin; $begin <= $end; $i->modify('+1 day')){
            $date = $i->format("Y-m-d");
            $date2 = $i->format("Y, m, d");
            $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
            $this->db->where('DATE(sales_date)', $date);
            $value = (int)$this->db->get('tb_sales')->num_rows();
            $sales_data_this_week[] = $value;
            $sales_cart[] = "[gt($date2), $value]";

            $this->db->select('SUM(sales_total) AS total', FALSE);
            $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
            $this->db->where('DATE(sales_date)', $date);
            $value_sum2 = (int)$this->db->get('tb_sales')->row('total');

            $this->db->select('SUM(order_total) AS total', FALSE);
            $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
            $this->db->where('DATE(order_date)', $date);
            $value_sum = (int)$this->db->get('tb_order')->row('total');
            $earning_total_this_week[] = $value_sum+$value_sum2;

            $this->db->where('outlet_id', $this->session->userdata('outlet_id'));
            $this->db->where('DATE(order_date)', $date);
            $value_od = (int)$this->db->get('tb_order')->num_rows();
            $order_data_this_week[] = $value_od;
            $order_cart[] = "[gt($date2), $value_od]";
        }

        //$this->M_base_config->tes( [$sales_cart,$order_cart] );
        
        $this->_example_output((object)array(
            'sales_total_this_week' => json_encode($earning_total_this_week),
            'sales_data_this_week' => json_encode($sales_data_this_week),
            'order_data_this_week' => json_encode($order_data_this_week),
            'total' => $total_sales+$total_order,
            'total2' => $total_sales_last_week+$total_order_last_week,
            'sales_qty' => $sales_qty,
            'sales_qty2' => $sales_qty_last_week,
            'order_qty' => $order_qty,
            'order_qty2' => $order_qty_last_week,
            'order_chart' => implode(',',$order_cart),
            'sales_chart' => implode(',',$sales_cart),
            'output' => '',
            'outlets' => $this->M_base_config->get_current_outlet(),
            'theme_path' => $path,
            'js_files' => array(
                $path."js/plugins/stat/flot/jquery.flot.min.js",
	            $path."js/plugins/stat/flot/jquery.flot.resize.min.js",
	            $path."js/plugins/stat/flot/jquery.flot.time.min.js",
	            $path."js/plugins/stat/flot/jquery.flot.tooltip.min.js",
                $path.'js/plugins/jquery-sparkline/jquery.sparkline.min.js',
                //$path.'js/king-chart-stat.js',
            ),
            'css_files' => array(),
        )
        );
    }

}