<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-content">
			<div class="col-lg-12">
				<h4>Laporan Transfer Masuk/Keluar</h4>
				<hr class="inner-separator" />
				<div class="row">
					<div class="col-lg-3">
						<select name="outlet" id="outlet" class="select2">
							<option value="all">Pilih Outlet</option>
							<option value="option3">Option 3</option>
							<option value="option4">Option 4</option>
						</select>
					</div>
					<div class="col-lg-2">
						<select name="user" id="user" class="select2">
							<option value="all"><?php echo date('M'); ?></option>
						</select>
					</div>
					<div class="col-lg-2">
						<select name="user" id="user" class="select2">
							<option value="all"><?php echo date('Y'); ?></option>
						</select>
					</div>
					<div class="col-lg-3">
						<select name="customer" id="customer" class="select2">
							<option value="option3">Transfer Masuk</option>
							<option value="option4">Transfer Keluar</option>
						</select>
					</div>
					<div class="col-lg-1">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i> Filter</button>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn btn-info btn-sm"><i class="fa fa-paper-plane"></i> Export</button>
					</div>
				</div>
				<hr class="inner-separator" />
				<div class="row">
					<table class="table table-condensed table-dark-header">
						<thead>
						<tr>
						<tr>
							<th>Produk</th>
							<?php for($y=1;$y<=31;$y++):?>
								<th class="text-right"><?php echo sprintf("%02d", $y);?></th>
							<?php endfor;?>
							<th class="text-right">Total</th>
						</tr>
						</tr>
						</thead>
						<tbody>
						<?php for($i=1;$i<=50;$i++):?>
						<tr>
							<td>Roti Bakery</td>
							<?php for($y=1;$y<=31;$y++):?>
							<td class="text-right">0</td>
							<?php endfor;?>
							<td class="text-right">1,000</td>
						</tr>
						<?php endfor;?>
						<tr>
							<th>Total</th>
							<?php for($y=1;$y<=31;$y++):?>
								<th class="text-right"><?php echo $y;?></th>
							<?php endfor;?>
							<th class="text-right">5,000,000</th>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
