<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_report_penjualan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output['theme_path'] = $path;
        $output['js_files'] = array();
        $output['css_files'] = array();
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_report', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','report_penjualan') ) show_404();
        $data = $this->base_config->panel_setting();
        $user = $this->ion_auth->user()->row();
        $f_outlet = $this->input->get('outlet');
        $f_shift = $this->input->get('shift');
        $f_user = $this->input->get('user');
        $f_from = $this->input->get('from');
        $f_to = $this->input->get('to');

        $result = array();
        $sales = array();
        $all_total = 0;
        $all_total_pay = 0;
        $all_total_disc = 0;
        $filter_outlet = 'Kosong';
        $filter_user = 'Kosong';
        $this->db->join('tb_customer','tb_customer.customer_id=tb_sales.customer_id');
        $this->db->join('tb_master_outlet','tb_master_outlet.master_outlet_id=tb_sales.outlet_id');
        $this->db->join('tb_user','tb_user.id=tb_sales.user_id');
        if( $f_user ) $this->db->where('user_id', $f_user);
        if( $f_shift ) $this->db->where('sales_shift', $f_shift);
        if( $f_outlet ) $this->db->where('outlet_id', $f_outlet);
        if( $f_from ) $this->db->where('sales_date >=', $f_from);
        if( $f_to ) $this->db->where('sales_date <=', $f_to);
        $this->db->order_by('sales_date','desc');
        $result_row = $this->db->get('tb_sales')->result();
        //$this->M_base_config->tes($result_row);
        foreach ($result_row as $v){
            $total = (int)$v->sales_total;
            $result[] = array(
                'id'        => $v->sales_id,
                'customer'  => $v->customer_name,
                'nota'      => $this->_callback_nota($v),
                'disc'      => $v->sales_discount,
                'total'     => number_format($total),
                'outlet'    => $v->master_outlet_name,
                'date'      => $v->sales_date,
                'telepon'   => $v->customer_phone,
                'pay'       => number_format( (int)$v->sales_pay),
                'detail'    => $this->_callback_detail($v),
                'shift'    => $v->sales_shift,
            );
            $all_total += $total;
            $all_total_pay += (int)$v->sales_pay;
            $all_total_disc += (int)$v->sales_discount;
            $filter_user = $v->username;
            $filter_outlet = $v->master_outlet_name;
        }
        $sales = array(
            'shift' => ( $f_shift ? $f_shift : 'Semua'),
            'outlet' => ( $f_outlet ? $filter_outlet : 'Semua'),
            'user' => ( $f_user ? $filter_user : 'Semua'),
            'from' => ( $f_from ? $f_from : 'Semua'),
            'to' => ( $f_to ? $f_to : 'Semua'),
        );
        $data['sales'] = $sales;
        $data['total'] = number_format($all_total);
        $data['total_pay'] = number_format($all_total_pay);
        $data['total_disc'] = number_format($all_total_disc);
        $data['title'] = 'LAPORAN PENJUALAN';
        $data['result'] = $result;
        $data['user'] = $this->_get_user();
        $data['outlet'] = $this->_get_outlet();
        $export = $this->input->get('export');
        if($export){
            switch ( $export ){
                case 'pdf':
                    $this->load->view( 'v_report_template', $data );
                    $html = $this->output->get_output();
                    $dompdf = new \Dompdf\Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4','potrait');
                    $dompdf->render();
                    $dompdf->stream("report.pdf",array('Attachment'=>0));
                    exit();
                    break;
                case 'excel':
                    header("Content-Type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename = report.xls");
                    $this->load->view( 'v_report_template', $data );
                    //exit();
                    break;
            }
        }else{
            $this->_example_output($data);
        }

    }

    protected  function _get_outlet()
    {
        $res = $this->db->get('tb_master_outlet')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->master_outlet_name,
                'id' => $v->master_outlet_id
            );
        }
        return $tmp;
    }

    protected  function _get_user()
    {
        $res = $this->db->get('tb_user')->result();
        $tmp = array();
        foreach ($res as $v){
            $tmp[] = array(
                'name' => $v->username,
                'id' => $v->id
            );
        }
        return $tmp;
    }

    protected function _callback_detail($row)
    {
        $this->M_base_config->cekaAuth();
        return '<a target="_blank" title="Detail Pemesanan" href="'.base_url('cms/daftar_penjualan/export/pdf/'. $row->sales_id .'').'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
    }

    protected function _callback_nota($row)
    {
        $this->M_base_config->cekaAuth();
        return 'PJ-'.sprintf("%04s", $row->sales_id);
    }
}