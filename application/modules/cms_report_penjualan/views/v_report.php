<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-content">

				<div class="row" style="margin-top: 15px;">
					<form data-url="<?php echo base_url('cms/report_pemesanan');?>" id="form-report-order" method="get">
					<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="outlet">
							<option value="">Outlet</option>
							<?php foreach ($outlet as $v):?>
							<option value="<?php echo $v['id'];?>" <?php $input = $this->input->get('outlet'); if($input && $v['id']==$input) echo 'selected';?>><?php echo $v['name'];?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="shift">
							<option value="">Shift</option>
							<option value="1" <?php $input = $this->input->get('shift'); if($input && '1'==$input) echo 'selected';?>>Shift 1</option>
							<option value="2" <?php $input = $this->input->get('shift'); if($input && '2'==$input) echo 'selected';?>>Shift 2</option>
						</select>
					</div>
					<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
						<select class="form-control" name="user">
							<option value="">User</option>
							<?php foreach ($user as $v):?>
								<option value="<?php echo $v['id'];?>" <?php $input = $this->input->get('user'); if($input && $v['id']==$input) echo 'selected';?>><?php echo $v['name'];?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
						<div class="input-group date form_date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" name="from" value="<?php echo $this->input->get('from');?>" class="form-control" placeholder="Tgl Dari">
						</div>
					</div>
					<div class="col-lg-2" style="width: 16%;padding-left: 1px;padding-right: 1px;">
						<div class="input-group date form_date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" name="to" value="<?php echo $this->input->get('to');?>" class="form-control" placeholder="Tgl Ke">
						</div>
					</div>
					<div class="col-lg-2" style="width: 8%;padding-left: 1px;padding-right: 1px;">
						<button type="submit" class="btn btn-info btn-block"><i class="fa fa-search"></i></button>
					</div>
					<div class="col-lg-2" style="width: 8%;padding-left: 1px;padding-right: 1px;">
						<button type="button" id="export-pdf" class="btn btn-warning btn-block"><i class="fa fa-file-pdf-o"></i></button>
					</div>
					</form>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<table class="table table-condensed table-dark-header">
						<thead>
						<tr>
							<th>Nota</th>
							<th>Tgl Pembelian</th>
							<th>Pemesan</th>
							<th>Telepon</th>
							<th>Diskon</th>
							<th>Total</th>
							<th>Shift</th>
							<th>Detail</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($result as $v):?>
						<tr>
							<td><?php echo $v['nota'];?></td>
							<td><?php echo $v['date'];?></td>
							<td><?php echo $v['customer'];?></td>
							<td><?php echo $v['telepon'];?></td>
							<td><?php echo $v['disc'];?></td>
							<td><?php echo $v['total'];?></td>
							<td><?php echo $v['shift'];?></td>
							<td><?php echo $v['detail'];?></td>
						</tr>
						<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
