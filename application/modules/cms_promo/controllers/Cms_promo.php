<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_promo extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('master_item');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_item_promo');
            $crud->set_subject('Promo');
            $crud->fields('outlet_id','item_id', 'item_promo_qty','status_promo_id','item_promo_free','item_promo_date_start','item_promo_date_end','item_promo_item_id');
            $crud->where('outlet_id', $this->session->userdata('outlet_id'));
            $crud->change_field_type('outlet_id','hidden', $this->session->userdata('outlet_id'));
            $crud->display_as('item_promo_qty','Minimal Pembelian');
            $crud->display_as('item_promo_free','Gratis');
            $crud->display_as('item_promo_date_start','Tanggal Aktif');
            $crud->display_as('item_promo_date_end','Sampai Tanggal');
            $crud->display_as('item_id','Nama Item Promo');
			$crud->set_relation('item_id', 'tb_master_item', 'item_name');
			$crud->display_as('status_promo_id','Type Promo');
			$crud->set_relation('status_promo_id', 'tb_status_promo', 'status_promo_name');
			$crud->display_as('item_promo_item_id','Item Gratis');
			$crud->set_relation('item_promo_item_id', 'tb_master_item', 'item_name');
            $crud->field_type('created','hidden');
            $crud->display_as('created', '<label class="control-inline fancy-checkbox custom-bgcolor-green"><input type="checkbox" name="removeall" id="removeall" data-md-icheck><span></span></label>');
            $crud->callback_column('created', array($this, '_callback_id'));
            $crud->callback_column('item_promo_free', array($this, '_callback_sparator'));
            $crud->callback_before_update(array($this,'_callback_remove_comma'));
            $crud->callback_before_insert(array($this,'_callback_remove_comma'));
            $crud->callback_field('item_promo_free', array($this, '_callback_input_number'));
            $crud->unset_print();
            $state = $crud->getState();
            if( $state == 'export' ){
                $crud->columns('item_id','item_promo_qty','status_promo_id','item_promo_free','item_promo_date_start','item_promo_date_end','item_promo_item_id');
            }else{
                $crud->columns('created','item_id','item_promo_qty','status_promo_id','item_promo_free','item_promo_date_start','item_promo_date_end','item_promo_item_id');
            }
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_input_number($value)
    {
        $number = number_format($value);
        return "<input type='text' class='form-control numeric2' name='item_promo_free' value='$number' required/>";
    }

    public function _callback_id($value, $row)
    {
        $id = $row->item_promo_id;
        $component = '<input type="checkbox" class="removelist" value="'.$id.'" name="'.$id.'" id="'.$id.'" data-md-icheck />';
        return '<label class="control-inline fancy-checkbox custom-bgcolor-green">'.$component.'<span></span></label>';
    }

    public function _callback_remove_comma($post_array, $primary_key=null)
    {
        $number = $post_array['item_promo_free'];
        $post_array['item_promo_free'] = intval(preg_replace('/[^\d.]/', '', $number ));
        return $post_array;
    }

    public function _callback_sparator($v)
    {
        return number_format($v);
    }


}