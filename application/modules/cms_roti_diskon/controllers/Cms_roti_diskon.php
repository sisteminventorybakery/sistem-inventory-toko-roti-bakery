<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property CI_Session session
 */
class Cms_roti_diskon extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('roti_diskon');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_master_item');
            $crud->set_subject('Roti Diskon');
            $crud->columns('item_name','item_disc','item_disc_percent', 'item_satuan', 'item_category', 'item_jenis');
            $crud->edit_fields('created','item_disc','item_disc_percent');
            $crud->field_type('created','hidden', date('Y-m-d H:i:s'));
            $crud->change_field_type('item_disc', 'integer');
            $crud->required_fields('item_disc');
            $crud->display_as('item_name', 'Nama Item');
            $crud->display_as('item_satuan', 'Satuan');
            $crud->display_as('item_category', 'Kategori');
            $crud->display_as('item_jenis', 'Jenis');
            $crud->display_as('item_disc', 'Diskon (Rp)');
            $crud->display_as('item_disc_percent', 'Diskon (%)');
            $crud->set_relation('item_category', 'tb_master_category', 'master_category_name');
            $crud->set_relation('item_jenis', 'tb_master_jenis', 'master_jenis_name');
            $crud->set_relation('item_satuan', 'tb_master_satuan', 'master_satuan_name');
            $crud->callback_column('item_disc', array($this, '_callback_sparator'));
            $crud->callback_column('item_disc_percent', array($this, '_callback_percent'));
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_delete();
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function _callback_sparator($v)
    {
        return number_format($v);
    }

    public function _callback_percent($v)
    {
        return $v. ' %';
    }

}