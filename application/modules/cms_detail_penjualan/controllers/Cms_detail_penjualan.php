<?php

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_detail_penjualan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        $data = $this->base_config->panel_setting();
        try {
            $crud = $this->base_config->groups_access('detail_penjualan');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_sales_detail');
            $crud->set_subject('Laporan Detail Penjualan');
            $crud->columns('item_id', 'sales_id','sales_detail_price', 'sales_detail_qty', 'sales_detail_total', 'sales_detail_date');
            //$crud->callback_column('detail_penjualan_enum', array($this, '_callback_webpage_url'));
            //$crud->set_relation_n_n('detail_penjualan_n_n', 'tb_terms', 'tb_category', 'post_id', 'category_id', 'category_name', null, array('category_type' => 'category'));
            $crud->set_relation('item_id', 'tb_master_item', 'item_name');
            $crud->display_as('item_id','Nama Item');
            $crud->display_as('sales_id','ID Transaksi');
            $crud->display_as('sales_detail_price','Harga');
            $crud->display_as('sales_detail_qty','Qty');
            $crud->display_as('sales_detail_total','Total');
            $crud->display_as('sales_detail_date','Tanggal');
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_delete();
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

}