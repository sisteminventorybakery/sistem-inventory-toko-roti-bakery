<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Cms_general extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->lang->load('auth');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('v_data', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        try {
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $crud = $this->base_config->groups_access('setting_general');
            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('tb_setting');
            $crud->set_subject('General Setting');
            $crud->columns('setting_name', 'setting_value');
            $crud->where('setting_type', 'setting_general');
            $crud->order_by('setting_id', 'asc');
            $crud->unset_texteditor('setting_value');
            $crud->unset_export();
            $crud->unset_print();
            $crud->unset_add();
            $crud->unset_delete();
            $crud->edit_fields('setting_value');
            $crud->set_bulkactionfalse(true);
            $output = $crud->render();
            $output->data = $data;
            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage());
        }

    }

}