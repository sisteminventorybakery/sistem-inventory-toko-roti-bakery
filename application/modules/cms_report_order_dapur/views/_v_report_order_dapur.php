<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<!-- main -->
	<div class="content">
		<div class="main-content">
			<div class="col-lg-12">
				<h4>Laporan Pesanan Dapur</h4>
				<hr class="inner-separator" />
				<div class="row">
					<div class="col-lg-2">
						<select name="outlet" id="outlet" class="select2">
							<option value="all">Pilih Outlet</option>
							<option value="option3">Option 3</option>
							<option value="option4">Option 4</option>
						</select>
					</div>
					<div class="col-lg-2">
						<div class="input-group date form_date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" id="from" name="from" class="form-control" readonly placeholder="dari">
						</div>
					</div>
					<div class="col-lg-2">
						<div class="input-group date form_date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" id="to" name="to" class="form-control" readonly placeholder="sampai">
						</div>
					</div>
					<div class="col-lg-2">
						<select name="user" id="user" class="select2">
							<option value="all">Pilih User</option>
							<option value="option3">Option 3</option>
							<option value="option4">Option 4</option>
						</select>
					</div>
					<div class="col-lg-2">
						<select name="customer" id="customer" class="select2">
							<option value="all">Pilih Customer</option>
							<option value="option3">Option 3</option>
							<option value="option4">Option 4</option>
						</select>
					</div>
					<div class="col-lg-1">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i> Filter</button>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn btn-info btn-sm"><i class="fa fa-paper-plane"></i> Export</button>
					</div>
				</div>
				<hr class="inner-separator" />
				<div class="row">
					<table class="table table-condensed table-dark-header">
						<thead>
						<tr>
							<th>#</th>
							<th>Tanggal</th>
							<th>No. Nota</th>
							<th>Outlet</th>
							<th>Nama Item</th>
							<th>Customer</th>
							<th>User</th>
							<th>Jns Bayar</th>
							<th>Packing</th>
							<th class="text-right">Disc Nota</th>
							<th class="text-right">Disc Produk</th>
							<th class="text-right">Harga</th>
							<th class="text-right">Jumlah</th>
							<th class="text-right">Sub Total</th>
						</tr>
						</thead>
						<tbody>
						<?php for($i=1;$i<=50;$i++):?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo date('d/m/Y h:i');?></td>
							<td>PJ00001</td>
							<td>Surabaya 001</td>
							<td>Aneka Selai</td>
							<td>Umum</td>
							<td>Kasir<?php echo $i;?></td>
							<td>Cash</td>
							<td>Dus</td>
							<td class="text-right">0</td>
							<td class="text-right">0</td>
							<td class="text-right">1,000</td>
							<td class="text-right">3</td>
							<td class="text-right">3,000</td>
						</tr>
						<?php endfor;?>
						<tr>
							<th colspan="9">Total</th>
							<th class="text-right">3,000</th>
							<th class="text-right">4,000</th>
							<th class="text-right">155,000</th>
							<th class="text-right">344</th>
							<th class="text-right">500,000</th>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
