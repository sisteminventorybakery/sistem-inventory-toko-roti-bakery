<?php

/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_Input $input
 * @property CI_User_agent $agent
 * @property CI_Form_validation $form_validation
 * @property CI_Session session
 * @property Item_model Item_model
 * @property Stok_model Stok_model
 */
class Cms_penjualan_dapur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('grocery_CRUD');
        $this->load->library('Base_config');
        $this->load->model('Item_model');
        $this->load->model('Stok_model');
    }

    public function _example_output($output = null)
    {
        $path = $this->base_config->asset_back();
        $output->theme_path = $path;
        $this->load->view('v_header', $output);
        $this->load->view('v_sidebar', $output);
        $this->load->view('_v_new_penjualan_dapur', $output);
        $this->load->view('v_footer', $output);
    }

    public function index()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu','penjualan_dapur') ) show_404();
        $data = $this->base_config->panel_setting();
        $output = new stdClass();
        $output->my_outlets = $this->M_base_config->get_my_outlet();
        $output->categories = $this->db->get('tb_master_category')->result();
        $output->items = $this->Item_model->get_inventory_dapur();
        $this->db->order_by('master_outlet_name', 'asc');
        $output->outlets = $this->db->get('tb_master_outlet')->result();
        $output->data = $data;
        $output->js_files = array();
        $output->css_files = array();
        $this->_example_output($output);
    }

    public function ajax()
    {
        if (!$this->ion_auth->logged_in()) {
            echo json_encode(array('status' => 0, 'message' => 'Harus login untuk mengakses halaman ini'));
            exit();
        }
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $this->form_validation->set_rules('outlet_id', 'kode outlet', 'required');
        $this->form_validation->set_rules('item_id[]', 'Item', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = 0;
            $data['message'] = validation_errors();
            echo json_encode($data);
            exit();
        } else {
            $qty_s = $this->input->post('qty');
            $item_s = $this->input->post('item_id');
            $discount = (int)$this->input->post('order_discount');
            $biaya_lain = (int)$this->input->post('biaya');
            $outlet_id = $this->input->post('outlet_id');
            $all_total = 0;

            foreach ($item_s as $key => $inv_id) {
                $this->db->where('inv_id', $inv_id);
                $result = $this->db->get('tb_inv_dapur');
                $price = (int)$result->row('inv_harga');
                $disc = 0;
                $disc_value = 0;
                if( $disc > 0 ) $disc_value = $price * ($disc/100);
                $total = ( $price - $disc_value ) * (int)$qty_s[$key];
                $all_total = $all_total + $total;
            }
            $data_order = array(
                'biaya_lain'        => $biaya_lain,
                'diskon_nota'       => $this->input->post('order_discount'),
                'keterangan'        => $this->input->post('order_note'),
                'outlet_id'         => $outlet_id,
                'user_id'           => $this->ion_auth->user()->row()->id,
                'total'             => $all_total - $discount,
            );
            $this->db->insert('tb_mutasi_dapur', $data_order);
            $order_id = $this->db->insert_id();
            $data_order_detail = array();
            foreach ($item_s as $key => $inv_id) {
                $this->db->where('inv_id', $inv_id);
                $result = $this->db->get('tb_inv_dapur');
                $price = (int)$result->row('inv_harga');
                $expired = $result->row('inv_expired');
                $item_id = $result->row('item_id');
                $disc = 0;
                $disc_value = 0;
                if( $disc > 0 ) $disc_value = $price * ($disc/100);
                $total = ( $price - $disc_value ) * (int)$qty_s[$key];
                $data_order_detail[] = array(
                    'harga'             => $price,
                    'jumlah'            => $qty_s[$key],
                    'subtotal'          => $total,
                    'diskon'            => $disc,
                    'mutasi_dapur_id'   => $order_id,
                    'item_id'           => $item_id,
                    'expired'           => $expired
                );
                $this->Stok_model->remove_inventory_dapur($item_id,$expired,$qty_s[$key]);
                $this->Stok_model->add_inventory_outlet($outlet_id,$item_id,$expired,$qty_s[$key]);
            }
            $query = $this->db->insert_batch('tb_mutasi_dapur_detail', $data_order_detail);
            if ($query) {
                echo json_encode(array('status' => 1, 'message' => 'Sukses Disimpan' ));
                exit();
            } else {
                echo json_encode(array('status' => 0, 'message' => 'Proses gagal, silahkan ulangi lagi'));
                exit();
            }
        }

    }
}