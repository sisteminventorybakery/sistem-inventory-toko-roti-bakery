<script type="text/javascript">
	function cek_roti(sales_detail_id) {
		// alert('tes');
		var url = base_url+"cms/new_retur_pembeli/get_inventory";

		$.get(url, function(data) {
			if( data.status ){
			var inventory = data.data;
			var html;
				$.each(inventory, function(index, value) {
					var item_id = value.item_id;
					var stok    = value.inventory_stok;
					var harga   = value.inventory_price_sales;
					var nama    = value.item_name;
					var exp    = value.inventory_expired;
					html += 
						`<tr>
							<td>`+ nama +` (EXP. `+exp+`)</td>
							<td>`+ stok +`</td>
							<td>`+ harga +`</td>
							<td><button onclick="ins_cek_roti('`+sales_detail_id+`','`+nama+`', '`+item_id+`', '`+harga+`', '`+exp+`')" class="btn btn-success"><i class="fa fa-check-circle"></i></button></td>
						</tr>`;
					$('#table-sales-modal > tbody').html( html);
				})
			}else{
				btn.parent().parent().parent().find('.msg').html( message_box('danger', data.message) );
				$('#table-sales > tbody').html('');
			}
			console.log(data);
		});
		$('#modal-retur').modal();
	}

	function cek_roti_dapur(sales_detail_id) {
		// alert('tes');
		var url = base_url+"cms/new_retur_dapur/get_inventory_dapur";

		$.get(url, function(data) {
			if( data.status ){
			var inventory = data.data;
			var html;
				$.each(inventory, function(index, value) {
					var item_id = value.item_id;
					var stok    = value.inv_stok;
					var harga   = value.inv_harga;
					var nama    = value.item_name;
					html += 
						`<tr>
							<td>`+ nama +`</td>
							<td>`+ stok +`</td>
							<td>`+ harga +`</td>
							<td><button onclick="ins_cek_roti('`+sales_detail_id+`','`+nama+`', '`+item_id+`', '`+harga+`')" class="btn btn-success"><i class="fa fa-check-circle"></i></button></td>
						</tr>`;
					$('#table-sales-modal > tbody').html( html);
				})
			}else{
				btn.parent().parent().parent().find('.msg').html( message_box('danger', data.message) );
				$('#table-sales > tbody').html('');
			}
			console.log(data);
		});
		$('#modal-retur').modal();
	}

	function ins_cek_roti(sales_detail_id, nama, id_item, harga, exp) {
		$('.harga-'+sales_detail_id).val(harga);
		$('.id-item-'+sales_detail_id).val(id_item);
		$('.nama-item-'+sales_detail_id).val(nama);
		$('.exp-'+sales_detail_id).val(exp);

		$('.qty_roti_'+sales_detail_id).prop('disabled', false);
		$('.nama-item-'+sales_detail_id).prop('disabled', false);

		$('.qty_uang_'+sales_detail_id).prop('disabled', true).val('');
		$('#modal-retur').modal('hide');
	}

	function qty_uang_enable(sales_detail_id) {
		$('.qty_roti_'+sales_detail_id).prop('disabled', true).val('');
		$('.nama-item-'+sales_detail_id).prop('disabled', true).val('');

		$('.qty_uang_'+sales_detail_id).prop('disabled', false);
	}

	/*RETUR OUTLET*/
	if( typeof page != 'undefined' && page == 'new_retur_outlet' ) {
		$("#btn-cek-no-retur").click(function (e) {
			var btn = $(this);
			var id_sales = $(this).parent().parent().find('#sales-id').val();
			var jenis 	 = $('input[name=retur_pembeli]:checked').val();
			var url;
			var kode;
			var d = new Date();
			var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

			if (jenis == 'tb_retur') {
				url = base_url+"cms/new_retur_outlet/get_retur/"+id_sales;
			}else{
				url = base_url+"cms/new_retur_outlet/get_mutasi/"+id_sales;
			}
			kode = 'RT';

			//var id_sales = '3';
			$.get( url, function( data ) {
				if( data.status ){
					btn.parent().parent().parent().find('.msg').html( message_box('success', data.message) );
					var sales = data.data;
					$.each(sales, function(index, value) {
						if (jenis == 'tb_retur') {
							var sales_id           = value.retur_id;
							var sales_date    	   = value.retur_date;
							var sales_total        = value.retur_total;
							$('#table-sales > thead').empty();
							$('#table-sales > thead').html(`
								<tr>
									<td>No Retur</td>
									<td>Tgl Retur</td>
									<td>Retur Total</td>
								</tr>
							`);
						}else{
							var sales_id           = value.mutasi_id;
							var sales_date    	   = value.mutasi_date;
							var sales_total        = value.mutasi_total;
							$('#table-sales > thead').empty();
							$('#table-sales > thead').html(`
								<tr>
									<td>No Mutasi</td>
									<td>Tgl Mutasi</td>
									<td>Mutasi Total</td>
								</tr>
							`);
						}

						var html = '<tr>'
							+' <td>'+ kode + ('000' + sales_id).substr(-3) +'</td><td>'
							+ sales_date
							+'</td>'
							+ '<td>'+ Intl.NumberFormat().format( sales_total ) +'</td>'
							+'</tr>';
						$('#table-sales > tbody').html( html);
					});
					var html;
					$.each(sales, function(index, value) {
						if (jenis == 'tb_retur') {
							var sales_detail_id    = value.retur_detail_id;
							var item_name          = value.item_name;
							var harga_sales        = value.retur_detail_price;
							var sales_detail_qty   = value.retur_detail_qty;
							var sales_detail_total = value.retur_detail_total;
							var expired      	   = value.expired;

							$('#table-sales-detail > thead').empty();
							$('#table-sales-detail > thead').html(`
								<tr>
									<th>Item</th>
									<th>Jumlah</th>
									<th>Harga</th>
									<th>Total</th>
									<th>EXP</th>
									<th>Retur Roti</th>
									<th style="width: 30">Jml Mutasi Roti</th>
									<th style="width: 30">Retur Uang</th>
								</tr>
							`);
						}else{
							var sales_detail_id    = value.mutasi_detail_id;
							var item_name          = value.item_name;
							var harga_sales        = value.mutasi_detail_price;
							var sales_detail_qty   = value.mutasi_detail_qty;
							var sales_detail_total = value.mutasi_detail_total;
							var expired      	   = value.expired;

							$('#table-sales-detail > thead').html(`
								<tr>
									<th>Item</th>
									<th>Jumlah</th>
									<th>Harga</th>
									<th>Total</th>
									<th>EXP</th>
									<th>Mutasi Roti</th>
									<th style="width: 30">Jml Mutasi Roti</th>
								</tr>
							`);
						}
						if (strDate > expired) {
							var warna = 'red';
						}
						
						if (jenis == 'tb_retur') {
							html += 
							`<tr>
								<td>`+ item_name +`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_qty )+`</td>
								<td>`+ Intl.NumberFormat().format( harga_sales )+`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_total ) +`</td>
								<td style='color: `+warna+`;'>`+ expired +`</td>
								<input name="exp[]" type="hidden" class="form-control exp-`+sales_detail_id+`">
								<td>
									<div class = "input-group">
									   <input name="item_id[]" type="hidden" class="form-control id-item-`+sales_detail_id+`">
									   <input name="harga[]" type="hidden" class="form-control harga-`+sales_detail_id+`">
									   <input disabled name="item_name[]" type="text" class="form-control nama-item-`+sales_detail_id+`">
									   <span class = "input-group-btn">
									      <button onclick="cek_roti(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
									         <i class="fa fa-search"></i>
									      </button>
									   </span>
									</div>
								</td>
								<td><input disabled class="form-control qty_roti_`+sales_detail_id+`" type='number' name='qty_roti[]'></td>
								<td>
									<div class = "input-group">
										<input disabled class="form-control qty_uang_`+sales_detail_id+`" type='number' name='qty_uang[]'>
										<span class = "input-group-btn">
										   <button onclick="qty_uang_enable(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
										      <i class="fa fa-check"></i>
										   </button>
										</span>
									</div>
								</td>
							</tr>`;
						}else{
							html += 
							`<tr>
								<td>`+ item_name +`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_qty )+`</td>
								<td>`+ Intl.NumberFormat().format( harga_sales )+`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_total ) +`</td>
								<td style='color: `+warna+`;'>`+ expired +`</td>
								<input name="exp[]" type="hidden" class="form-control exp-`+sales_detail_id+`">
								<td>
									<div class = "input-group">
									   <input name="item_id[]" type="hidden" class="form-control id-item-`+sales_detail_id+`">
									   <input name="harga[]" type="hidden" class="form-control harga-`+sales_detail_id+`">
									   <input disabled name="item_name[]" type="text" class="form-control nama-item-`+sales_detail_id+`">
									   <span class = "input-group-btn">
									      <button onclick="cek_roti(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
									         <i class="fa fa-search"></i>
									      </button>
									   </span>
									</div>
								</td>
								<td><input disabled class="form-control qty_roti_`+sales_detail_id+`" type='number' name='qty_roti[]'></td>
							</tr>`;
						}
						$('#table-sales-detail > tbody').html( html);
						$('#table-sales-detail > tbody').html( html);
					});
					$("#form").find(".btn-retur-submit").removeClass('disabled');
				}else {
					btn.parent().parent().parent().find('.msg').html( message_box('danger', data.message) );
					$('#table-sales > tbody').html('');
					$('#table-sales-detail > tbody').html('');
					$("#form").find(".btn-retur-submit").addClass('disabled');
				}
			});
			e.preventDefault();
		});
	}

	/*RETUR DAPUR*/
	if( typeof page != 'undefined' && page == 'new_retur_dapur' ) {
		$("#btn-cek-no-retur-dapur").click(function (e) {
			var btn = $(this);
			var id_sales = $(this).parent().parent().find('#sales-id').val();
			var jenis 	 = $('input[name=retur_pembeli]:checked').val();
			var url;
			var kode;
			var d = new Date();
			var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

			url = base_url+"cms/new_retur_dapur/get_retur/"+id_sales;
			kode = 'RT';

			//var id_sales = '3';
			$.get( url, function( data ) {
				if( data.status ){
					btn.parent().parent().parent().find('.msg').html( message_box('success', data.message) );
					var sales = data.data;
					$.each(sales, function(index, value) {
						var sales_id           = value.retur_id;
						var sales_date    	   = value.retur_date;
						var sales_total        = value.retur_total;
						

						var html = '<tr>'
							+' <td>'+ kode + ('000' + sales_id).substr(-3) +'</td><td>'
							+ sales_date
							+'</td>'
							+ '<td>'+ Intl.NumberFormat().format( sales_total ) +'</td>'
							+'</tr>';
						$('#table-sales > tbody').html( html);
					});
					var html;
					$.each(sales, function(index, value) {
						var sales_detail_id    = value.retur_detail_id;
						var item_name          = value.item_name;
						var harga_sales        = value.retur_detail_price;
						var sales_detail_qty   = value.retur_detail_qty;
						var sales_detail_total = value.retur_detail_total;
						var expired      	   = value.expired;
						if (strDate > expired) {
							var warna = 'red';
						}
						
						html += 
							`<tr>
								<td>`+ item_name +`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_qty )+`</td>
								<td>`+ Intl.NumberFormat().format( harga_sales )+`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_total ) +`</td>
								<td style='color: `+warna+`;'>`+ expired +`</td>
								<td>
									<div class = "input-group">
									   <input name="item_id[]" type="hidden" class="form-control id-item-`+sales_detail_id+`">
									   <input name="harga[]" type="hidden" class="form-control harga-`+sales_detail_id+`">
									   <input disabled name="item_name[]" type="text" class="form-control nama-item-`+sales_detail_id+`">
									   <span class = "input-group-btn">
									      <button onclick="cek_roti_dapur(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
									         <i class="fa fa-search"></i>
									      </button>
									   </span>
									</div>
								</td>
								<td><input disabled class="form-control qty_roti_`+sales_detail_id+`" type='number' name='qty_roti[]'></td>
								<td>
									<div class = "input-group">
										<input disabled class="form-control qty_uang_`+sales_detail_id+`" type='number' name='qty_uang[]'>
										<span class = "input-group-btn">
										   <button onclick="qty_uang_enable(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
										      <i class="fa fa-check"></i>
										   </button>
										</span>
									</div>
								</td>
							</tr>`;
						$('#table-sales-detail > tbody').html( html);
					});
					$("#form").find(".btn-retur-submit").removeClass('disabled');
				}else {
					btn.parent().parent().parent().find('.msg').html( message_box('danger', data.message) );
					$('#table-sales > tbody').html('');
					$('#table-sales-detail > tbody').html('');
					$("#form").find(".btn-retur-submit").addClass('disabled');
				}
			});
			e.preventDefault();
		});
	}
</script>