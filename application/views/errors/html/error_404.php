<?php
$CI = get_instance();
$CI->load->library('Base_config');
$path = $CI->base_config->asset_back();
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>404 halaman tidak ditemukan | <?php echo $CI->base_config->sigleSetting('setting_company','company name');?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="nurza.cool@gmail.com">
	<!-- CSS -->
	<!--[if lte IE 9]>
	<link href="<?php echo $path; ?>css/main-ie.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $path; ?>css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!-- Fav and touch icons -->
	<link type="text/css" rel="stylesheet" href="<?php echo $path?>css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo $path?>css/font-awesome.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo $path?>css/main.css" />
	<script src="<?php echo $path?>js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo $path?>js/bootstrap/bootstrap.js"></script>
	<script src="<?php echo $path?>js/plugins/modernizr/modernizr.js"></script>
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $path?>ico/my-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $path?>ico/my-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $path?>ico/my-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $path?>ico/my-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo $path?>ico/favicon.png">
</head>

<body>
<div class="wrapper full-page-wrapper page-error text-center">
	<div class="inner-page">
		<h1>
				<span class="clearfix title">
					<span class="number">404</span> <span class="text">Oops! <br/>Halaman tidak ditemukan</span>
				</span>
		</h1>
		<p>Halaman yang anda cari tidak ditemukan, silahkan kembali ke <a href="http://localhost/duniamod2/">halaman utama</a></p>
		<br/><br/>
		<br/>			<div>
			<a href="javascript:history.go(-1)" class="btn btn-custom-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="http://localhost/duniamod2/" class="btn btn-primary"><i class="fa fa-home"></i> Home</a>
		</div>
	</div>
</div>
<footer class="footer"><?php echo $CI->base_config->sigleSetting('setting_company','company footer');?></footer>
</body>
</html>
