<!-- left sidebar -->
<div class="col-md-2 left-sidebar">
    <!-- main-nav -->
    <nav class="main-nav">
        <ul id="nav" class="main-menu">
            <li class="<?php $active = false;
            if ($this->uri->segment(1) == 'cms' && $this->uri->segment(2) == '') echo "active"; ?>">
                <a href="<?php echo base_url('cms'); ?>"><i class="fa fa-dashboard fa-fw"></i><span class="text">Dashboard</span></a>
            </li>
            <li class="<?php $active = $this->base_config->active_link_arr(array('master_jenis', 'customers', 'master_item', 'master_outlet', 'master_resep', 'master_category', 'master_satuan', 'master_bahan','master_retail','master_status_promo'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-clipboard fa-fw"></i><span
                        class="text">Master</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_outlet');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('master_outlet', true); ?>">
                        <a href="<?php echo base_url('cms/master_outlet'); ?>"><span class="text">Outlet</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_category');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('master_category', true); ?>">
                        <a href="<?php echo base_url('cms/master_category'); ?>"><span class="text">Kategori Item</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_jenis');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('master_jenis', true); ?>">
                        <a href="<?php echo base_url('cms/master_jenis'); ?>"><span class="text">Jenis Item</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_satuan');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('master_satuan', true); ?>">
                        <a href="<?php echo base_url('cms/master_satuan'); ?>"><span class="text">Satuan</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'customers');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('customers', true); ?>">
                        <a href="<?php echo base_url('cms/customers'); ?>"><span class="text">Customers</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_item');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('master_item', true); ?>">
                        <a href="<?php echo base_url('cms/master_item'); ?>"><span class="text">Items</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_retail');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('master_retail', true); ?>">
                            <a href="<?php echo base_url('cms/master_retail'); ?>"><span class="text">Retail</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_bahan');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('master_bahan', true); ?>">
                            <a href="<?php echo base_url('cms/master_bahan'); ?>"><span class="text">Bahan</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'master_status_promo');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('master_status_promo', true); ?>">
                            <a href="<?php echo base_url('cms/master_status_promo'); ?>"><span class="text">Status Promo</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>
            
            <li class="<?php $active = $this->base_config->active_link_arr(array('pemesanan', 'penjualan', 'retur'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-bar-chart-o fw"></i><span
                        class="text">Transaksi</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'pemesanan');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('pemesanan', true); ?>">
                        <a href="<?php echo base_url('cms/pemesanan'); ?>"><span class="text">Pemesanan</span></a></li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'penjualan');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('penjualan', true); ?>">
                        <a href="<?php echo base_url('cms/penjualan'); ?>"><span class="text">Penjualan</span></a>
                    </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('promo','stok','stok_retur','stok_roti_diskon','stok_tidak_laku','harga_item'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-copy fw"></i><span class="text">Manajemen</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'stok');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('stok', true); ?>">
                            <a href="<?php echo base_url('cms/stok'); ?>"><span class="text">Stok Item</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'stok_retur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('stok_retur', true); ?>">
                            <a href="<?php echo base_url('cms/stok_retur'); ?>"><span class="text">Stok Retur</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'stok_roti_diskon');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('stok_roti_diskon', true); ?>">
                            <a href="<?php echo base_url('cms/stok_roti_diskon'); ?>"><span class="text">Stok Roti Diskon</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'stok_tidak_laku');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('stok_tidak_laku', true); ?>">
                            <a href="<?php echo base_url('cms/stok_tidak_laku'); ?>"><span class="text">Stok TL</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'promo');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('promo', true); ?>">
                            <a href="<?php echo base_url('cms/promo'); ?>"><span class="text">Promo</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'harga_item');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('harga_item', true); ?>">
                            <a href="<?php echo base_url('cms/harga_item'); ?>"><span class="text">Harga Item</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('new_retur_dapur','new_retur_outlet','new_retur_pembeli','retur_pembeli','retur_dapur','retur_outlet_pusat'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-recycle fw"></i><span class="text">Retur</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'new_retur_pembeli');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('new_retur_pembeli', true); ?>">
                            <a href="<?php echo base_url('cms/new_retur_pembeli'); ?>"><span class="text">New Retur Pembeli</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'retur_pembeli');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('retur_pembeli', true); ?>">
                            <a href="<?php echo base_url('cms/retur_pembeli'); ?>"><span class="text">Retur Pembeli</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'new_retur_outlet');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('new_retur_outlet', true); ?>">
                            <a href="<?php echo base_url('cms/new_retur_outlet'); ?>"><span class="text">New Retur Outlet</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'retur_outlet_pusat');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('retur_outlet_pusat', true); ?>">
                            <a href="<?php echo base_url('cms/retur_outlet_pusat'); ?>"><span class="text">Retur Outlet Pusat</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'new_retur_dapur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('new_retur_dapur', true); ?>">
                            <a href="<?php echo base_url('cms/new_retur_dapur'); ?>"><span class="text">New Retur Dapur</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'retur_dapur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('retur_dapur', true); ?>">
                            <a href="<?php echo base_url('cms/retur_dapur'); ?>"><span class="text">Retur Dapur</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('barang_masuk','barang_keluar','mutasi_baru','mutasi_dapur'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-send fw"></i><span class="text">Mutasi</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'mutasi_baru');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('mutasi_baru', true); ?>">
                            <a href="<?php echo base_url('cms/mutasi_baru'); ?>"><span class="text">Mutasi Baru</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'barang_masuk');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('barang_masuk', true); ?>">
                            <a href="<?php echo base_url('cms/barang_masuk'); ?>"><span class="text">Barang Masuk</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'barang_keluar');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('barang_keluar', true); ?>">
                            <a href="<?php echo base_url('cms/barang_keluar'); ?>"><span class="text">Barang Keluar</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('giro','kredit','cicilan'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-money fw"></i><span class="text">Hutang Piutang</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'kredit');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('kredit', true); ?>">
                            <a href="<?php echo base_url('cms/kredit'); ?>"><span class="text">Kredit</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'giro');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('giro', true); ?>">
                            <a href="<?php echo base_url('cms/giro'); ?>"><span class="text">Giro</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'cicilan');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('cicilan', true); ?>">
                            <a href="<?php echo base_url('cms/cicilan'); ?>"><span class="text">Cicilan</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('pembelian_bahan','daftar_pembelian_bahan','penjualan_dapur','daftar_penjualan_dapur','stok_bahan','stok_roti_dapur','resep','daftar_resep'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-shopping-basket fw"></i><span class="text">Produksi</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'pembelian_bahan');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('pembelian_bahan', true); ?>">
                            <a href="<?php echo base_url('cms/pembelian_bahan'); ?>"><span class="text">Pembelian Bahan</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'daftar_pembelian_bahan');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('daftar_pembelian_bahan', true); ?>">
                            <a href="<?php echo base_url('cms/daftar_pembelian_bahan'); ?>"><span class="text">Daftar Pembelian Bahan</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'resep');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('resep', true); ?>">
                            <a href="<?php echo base_url('cms/resep'); ?>"><span class="text">Resep</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'daftar_resep');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('daftar_resep', true); ?>">
                            <a href="<?php echo base_url('cms/daftar_resep'); ?>"><span class="text">Daftar Resep</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'penjualan_dapur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('penjualan_dapur', true); ?>">
                            <a href="<?php echo base_url('cms/penjualan_dapur'); ?>"><span class="text">Penjualan Dapur</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'daftar_penjualan_dapur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('daftar_penjualan_dapur', true); ?>">
                            <a href="<?php echo base_url('cms/daftar_penjualan_dapur'); ?>"><span class="text">Daftar Penjualan Dapur</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'stok_bahan');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('stok_bahan', true); ?>">
                            <a href="<?php echo base_url('cms/stok_bahan'); ?>"><span class="text">Stok Bahan</span></a>
                        </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'stok_roti_dapur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('stok_roti_dapur', true); ?>">
                            <a href="<?php echo base_url('cms/stok_roti_dapur'); ?>"><span class="text">Stok Roti Dapur</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('report_retur','report_mutasi','report_pemesanan', 'report_penjualan','report_laba_rugi','report_utang_piutang','report_cashflow','report_pembelian_bahan','report_penjualan_dapur','report_resep'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-edit fw"></i><span class="text">Laporan</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_pemesanan');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('report_pemesanan', true); ?>">
                        <a href="<?php echo base_url('cms/report_pemesanan'); ?>"><span
                                class="text">Pemesanan</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_penjualan');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('report_penjualan', true); ?>">
                        <a href="<?php echo base_url('cms/report_penjualan'); ?>"><span
                                class="text">Penjualan</span></a>
                    </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_mutasi');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_mutasi', true); ?>">
                            <a href="<?php echo base_url('cms/report_mutasi'); ?>"><span
                                    class="text">Mutasi</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_retur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_retur', true); ?>">
                            <a href="<?php echo base_url('cms/report_retur'); ?>"><span
                                    class="text">Retur</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_laba_rugi');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_laba_rugi', true); ?>">
                            <a href="<?php echo base_url('cms/report_laba_rugi'); ?>"><span
                                    class="text">Laba Rugi</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_utang_piutang');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_utang_piutang', true); ?>">
                            <a href="<?php echo base_url('cms/report_utang_piutang'); ?>"><span
                                    class="text">Utang Piutang</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_cashflow');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_cashflow', true); ?>">
                            <a href="<?php echo base_url('cms/report_cashflow'); ?>"><span
                                    class="text">Cashflow</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_pembelian_bahan');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_pembelian_bahan', true); ?>">
                            <a href="<?php echo base_url('cms/report_pembelian_bahan'); ?>"><span
                                    class="text">Pembelian Bahan</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_penjualan_dapur');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_penjualan_dapur', true); ?>">
                            <a href="<?php echo base_url('cms/report_penjualan_dapur'); ?>"><span
                                    class="text">Penjualan Dapur</span></a>
                        </li>
                    <?php endif;?>

                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'report_resep');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('report_resep', true); ?>">
                            <a href="<?php echo base_url('cms/report_resep'); ?>"><span
                                    class="text">Resep</span></a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('users', 'groups'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-users fw"></i><span class="text">User</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'users');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('users', true); ?>"><a
                            href="<?php echo base_url('cms/users'); ?>"><span class="text">Users</span></a></li>
                    <li class="<?php echo $this->base_config->activelinknav('add', 'users'); ?>"><a
                            href="<?php echo base_url('cms/users/index/add'); ?>"><span class="text">Add new</span></a>
                    </li>
                    <?php endif;?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'groups');
                    if ($menu==false): ?>
                    <li class="<?php echo $this->base_config->activelinknav('groups', true); ?>"><a
                            href="<?php echo base_url('cms/groups'); ?>"><span class="text">Jabatan</span></a></li>
                    <li class="<?php echo $this->base_config->activelinknav('add', 'groups'); ?>"><a
                            href="<?php echo base_url('cms/groups/index/add'); ?>"><span class="text">Add Jabatan</span></a>
                    </li>
                    <?php endif;?>
                </ul>
            </li>

            <li class="<?php $active = $this->base_config->active_link_arr(array('general', 'company','setting_sales'));
            if ($active) echo "active"; ?>">
                <a href="#" class="js-sub-menu-toggle"><i class="fa fa-gears fw"></i><span class="text">Setting</span>
                    <i class="toggle-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu " style="<?php if ($active == true) echo 'display:block;';
                $active = false; ?>">
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'setting_general');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('general', true); ?>"><a
                                href="<?php echo base_url('cms/general'); ?>"><span class="text">General</span></a></li>
                    <?php endif; ?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'setting_company');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('company', true); ?>"><a
                                href="<?php echo base_url('cms/company'); ?>"><span class="text">Company</span></a></li>
                    <?php endif; ?>
                    <?php $menu = $this->base_config->groups_access_sigle('menu', 'setting_sales');
                    if ($menu==false): ?>
                        <li class="<?php echo $this->base_config->activelinknav('setting_sales', true); ?>"><a
                                href="<?php echo base_url('cms/setting_sales'); ?>"><span class="text">Sales</span></a></li>
                    <?php endif; ?>
                </ul>
            </li>

            <li>
                <a href="<?php echo base_url('cms/auth/logout'); ?>">
                    <i class="fa fa-sign-out fa-fw"></i><span class="text">Keluar</span>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /main-nav -->
    <div class="sidebar-minified js-toggle-minified">
        <i class="fa fa-angle-left"></i>
    </div>
    <!-- sidebar content -->
    <!--<div class="sidebar-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5><i class="fa fa-lightbulb-o"></i> Tips</h5></div>
            <div class="panel-body">
                <p>Lakukan yang terbaik dan buat orang menyukainya, maka anda telah selangkah lebih maju untuk
                    sukses</p>
            </div>
        </div>
        <h5 class="label label-default"><i class="fa fa-info-circle"></i> Informasi Server</h5>
        <ul class="list-unstyled list-info-sidebar bottom-30px">
            <li class="data-row">
                <div class="data-name">Disk Space Usage</div>
                <div class="data-value">
                    274.43 / 2 GB
                    <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10"
                             aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                            <span class="sr-only">10%</span>
                        </div>
                    </div>
                </div>
            </li>
            <li class="data-row">
                <div class="data-name">Monthly Bandwidth Transfer</div>
                <div class="data-value">
                    230 / 500 GB
                    <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="46"
                             aria-valuemin="0" aria-valuemax="100" style="width: 46%">
                            <span class="sr-only">46%</span>
                        </div>
                    </div>
                </div>
            </li>
            <li class="data-row">
                <span class="data-name">Database Disk Space</span>
                <span class="data-value">219.45 MB</span>
            </li>
            <li class="data-row">
                <span class="data-name">Operating System</span>
                <span class="data-value">Linux</span>
            </li>
            <li class="data-row">
                <span class="data-name">Apache Version</span>
                <span class="data-value">2.4.6</span>
            </li>
            <li class="data-row">
                <span class="data-name">PHP Version</span>
                <span class="data-value">5.3.27</span>
            </li>
            <li class="data-row">
                <span class="data-name">MySQL Version</span>
                <span class="data-value">5.5.34-cll</span>
            </li>
            <li class="data-row">
                <span class="data-name">Architecture</span>
                <span class="data-value">x86_64</span>
            </li>
        </ul>
    </div>-->
    <!-- end sidebar content -->
</div>
<!-- end left sidebar -->
					