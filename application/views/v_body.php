<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
	<div class="row">
		<div class="col-lg-4 ">
			<ul class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="#">Home</a></li>
				<li class="active">Dashboard</li>
			</ul>
		</div>
		<div class="col-lg-8 ">
			<div class="top-content">
				<ul class="list-inline quick-access">
					<li>
						<a target="_blank" href="<?php echo base_url('pos');?>">
							<div class="quick-access-item bg-color-green">
								<i class="fa fa-bar-chart-o"></i>
								<h5>POS</h5><em>Transaksi penjualan</em>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- main -->
	<div class="content">
		<div class="main-header">
			<h2>DASHBOARD</h2>
			<em>dashboard alternative</em>
		</div>
		<div class="main-content">

		</div>
		<!-- /main-content -->
	</div>
	<!-- /main -->
</div>
<!-- /content-wrapper -->
