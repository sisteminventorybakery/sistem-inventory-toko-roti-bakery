				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- END BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
	</div>
	<!-- /wrapper -->
	<!-- FOOTER -->
	<footer class="footer"><?php echo $this->base_config->sigleSetting('setting_company','company footer');?></footer>
	<!-- END FOOTER -->

	<div class="modal fade" id="modal-sales" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<table class="table">
						<thead>
						<tr>
							<td>Nama Item</td>
							<td class="text-right">Harga</td>
							<td class="text-right">Diskon</td>
							<td class="text-right">Harga Setelah Diskon</td>
							<td class="text-center">Jumlah</td>
							<td class="text-right">Sub Total</td>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<!-- Javascript -->
				<script src="<?php /** @var string $theme_path */
echo $theme_path;?>js/plugins/bootstrap-tour/bootstrap-tour.custom.js"></script>
<script src="<?php echo $theme_path;?>js/king-common.js"></script>
<script src="<?php echo $theme_path;?>js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo $theme_path;?>js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo $theme_path;?>js/plugins/moment/moment.min.js"></script>
<script src="<?php echo $theme_path;?>js/plugins/select2/select2.min.js"></script>
<script src="<?php echo $theme_path;?>js/king-elements.js"></script>
<script src="<?php echo base_url()?>assets/js/myscript.js"></script>

<script type="text/javascript">

    jQuery(function($) {
        $('.numeric2').autoNumeric('init',{aPad: false});
    });

    /*NAV SIDEBAR ONLY*/
    $("#nav>li").each(function (index,value) {
        var length_li = $(this).children().children('li').length;
        if( index != 0 && index != $("#nav>li").length-1 ){
            if( length_li == 0 ){
                $(this).hide();
            }
        }
    });

	$(".form-datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss'
	});

	$(".form-date").datepicker({
		format: 'yyyy-mm-dd',
		autoclose:true,
		todayHighlight:true
	});

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	function message_box( type, message) {
		var html = '';
		html += '<div class="alert alert-'+type+' alert-dismissible" role="alert">';
		html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
		html += message;
		html += '</div>';
		return html;
	}

	$(document).ready(function () {
        
        $(".form_datetime").datetimepicker({
            format: 'yyyy-mm-dd hh:ii'
        });

        $(".form_date").datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            todayHighlight:true
        });

	});

    /*LOCAL STORAGE*/
	if( typeof page != 'undefined' && page == 'home' ) {
		localStorage.removeItem('orders');
	}
    var orders = JSON.parse( localStorage.getItem('orders') );
    var order_total = 0;
    var order_ongkir = 0;
    var order_nota_discount = 0;
	var order_current_qty = 1;
	var order_current_price = 0;
	var order_current_discount = 0;
	var order_current_subtotal = 0;
    if( !orders ){
        orders = [];
        localStorage.setItem('orders', JSON.stringify( orders ));
    }

    $("#btn-add-order").click(function (e) {
        if( order_current_subtotal < 0 ) {
        	alert('Tidak bisa menambahkan data');
		}else{
			var item_name = $( "#item_id option:selected" ).text();
			var item_id = parseInt( $( "#item_id option:selected" ).val() );
			var price = parseInt( $( "#item_id option:selected" ).attr('data-price') );
			var stok = parseInt( $( "#item_id option:selected" ).attr('data-stok') );
			var discount = $("#discount_product").autoNumeric('get');
			var qty = parseInt( $('#qty').val() );
			var discount_total = 0;
			if( discount > 0 ) discount_total += discount;
			var total = qty * (price - discount_total);
			var item_exist = 0;
			var item_exist_index = -1;
			if( orders ){
				$.each(orders, function(index, value) {
					if(value.item_id == item_id){
						item_exist = 1;
						item_exist_index = index;
						qty = qty + value.qty;
					}
				});
			}
			if( item_exist ) orders.splice(item_exist_index, 1);
			var new_orders = {
				'item_name'         :item_name,
				'item_id'           :item_id,
				'price'             :price,
				'qty'               :qty,
				'discount'          :discount,
				'total'             :total
			};
			orders.push( new_orders );
			localStorage.setItem('orders', JSON.stringify( orders ));
			refreshCart();
		}
		e.preventDefault();
    });

	var resetOrder = function () {
		order_total = 0;
		order_ongkir = 0;
		order_nota_discount = 0;
		blank_array = [];
		localStorage.setItem('orders', JSON.stringify( blank_array ));
		refreshCart();
	};

    var refreshCart = function(){
        $('#table-order > tbody').empty();
        order_total = 0;
        orders = JSON.parse( localStorage.getItem('orders') );
        if( orders ){
            $.each(orders, function(index, value) {
                var item_name = value.item_name;
                var item_id = value.item_id;
                var price = value.price;
                var qty = value.qty;
                var discount = value.discount;
                var discount_total = 0;
                if( discount > 0 ) discount_total += discount;
                var total = qty * (price - discount_total);
                order_total += total;
                var input = '<input type="hidden" name="item_id[]" value="'+item_id+'">'
					+ '<input type="hidden" name="qty[]" value="'+qty+'">'
					+ '<input type="hidden" name="discount[]" value="'+discount_total+'">';
                var html = '<tr>'+input+' <td>'+ item_name +'</td> <td class="text-right">'+ Intl.NumberFormat().format( price ) +'</td> <td class="text-center">'+ qty +'</td> <td class="text-center">'+ Intl.NumberFormat().format(discount_total) +'</td> <td class="text-right">'+ Intl.NumberFormat().format(total) +'</td>'+ '<td class="text-center"><a title="Hapus" href="#" class="red-font btn-remove-order"><i class="fa fa-trash"></i></a></td>' +'</tr>';
                $('#table-order > tbody').append( html);
            });
        }

		var order_nota_total = 0;
		if( order_nota_discount > 0 ) order_nota_total += order_nota_discount;
		var total_order_after_discount = order_total - order_nota_total;
		var total_all = total_order_after_discount + order_ongkir;
        $("#total-order").html( Intl.NumberFormat().format( total_order_after_discount ) );
        $("#total-order-before-discount").html( Intl.NumberFormat().format( order_total ) );
        $("#total-order-bayar").html( Intl.NumberFormat().format( Math.ceil( total_all/100)*100 ));
    };

    $("body").on("click", ".btn-remove-order", function (e) {
        var item_row = $(this).parent().parent();
        var item_index = item_row.index();
        item_row.remove();
        orders.splice(item_index, 1);
        localStorage.setItem('orders', JSON.stringify( orders ));
        refreshCart();
        e.preventDefault();
    });

    $("#form").submit(function (e) {
    	$('body').append('<div class="loading">Loading&#8230;</div>');
        var form = $(this);
        $.ajax({
            type: "POST",
            url: window.location.href+'/ajax',
            data: form.serialize(),
            success: function( response ) {
                $( "div.loading" ).remove();
                obj = JSON.parse(response);
                if (obj === undefined || obj === null) {
                    form.find(".message").html(message_box('danger', 'Error'));
                }
                if( obj.status ){
                	if (form.attr('tipe') == 'retur-outlet') {
                		window.open(window.location.href+'/cetak/'+obj.ids+'/'+obj.tipe, '_blank');
                	}

                    form.trigger('reset');
                    resetOrder();
                    form.find(".message").html( message_box('success', obj.message) );
                }else{
                    form.find(".message").html( message_box('danger', obj.message) );
                }
            }
        });
        e.preventDefault();
    });

    $("#form-order").submit(function (e) {
    	$('body').append('<div class="loading">Loading&#8230;</div>');
        var form = $(this);
        $.ajax({
            type: "POST",
            url: base_url+'cms/pemesanan/ajax',
            data: form.serialize(),
            success: function( response ) {
                $( "div.loading" ).remove();
                obj = JSON.parse(response);
                if (obj === undefined || obj === null) {
					form.find(".message").html(message_box('danger', 'Error'));
                }
                if( obj.status ){
                    form.trigger('reset');
                    resetOrder();
                    form.find(".message").html( message_box('success', obj.message) );
                }else{
					form.find(".message").html( message_box('danger', obj.message) );
                }
            }
        });
        e.preventDefault();
    });

	$("#form-customer").submit(function (e) {
		$('body').append('<div class="loading">Loading&#8230;</div>');
		var form = $(this);
		$.ajax({
			type: "POST",
			url: base_url+'cms/customers/add',
			data: form.serialize(),
			success: function( obj ) {
                $( "div.loading" ).remove();
				if (obj === undefined || obj === null) {
					form.find(".message").html(message_box('danger', 'Sistem Error'));
				}
				if( obj.status ){
					form.trigger('reset');
					form.find(".msg").html( message_box('success', obj.message) );
					location.reload();
				}else{
					form.find(".msg").html( message_box('danger', obj.message) );
				}
			}
		});
		e.preventDefault();
	});

    refreshCart();

    $("#export-pdf").click(function () {
        $('form').append('<input type="hidden" name="export" value="pdf" />').attr('target', '_blank').submit();
        $('form').removeAttr("target");
        $("form").find('input[type="hidden"][name="export"]').remove();
    });
    $("#export-excel").click(function () {
        $('form').append('<input type="hidden" name="export" value="excel" />').attr('target', '_blank').submit();
        $('form').removeAttr("target");
        $("form").find('input[type="hidden"][name="export"]').remove();
    });

	/*RETUR*/
	if( typeof page != 'undefined' && page == 'new_retur_pembeli' ) {
		$("#btn-cek-nota").click(function (e) {
			var btn = $(this);
			var id_sales = $(this).parent().parent().find('#sales-id').val();
			var jenis 	 = $('input[name=retur_pembeli]:checked').val();
			var url;
			var kode;
			var d = new Date();
			var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

			if (jenis == 'tb_sales') {
				url = base_url+"cms/new_retur_pembeli/get_sales/"+id_sales;
				kode = 'PJ';
			}else{
				url = base_url+"cms/new_retur_pembeli/get_order/"+id_sales;
				kode = 'OD';
			}
			//var id_sales = '3';
			$.get( url, function( data ) {
				if( data.status ){
					btn.parent().parent().parent().find('.msg').html( message_box('success', data.message) );
					var sales = data.data;
					$.each(sales, function(index, value) {
						if (jenis == 'tb_sales') {
							var sales_id           = value.sales_id;
							var sales_discount     = value.sales_discount;
							var sales_total        = value.sales_total;
							var sales_disc_product = value.sales_disc_product;
							var customer_name      = value.customer_name;
						}else{
							var sales_id           = value.order_id;
							var sales_discount     = value.order_discount_nota;
							var sales_total        = value.order_total;
							var sales_disc_product = value.order_discount_product;
							var customer_name      = value.customer_name;
						}

						var html = '<tr>'
							+' <td>'+ kode + ('000' + sales_id).substr(-3) +'</td><td>'+ customer_name +'</td><td>'
							+ Intl.NumberFormat().format( sales_discount )
							+'%</td><td>'
							+ Intl.NumberFormat().format( sales_disc_product ) +'</td>'
							+ '<td>'+ Intl.NumberFormat().format( sales_total ) +'</td>'
							+'</tr>';
						$('#table-sales > tbody').html( html);
					});
					var html;
					$.each(sales, function(index, value) {
						if (jenis == 'tb_sales') {
							var sales_detail_id    = value.sales_detail_id;
							var item_name          = value.item_name;
							var harga_sales        = value.harga_sales;
							var item_stok          = value.item_stok;
							var sales_detail_qty   = value.sales_detail_qty;
							var sales_detail_disc  = value.sales_detail_disc;
							var sales_detail_total = value.sales_detail_total;
							var expired      	   = value.expired;
							if (strDate > expired) {
								var warna = 'red';
							}
						}else{
							var sales_detail_id    = value.order_detail_id;
							var item_name          = value.item_name;
							var harga_sales        = value.order_detail_price;
							var sales_detail_qty   = value.order_detail_qty;
							var sales_detail_disc  = value.order_detail_disc;
							var sales_detail_total = value.order_detail_total;
							var expired      	   = value.expired;
							if (strDate > expired) {
								var warna = 'red';
							}
						}
						html += 
							`<tr>
								<td>`+ item_name +`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_qty )+`</td>
								<td>`+ Intl.NumberFormat().format( harga_sales )+`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_disc ) +`</td>
								<td>`+ Intl.NumberFormat().format( sales_detail_total ) +`</td>
								<td style='color: `+warna+`;'>`+ expired +`</td>
								<td>
									<div class = "input-group">
									   <input name="item_id[]" type="hidden" class="form-control id-item-`+sales_detail_id+`">

									   <input name="exp[]" type="hidden" class="form-control exp-`+sales_detail_id+`">
									   <input name="harga[]" type="hidden" class="form-control harga-`+sales_detail_id+`">
									   <input disabled name="item_name[]" type="text" class="form-control nama-item-`+sales_detail_id+`">
									   <span class = "input-group-btn">
									      <button onclick="cek_roti(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
									         <i class="fa fa-search"></i>
									      </button>
									   </span>
									</div>
								</td>
								<td><input disabled class="form-control qty_roti_`+sales_detail_id+`" type='number' name='qty_roti[]'></td>
								<td>
									<div class = "input-group">
										<input disabled class="form-control qty_uang_`+sales_detail_id+`" type='number' name='qty_uang[]'>
										<span class = "input-group-btn">
										   <button onclick="qty_uang_enable(`+sales_detail_id+`)" class = "btn btn-primary" type = "button">
										      <i class="fa fa-check"></i>
										   </button>
										</span>
									</div>
								</td>
							</tr>`;
						$('#table-sales-detail > tbody').html( html);
					});
					$("#form").find(".btn-retur-submit").removeClass('disabled');
				}else {
					btn.parent().parent().parent().find('.msg').html( message_box('danger', data.message) );
					$('#table-sales > tbody').html('');
					$('#table-sales-detail > tbody').html('');
					$("#form").find(".btn-retur-submit").addClass('disabled');
				}
			});
			e.preventDefault();
		});
	}

	$( "#item_id" ).change(function() {
		order_current_qty = 1;
		$("#qty").autoNumeric('set', 1);
		order_current_price = parseInt( $( "#item_id option:selected" ).attr('data-price') );
		order_current_discount = parseInt( $( "#item_id option:selected" ).attr('data-discount') );
		order_current_subtotal = order_current_price*order_current_qty-order_current_discount;
		var persen = order_current_discount/order_current_subtotal*100;
		$("#price_product").val( Intl.NumberFormat().format( order_current_price ) );
		$("#discount_product").autoNumeric( 'set',order_current_discount  );
		$("#order_product_subtotal").val( Intl.NumberFormat().format(order_current_subtotal)  );
		$("#discount_product_percent").autoNumeric('set', persen);
	});

	$("#qty").keyup(function () {
		if( $(this).val().length == 0 ){
			order_current_qty = 1;
		}else{
			order_current_qty = $(this).autoNumeric('get');
			order_current_subtotal = order_current_qty*order_current_price-(order_current_discount*order_current_qty);
			$("#order_product_subtotal").val( Intl.NumberFormat().format(order_current_subtotal)  );
		}
	});

	/*PEMESANAN*/
	if( typeof page != 'undefined' && page == 'pemesanan' ) {
		$( "#customer_id" ).change(function() {
			var address = $( "#customer_id option:selected" ).attr('data-address');
			$("#order_address").val( address  );
		});

		$("#order_type").on('change', function (e) {
			var input_bayar = $("#order_pay");
			var nama = $("#box_nama");
			var nomor_giro = $("#box_nomor_giro");
			var nama_bank = $("#box_nama_bank");
			var nomor_kartu = $("#box_nomor_kartu");
			var tanggal_cair = $("#box_tanggal_cair");
			var lunas = $("#order_lunas");
			lunas.removeAttr('disabled');
			input_bayar.show();
			$('.box-hide').hide();
			$(".btn-order-submit").attr('disabled', 'disabled');
			switch ( $(this).val() ){
				case 'kartu_kredit':
					$(".btn-order-submit").removeAttr('disabled');
					input_bayar.hide();
					lunas.val('ya').prop('selected', true);
					lunas.attr('disabled','disabled');
					nama.show();
					nama_bank.show();
					nomor_kartu.show();
					break;
				case 'kredit':
					lunas.val('tidak').prop('selected', true);
					lunas.attr('disabled','disabled');
					input_bayar.attr('placeholder', 'Down Payment (Rp)');
					break;
				case 'giro':
					$(".btn-order-submit").removeAttr('disabled');
					lunas.val('tidak').prop('selected', true);
					lunas.attr('disabled','disabled');
					input_bayar.hide();
					tanggal_cair.show();
					nomor_giro.show();
					nama_bank.show();
					break;
				case 'cash':
					input_bayar.attr('placeholder', 'Bayar (Rp)');
					lunas.removeAttr('disabled');
					lunas.val('ya').prop('selected', true);
					lunas.attr('disabled','disabled');
					break;
			}
			e.preventDefault();
		});

		$("#order_pay").keyup(function () {
			var type = $("#order_type");
			if( $(this).val().length == 0 ){
				$(".btn-order-submit").attr('disabled', 'disabled');
			}else{
				switch (type.val()){
					case 'cash':
						if( $(this).autoNumeric('get') >= order_total ){
							$(".btn-order-submit").removeAttr('disabled');
						}else{
							$(".btn-order-submit").attr('disabled', 'disabled');
						}
						break;
					case 'kredit':
						if( $(this).autoNumeric('get') > 0 && $(this).autoNumeric('get') < order_total ){
							$(".btn-order-submit").removeAttr('disabled');
						}else{
							$(".btn-order-submit").attr('disabled', 'disabled');
						}
						break;
				}
			}
			refreshCart();
		});

		$("#order_ongkir").keyup(function () {
			if( $(this).val().length == 0 ){
				order_ongkir = 0;
			}else{
				order_ongkir = parseInt($(this).autoNumeric('get'));
			}
			refreshCart();
		});

		$("#discount_product").keyup(function () {
			if( $(this).val().length == 0 ){
				order_current_discount = 0;
			}else{
				var this_value = $(this).autoNumeric('get');
				var persen = this_value/(order_current_qty*order_current_price)*100;
				$("#discount_product_percent").autoNumeric('set', persen);
				order_current_discount = this_value;
			}
			order_current_subtotal = order_current_qty*order_current_price-(order_current_discount*order_current_qty);
			$("#order_product_subtotal").val( Intl.NumberFormat().format(order_current_subtotal)  );
		});

		$("#discount_product_percent").keyup(function () {
			if( $(this).val().length == 0 ){
				order_current_discount = 0;
			}else{
				var this_value = $(this).autoNumeric('get');
				var nominal = this_value/100*(order_current_qty*order_current_price);
				$("#discount_product").autoNumeric('set', nominal);
				order_current_discount = nominal;
			}
			order_current_subtotal = order_current_qty*order_current_price-(order_current_discount*order_current_qty);
			$("#order_product_subtotal").val( Intl.NumberFormat().format(order_current_subtotal)  );
		});

		$("#order_discount").keyup(function () {
			if( $(this).val().length == 0 ){
				order_nota_discount = 0;
			}else{
				var this_value = $(this).autoNumeric('get');
				var persen = this_value/order_total*100;
				$("#order_discount_percent").autoNumeric('set', persen);
				order_nota_discount = this_value;
			}
			refreshCart();
		});

		$("#order_discount_percent").keyup(function () {
			if( $(this).val().length == 0 ){
				order_nota_discount = 0;
			}else{
				var this_value = $(this).autoNumeric('get');
				var nominal = this_value/100*order_total;
				$("#order_discount").autoNumeric('set', nominal);
				order_nota_discount = nominal;
			}
			refreshCart();
		});
	}


	$("body").on("click", ".btn-preview", function (e) {
		var id = $(this).attr('data-id');
		var modul = $(this).attr('data-modul');
		switch (modul){
			case 'sales':
				$.get( base_url+"cms/penjualan/get/"+id, function( data ) {
					var modal = $("#modal-sales");
					var html = '';
					var nota = 'PJ-0000';
					var all_total = 0;
					var disc_nota = 0;
					$.each(data, function(index, value) {
						nota = ("0" + value.sales_id).slice(-3);
						var item_name = value.item_name;
						var price = value.sales_detail_price;
						var qty = value.sales_detail_qty;
						var disc = value.sales_detail_disc;
						var total = value.sales_detail_total;
						disc_nota = value.sales_discount;
						all_total = value.sales_total;
						html += '<tr><td>'+item_name+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( price )+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( disc )+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( price-disc )+'</td>' +
							'<td class="text-center">'+qty+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( total )+'</td></tr>';
					});
					html += '<tr><th colspan="5" class="text-right">Diskon Nota</th><th class="text-right">'+Intl.NumberFormat().format( disc_nota )+'</th></tr>';
					html += '<tr><th colspan="5" class="text-right">Total</th><th class="text-right">'+Intl.NumberFormat().format( all_total )+'</th></tr>';
					modal.find('.table > tbody').html( html );
					modal.find(".modal-title").html('PJ-'+nota);
					modal.modal('show');
				});
				break;
			case 'order':
				$.get( base_url+"cms/pemesanan/get/"+id, function( data ) {
					var modal = $("#modal-sales");
					var html = '';
					var nota = 'OD-0000';
					var all_total = 0;
					var disc_nota = 0;
					var ongkir = 0;
					$.each(data, function(index, value) {
						nota = ("0" + value.order_id).slice(-3);
						var item_name = value.item_name;
						var price = value.order_detail_price;
						var qty = value.order_detail_qty;
						var disc = value.order_detail_disc;
						var total = value.order_detail_total;
						disc_nota = value.order_discount_nota;
						ongkir = value.order_ongkir;
						all_total = value.order_total;
						html += '<tr><td>'+item_name+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( price )+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( disc )+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( price-disc )+'</td>' +
							'<td class="text-center">'+qty+'</td>' +
							'<td class="text-right">'+Intl.NumberFormat().format( total )+'</td></tr>';
					});
					html += '<tr><th colspan="5" class="text-right">Diskon Nota</th><th class="text-right">'+Intl.NumberFormat().format( disc_nota )+'</th></tr>';
					html += '<tr><th colspan="5" class="text-right">Ongkos Kirim</th><th class="text-right">'+Intl.NumberFormat().format( ongkir )+'</th></tr>';
					html += '<tr><th colspan="5" class="text-right">Total Bayar</th><th class="text-right">'+Intl.NumberFormat().format( all_total )+'</th></tr>';
					modal.find('.table > tbody').html( html );
					modal.find(".modal-title").html('OD-'+nota);
					modal.modal('show');
				});
				break;
		}
		e.preventDefault();
	});

</script>
</body>
</html>