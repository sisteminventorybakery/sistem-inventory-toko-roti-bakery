<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<!--[if IE 9 ]>
<html class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>Admin Dashboard - <?php echo $this->base_config->sigleSetting('setting_general','Site Title');?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Admin Dashboard">
	<meta name="author" content="nurza.cool@gmail.com">
	<!-- CSS -->
	<link href="<?php /** @var string $theme_path */
	echo $theme_path;?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $theme_path;?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $theme_path;?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $theme_path;?>css/main.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $theme_path;?>css/style.css" rel="stylesheet" type="text/css">
	<?php
	/** @var array $css_files */
	foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php endforeach; ?>
	<!--[if lte IE 9]>
	<link href="<?php echo $theme_path;?>css/main-ie.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $theme_path;?>css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<script src="<?php echo $theme_path; ?>js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo $theme_path; ?>js/common.min.js"></script>
	<script type="text/javascript">
		var jQ = $.noConflict(true);
		var base_url = '<?php echo base_url()?>';
	</script>
	<?php /** @var array $js_files */
	foreach($js_files as $file): ?>
		<script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>
	<script src="<?php echo $theme_path; ?>js/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo $theme_path; ?>js/plugins/bootbox/bootbox.min.js"></script>
	<script src="<?php echo $theme_path; ?>js/plugins/auto-numeric/autoNumeric-min.js"></script>
	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $theme_path;?>ico/my-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $theme_path;?>ico/my-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $theme_path;?>ico/my-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $theme_path;?>ico/my-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo $theme_path;?>ico/favicon.ico">
	<style type="text/css">
		#main {
			width: 350px;
			margin:auto;
			background: #ececec;
			padding: 20px;
			border: 1px solid #ccc;
		}

		#image-list {
			list-style:none;
			margin:0;
			padding:0;
		}
		#image-list li {
			background: #fff;
			border: 1px solid #ccc;
			text-align:center;
			padding:20px;
			margin-bottom:19px;
		}
		#image-list li img {
			width: 258px;
			vertical-align: middle;
			border:1px solid #474747;
		}

		.progress {
			display: block;
			text-align: center;
			width: 0;
			height: 5px;
			background: #0091EA;
			transition: width .3s;
		}
		.progress.hide {
			opacity: 0;
			transition: opacity 1.3s;
		}

		/* Absolute Center Spinner */
		.loading {
			position: fixed;
			z-index: 999;
			height: 2em;
			width: 2em;
			overflow: show;
			margin: auto;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
		}

		/* Transparent Overlay */
		.loading:before {
			content: '';
			display: block;
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0,0,0,0.3);
		}

		/* :not(:required) hides these rules from IE9 and below */
		.loading:not(:required) {
			/* hide "loading..." text */
			font: 0/0 a;
			color: transparent;
			text-shadow: none;
			background-color: transparent;
			border: 0;
		}

		.loading:not(:required):after {
			content: '';
			display: block;
			font-size: 10px;
			width: 1em;
			height: 1em;
			margin-top: -0.5em;
			-webkit-animation: spinner 1500ms infinite linear;
			-moz-animation: spinner 1500ms infinite linear;
			-ms-animation: spinner 1500ms infinite linear;
			-o-animation: spinner 1500ms infinite linear;
			animation: spinner 1500ms infinite linear;
			border-radius: 0.5em;
			-webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
			box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
		}

		/* Animation */

		@-webkit-keyframes spinner {
			0% {
				-webkit-transform: rotate(0deg);
				-moz-transform: rotate(0deg);
				-ms-transform: rotate(0deg);
				-o-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(360deg);
				-moz-transform: rotate(360deg);
				-ms-transform: rotate(360deg);
				-o-transform: rotate(360deg);
				transform: rotate(360deg);
			}
		}
		@-moz-keyframes spinner {
			0% {
				-webkit-transform: rotate(0deg);
				-moz-transform: rotate(0deg);
				-ms-transform: rotate(0deg);
				-o-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(360deg);
				-moz-transform: rotate(360deg);
				-ms-transform: rotate(360deg);
				-o-transform: rotate(360deg);
				transform: rotate(360deg);
			}
		}
		@-o-keyframes spinner {
			0% {
				-webkit-transform: rotate(0deg);
				-moz-transform: rotate(0deg);
				-ms-transform: rotate(0deg);
				-o-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(360deg);
				-moz-transform: rotate(360deg);
				-ms-transform: rotate(360deg);
				-o-transform: rotate(360deg);
				transform: rotate(360deg);
			}
		}
		@keyframes spinner {
			0% {
				-webkit-transform: rotate(0deg);
				-moz-transform: rotate(0deg);
				-ms-transform: rotate(0deg);
				-o-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(360deg);
				-moz-transform: rotate(360deg);
				-ms-transform: rotate(360deg);
				-o-transform: rotate(360deg);
				transform: rotate(360deg);
			}
		}

	</style>
</head>

<body class="dashboard2">
<!-- WRAPPER -->
<div class="wrapper">
	<!-- TOP BAR -->
	<div class="top-bar">
		<div class="container">
			<div class="row">
				<!-- logo -->
				<div class="col-md-2 logo">
					<a href="<?php echo base_url('cms');?>"><img src="<?php echo $theme_path;?>img/my-logo-white.png" alt="Admin Dashboard" /></a>
					<h1 class="sr-only">Admin Dashboard</h1>
				</div>
				<!-- end logo -->
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-3" style="width: 22%;">

						</div>
						<div class="col-md-9">
							<div class="top-bar-right">
								<!-- responsive menu bar icon -->
								<a href="#" class="hidden-md hidden-lg main-nav-toggle"><i class="fa fa-bars"></i></a>
								<!-- end responsive menu bar icon -->

								<div class="notifications">
									<ul>
										<button type="button" data-toggle="modal" data-target="#outlet-modal" id="setting-outlet" class="btn btn-link"><i class="fa fa-home"></i> Pilih Outlet</button>

										<!-- notification: general -->
										<li class="notification-item general">
											<div class="btn-group">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="fa fa-bell"></i>
													<?php $allnotif = $this->M_base_config->countDatamultiple(array('table'=>'tb_notification','where'=>array(array('wherefield'=>'notification_status','where_value'=>'active'),array('wherefield'=>'notification_user','where_value'=>$this->ion_auth->user()->row()->id))));
													if(!empty($allnotif)){ echo '<span class="count">'.$allnotif.'</span>'; } ?>

													<span class="circle"></span>
												</a>
												<ul class="dropdown-menu" role="menu">
													<li class="notification-header">
														<?php if(!empty($allnotif)){ echo '<em>'.$allnotif.' Notification</em>'; } ?>
													</li>
													<?php $nonotif =false; $notif = $this->base_config->notificationlist(); ?>
													<?php foreach ( $notif as $notifval ): ?>
														<?php $nonotif =true; ?>
														<li>
															<a target="_blank" href="<?php echo $notifval->notification_link; ?>">
																<?php
																switch( $notifval->notification_icon ){
																	case 'update':
																		echo '<i class="fa fa-pencil yellow-font"></i>';
																		break;
																	case 'new':
																		echo '<i class="fa fa-plus-square green-font"></i>';
																		break;
																	case 'delete':
																		echo '<i class="fa fa-trash red-font"></i>';
																		break;
																	default:
																		echo '<i class="fa fa-plus-square green-font"></i>';
																}
																?>
																<span class="text"><?php echo $notifval->notification_desc; ?></span>
																<span class="timestamp"><?php echo $this->base_config->timeAgo($notifval->notification_date);?></span>
															</a>
														</li>
													<?php endforeach;?>
													<?php if($nonotif == false): ?>
														<li>
															<a href="#">
																<span class="text">Lihat Notifikasi</span>
															</a>
														</li>
													<?php endif;?>
													<?php if($nonotif): ?>
														<li class="notification-footer">
															<a href="<?php echo base_url('cms/notification');?>">All Notifications</a>
														</li>
													<?php endif;?>
												</ul>
											</div>
										</li>
										<!-- end notification: general -->
									</ul>
								</div>
								<!-- logged user and the menu -->
								<div class="logged-user">
									<div class="btn-group">
										<a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
											<img src="<?php echo $theme_path;?>img/user-avatar.png" alt="User Avatar" />
											<span class="name"><?php echo $this->ion_auth->user()->row()->username;?></span> <span class="caret"></span>
										</a>
										<ul class="dropdown-menu" role="menu">
											<?php if( $this->ion_auth->is_admin() ):?>
											<li>
												<a href="<?php echo base_url('cms/general');?>">
													<i class="fa fa-cog"></i>
													<span class="text">Setting</span>
												</a>
											</li>
											<?php endif;?>
											<li>
												<a href="<?php echo base_url('cms/auth/logout');?>">
													<i class="fa fa-power-off"></i>
													<span class="text">Keluar</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<!-- end logged user and the menu -->
							</div>
							<!-- /top-bar-right -->
						</div>
					</div>
					<!-- /row -->
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /top -->
	<div class="modal fade" id="outlet-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form action="<?php echo base_url('cms');?>" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Pilih Outlet</h4>
				</div>
				<div class="modal-body">
					<label for="select-outlet">Pilih Outlet : </label>
					<select class="form-control" name="outlet" id="select-outlet" required>
						<?php foreach ($this->M_base_config->get_my_outlet() as $v):?>
						<option value="<?php echo $v['id'];?>" <?php if($this->session->userdata('outlet_id') == $v['id']) echo 'selected';?>><?php echo $v['value'];?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Batal</button>
					<button type="submit" class="btn btn-custom-primary"><i class="fa fa-check-circle"></i> Simpan</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
	<div class="bottom">
		<div class="container">
			<div class="row">