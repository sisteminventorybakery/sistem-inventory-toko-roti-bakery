<?php
$CI = get_instance();
$CI->load->library('Base_config');
$path = $CI->base_config->asset_back();
?>
<?php foreach($query as $u){ 	?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>

	<!-- Define Charset -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<!-- Responsive Meta Tag -->
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

	
    <title><?php echo $CI->base_config->sigleSetting('setting_company','company name');?></title><!-- Responsive Styles and Valid Styles -->

    <style type="text/css">
    
	    body{
            width: 100%; 
            background-color: #ffffff; 
            margin:0; 
            padding:0; 
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt:0px; mso-margin-bottom-alt:0px; mso-padding-alt: 0px 0px 0px 0px;
        }
        
        p,h1,h2,h3,h4{
	        margin-top:0;
			margin-bottom:0;
			padding-top:0;
			padding-bottom:0;
        }
        
        span.preheader{display: none; font-size: 1px;}
        
        html{
            width: 100%; 
        }
        
        table{
            font-size: 16px;
            border: 0;
        }

		.center a {
			font-size: 19px !important;
			font-weight: 600;
			color: red;
		}
		
		 /* ----------- responsivity ----------- */
        @media only screen and (max-width: 640px){
			/*------ top header ------ */	
            body[yahoo] .show{display: block !important;}
            body[yahoo] .hide{display: none !important;}
            
            /*----- main image -------*/
			body[yahoo] .main-image img{width: 440px !important; height: auto !important;}
			 
			/* ====== divider ====== */
			body[yahoo] .divider img{width: 440px !important;}
			
			/*--------- banner ----------*/
			body[yahoo] .banner img{width: 440px !important; height: auto !important;}
			/*-------- container --------*/			
			body[yahoo] .container590{width: 440px !important;}
			body[yahoo] .container580{width: 400px !important;}
			body[yahoo] .container1{width: 420px !important;}
			body[yahoo] .container2{width: 400px !important;}
			body[yahoo] .container3{width: 380px !important;}
           
			/*-------- secions ----------*/
			body[yahoo] .section-item{width: 440px !important;}
            body[yahoo] .section-img img{width: 440px !important; height: auto !important;}        
        }

		@media only screen and (max-width: 479px){
			/*------ top header ------ */
			body[yahoo] .main-header{font-size: 24px !important;}
            body[yahoo] .resize-text{font-size: 14px !important;}
            
            /*----- main image -------*/
			body[yahoo] .main-image img{width: 280px !important; height: auto !important;}
			 
			/* ====== divider ====== */
			body[yahoo] .divider img{width: 280px !important;}
			body[yahoo] .align-center{text-align: center !important;}
			
			
			/*-------- container --------*/			
			body[yahoo] .container590{width: 280px !important;}
			body[yahoo] .container580{width: 240px !important;}
            body[yahoo] .container1{width: 260px !important;}
			body[yahoo] .container2{width: 240px !important;}
			body[yahoo] .container3{width: 220px !important;}
			
            /*------- CTA -------------*/
			body[yahoo] .cta-button{width: 240px !important;}
			body[yahoo] .cta-text{font-size: 16px !important;}
		}
		
</style>
</head>

<body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	
	<!-- ======= main section ======= -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="2b2e34" style="background-image: url(http://visitingjogja.com/assets/uploads/160_museumgunung.jpg); background-size: 100% 100%; background-position: top center;" background="http://themastermail.com/alerta/notif5/img/bg.png">
		
		<tr><td height="90" style="font-size: 90px; line-height: 90px;">&nbsp;</td></tr>
		
		<tr>
			<td>
				<table border="0" align="center" width="450" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container3 bodybg_color" style="border-top-left-radius: 4px; border-top-right-radius: 4px;">
					
					<tr><td height="3" style="font-size: 3px; line-height: 3px;">&nbsp;</td></tr>
				</table>
			</td>
		</tr>	
		<tr><td height="3" style="font-size: 3px; line-height: 3px;">&nbsp;</td></tr>
		<tr>
			<td>
				<table border="0" align="center" width="470" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container2 bodybg_color" style="border-top-left-radius: 4px; border-top-right-radius: 4px;">
					
					<tr><td height="3" style="font-size: 3px; line-height: 3px;">&nbsp;</td></tr>
				</table>
			</td>
		</tr>	
		<tr><td height="3" style="font-size: 3px; line-height: 3px;">&nbsp;</td></tr>
		<tr>
			<td>
				<table border="0" align="center" width="490" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container1 bodybg_color" style="border-top-left-radius: 4px; border-top-right-radius: 4px;">
					
					<tr><td height="3" style="font-size: 3px; line-height: 3px;">&nbsp;</td></tr>
				</table>
			</td>
		</tr>	
		<tr><td height="3" style="font-size: 3px; line-height: 3px;">&nbsp;</td></tr>
		<tr>
			<td>
				<table border="0" align="center" width="510" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590 bodybg_color" style="border-radius: 4px;">
					
					<tr><td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td></tr>
					
					<tr>
						<!-- ======= logo ======= -->
						<td align="center">
							<a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="110" border="0" style="display: block; width:110px;" src="http://visitingjogja.com/assets/uploads/e7206-baner2.gif" alt="logo" /></a>
						</td>			
					</tr>
					
					<tr><td height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center" style="color: #181c27; font-size: 30px; font-family: 'Oxygen', sans-serif; mso-line-height-rule: exactly; line-height: 30px;" class="title_color main-header">
							
							<!-- ======= section header ======= -->
							
							<div style="line-height: 30px;">

	        					
	        						Ayo Download mod dari duniamod.com
	        					
							</div>
								<div class="circular">
								  <img alt="" height="110" width="110" src="http://visitingjogja.com/assets/uploads/<?php echo $u->user_avatar; ?>" alt='breeze'>
								</div>
								<div class="center"><a alt="Tampilkan gambar , lihat pengaturan email anda" href="http://visitingjogja.com/citizen/<?php echo $u->username; ?>"><?php echo $u->user_display_name; ?></a></div>
        				</td>
					</tr>
					
					<tr><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
					
					<tr>
						<td>
							<table border="0" width="440" align="center" cellpadding="0" cellspacing="0" class="container580">				
								<tr>
									<td align="center" style="color: #8d94a3; font-size: 16px; font-family: 'Oxygen', sans-serif; mso-line-height-rule: exactly; line-height: 24px;" class="resize-text text_color">
										<div style="line-height: 24px">
											
											<!-- ======= section text ======= -->

				        						Email ini merupakan email notifikasi untuk anda.
				        					
										</div>
			        				</td>	
								</tr>
							</table>
						</td>
					</tr>
					
					<tr><td height="35" style="font-size: 35px; line-height: 35px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center" style="background-image: url(http://themastermail.com/alerta/notif5/img/divider-bg.png); background-size: 100% 100%; background-position: top center;" background="http://themastermail.com/alerta/notif5/img/divider-bg.png">
							
							<table border="0" align="center" width="300" cellpadding="0" cellspacing="0" bgcolor="2dabcb" style="border-radius: 50px; box-shadow: 0 2px 0 2px #239bb9;" class="cta-button main_color">
								
								<tr><td height="17" style="font-size: 17px; line-height: 17px;">&nbsp;</td></tr>
								
								<tr>
									
	                				<td align="center" style="color: #ffffff; font-size: 18px; font-family: 'Questrial', sans-serif;" class="cta-text">
	                					<!-- ======= main section button ======= -->
	                					
		                    			<div style="line-height: 24px;">
			                    			<a href="<?php echo base_url();?>" style="color: #ffffff; text-decoration: none;">Download mod</a> <br>
		                    			</div>
		                    		</td>
		                    		
	                			</tr>
								
								<tr><td height="17" style="font-size: 17px; line-height: 17px;">&nbsp;</td></tr>
							
							</table>
						</td>
					</tr>
					
					<tr><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center" style="color: #8d94a3; font-size: 14px; font-family: 'Questrial', sans-serif; line-height: 22px;" class="text_color">
							<!-- ======= section subtitle ====== -->
							
							<div style="line-height: 22px;">
	        					
	        						
										<a href="<?php echo base_url();?>" style="color: #8d94a3; text-decoration: none;"><?php echo $CI->base_config->sigleSetting('setting_company','company name');?></a>
            						
	        					
							</div>
        				</td>
					</tr>
					
					<tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
					
					<tr><td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td></tr>
														
				</table>
			</td>
		</tr>
		
		<tr><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
		
		<tr>
			<td>
				<table border="0" width="540" align="center" cellpadding="0" cellspacing="0" class="container580">
					<tr>
						
						<td align="center" style="color: #ffffff; font-size: 14px; font-family: 'Questrial', sans-serif; mso-line-height-rule: exactly; line-height: 30px;" class="text_color">
							<div style="line-height: 30px">
								
								<!-- ======= section text ======= -->


								© <?php echo date('Y');?> <?php echo $CI->base_config->sigleSetting('setting_company','company name');?>. All Rights Reserved.
	        					
							</div>
        				</td>	
					</tr>
				</table>
			</td>
		</tr>
		
		<tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>
		
	</table>
	<!-- ======= end header ======= -->
	
	
</body>
</html>
<?php } ?>