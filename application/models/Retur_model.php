<?php

/**
 * @property CI_DB_query_builder db
 * @property M_base_config M_base_config
 */
class Retur_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_detail_retur_outlet($retur_id)
    {
        $this->db->where('tb_retur.retur_id', $retur_id);
        $this->db->where('tb_retur.retur_type', 'OUTLET');
        $this->db->join('tb_master_outlet', 'tb_master_outlet.master_outlet_id = tb_retur.outlet_id');
        $this->db->join('tb_retur_detail', 'tb_retur_detail.retur_id = tb_retur.retur_id');
        $this->db->join('tb_master_item', 'tb_master_item.item_id = tb_retur_detail.item_id');
        $results = $this->db->get('tb_retur')->result();

        return $results;
    }

    public function get_detail_mutasi($mutasi_id)
    {
        $this->db->join('tb_mutasi_detail', 'tb_mutasi_detail.mutasi_id = tb_mutasi.mutasi_id');
        $this->db->join('tb_master_item', 'tb_master_item.item_id = tb_mutasi_detail.item_id');
        $this->db->join('tb_master_outlet', 'tb_master_outlet.master_outlet_id = tb_mutasi.mutasi_to');
        $this->db->where('tb_mutasi.mutasi_id', $mutasi_id);
        $this->db->where('mutasi_from', $this->session->userdata('outlet_id'));
        $this->db->where('mutasi_status', 'YA');
        $results = $this->db->get('tb_mutasi')->result();

        return $results;
    }

}
