<?php

/**
 * @property CI_DB_query_builder db
 * @property Ion_auth_model|Ion_auth ion_auth
 */
class Notif_model extends CI_Model
{
    /*
     * Transaksi Pembelian
     * Transaksi Pemesanan
     * Retur
     * Mutasi
     * User
     * Product Expired
     * Stok Limit Item Outlet
     * Stok Limit Bahan Dapur
     * Roti Diskon Yang akan expired
     * Jatuh Tempo Utang Piutang
     * New Utang Piutang
     * new giro
     * new transfer
     * new kredit
    */

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function add($id,$link,$description,$icon,$type='timeline' )
    {
        if( $icon == 'delete' ) {
            $link = 'javascript:void(0)';
        }else{
           $link = $link.'/'.$id;
        }
        $usr = $this->ion_auth->user()->row();
        $user_notification = [];
        if( !$this->ion_auth->is_admin() ){
            $user_notification[] = array(
                "notification_type" => $type,
                "notification_user" => $this->ion_auth->get_user_id(),
                "notification_parent" => $id,
                "notification_link" => $link,
                "notification_desc" => $description,
                "notification_status" => 'active',
                "notification_icon" => $icon,
                "notification_date" => date('Y-m-d H:i:s')
            );
        }
        $users = $this->ion_auth->users(1)->result_array(); //GET ALL USER ROLE ADMIN
        foreach ($users as $user) {
            if( $this->ion_auth->get_user_id() != $user['id'] ) $description = $description.' By '.$usr->username;
            $user_notification[] = array(
                "notification_type" => $type,
                "notification_user" => $user['id'],
                "notification_parent" => $id,
                "notification_link" => $link,
                "notification_desc" => $description,
                "notification_status" => 'active',
                "notification_icon" => $icon,
                "notification_date" => date('Y-m-d H:i:s')
            );
        }
        return $this->db->insert_batch('tb_notification', $user_notification);
    }

}
