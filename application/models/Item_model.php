<?php

/**
 * @property CI_DB_query_builder db
 * @property M_base_config M_base_config
 */
class Item_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_inventory_dapur($item_id=null, $expired=null)
    {
        $tmp = [];
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_inv_dapur.item_id');
        if( $item_id ) $this->db->where('item_id', $item_id);
        if( $expired ) $this->db->where('inv_expired', $expired);
        $this->db->where('inv_stok >', '0');
        $this->db->order_by('item_name', 'asc');
        $results = $this->db->get('tb_inv_dapur');
        foreach ($results->result_array() as $item) {
            $tmp[] = [
                'inv_stok'    => $item['inv_stok'],
                'inv_expired' => $this->M_base_config->my_date($item['inv_expired']),
                'inv_harga'   => $item['inv_harga'],
                'item_id'     => $item['item_id'],
                'inv_id'      => $item['inv_id'],
                'item_name'   => $item['item_name'],
            ];
        }
        return $tmp;
    }

    public function get_inventory_item($outlet_id,$item_id=null,$expired=null)
    {
        $tmp = [];
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_inventory.item_id');
        $this->db->where('tb_inventory.outlet_id', $outlet_id);
        if( $item_id ) $this->db->where('tb_master_item.item_id', $item_id);
        if( $expired ) $this->db->where('inventory_expired', $expired);
        $this->db->where('inventory_stok >', '0');
        $this->db->order_by('item_name', 'asc');
        $results = $this->db->get('tb_inventory');
        foreach ($results->result_array() as $item) {
            $price        = $this->get_price($outlet_id, $item['item_id']);
            $price_outlet = $price['outlet'];
            $price_sales  = $price['sales'];
            $promo        = $this->get_promo($item['item_id'], null, $outlet_id);
            if( $price_outlet > 0 && $price_sales > 0 ){
                $tmp[] = [
                    'item_disc'              => 0,
                    'item_disc_percent'      => 0,
                    'inventory_stok'         => $item['inventory_stok'],
                    'inventory_expired'      => $this->M_base_config->my_date($item['inventory_expired']),
                    'inventory_price_sales'  => $price_sales,
                    'inventory_price_outlet' => $price_outlet,
                    'inventory_price_real'   => $this->get_price_real($item['item_id'], $item['inventory_expired']),
                    'item_id'                => $item['item_id'],
                    'inventory_id'           => $item['inventory_id'],
                    'item_name'              => $item['item_name'],
                    'has_promo'              => $promo['has_promo'],
                    'promo_type'             => $promo['type'],
                    'promo_item_name'        => $promo['item_name'],
                    'promo_item_id'          => $promo['item_id'],
                    'promo_gratis'           => $promo['gratis'],
                    'promo_qty'              => $promo['item_qty'],
                ];
            }
        }
        return $tmp;
    }

    public function get_inventory_diskon($outlet_id,$item_id=null,$expired=null)
    {
        $tmp = [];
        $this->db->like('master_category_name', 'Roti Diskon ', 'after');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_inventory.item_id');
        $this->db->join('tb_master_category', 'tb_master_category.master_category_id = tb_master_item.item_category');
        $this->db->where('tb_inventory.outlet_id', $outlet_id);
        if( $item_id ) $this->db->where('tb_master_item.item_id', $item_id);
        if( $expired ) $this->db->where('inventory_expired', $expired);
        $this->db->where('inventory_stok >', '0');
        $this->db->order_by('item_name', 'asc');
        $results = $this->db->get('tb_inven]tory');
        foreach ($results->result_array() as $item) {
            $price        = $this->get_price($outlet_id, $item['item_id']);
            $price_outlet = $price['outlet'];
            $price_sales  = $price['sales'];
            $promo        = $this->get_promo($item['item_id'], null, $outlet_id);
            if( $price_outlet > 0 && $price_sales > 0 ){
                $tmp[] = [
                    'item_disc'              => 0,
                    'item_disc_percent'      => 0,
                    'inventory_stok'         => $item['inventory_stok'],
                    'inventory_expired'      => $this->M_base_config->my_date($item['inventory_expired']),
                    'inventory_price_sales'  => $price_sales,
                    'inventory_price_outlet' => $price_outlet,
                    'inventory_price_real'   => $this->get_price_real($item['item_id'], $item['inventory_expired']),
                    'item_id'                => $item['item_id'],
                    'inventory_id'           => $item['inventory_id'],
                    'item_name'              => $item['item_name'],
                    'has_promo'              => $promo['has_promo'],
                    'promo_type'             => $promo['type'],
                    'promo_item_name'        => $promo['item_name'],
                    'promo_item_id'          => $promo['item_id'],
                    'promo_gratis'           => $promo['gratis'],
                    'promo_qty'              => $promo['item_qty'],
                ];
            }
        }
        return $tmp;
    }

    public function get_price($outlet_id, $item_id)
    {
        $this->db->where('outlet_id', $outlet_id);
        $this->db->where('item_id', $item_id);
        $result = $this->db->get('tb_harga',1);
        return [ 'sales' => (int)$result->row('harga_sales'),'outlet' => (int)$result->row('harga_outlet') ];
    }

    public function get_price_real($item_id, $expired)
    {
        $this->db->where('inv_expired', $expired);
        $this->db->where('item_id', $item_id);
        $result = $this->db->get('tb_inv_dapur',1);
        return (int)$result->row('inv_harga');
    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }

    public function get_promo($item_id, $date=null,$outlet_id=null)
    {
        if( !$date ) $date = date('Y-m-d');
        $this->db->join('tb_status_promo','tb_status_promo.status_promo_id=tb_item_promo.status_promo_id');
        $this->db->join('tb_master_item','tb_master_item.item_id=tb_item_promo.item_promo_item_id','left');
        $this->db->where('tb_item_promo.item_id', $item_id);
        if( !$outlet_id ) $this->db->where('outlet_id', $this->outlet_id());
        $this->db->where('item_promo_date_start <=', $date);
        $this->db->where('item_promo_date_end >=', $date);
        $result = $this->db->get('tb_item_promo',1);
        $tmp = [
            'has_promo' => $result->num_rows(),
            'type'      => $result->row('status_promo_name'),
            'item_name' => $result->row('item_name'),
            'item_id'   => $result->row('item_promo_item_id'),
            'gratis'    => $result->row('item_promo_free'),
            'item_qty'  => $result->row('item_promo_qty'),
        ];
        return $tmp;
    }

}
