<?php

/**
 * @property CI_DB_query_builder db
 * @property Item_model Item_model
 */
class Stok_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Item_model');
    }

    public function add_inventory_outlet($outlet_id,$item_id,$expired,$qty=1)
    {
        $this->db->where('outlet_id', $outlet_id);
        $this->db->where('item_id', $item_id);
        $this->db->where('inventory_expired', $expired);
        $results = $this->db->get('tb_inventory',1);
        if( $results->num_rows() ){
            $data = [];
            foreach ($results->result_array() as $result) {
                $stok = (int)$result['inv_stok'];
                $data = [
                    'inventory_stok' => $qty+$stok,
                ];
            }
            $this->db->where('item_id', $item_id);
            $this->db->where('inventory_expired', $expired);
            return $this->db->update('tb_inventory', $data);
        }else{
            $data = [
                'inventory_stok' => $qty,
                'inventory_discount' => 0,
                'outlet_id' => $outlet_id,
                'item_id' => $item_id,
                'inventory_expired' => $expired,
            ];
            return $this->db->insert('tb_inventory', $data);
        }
    }

    public function remove_inventory_outlet($outlet_id,$item_id,$expired,$qty=1)
    {
        $this->db->where('outlet_id', $outlet_id);
        $this->db->where('item_id', $item_id);
        $this->db->where('inventory_expired', $expired);
        $results = $this->db->get('tb_inventory',1);
        $stok = (int)$results->row('inventory_stok');
        $stok-=(int)$qty;
        $this->db->where('outlet_id', $outlet_id);
        $this->db->where('item_id', $item_id);
        $this->db->where('inventory_expired', $expired);
        return $this->db->update('tb_inventory', ['inventory_stok' => $stok]);
    }

    public function add_bahan($item_id,$jumlah_satuan_besar=1)
    {
        $this->db->where('master_bahan_id', $item_id);
        $stok = (int)$this->db->get('tb_master_bahan')->row('stok');
        $stok+=(int)$jumlah_satuan_besar;
        $this->db->where('master_bahan_id', $item_id);
        return $this->db->update('tb_master_bahan', ['stok' => $stok]);
    }

    public function remove_bahan($item_id,$jumlah_satuan_kecil=1)
    {
        $this->db->where('master_bahan_id', $item_id);
        $result = $this->db->get('tb_master_bahan',1);
        $stok_kecil = (int)$result->row('stok_sisa');
        $stok_besar = (int)$result->row('stok');
        $konversi = (int)$result->row('konversi');
        $stok_all = ($stok_besar*$konversi) + $stok_kecil;
        $stok_all-=(int)$jumlah_satuan_kecil;
        $stok_besar = floor($stok_all/$konversi);
        $stok_kecil = $stok_all % $konversi;
        $this->db->where('master_bahan_id', $item_id);
        return $this->db->update('tb_master_bahan', ['stok' => $stok_besar, 'stok_sisa' => $stok_kecil]);
    }

    public function add_inventory_dapur($item_id,$expired,$price,$qty=1)
    {
        $this->db->where('item_id', $item_id);
        $this->db->where('inv_expired', $expired);
        $results = $this->db->get('tb_inv_dapur',1);
        if( $results->num_rows() ){
            $data = [];
            foreach ($results->result_array() as $result) {
                $stok = (int)$result['inv_stok'];
                $data = [
                    'inv_stok' => $qty+$stok,
                ];
            }
            $this->db->where('item_id', $item_id);
            $this->db->where('inv_expired', $expired);
            return $this->db->update('tb_inv_dapur', $data);
        }else{
            $data = [
                'inv_stok' => $qty,
                'inv_expired' => $expired,
                'inv_harga' => $price,
                'item_id' => $item_id,
            ];
            return $this->db->insert('tb_inv_dapur', $data);
        }
    }

    public function remove_inventory_dapur($item_id,$expired,$qty=1)
    {
        $this->db->where('item_id', $item_id);
        $this->db->where('inv_expired', $expired);
        $results = $this->db->get('tb_inv_dapur',1);
        $stok = (int)$results->row('inv_stok');
        $stok-=(int)$qty;
        $this->db->where('item_id', $item_id);
        $this->db->where('inv_expired', $expired);
        return $this->db->update('tb_inv_dapur', ['inv_stok' => $stok]);
    }

    public function get_inventory_dapur($item_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->where('inv_stok >', '0');
        $results = $this->db->get('tb_inv_dapur');
        return $results->result_array();
    }

    public function get_inventory_item($outlet_id,$item_id)
    {
        $this->db->where('outlet_id', $outlet_id);
        $this->db->where('item_id', $item_id);
        $this->db->where('inventory_stok >', '0');
        $results = $this->db->get('tb_inventory');
        return $results->result_array();
    }

    protected function outlet_id()
    {
        return $this->session->userdata('outlet_id');
    }

}
