-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2016 at 02:12 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2016_db_roti`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_giro`
--

CREATE TABLE `tb_giro` (
  `giro_id` int(11) NOT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `jumlah_hutang` bigint(20) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_giro`
--

INSERT INTO `tb_giro` (`giro_id`, `nomor`, `tanggal`, `jatuh_tempo`, `jumlah_hutang`, `customer_id`, `nama_bank`, `created`) VALUES
(1, '99994231', '2016-10-19', '2016-10-22', 69000, 1, 'BANK BRI', '2016-10-18 23:34:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_giro`
--
ALTER TABLE `tb_giro`
  ADD PRIMARY KEY (`giro_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_giro`
--
ALTER TABLE `tb_giro`
  MODIFY `giro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
